﻿using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;
using ENTIDADES;

namespace AD.EntidadesAD
{


    public class BusesAD : Repositorio<CC_CONDUCTORES>, IConductores
    {
        private readonly OracleConnection conn;

        public BusesAD(ref Object _bdConn)
        {
            _bdConn = Conexion.iniciar(ref conn, _bdConn);
        }


        public List<CC_CORREDOR> getRutaCorredor() //cabecera de la programación
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_BUSES.GET_LISTA_CORREDORES";
            var result = SqlMapper.Query<CC_CORREDOR>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;

        }

        public List<CC_BUSES> Listar_Buses()
        {
            List<CC_BUSES> resultado = new List<CC_BUSES>();

            OracleParameter[] bdParameters = new OracleParameter[1];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            using (var bdCmd = new OracleCommand("PKG_BUSES.listarPlacas", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_BUSES();
                            if (!DBNull.Value.Equals(bdRd["BS_ID"])) { item.BS_ID = Convert.ToInt32(bdRd["BS_ID"]); }
                            if (!DBNull.Value.Equals(bdRd["ID_CORREDOR"])) { item.ID_CORREDOR = Convert.ToInt32(bdRd["ID_CORREDOR"]); }
                            if (!DBNull.Value.Equals(bdRd["ABREVIATURA"])) { item.ABREVIATURA = bdRd["ABREVIATURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_FECINI_PT"])) { item.BS_FECINI_PT = bdRd["BS_FECINI_PT"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_NOM_EMPRE"])) { item.BS_NOM_EMPRE = bdRd["BS_NOM_EMPRE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PLACA"])) { item.BS_PLACA = bdRd["BS_PLACA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PROPIETARIO"])) { item.BS_PROPIETARIO = bdRd["BS_PROPIETARIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_CONCESION"])) { item.BS_PAQUETE_CONCESION = bdRd["BS_PAQUETE_CONCESION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_SERVICIO"])) { item.BS_PAQUETE_SERVICIO = bdRd["BS_PAQUETE_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TIPO_SERVICIO"])) { item.BS_TIPO_SERVICIO = bdRd["BS_TIPO_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ESTADO"])) { item.BS_ESTADO = bdRd["BS_ESTADO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MARCA"])) { item.BS_MARCA = bdRd["BS_MARCA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MODELO"])) { item.BS_MODELO = bdRd["BS_MODELO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AÑO_FABRICACION"])) { item.BS_AÑO_FABRICACION = bdRd["BS_AÑO_FABRICACION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COMBUSTIBLE"])) { item.BS_COMBUSTIBLE = bdRd["BS_COMBUSTIBLE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TECNOLOGIA_EURO"])) { item.BS_TECNOLOGIA_EURO = bdRd["BS_TECNOLOGIA_EURO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POTENCIA_MOTOR"])) { item.BS_POTENCIA_MOTOR = bdRd["BS_POTENCIA_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_MOTOR"])) { item.BS_SERIE_MOTOR = bdRd["BS_SERIE_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_CHASIS"])) { item.BS_SERIE_CHASIS = bdRd["BS_SERIE_CHASIS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COLOR_VEHICULO"])) { item.BS_COLOR_VEHICULO = bdRd["BS_COLOR_VEHICULO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_LONGITUD"])) { item.BS_LONGITUD = bdRd["BS_LONGITUD"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ASIENTOS"])) { item.BS_ASIENTOS = bdRd["BS_ASIENTOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AREA_PASILLO"])) { item.BS_AREA_PASILLO = bdRd["BS_AREA_PASILLO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_NETO"])) { item.BS_PESO_NETO = bdRd["BS_PESO_NETO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_BRUTO"])) { item.BS_PESO_BRUTO = bdRd["BS_PESO_BRUTO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ALTURA"])) { item.BS_ALTURA = bdRd["BS_ALTURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ANCHO"])) { item.BS_ANCHO = bdRd["BS_ANCHO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CARGA_UTIL"])) { item.BS_CARGA_UTIL = bdRd["BS_CARGA_UTIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PUERTA_IZQUIERDA"])) { item.BS_PUERTA_IZQUIERDA = bdRd["BS_PUERTA_IZQUIERDA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PARTIDA_REGISTRAL"])) { item.BS_PARTIDA_REGISTRAL = bdRd["BS_PARTIDA_REGISTRAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CODIGO_CVS"])) { item.BS_CODIGO_CVS = bdRd["BS_CODIGO_CVS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_INIC"])) { item.BS_CVS_FEC_INIC = bdRd["BS_CVS_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_FIN"])) { item.BS_CVS_FEC_FIN = bdRd["BS_CVS_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_INIC"])) { item.BS_SOAT_FEC_INIC = bdRd["BS_SOAT_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_FIN"])) { item.BS_SOAT_FEC_FIN = bdRd["BS_SOAT_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_VEHICULOS"])) { item.BS_POLIZA_VEHICULOS = bdRd["BS_POLIZA_VEHICULOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_INIC"])) { item.BS_VEHICULOS_INIC = bdRd["BS_VEHICULOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_FIN"])) { item.BS_VEHICULOS_FIN = bdRd["BS_VEHICULOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_CIVIL"])) { item.BS_POLIZA_CIVIL = bdRd["BS_POLIZA_CIVIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_INICIO"])) { item.BS_RC_INICIO = bdRd["BS_RC_INICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_FIN"])) { item.BS_RC_FIN = bdRd["BS_RC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_ACCI_COLECTIVOS"])) { item.BS_POLIZA_ACCI_COLECTIVOS = bdRd["BS_POLIZA_ACCI_COLECTIVOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_INIC"])) { item.BS_APCP_INIC = bdRd["BS_APCP_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_FIN"])) { item.BS_APCP_FIN = bdRd["BS_APCP_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_MULTIRIESGOS"])) { item.BS_POLIZA_MULTIRIESGOS = bdRd["BS_POLIZA_MULTIRIESGOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_INIC"])) { item.BS_MULTIRESGOS_INIC = bdRd["BS_MULTIRESGOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_FIN"])) { item.BS_MULTIRESGOS_FIN = bdRd["BS_MULTIRESGOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_INIC"])) { item.BS_RTV_INIC = bdRd["BS_RTV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_FIN"])) { item.BS_RTV_FIN = bdRd["BS_RTV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_INIC"])) { item.BS_REVI_ANUAL_GNV_INIC = bdRd["BS_REVI_ANUAL_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_FIN"])) { item.BS_REVI_ANUAL_GNV_FIN = bdRd["BS_REVI_ANUAL_GNV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_INIC"])) { item.BS_REVISION_CILINDROS_GNV_INIC = bdRd["BS_REVISION_CILINDROS_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_FIN"])) { item.BS_REVISION_CILINDROS_GNV_FIN = bdRd["BS_REVISION_CILINDROS_GNV_FIN"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["COD_CAC"])) { item.COD_CAC = bdRd["COD_CAC"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["ESTADO_VEHICULO"])) { item.ESTADO_VEHICULO = bdRd["ESTADO_VEHICULO"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["PLACA_REEMPLAZADA"])) { item.PLACA_REEMPLAZADA = bdRd["PLACA_REEMPLAZADA"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["USUREG"])) { item.USUREG = bdRd["USUREG"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["ID_ESTADO"])) { item.ID_ESTADO = Convert.ToInt32(bdRd["ID_ESTADO"]); }



                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }


        public List<CC_BUSES> Listar_Buses_by_Id(int idCorredor)
        {
            List<CC_BUSES> resultado = new List<CC_BUSES>();

            OracleParameter[] bdParameters = new OracleParameter[2];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters[1] = new OracleParameter("P_ID_TIPO_PARADERO", OracleDbType.Varchar2) { Value = idCorredor };

            using (var bdCmd = new OracleCommand("PKG_BUSES.listarPlacasbyID", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_BUSES();
                            if (!DBNull.Value.Equals(bdRd["BS_ID"])) { item.BS_ID = Convert.ToInt32(bdRd["BS_ID"]); }
                            if (!DBNull.Value.Equals(bdRd["ID_CORREDOR"])) { item.ID_CORREDOR = Convert.ToInt32(bdRd["ID_CORREDOR"]); }
                            if (!DBNull.Value.Equals(bdRd["ABREVIATURA"])) { item.ABREVIATURA = bdRd["ABREVIATURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_FECINI_PT"])) { item.BS_FECINI_PT = bdRd["BS_FECINI_PT"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_NOM_EMPRE"])) { item.BS_NOM_EMPRE = bdRd["BS_NOM_EMPRE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PLACA"])) { item.BS_PLACA = bdRd["BS_PLACA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PROPIETARIO"])) { item.BS_PROPIETARIO = bdRd["BS_PROPIETARIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_CONCESION"])) { item.BS_PAQUETE_CONCESION = bdRd["BS_PAQUETE_CONCESION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_SERVICIO"])) { item.BS_PAQUETE_SERVICIO = bdRd["BS_PAQUETE_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TIPO_SERVICIO"])) { item.BS_TIPO_SERVICIO = bdRd["BS_TIPO_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ESTADO"])) { item.BS_ESTADO = bdRd["BS_ESTADO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MARCA"])) { item.BS_MARCA = bdRd["BS_MARCA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MODELO"])) { item.BS_MODELO = bdRd["BS_MODELO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AÑO_FABRICACION"])) { item.BS_AÑO_FABRICACION = bdRd["BS_AÑO_FABRICACION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COMBUSTIBLE"])) { item.BS_COMBUSTIBLE = bdRd["BS_COMBUSTIBLE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TECNOLOGIA_EURO"])) { item.BS_TECNOLOGIA_EURO = bdRd["BS_TECNOLOGIA_EURO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POTENCIA_MOTOR"])) { item.BS_POTENCIA_MOTOR = bdRd["BS_POTENCIA_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_MOTOR"])) { item.BS_SERIE_MOTOR = bdRd["BS_SERIE_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_CHASIS"])) { item.BS_SERIE_CHASIS = bdRd["BS_SERIE_CHASIS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COLOR_VEHICULO"])) { item.BS_COLOR_VEHICULO = bdRd["BS_COLOR_VEHICULO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_LONGITUD"])) { item.BS_LONGITUD = bdRd["BS_LONGITUD"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ASIENTOS"])) { item.BS_ASIENTOS = bdRd["BS_ASIENTOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AREA_PASILLO"])) { item.BS_AREA_PASILLO = bdRd["BS_AREA_PASILLO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_NETO"])) { item.BS_PESO_NETO = bdRd["BS_PESO_NETO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_BRUTO"])) { item.BS_PESO_BRUTO = bdRd["BS_PESO_BRUTO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ALTURA"])) { item.BS_ALTURA = bdRd["BS_ALTURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ANCHO"])) { item.BS_ANCHO = bdRd["BS_ANCHO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CARGA_UTIL"])) { item.BS_CARGA_UTIL = bdRd["BS_CARGA_UTIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PUERTA_IZQUIERDA"])) { item.BS_PUERTA_IZQUIERDA = bdRd["BS_PUERTA_IZQUIERDA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PARTIDA_REGISTRAL"])) { item.BS_PARTIDA_REGISTRAL = bdRd["BS_PARTIDA_REGISTRAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CODIGO_CVS"])) { item.BS_CODIGO_CVS = bdRd["BS_CODIGO_CVS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_INIC"])) { item.BS_CVS_FEC_INIC = bdRd["BS_CVS_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_FIN"])) { item.BS_CVS_FEC_FIN = bdRd["BS_CVS_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_INIC"])) { item.BS_SOAT_FEC_INIC = bdRd["BS_SOAT_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_FIN"])) { item.BS_SOAT_FEC_FIN = bdRd["BS_SOAT_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_VEHICULOS"])) { item.BS_POLIZA_VEHICULOS = bdRd["BS_POLIZA_VEHICULOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_INIC"])) { item.BS_VEHICULOS_INIC = bdRd["BS_VEHICULOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_FIN"])) { item.BS_VEHICULOS_FIN = bdRd["BS_VEHICULOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_CIVIL"])) { item.BS_POLIZA_CIVIL = bdRd["BS_POLIZA_CIVIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_INICIO"])) { item.BS_RC_INICIO = bdRd["BS_RC_INICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_FIN"])) { item.BS_RC_FIN = bdRd["BS_RC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_ACCI_COLECTIVOS"])) { item.BS_POLIZA_ACCI_COLECTIVOS = bdRd["BS_POLIZA_ACCI_COLECTIVOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_INIC"])) { item.BS_APCP_INIC = bdRd["BS_APCP_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_FIN"])) { item.BS_APCP_FIN = bdRd["BS_APCP_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_MULTIRIESGOS"])) { item.BS_POLIZA_MULTIRIESGOS = bdRd["BS_POLIZA_MULTIRIESGOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_INIC"])) { item.BS_MULTIRESGOS_INIC = bdRd["BS_MULTIRESGOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_FIN"])) { item.BS_MULTIRESGOS_FIN = bdRd["BS_MULTIRESGOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_INIC"])) { item.BS_RTV_INIC = bdRd["BS_RTV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_FIN"])) { item.BS_RTV_FIN = bdRd["BS_RTV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_INIC"])) { item.BS_REVI_ANUAL_GNV_INIC = bdRd["BS_REVI_ANUAL_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_FIN"])) { item.BS_REVI_ANUAL_GNV_FIN = bdRd["BS_REVI_ANUAL_GNV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_INIC"])) { item.BS_REVISION_CILINDROS_GNV_INIC = bdRd["BS_REVISION_CILINDROS_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_FIN"])) { item.BS_REVISION_CILINDROS_GNV_FIN = bdRd["BS_REVISION_CILINDROS_GNV_FIN"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["COD_CAC"])) { item.COD_CAC = bdRd["COD_CAC"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["ESTADO_VEHICULO"])) { item.ESTADO_VEHICULO = bdRd["ESTADO_VEHICULO"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["PLACA_REEMPLAZADA"])) { item.PLACA_REEMPLAZADA = bdRd["PLACA_REEMPLAZADA"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["USUREG"])) { item.USUREG = bdRd["USUREG"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["ID_ESTADO"])) { item.ID_ESTADO = Convert.ToInt32(bdRd["ID_ESTADO"]); }



                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }


        public RPTA_GENERAL Insertar_Buses_Nuevos(CC_BUSES modelo)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();

            OracleParameter[] bdParameters = new OracleParameter[56];

            bdParameters[0] = new OracleParameter("P_ID_CORREDOR", OracleDbType.Int32) { Value = modelo.ID_CORREDOR };
            bdParameters[1] = new OracleParameter("P_BS_FECINI_PT", OracleDbType.Varchar2) { Value = modelo.BS_FECINI_PT };
            bdParameters[2] = new OracleParameter("P_BS_NOM_EMPRE", OracleDbType.Varchar2) { Value = modelo.BS_NOM_EMPRE };
            bdParameters[3] = new OracleParameter("P_BS_PLACA", OracleDbType.Varchar2) { Value = modelo.BS_PLACA };
            bdParameters[4] = new OracleParameter("P_BS_PROPIETARIO", OracleDbType.Varchar2) { Value = modelo.BS_PROPIETARIO };
            bdParameters[5] = new OracleParameter("P_BS_PAQUETE_CONCESION", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_CONCESION };
            bdParameters[6] = new OracleParameter("P_BS_PAQUETE_SERVICIO", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_SERVICIO };
            bdParameters[7] = new OracleParameter("P_BS_TIPO_SERVICIO", OracleDbType.Varchar2) { Value = modelo.BS_TIPO_SERVICIO };
            bdParameters[8] = new OracleParameter("P_BS_ESTADO", OracleDbType.Varchar2) { Value = modelo.BS_ESTADO };
            bdParameters[9] = new OracleParameter("P_BS_MARCA", OracleDbType.Varchar2) { Value = modelo.BS_MARCA };
            bdParameters[10] = new OracleParameter("P_BS_MODELO", OracleDbType.Varchar2) { Value = modelo.BS_MODELO };
            bdParameters[11] = new OracleParameter("P_BS_AÑO_FABRICACION", OracleDbType.Varchar2) { Value = modelo.BS_AÑO_FABRICACION };
            bdParameters[12] = new OracleParameter("P_BS_COMBUSTIBLE", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[13] = new OracleParameter("P_BS_TECNOLOGIA_EURO", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[14] = new OracleParameter("P_BS_POTENCIA_MOTOR", OracleDbType.Varchar2) { Value = modelo.BS_POTENCIA_MOTOR };
            bdParameters[15] = new OracleParameter("P_BS_SERIE_MOTOR", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_MOTOR };
            bdParameters[16] = new OracleParameter("P_BS_SERIE_CHASIS", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_CHASIS };
            bdParameters[17] = new OracleParameter("P_BS_COLOR_VEHICULO", OracleDbType.Varchar2) { Value = modelo.BS_COLOR_VEHICULO };
            bdParameters[18] = new OracleParameter("P_BS_LONGITUD", OracleDbType.Varchar2) { Value = modelo.BS_LONGITUD };
            bdParameters[19] = new OracleParameter("P_BS_ASIENTOS", OracleDbType.Varchar2) { Value = modelo.BS_ASIENTOS };
            bdParameters[20] = new OracleParameter("P_BS_AREA_PASILLO", OracleDbType.Varchar2) { Value = modelo.BS_AREA_PASILLO };
            bdParameters[21] = new OracleParameter("P_BS_PESO_NETO", OracleDbType.Varchar2) { Value = modelo.BS_PESO_NETO };
            bdParameters[22] = new OracleParameter("P_BS_PESO_BRUTO", OracleDbType.Varchar2) { Value = modelo.BS_PESO_BRUTO };
            bdParameters[23] = new OracleParameter("P_BS_ALTURA", OracleDbType.Varchar2) { Value = modelo.BS_ALTURA };
            bdParameters[24] = new OracleParameter("P_BS_ANCHO", OracleDbType.Varchar2) { Value = modelo.BS_ANCHO };
            bdParameters[25] = new OracleParameter("P_BS_CARGA_UTIL", OracleDbType.Varchar2) { Value = modelo.BS_CARGA_UTIL };
            bdParameters[26] = new OracleParameter("P_BS_PUERTA_IZQUIERDA", OracleDbType.Varchar2) { Value = modelo.BS_PUERTA_IZQUIERDA };
            bdParameters[27] = new OracleParameter("P_BS_PARTIDA_REGISTRAL", OracleDbType.Varchar2) { Value = modelo.BS_PARTIDA_REGISTRAL };
            bdParameters[28] = new OracleParameter("P_BS_CODIGO_CVS", OracleDbType.Varchar2) { Value = modelo.BS_CODIGO_CVS };
            bdParameters[29] = new OracleParameter("P_BS_CVS_FEC_INIC", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_INIC };
            bdParameters[30] = new OracleParameter("P_BS_CVS_FEC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_FIN };
            bdParameters[31] = new OracleParameter("P_BS_SOAT_FEC_INIC", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_INIC };
            bdParameters[32] = new OracleParameter("P_BS_SOAT_FEC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_FIN };
            bdParameters[33] = new OracleParameter("P_BS_POLIZA_VEHICULOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_VEHICULOS };
            bdParameters[34] = new OracleParameter("P_BS_VEHICULOS_INIC", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_INIC };
            bdParameters[35] = new OracleParameter("P_BS_VEHICULOS_FIN", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_FIN };
            bdParameters[36] = new OracleParameter("P_BS_POLIZA_CIVIL", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_CIVIL };
            bdParameters[37] = new OracleParameter("P_BS_RC_INICIO", OracleDbType.Varchar2) { Value = modelo.BS_RC_INICIO };
            bdParameters[38] = new OracleParameter("P_BS_RC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_RC_FIN };
            bdParameters[39] = new OracleParameter("P_BS_POLIZA_ACCI_COLECTIVOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_ACCI_COLECTIVOS };
            bdParameters[40] = new OracleParameter("P_BS_APCP_INIC", OracleDbType.Varchar2) { Value = modelo.BS_APCP_INIC };
            bdParameters[41] = new OracleParameter("P_BS_APCP_FIN", OracleDbType.Varchar2) { Value = modelo.BS_APCP_FIN };
            bdParameters[42] = new OracleParameter("P_BS_POLIZA_MULTIRIESGOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_MULTIRIESGOS };
            bdParameters[43] = new OracleParameter("P_BS_MULTIRESGOS_INIC", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_INIC };
            bdParameters[44] = new OracleParameter("P_BS_MULTIRESGOS_FIN", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_FIN };
            bdParameters[45] = new OracleParameter("P_BS_RTV_INIC", OracleDbType.Varchar2) { Value = modelo.BS_RTV_INIC };
            bdParameters[46] = new OracleParameter("P_BS_RTV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_RTV_FIN };
            bdParameters[47] = new OracleParameter("P_BS_REVI_ANUAL_GNV_INIC", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_INIC };
            bdParameters[48] = new OracleParameter("P_BS_REVI_ANUAL_GNV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_FIN };
            bdParameters[49] = new OracleParameter("P_BS_REVISION_CILINDROS_GNV_IN", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_INIC };
            bdParameters[50] = new OracleParameter("P_BS_REVISION_CILINDROS_GNV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_FIN };
            bdParameters[51] = new OracleParameter("P_ESTADO_VEHICULO", OracleDbType.Varchar2) { Value = modelo.ESTADO_VEHICULO };
            bdParameters[52] = new OracleParameter("P_PLACA_REEMPLAZADA", OracleDbType.Varchar2) { Value = modelo.PLACA_REEMPLAZADA };
            bdParameters[53] = new OracleParameter("P_USU_REG", OracleDbType.Varchar2) { Value = modelo.USU_REG };
            bdParameters[54] = new OracleParameter("P_FECHA_REG", OracleDbType.Varchar2) { Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") };
            bdParameters[55] = new OracleParameter("P_ID_ESTADO", OracleDbType.Int32) { Value = modelo.ID_ESTADO };


            try
            {
                using (var bdCmd = new OracleCommand("PKG_BUSES.SP_INSERTAR_BUSES_NUEVOS", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();

                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se registró correctamente";
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }

        public RPTA_GENERAL anularBus(int idBus, string usuarioAnula)
        {
            RPTA_GENERAL r = new RPTA_GENERAL();
            OracleParameter[] bdParameters = new OracleParameter[3];
            bdParameters[0] = new OracleParameter("P_BS_ID", OracleDbType.Int32) { Value = idBus };
            bdParameters[1] = new OracleParameter("P_USU_ANULA", OracleDbType.Varchar2) { Value = usuarioAnula };
            bdParameters[2] = new OracleParameter("P_FECHA_ANULA", OracleDbType.Varchar2) { Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") };

            try
            {
                using (var bdCmd = new OracleCommand("PKG_BUSES.anularBus", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se anuló correctamente";
                }
            }
            catch (Exception ex)
            {
                r.AUX = 0;
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }

        public RPTA_GENERAL desafectarBus(int id_placa_reemplazada, string placa_nueva)
        {
            RPTA_GENERAL r = new RPTA_GENERAL();
            OracleParameter[] bdParameters = new OracleParameter[2];
            bdParameters[0] = new OracleParameter("P_BS_ID", OracleDbType.Int32) { Value = id_placa_reemplazada };
            bdParameters[1] = new OracleParameter("P_PLACA_REEMPLAZADA", OracleDbType.Varchar2) { Value = placa_nueva };


            try
            {
                using (var bdCmd = new OracleCommand("PKG_BUSES.SP_DESAFECTAR_BUS", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se Actualizo correctamente";
                }
            }
            catch (Exception ex)
            {
                r.AUX = 0;
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }

        public List<CC_BUSES> Listar_Buses_Desafectados()
        {
            List<CC_BUSES> resultado = new List<CC_BUSES>();

            OracleParameter[] bdParameters = new OracleParameter[1];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            using (var bdCmd = new OracleCommand("PKG_BUSES.listarBusesDesafectados", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_BUSES();
                            if (!DBNull.Value.Equals(bdRd["BS_ID"])) { item.BS_ID = Convert.ToInt32(bdRd["BS_ID"]); }
                            if (!DBNull.Value.Equals(bdRd["ID_CORREDOR"])) { item.ID_CORREDOR = Convert.ToInt32(bdRd["ID_CORREDOR"]); }
                            if (!DBNull.Value.Equals(bdRd["ABREVIATURA"])) { item.ABREVIATURA = bdRd["ABREVIATURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_FECINI_PT"])) { item.BS_FECINI_PT = bdRd["BS_FECINI_PT"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_NOM_EMPRE"])) { item.BS_NOM_EMPRE = bdRd["BS_NOM_EMPRE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PLACA"])) { item.BS_PLACA = bdRd["BS_PLACA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PROPIETARIO"])) { item.BS_PROPIETARIO = bdRd["BS_PROPIETARIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_CONCESION"])) { item.BS_PAQUETE_CONCESION = bdRd["BS_PAQUETE_CONCESION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_SERVICIO"])) { item.BS_PAQUETE_SERVICIO = bdRd["BS_PAQUETE_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TIPO_SERVICIO"])) { item.BS_TIPO_SERVICIO = bdRd["BS_TIPO_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ESTADO"])) { item.BS_ESTADO = bdRd["BS_ESTADO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MARCA"])) { item.BS_MARCA = bdRd["BS_MARCA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MODELO"])) { item.BS_MODELO = bdRd["BS_MODELO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AÑO_FABRICACION"])) { item.BS_AÑO_FABRICACION = bdRd["BS_AÑO_FABRICACION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COMBUSTIBLE"])) { item.BS_COMBUSTIBLE = bdRd["BS_COMBUSTIBLE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TECNOLOGIA_EURO"])) { item.BS_TECNOLOGIA_EURO = bdRd["BS_TECNOLOGIA_EURO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POTENCIA_MOTOR"])) { item.BS_POTENCIA_MOTOR = bdRd["BS_POTENCIA_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_MOTOR"])) { item.BS_SERIE_MOTOR = bdRd["BS_SERIE_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_CHASIS"])) { item.BS_SERIE_CHASIS = bdRd["BS_SERIE_CHASIS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COLOR_VEHICULO"])) { item.BS_COLOR_VEHICULO = bdRd["BS_COLOR_VEHICULO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_LONGITUD"])) { item.BS_LONGITUD = bdRd["BS_LONGITUD"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ASIENTOS"])) { item.BS_ASIENTOS = bdRd["BS_ASIENTOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AREA_PASILLO"])) { item.BS_AREA_PASILLO = bdRd["BS_AREA_PASILLO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_NETO"])) { item.BS_PESO_NETO = bdRd["BS_PESO_NETO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_BRUTO"])) { item.BS_PESO_BRUTO = bdRd["BS_PESO_BRUTO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ALTURA"])) { item.BS_ALTURA = bdRd["BS_ALTURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ANCHO"])) { item.BS_ANCHO = bdRd["BS_ANCHO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CARGA_UTIL"])) { item.BS_CARGA_UTIL = bdRd["BS_CARGA_UTIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PUERTA_IZQUIERDA"])) { item.BS_PUERTA_IZQUIERDA = bdRd["BS_PUERTA_IZQUIERDA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PARTIDA_REGISTRAL"])) { item.BS_PARTIDA_REGISTRAL = bdRd["BS_PARTIDA_REGISTRAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CODIGO_CVS"])) { item.BS_CODIGO_CVS = bdRd["BS_CODIGO_CVS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_INIC"])) { item.BS_CVS_FEC_INIC = bdRd["BS_CVS_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_FIN"])) { item.BS_CVS_FEC_FIN = bdRd["BS_CVS_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_INIC"])) { item.BS_SOAT_FEC_INIC = bdRd["BS_SOAT_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_FIN"])) { item.BS_SOAT_FEC_FIN = bdRd["BS_SOAT_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_VEHICULOS"])) { item.BS_POLIZA_VEHICULOS = bdRd["BS_POLIZA_VEHICULOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_INIC"])) { item.BS_VEHICULOS_INIC = bdRd["BS_VEHICULOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_FIN"])) { item.BS_VEHICULOS_FIN = bdRd["BS_VEHICULOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_CIVIL"])) { item.BS_POLIZA_CIVIL = bdRd["BS_POLIZA_CIVIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_INICIO"])) { item.BS_RC_INICIO = bdRd["BS_RC_INICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_FIN"])) { item.BS_RC_FIN = bdRd["BS_RC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_ACCI_COLECTIVOS"])) { item.BS_POLIZA_ACCI_COLECTIVOS = bdRd["BS_POLIZA_ACCI_COLECTIVOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_INIC"])) { item.BS_APCP_INIC = bdRd["BS_APCP_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_FIN"])) { item.BS_APCP_FIN = bdRd["BS_APCP_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_MULTIRIESGOS"])) { item.BS_POLIZA_MULTIRIESGOS = bdRd["BS_POLIZA_MULTIRIESGOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_INIC"])) { item.BS_MULTIRESGOS_INIC = bdRd["BS_MULTIRESGOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_FIN"])) { item.BS_MULTIRESGOS_FIN = bdRd["BS_MULTIRESGOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_INIC"])) { item.BS_RTV_INIC = bdRd["BS_RTV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_FIN"])) { item.BS_RTV_FIN = bdRd["BS_RTV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_INIC"])) { item.BS_REVI_ANUAL_GNV_INIC = bdRd["BS_REVI_ANUAL_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_FIN"])) { item.BS_REVI_ANUAL_GNV_FIN = bdRd["BS_REVI_ANUAL_GNV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_INIC"])) { item.BS_REVISION_CILINDROS_GNV_INIC = bdRd["BS_REVISION_CILINDROS_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_FIN"])) { item.BS_REVISION_CILINDROS_GNV_FIN = bdRd["BS_REVISION_CILINDROS_GNV_FIN"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["COD_CAC"])) { item.COD_CAC = bdRd["COD_CAC"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["ESTADO_VEHICULO"])) { item.ESTADO_VEHICULO = bdRd["ESTADO_VEHICULO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["PLACA_REEMPLAZADA"])) { item.PLACA_REEMPLAZADA = bdRd["PLACA_REEMPLAZADA"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["USUREG"])) { item.USUREG = bdRd["USUREG"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["ID_ESTADO"])) { item.ID_ESTADO = Convert.ToInt32(bdRd["ID_ESTADO"]); }

                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }

        public RPTA_GENERAL Insertar_Buses_Afectados(CC_BUSES modelo)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();

            OracleParameter[] bdParameters = new OracleParameter[57];
            //bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            //bdParameters[0] = new OracleParameter("BS_ID", OracleDbType.Int32) { Value = modelo.BS_ID };
            bdParameters[0] = new OracleParameter("P_ID_CORREDOR", OracleDbType.Int32) { Value = modelo.ID_CORREDOR };
            bdParameters[1] = new OracleParameter("P_BS_FECINI_PT", OracleDbType.Varchar2) { Value = modelo.BS_FECINI_PT };
            bdParameters[2] = new OracleParameter("P_BS_NOM_EMPRE", OracleDbType.Varchar2) { Value = modelo.BS_NOM_EMPRE };
            bdParameters[3] = new OracleParameter("P_BS_PLACA", OracleDbType.Varchar2) { Value = modelo.BS_PLACA };
            bdParameters[4] = new OracleParameter("P_BS_PROPIETARIO", OracleDbType.Varchar2) { Value = modelo.BS_PROPIETARIO };
            bdParameters[5] = new OracleParameter("P_BS_PAQUETE_CONCESION", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_CONCESION };
            bdParameters[6] = new OracleParameter("P_BS_PAQUETE_SERVICIO", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_SERVICIO };
            bdParameters[7] = new OracleParameter("P_BS_TIPO_SERVICIO", OracleDbType.Varchar2) { Value = modelo.BS_TIPO_SERVICIO };
            bdParameters[8] = new OracleParameter("P_BS_ESTADO", OracleDbType.Varchar2) { Value = modelo.BS_ESTADO };
            bdParameters[9] = new OracleParameter("P_BS_MARCA", OracleDbType.Varchar2) { Value = modelo.BS_MARCA };
            bdParameters[10] = new OracleParameter("P_BS_MODELO", OracleDbType.Varchar2) { Value = modelo.BS_MODELO };
            bdParameters[11] = new OracleParameter("P_BS_AÑO_FABRICACION", OracleDbType.Varchar2) { Value = modelo.BS_AÑO_FABRICACION };
            bdParameters[12] = new OracleParameter("P_BS_COMBUSTIBLE", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[13] = new OracleParameter("P_BS_TECNOLOGIA_EURO", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[14] = new OracleParameter("P_BS_POTENCIA_MOTOR", OracleDbType.Varchar2) { Value = modelo.BS_POTENCIA_MOTOR };
            bdParameters[15] = new OracleParameter("P_BS_SERIE_MOTOR", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_MOTOR };
            bdParameters[16] = new OracleParameter("P_BS_SERIE_CHASIS", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_CHASIS };
            bdParameters[17] = new OracleParameter("P_BS_COLOR_VEHICULO", OracleDbType.Varchar2) { Value = modelo.BS_COLOR_VEHICULO };
            bdParameters[18] = new OracleParameter("P_BS_LONGITUD", OracleDbType.Varchar2) { Value = modelo.BS_LONGITUD };
            bdParameters[19] = new OracleParameter("P_BS_ASIENTOS", OracleDbType.Varchar2) { Value = modelo.BS_ASIENTOS };
            bdParameters[20] = new OracleParameter("P_BS_AREA_PASILLO", OracleDbType.Varchar2) { Value = modelo.BS_AREA_PASILLO };
            bdParameters[21] = new OracleParameter("P_BS_PESO_NETO", OracleDbType.Varchar2) { Value = modelo.BS_PESO_NETO };
            bdParameters[22] = new OracleParameter("P_BS_PESO_BRUTO", OracleDbType.Varchar2) { Value = modelo.BS_PESO_BRUTO };
            bdParameters[23] = new OracleParameter("P_BS_ALTURA", OracleDbType.Varchar2) { Value = modelo.BS_ALTURA };
            bdParameters[24] = new OracleParameter("P_BS_ANCHO", OracleDbType.Varchar2) { Value = modelo.BS_ANCHO };
            bdParameters[25] = new OracleParameter("P_BS_CARGA_UTIL", OracleDbType.Varchar2) { Value = modelo.BS_CARGA_UTIL };
            bdParameters[26] = new OracleParameter("P_BS_PUERTA_IZQUIERDA", OracleDbType.Varchar2) { Value = modelo.BS_PUERTA_IZQUIERDA };
            bdParameters[27] = new OracleParameter("P_BS_PARTIDA_REGISTRAL", OracleDbType.Varchar2) { Value = modelo.BS_PARTIDA_REGISTRAL };
            bdParameters[28] = new OracleParameter("P_BS_CODIGO_CVS", OracleDbType.Varchar2) { Value = modelo.BS_CODIGO_CVS };
            bdParameters[29] = new OracleParameter("P_BS_CVS_FEC_INIC", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_INIC };
            bdParameters[30] = new OracleParameter("P_BS_CVS_FEC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_FIN };
            bdParameters[31] = new OracleParameter("P_BS_SOAT_FEC_INIC", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_INIC };
            bdParameters[32] = new OracleParameter("P_BS_SOAT_FEC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_FIN };
            bdParameters[33] = new OracleParameter("P_BS_POLIZA_VEHICULOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_VEHICULOS };
            bdParameters[34] = new OracleParameter("P_BS_VEHICULOS_INIC", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_INIC };
            bdParameters[35] = new OracleParameter("P_BS_VEHICULOS_FIN", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_FIN };
            bdParameters[36] = new OracleParameter("P_BS_POLIZA_CIVIL", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_CIVIL };
            bdParameters[37] = new OracleParameter("P_BS_RC_INICIO", OracleDbType.Varchar2) { Value = modelo.BS_RC_INICIO };
            bdParameters[38] = new OracleParameter("P_BS_RC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_RC_FIN };
            bdParameters[39] = new OracleParameter("P_BS_POLIZA_ACCI_COLECTIVOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_ACCI_COLECTIVOS };
            bdParameters[40] = new OracleParameter("P_BS_APCP_INIC", OracleDbType.Varchar2) { Value = modelo.BS_APCP_INIC };
            bdParameters[41] = new OracleParameter("P_BS_APCP_FIN", OracleDbType.Varchar2) { Value = modelo.BS_APCP_FIN };
            bdParameters[42] = new OracleParameter("P_BS_POLIZA_MULTIRIESGOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_MULTIRIESGOS };
            bdParameters[43] = new OracleParameter("P_BS_MULTIRESGOS_INIC", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_INIC };
            bdParameters[44] = new OracleParameter("P_BS_MULTIRESGOS_FIN", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_FIN };
            bdParameters[45] = new OracleParameter("P_BS_RTV_INIC", OracleDbType.Varchar2) { Value = modelo.BS_RTV_INIC };
            bdParameters[46] = new OracleParameter("P_BS_RTV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_RTV_FIN };
            bdParameters[47] = new OracleParameter("P_BS_REVI_ANUAL_GNV_INIC", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_INIC };
            bdParameters[48] = new OracleParameter("P_BS_REVI_ANUAL_GNV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_FIN };
            bdParameters[49] = new OracleParameter("P_BS_REVISION_CILINDROS_GNV_IN", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_INIC };
            bdParameters[50] = new OracleParameter("P_BS_REVISION_CILINDROS_GNV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_FIN };
            bdParameters[51] = new OracleParameter("P_ESTADO_VEHICULO", OracleDbType.Varchar2) { Value = modelo.ESTADO_VEHICULO };
            bdParameters[52] = new OracleParameter("P_PLACA_REEMPLAZADA", OracleDbType.Varchar2) { Value = modelo.PLACA_REEMPLAZADA };
            bdParameters[53] = new OracleParameter("P_USU_REG", OracleDbType.Varchar2) { Value = modelo.USU_REG };
            bdParameters[54] = new OracleParameter("P_FECHA_REG", OracleDbType.Varchar2) { Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") };
            bdParameters[55] = new OracleParameter("P_ID_ESTADO", OracleDbType.Int32) { Value = modelo.ID_ESTADO };
            bdParameters[56] = new OracleParameter("P_PLACA_", OracleDbType.Varchar2, 50);
            bdParameters[56].Direction = ParameterDirection.Output;


            try
            {
                using (var bdCmd = new OracleCommand("PKG_BUSES.SP_INSERT_BUS_RETURN_PLACA", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    var placa = bdCmd.Parameters["P_PLACA_"].Value.ToString();

                    r.AUX = 1;
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = placa;
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }




        public RPTA_GENERAL Modificar_Bus(CC_BUSES modelo)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();

            OracleParameter[] bdParameters = new OracleParameter[54];
            //bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var fecha_actual = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff");


            bdParameters[0] = new OracleParameter("P_BS_ID", OracleDbType.Int32) { Value = modelo.BS_ID };
            bdParameters[1] = new OracleParameter("P_ID_CORREDOR", OracleDbType.Int32) { Value = modelo.ID_CORREDOR };
            bdParameters[2] = new OracleParameter("P_BS_FECINI_PT", OracleDbType.Varchar2) { Value = modelo.BS_FECINI_PT };
            bdParameters[3] = new OracleParameter("P_BS_NOM_EMPRE", OracleDbType.Varchar2) { Value = modelo.BS_NOM_EMPRE };
            bdParameters[4] = new OracleParameter("P_BS_PLACA", OracleDbType.Varchar2) { Value = modelo.BS_PLACA };
            bdParameters[5] = new OracleParameter("P_BS_PROPIETARIO", OracleDbType.Varchar2) { Value = modelo.BS_PROPIETARIO };
            bdParameters[6] = new OracleParameter("P_BS_PAQUETE_CONCESION", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_CONCESION };
            bdParameters[7] = new OracleParameter("P_BS_PAQUETE_SERVICIO", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_SERVICIO };
            bdParameters[8] = new OracleParameter("P_BS_TIPO_SERVICIO", OracleDbType.Varchar2) { Value = modelo.BS_TIPO_SERVICIO };
            bdParameters[9] = new OracleParameter("P_BS_ESTADO", OracleDbType.Varchar2) { Value = modelo.BS_ESTADO };
            bdParameters[10] = new OracleParameter("P_BS_MARCA", OracleDbType.Varchar2) { Value = modelo.BS_MARCA };
            bdParameters[11] = new OracleParameter("P_BS_MODELO", OracleDbType.Varchar2) { Value = modelo.BS_MODELO };
            bdParameters[12] = new OracleParameter("P_BS_AÑO_FABRICACION", OracleDbType.Varchar2) { Value = modelo.BS_AÑO_FABRICACION };
            bdParameters[13] = new OracleParameter("P_BS_COMBUSTIBLE", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[14] = new OracleParameter("P_BS_TECNOLOGIA_EURO", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[15] = new OracleParameter("P_BS_POTENCIA_MOTOR", OracleDbType.Varchar2) { Value = modelo.BS_POTENCIA_MOTOR };
            bdParameters[16] = new OracleParameter("P_BS_SERIE_MOTOR", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_MOTOR };
            bdParameters[17] = new OracleParameter("P_BS_SERIE_CHASIS", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_CHASIS };
            bdParameters[18] = new OracleParameter("P_BS_COLOR_VEHICULO", OracleDbType.Varchar2) { Value = modelo.BS_COLOR_VEHICULO };
            bdParameters[19] = new OracleParameter("P_BS_LONGITUD", OracleDbType.Varchar2) { Value = modelo.BS_LONGITUD };
            bdParameters[20] = new OracleParameter("P_BS_ASIENTOS", OracleDbType.Varchar2) { Value = modelo.BS_ASIENTOS };
            bdParameters[21] = new OracleParameter("P_BS_AREA_PASILLO", OracleDbType.Varchar2) { Value = modelo.BS_AREA_PASILLO };
            bdParameters[22] = new OracleParameter("P_BS_PESO_NETO", OracleDbType.Varchar2) { Value = modelo.BS_PESO_NETO };
            bdParameters[23] = new OracleParameter("P_BS_PESO_BRUTO", OracleDbType.Varchar2) { Value = modelo.BS_PESO_BRUTO };
            bdParameters[24] = new OracleParameter("P_BS_ALTURA", OracleDbType.Varchar2) { Value = modelo.BS_ALTURA };
            bdParameters[25] = new OracleParameter("P_BS_ANCHO", OracleDbType.Varchar2) { Value = modelo.BS_ANCHO };
            bdParameters[26] = new OracleParameter("P_BS_CARGA_UTIL", OracleDbType.Varchar2) { Value = modelo.BS_CARGA_UTIL };
            bdParameters[27] = new OracleParameter("P_BS_PUERTA_IZQUIERDA", OracleDbType.Varchar2) { Value = modelo.BS_PUERTA_IZQUIERDA };
            bdParameters[28] = new OracleParameter("P_BS_PARTIDA_REGISTRAL", OracleDbType.Varchar2) { Value = modelo.BS_PARTIDA_REGISTRAL };
            bdParameters[29] = new OracleParameter("P_BS_CODIGO_CVS", OracleDbType.Varchar2) { Value = modelo.BS_CODIGO_CVS };
            bdParameters[30] = new OracleParameter("P_BS_CVS_FEC_INIC", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_INIC };
            bdParameters[31] = new OracleParameter("P_BS_CVS_FEC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_FIN };
            bdParameters[32] = new OracleParameter("P_BS_SOAT_FEC_INIC", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_INIC };
            bdParameters[33] = new OracleParameter("P_BS_SOAT_FEC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_FIN };
            bdParameters[34] = new OracleParameter("P_BS_POLIZA_VEHICULOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_VEHICULOS };
            bdParameters[35] = new OracleParameter("P_BS_VEHICULOS_INIC", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_INIC };
            bdParameters[36] = new OracleParameter("P_BS_VEHICULOS_FIN", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_FIN };
            bdParameters[37] = new OracleParameter("P_BS_POLIZA_CIVIL", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_CIVIL };
            bdParameters[38] = new OracleParameter("P_BS_RC_INICIO", OracleDbType.Varchar2) { Value = modelo.BS_RC_INICIO };
            bdParameters[39] = new OracleParameter("P_BS_RC_FIN", OracleDbType.Varchar2) { Value = modelo.BS_RC_FIN };
            bdParameters[40] = new OracleParameter("P_BS_POLIZA_ACCI_COLECTIVOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_ACCI_COLECTIVOS };
            bdParameters[41] = new OracleParameter("P_BS_APCP_INIC", OracleDbType.Varchar2) { Value = modelo.BS_APCP_INIC };
            bdParameters[42] = new OracleParameter("P_BS_APCP_FIN", OracleDbType.Varchar2) { Value = modelo.BS_APCP_FIN };
            bdParameters[43] = new OracleParameter("P_BS_POLIZA_MULTIRIESGOS", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_MULTIRIESGOS };
            bdParameters[44] = new OracleParameter("P_BS_MULTIRESGOS_INIC", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_INIC };
            bdParameters[45] = new OracleParameter("P_BS_MULTIRESGOS_FIN", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_FIN };
            bdParameters[46] = new OracleParameter("P_BS_RTV_INIC", OracleDbType.Varchar2) { Value = modelo.BS_RTV_INIC };
            bdParameters[47] = new OracleParameter("P_BS_RTV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_RTV_FIN };
            bdParameters[48] = new OracleParameter("P_BS_REVI_ANUAL_GNV_INIC", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_INIC };
            bdParameters[49] = new OracleParameter("P_BS_REVI_ANUAL_GNV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_FIN };
            bdParameters[50] = new OracleParameter("P_BS_REVISION_CILINDROS_GNV_IN", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_INIC };
            bdParameters[51] = new OracleParameter("P_BS_REVISION_CILINDROS_GNV_FIN", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_FIN };
            //bdParameters[52] = new OracleParameter("P_ESTADO_VEHICULO", OracleDbType.Varchar2) { Value = modelo.ESTADO_VEHICULO };
            bdParameters[52] = new OracleParameter("P_USU_MODIF", OracleDbType.Varchar2) { Value = modelo.USU_MODIF };
            bdParameters[53] = new OracleParameter("P_FECHA_MODIF", OracleDbType.Varchar2) { Value = fecha_actual };



            try
            {
                using (var bdCmd = new OracleCommand("PKG_BUSES.SP_EDITAR_BUSES", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    //BS_ID = int.Parse(bdCmd.Parameters["BS_ID"].Value.ToString());
                    //r.AUX = BS_ID;
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se modificó correctamente";
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }


        public RPTA_GENERAL Insertar_Buses_Nuevospr(CC_BUSES modelo)
        {

            RPTA_GENERAL resultado = new RPTA_GENERAL();


            OracleParameter[] bdParameters = new OracleParameter[57];

            bdParameters[0] = new OracleParameter("BS_ID_", OracleDbType.Int32) { Value = modelo.BS_ID };
            bdParameters[1] = new OracleParameter("ID_CORREDOR_", OracleDbType.Int32) { Value = modelo.ID_CORREDOR };
            bdParameters[2] = new OracleParameter("BS_FECINI_PT_", OracleDbType.Varchar2) { Value = modelo.BS_FECINI_PT };
            bdParameters[3] = new OracleParameter("BS_NOM_EMPRE_", OracleDbType.Varchar2) { Value = modelo.BS_NOM_EMPRE };
            bdParameters[4] = new OracleParameter("BS_PLACA_", OracleDbType.Varchar2) { Value = modelo.BS_PLACA };
            bdParameters[5] = new OracleParameter("BS_PROPIETARIO_", OracleDbType.Varchar2) { Value = modelo.BS_PROPIETARIO };
            bdParameters[6] = new OracleParameter("BS_PAQUETE_CONCESION_", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_CONCESION };
            bdParameters[7] = new OracleParameter("BS_PAQUETE_SERVICIO_", OracleDbType.Varchar2) { Value = modelo.BS_PAQUETE_SERVICIO };
            bdParameters[8] = new OracleParameter("BS_TIPO_SERVICIO_", OracleDbType.Varchar2) { Value = modelo.BS_TIPO_SERVICIO };
            bdParameters[9] = new OracleParameter("BS_ESTADO_", OracleDbType.Varchar2) { Value = modelo.BS_ESTADO };
            bdParameters[10] = new OracleParameter("BS_MARCA_", OracleDbType.Varchar2) { Value = modelo.BS_MARCA };
            bdParameters[11] = new OracleParameter("BS_MODELO_", OracleDbType.Varchar2) { Value = modelo.BS_MODELO };
            bdParameters[12] = new OracleParameter("BS_AÑO_FABRICACION_", OracleDbType.Varchar2) { Value = modelo.BS_AÑO_FABRICACION };
            bdParameters[13] = new OracleParameter("BS_COMBUSTIBLE_", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[14] = new OracleParameter("BS_TECNOLOGIA_EURO_", OracleDbType.Varchar2) { Value = modelo.BS_COMBUSTIBLE };
            bdParameters[15] = new OracleParameter("BS_POTENCIA_MOTOR_", OracleDbType.Varchar2) { Value = modelo.BS_POTENCIA_MOTOR };
            bdParameters[16] = new OracleParameter("BS_SERIE_MOTOR_", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_MOTOR };
            bdParameters[17] = new OracleParameter("BS_SERIE_CHASIS_", OracleDbType.Varchar2) { Value = modelo.BS_SERIE_CHASIS };
            bdParameters[18] = new OracleParameter("BS_COLOR_VEHICULO_", OracleDbType.Varchar2) { Value = modelo.BS_COLOR_VEHICULO };
            bdParameters[19] = new OracleParameter("BS_LONGITUD_", OracleDbType.Varchar2) { Value = modelo.BS_LONGITUD };
            bdParameters[20] = new OracleParameter("BS_ASIENTOS_", OracleDbType.Varchar2) { Value = modelo.BS_ASIENTOS };
            bdParameters[21] = new OracleParameter("BS_AREA_PASILLO_", OracleDbType.Varchar2) { Value = modelo.BS_AREA_PASILLO };
            bdParameters[22] = new OracleParameter("BS_PESO_NETO_", OracleDbType.Varchar2) { Value = modelo.BS_PESO_NETO };
            bdParameters[23] = new OracleParameter("BS_PESO_BRUTO_", OracleDbType.Varchar2) { Value = modelo.BS_PESO_BRUTO };
            bdParameters[24] = new OracleParameter("BS_ALTURA_", OracleDbType.Varchar2) { Value = modelo.BS_ALTURA };
            bdParameters[25] = new OracleParameter("BS_ANCHO_", OracleDbType.Varchar2) { Value = modelo.BS_ANCHO };
            bdParameters[26] = new OracleParameter("BS_CARGA_UTIL_", OracleDbType.Varchar2) { Value = modelo.BS_CARGA_UTIL };
            bdParameters[27] = new OracleParameter("BS_PUERTA_IZQUIERDA_", OracleDbType.Varchar2) { Value = modelo.BS_PUERTA_IZQUIERDA };
            bdParameters[28] = new OracleParameter("BS_PARTIDA_REGISTRAL_", OracleDbType.Varchar2) { Value = modelo.BS_PARTIDA_REGISTRAL };
            bdParameters[29] = new OracleParameter("BS_CODIGO_CVS_", OracleDbType.Varchar2) { Value = modelo.BS_CODIGO_CVS };
            bdParameters[30] = new OracleParameter("BS_CVS_FEC_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_INIC };
            bdParameters[31] = new OracleParameter("BS_CVS_FEC_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_CVS_FEC_FIN };
            bdParameters[32] = new OracleParameter("BS_SOAT_FEC_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_INIC };
            bdParameters[33] = new OracleParameter("BS_SOAT_FEC_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_SOAT_FEC_FIN };
            bdParameters[34] = new OracleParameter("BS_POLIZA_VEHICULOS_", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_VEHICULOS };
            bdParameters[35] = new OracleParameter("BS_VEHICULOS_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_INIC };
            bdParameters[36] = new OracleParameter("BS_VEHICULOS_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_VEHICULOS_FIN };
            bdParameters[37] = new OracleParameter("BS_POLIZA_CIVIL_", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_CIVIL };
            bdParameters[38] = new OracleParameter("BS_RC_INICIO_", OracleDbType.Varchar2) { Value = modelo.BS_RC_INICIO };
            bdParameters[39] = new OracleParameter("BS_RC_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_RC_FIN };
            bdParameters[40] = new OracleParameter("BS_POLIZA_ACCI_COLECTIVOS_", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_ACCI_COLECTIVOS };
            bdParameters[41] = new OracleParameter("BS_APCP_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_APCP_INIC };
            bdParameters[42] = new OracleParameter("BS_APCP_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_APCP_FIN };
            bdParameters[43] = new OracleParameter("BS_POLIZA_MULTIRIESGOS_", OracleDbType.Varchar2) { Value = modelo.BS_POLIZA_MULTIRIESGOS };
            bdParameters[44] = new OracleParameter("BS_MULTIRESGOS_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_INIC };
            bdParameters[45] = new OracleParameter("BS_MULTIRESGOS_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_MULTIRESGOS_FIN };
            bdParameters[46] = new OracleParameter("BS_RTV_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_RTV_INIC };
            bdParameters[47] = new OracleParameter("BS_RTV_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_RTV_FIN };
            bdParameters[48] = new OracleParameter("BS_REVI_ANUAL_GNV_INIC_", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_INIC };
            bdParameters[49] = new OracleParameter("BS_REVI_ANUAL_GNV_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_REVI_ANUAL_GNV_FIN };
            bdParameters[50] = new OracleParameter("BS_REVISION_CILINDROS_GNV_IN_", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_INIC };
            bdParameters[51] = new OracleParameter("BS_REVISION_CILINDROS_GNV_FIN_", OracleDbType.Varchar2) { Value = modelo.BS_REVISION_CILINDROS_GNV_FIN };
            bdParameters[52] = new OracleParameter("ESTADO_VEHICULO_", OracleDbType.Varchar2) { Value = modelo.ESTADO_VEHICULO };
            bdParameters[53] = new OracleParameter("PLACA_REEMPLAZADA_", OracleDbType.Varchar2) { Value = modelo.PLACA_REEMPLAZADA };
            bdParameters[54] = new OracleParameter("USUREG_", OracleDbType.Varchar2) { Value = modelo.USU_REG };
            bdParameters[55] = new OracleParameter("FECHA_", OracleDbType.Varchar2) { Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") };
            bdParameters[56] = new OracleParameter("ID_ESTADO_", OracleDbType.Int32) { Value = modelo.ID_ESTADO };


            using (var bdCmd = new OracleCommand("PKG_BUSES.SP_INSERTAR_BUSES_NUEVOS", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_BUSES();
                            if (!DBNull.Value.Equals(bdRd["BS_ID"])) { item.BS_ID = Convert.ToInt32(bdRd["BS_ID"]); }
                            if (!DBNull.Value.Equals(bdRd["ID_CORREDOR"])) { item.ID_CORREDOR = Convert.ToInt32(bdRd["ID_CORREDOR"]); }
                            if (!DBNull.Value.Equals(bdRd["BS_FECINI_PT"])) { item.BS_FECINI_PT = bdRd["BS_FECINI_PT"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_NOM_EMPRE"])) { item.BS_NOM_EMPRE = bdRd["BS_NOM_EMPRE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PLACA"])) { item.BS_PLACA = bdRd["BS_PLACA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PROPIETARIO"])) { item.BS_PROPIETARIO = bdRd["BS_PROPIETARIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_CONCESION"])) { item.BS_PAQUETE_CONCESION = bdRd["BS_PAQUETE_CONCESION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PAQUETE_SERVICIO"])) { item.BS_PAQUETE_SERVICIO = bdRd["BS_PAQUETE_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TIPO_SERVICIO"])) { item.BS_TIPO_SERVICIO = bdRd["BS_TIPO_SERVICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ESTADO"])) { item.BS_ESTADO = bdRd["BS_ESTADO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MARCA"])) { item.BS_MARCA = bdRd["BS_MARCA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MODELO"])) { item.BS_MODELO = bdRd["BS_MODELO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AÑO_FABRICACION"])) { item.BS_AÑO_FABRICACION = bdRd["BS_AÑO_FABRICACION"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COMBUSTIBLE"])) { item.BS_COMBUSTIBLE = bdRd["BS_COMBUSTIBLE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_TECNOLOGIA_EURO"])) { item.BS_TECNOLOGIA_EURO = bdRd["BS_TECNOLOGIA_EURO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POTENCIA_MOTOR"])) { item.BS_POTENCIA_MOTOR = bdRd["BS_POTENCIA_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_MOTOR"])) { item.BS_SERIE_MOTOR = bdRd["BS_SERIE_MOTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SERIE_CHASIS"])) { item.BS_SERIE_CHASIS = bdRd["BS_SERIE_CHASIS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_COLOR_VEHICULO"])) { item.BS_COLOR_VEHICULO = bdRd["BS_COLOR_VEHICULO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_LONGITUD"])) { item.BS_LONGITUD = bdRd["BS_LONGITUD"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ASIENTOS"])) { item.BS_ASIENTOS = bdRd["BS_ASIENTOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_AREA_PASILLO"])) { item.BS_AREA_PASILLO = bdRd["BS_AREA_PASILLO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_NETO"])) { item.BS_PESO_NETO = bdRd["BS_PESO_NETO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PESO_BRUTO"])) { item.BS_PESO_BRUTO = bdRd["BS_PESO_BRUTO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ALTURA"])) { item.BS_ALTURA = bdRd["BS_ALTURA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_ANCHO"])) { item.BS_ANCHO = bdRd["BS_ANCHO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CARGA_UTIL"])) { item.BS_CARGA_UTIL = bdRd["BS_CARGA_UTIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PUERTA_IZQUIERDA"])) { item.BS_PUERTA_IZQUIERDA = bdRd["BS_PUERTA_IZQUIERDA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_PARTIDA_REGISTRAL"])) { item.BS_PARTIDA_REGISTRAL = bdRd["BS_PARTIDA_REGISTRAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CODIGO_CVS"])) { item.BS_CODIGO_CVS = bdRd["BS_CODIGO_CVS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_INIC"])) { item.BS_CVS_FEC_INIC = bdRd["BS_CVS_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_CVS_FEC_FIN"])) { item.BS_CVS_FEC_FIN = bdRd["BS_CVS_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_INIC"])) { item.BS_SOAT_FEC_INIC = bdRd["BS_SOAT_FEC_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_SOAT_FEC_FIN"])) { item.BS_SOAT_FEC_FIN = bdRd["BS_SOAT_FEC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_VEHICULOS"])) { item.BS_POLIZA_VEHICULOS = bdRd["BS_POLIZA_VEHICULOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_INIC"])) { item.BS_VEHICULOS_INIC = bdRd["BS_VEHICULOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_VEHICULOS_FIN"])) { item.BS_VEHICULOS_FIN = bdRd["BS_VEHICULOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_CIVIL"])) { item.BS_POLIZA_CIVIL = bdRd["BS_POLIZA_CIVIL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_INICIO"])) { item.BS_RC_INICIO = bdRd["BS_RC_INICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RC_FIN"])) { item.BS_RC_FIN = bdRd["BS_RC_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_ACCI_COLECTIVOS"])) { item.BS_POLIZA_ACCI_COLECTIVOS = bdRd["BS_POLIZA_ACCI_COLECTIVOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_INIC"])) { item.BS_APCP_INIC = bdRd["BS_APCP_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_APCP_FIN"])) { item.BS_APCP_FIN = bdRd["BS_APCP_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_POLIZA_MULTIRIESGOS"])) { item.BS_POLIZA_MULTIRIESGOS = bdRd["BS_POLIZA_MULTIRIESGOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_INIC"])) { item.BS_MULTIRESGOS_INIC = bdRd["BS_MULTIRESGOS_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_MULTIRESGOS_FIN"])) { item.BS_MULTIRESGOS_FIN = bdRd["BS_MULTIRESGOS_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_INIC"])) { item.BS_RTV_INIC = bdRd["BS_RTV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_RTV_FIN"])) { item.BS_RTV_FIN = bdRd["BS_RTV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_INIC"])) { item.BS_REVI_ANUAL_GNV_INIC = bdRd["BS_REVI_ANUAL_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVI_ANUAL_GNV_FIN"])) { item.BS_REVI_ANUAL_GNV_FIN = bdRd["BS_REVI_ANUAL_GNV_FIN"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_INIC"])) { item.BS_REVISION_CILINDROS_GNV_INIC = bdRd["BS_REVISION_CILINDROS_GNV_INIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["BS_REVISION_CILINDROS_GNV_FIN"])) { item.BS_REVISION_CILINDROS_GNV_FIN = bdRd["BS_REVISION_CILINDROS_GNV_FIN"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["COD_CAC"])) { item.COD_CAC = bdRd["COD_CAC"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["ESTADO_VEHICULO"])) { item.ESTADO_VEHICULO = bdRd["ESTADO_VEHICULO"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["PLACA_REEMPLAZADA"])) { item.PLACA_REEMPLAZADA = bdRd["PLACA_REEMPLAZADA"].ToString(); }
                            //if (!DBNull.Value.Equals(bdRd["USUREG"])) { item.USUREG = bdRd["USUREG"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["ID_ESTADO"])) { item.ID_ESTADO = Convert.ToInt32(bdRd["ID_ESTADO"]); }

                            //resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }

        public RPTA_GENERAL Verifica_Placa_Existente(string placa)
        {

            var item = new RPTA_GENERAL();

            OracleParameter[] bdParameters = new OracleParameter[2];

            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters[1] = new OracleParameter("P_BS_PLACA", OracleDbType.Varchar2) { Value = placa };

            using (var bdCmd = new OracleCommand("PKG_BUSES.RPTA_PLACA_EXISTENTE", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {

                            if (!DBNull.Value.Equals(bdRd["AUX"])) { item.AUX = Convert.ToInt32(bdRd["AUX"]); }

                        }
                    }
                }
            }
            return item;
        }




        public CC_BUSES Buscar_Ultimo_Bus()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select BS_ID ID FROM CC_BUSES order by 1 DESC ";
            var result = SqlMapper.QueryFirst<CC_BUSES>(conn, query);
            return result;
        }


        public void Eliminar_Antiguo_Bus(int id)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "DELETE FROM CC_BUSES WHERE BS_ID=" + id + "";
            SqlMapper.Query(conn, query);

        }



        public CC_BUSES Estado_Vigencias(CC_BUSES model)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "SELECT BS_PLACA,BS_NOM_EMPRE ,BS_CVS_FEC_FIN ,BS_VEHICULOS_FIN ,BS_RTV_FIN ,BS_SOAT_FEC_FIN,BS_RC_FIN  FROM CC_BUSES WHERE BS_PLACA='" + model.BS_PLACA + "'";
            var result = SqlMapper.QueryFirst<CC_BUSES>(conn, query);
            return result;
        }

        public BUSES_DESPACHO Ultimo_Buses_Despacho()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "SELECT *FROM BUSES_DESPACHO ORDER BY 1 DESC";
            var result = SqlMapper.QueryFirst<BUSES_DESPACHO>(conn, query);
            return result;
        }

        public BUSES_DESPACHO Ultimo_Codigo_Archivo(string placa)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select  * from buses_despacho  where bs_placa='" + placa + "' ORDER BY BD_COD_DESPACHO DESC";
            var result = SqlMapper.QueryFirst<BUSES_DESPACHO>(conn, query);
            return result;
        }



        public void Insertar_Buses_Despacho(int BD_ID, int CD_ID, string BS_PLACA, string BD_ESTADO, string BD_CALIDAD, string BD_OBSERVACION, string BD_DIRECCION, int? BD_KM, string BD_CONCESIONARIO, string BD_FECHA, string USUARIO, int ESTADO_DATO, string longuitud, string latitud, int cod_despacho, string url_foto, string url_foto2, string url_foto3, string url_foto4, string estado_bus,string comentario)
        {
            var bdParameters = new OracleDynamicParameters();
            if (BD_CALIDAD == null || BD_CALIDAD == "NO TIENE") BD_CALIDAD = " ";
            if (BD_ESTADO == "NO TIENE") BD_ESTADO = " ";

            var query = "PKG_BUSES.SP_INSERTAR_FORMATO_DESPACHO";
            bdParameters.Add("BD_ID", OracleDbType.Int32, ParameterDirection.Input, BD_ID);
            bdParameters.Add("CD_ID", OracleDbType.Int32, ParameterDirection.Input, CD_ID);
            bdParameters.Add("BS_PLACA", OracleDbType.Varchar2, ParameterDirection.Input, BS_PLACA);
            bdParameters.Add("BD_ESTADO", OracleDbType.Varchar2, ParameterDirection.Input, BD_ESTADO);
            bdParameters.Add("BD_CALIDAD", OracleDbType.Varchar2, ParameterDirection.Input, BD_CALIDAD);
            bdParameters.Add("BD_OBSERVACION", OracleDbType.Varchar2, ParameterDirection.Input, BD_OBSERVACION);
            bdParameters.Add("BD_DIRECCION", OracleDbType.Varchar2, ParameterDirection.Input, BD_DIRECCION);
            bdParameters.Add("BD_KM", OracleDbType.Varchar2, ParameterDirection.Input, BD_KM);
            bdParameters.Add("BD_CONCESIONARIO", OracleDbType.Varchar2, ParameterDirection.Input, BD_CONCESIONARIO);
            bdParameters.Add("USUREG", OracleDbType.Varchar2, ParameterDirection.Input, USUARIO);
            bdParameters.Add("ESTREG", OracleDbType.Int32, ParameterDirection.Input, ESTADO_DATO);
            bdParameters.Add("BD_LATITUD", OracleDbType.Varchar2, ParameterDirection.Input, latitud);
            bdParameters.Add("BD_LONGUITUD", OracleDbType.Varchar2, ParameterDirection.Input, longuitud);
            bdParameters.Add("BD_COD_DESPACHO", OracleDbType.Int32, ParameterDirection.Input, cod_despacho);
            bdParameters.Add("BD_URL_FOTO_", OracleDbType.Varchar2, ParameterDirection.Input, url_foto);
            bdParameters.Add("BD_URL_FOTO2_", OracleDbType.Varchar2, ParameterDirection.Input, url_foto2);
            bdParameters.Add("BD_URL_FOTO3_", OracleDbType.Varchar2, ParameterDirection.Input, url_foto3);
            bdParameters.Add("BD_URL_FOTO4_", OracleDbType.Varchar2, ParameterDirection.Input, url_foto4);
            bdParameters.Add("ESTADO_BUS_", OracleDbType.Varchar2, ParameterDirection.Input, estado_bus);
            bdParameters.Add("COMENTARIO_", OracleDbType.Varchar2, ParameterDirection.Input, comentario);

            



            SqlMapper.QueryFirstOrDefault(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);

        }




        public void Editar_Buses(CC_BUSES modelo)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_BUSES.SP_EDITAR_BUSES";

            DateTime fecha_actual = DateTime.Now;
            string fecha = fecha_actual.ToString("dd/MM/yyyy");
            if (modelo.ID_ESTADO != 0) { modelo.ID_ESTADO = 1; }

            bdParameters.Add("BS_ID_", OracleDbType.Int32, ParameterDirection.Input, modelo.BS_ID);
            bdParameters.Add("BS_FECINI_PT_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_FECINI_PT);
            bdParameters.Add("BS_NOM_EMPRE_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_NOM_EMPRE);
            bdParameters.Add("BS_PLACA_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PLACA);
            bdParameters.Add("BS_PROPIETARIO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PROPIETARIO);
            bdParameters.Add("BS_PAQUETE_CONCESION_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PAQUETE_CONCESION);
            bdParameters.Add("BS_PAQUETE_SERVICIO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PAQUETE_SERVICIO);
            bdParameters.Add("BS_TIPO_SERVICIO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_TIPO_SERVICIO);
            bdParameters.Add("BS_ESTADO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_ESTADO);
            bdParameters.Add("BS_MARCA_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_MARCA);
            bdParameters.Add("BS_MODELO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_MODELO);
            bdParameters.Add("BS_AÑO_FABRICACION_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_AÑO_FABRICACION);
            bdParameters.Add("BS_COMBUSTIBLE_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_COMBUSTIBLE);
            bdParameters.Add("BS_TECNOLOGIA_EURO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_TECNOLOGIA_EURO);
            bdParameters.Add("BS_POTENCIA_MOTOR_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_POTENCIA_MOTOR);
            bdParameters.Add("BS_SERIE_MOTOR_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_SERIE_MOTOR);
            bdParameters.Add("BS_SERIE_CHASIS_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_SERIE_CHASIS);
            bdParameters.Add("BS_COLOR_VEHICULO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_COLOR_VEHICULO);
            bdParameters.Add("BS_LONGITUD_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_LONGITUD);
            bdParameters.Add("BS_ASIENTOS_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_ASIENTOS);
            bdParameters.Add("BS_AREA_PASILLO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_AREA_PASILLO);
            bdParameters.Add("BS_PESO_NETO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PESO_NETO);
            bdParameters.Add("BS_PESO_BRUTO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PESO_BRUTO);
            bdParameters.Add("BS_ALTURA_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_ALTURA);
            bdParameters.Add("BS_ANCHO_ ", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_ANCHO);
            bdParameters.Add("BS_CARGA_UTIL_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_CARGA_UTIL);
            bdParameters.Add("BS_PUERTA_IZQUIERDA_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PUERTA_IZQUIERDA);
            bdParameters.Add("BS_PARTIDA_REGISTRAL_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_PARTIDA_REGISTRAL);
            bdParameters.Add("BS_CODIGO_CVS_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_CODIGO_CVS);
            bdParameters.Add("BS_CVS_FEC_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_CVS_FEC_INIC);
            bdParameters.Add("BS_CVS_FEC_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_CVS_FEC_FIN);
            bdParameters.Add("BS_SOAT_FEC_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_SOAT_FEC_INIC);
            bdParameters.Add("BS_SOAT_FEC_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_SOAT_FEC_FIN);
            bdParameters.Add("BS_POLIZA_VEHICULOS_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_POLIZA_VEHICULOS);
            bdParameters.Add("BS_VEHICULOS_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_VEHICULOS_INIC);
            bdParameters.Add("BS_VEHICULOS_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_VEHICULOS_FIN);
            bdParameters.Add("BS_POLIZA_CIVIL_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_POLIZA_CIVIL);
            bdParameters.Add("BS_RC_INICIO_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_RC_INICIO);
            bdParameters.Add("BS_RC_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_RC_FIN);

            bdParameters.Add("BS_POLIZA_ACCI_COLECTIVOS_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_POLIZA_ACCI_COLECTIVOS);
            bdParameters.Add("BS_APCP_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_APCP_INIC);
            bdParameters.Add("BS_APCP_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_APCP_FIN);
            bdParameters.Add("BS_POLIZA_MULTIRIESGOS_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_POLIZA_MULTIRIESGOS);
            bdParameters.Add("BS_MULTIRESGOS_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_MULTIRESGOS_INIC);
            bdParameters.Add("BS_MULTIRESGOS_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_MULTIRESGOS_FIN);

            bdParameters.Add("BS_RTV_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_RTV_INIC);
            bdParameters.Add("BS_RTV_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_RTV_FIN);
            bdParameters.Add("BS_REVI_ANUAL_GNV_INIC_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_REVI_ANUAL_GNV_INIC);
            bdParameters.Add("BS_REVI_ANUAL_GNV_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_REVI_ANUAL_GNV_FIN);
            bdParameters.Add("BS_REVISION_CILINDROS_GNV_IN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_REVISION_CILINDROS_GNV_INIC);
            bdParameters.Add("BS_REVISION_CILINDROS_GNV_FIN_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.BS_REVISION_CILINDROS_GNV_FIN);

            bdParameters.Add("ESTADO_VEHICULO_", OracleDbType.Varchar2, ParameterDirection.Input, "NORMAL");
            bdParameters.Add("PLACA_REEMPLAZADA_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.PLACA_REEMPLAZADA);
            bdParameters.Add("USUREG_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.USU_REG);
            bdParameters.Add("FECHA_", OracleDbType.Varchar2, ParameterDirection.Input, fecha);
            bdParameters.Add("ESTREG_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.ID_ESTADO);
            bdParameters.Add("CORREDOR_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.ID_CORREDOR);

            var result = SqlMapper.QueryFirstOrDefault<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);

        }



        public List<BUSES_DESPACHO> Lista_Conceptos_Dentro()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "SELECT * FROM CONCEPTOS_DESPACHO WHERE CD_ID <7";
            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query).ToList();
            return result;
        }
        public List<BUSES_DESPACHO> Lista_Conceptos_Exterior()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "SELECT * FROM CONCEPTOS_DESPACHO WHERE CD_ID >6 AND CD_ID<39";
            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query).ToList();
            return result;
        }
        public List<BUSES_DESPACHO> Lista_Cabina_Vehiculo()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "SELECT * FROM CONCEPTOS_DESPACHO WHERE CD_ID>38";
            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query).ToList();
            return result;
        }


        public List<BUSES_DESPACHO> Buscar_Documentacion_Bus(string placa, int codigo_despacho)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_BUSES.SP_BUSCAR_DOCUMENTOS_BUS";

            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("BS_PLACA_", OracleDbType.Varchar2, ParameterDirection.Input, placa);
            bdParameters.Add("ESTADO_BUS_", OracleDbType.Int32, ParameterDirection.Input, codigo_despacho);


            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }

        public List<BUSES_DESPACHO> Buscar_Exterior_Bus(string placa, int codigo_despacho)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_BUSES.SP_BUSCAR_EXTERIOR_BUS";

            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("BS_PLACA_", OracleDbType.Varchar2, ParameterDirection.Input, placa);
            bdParameters.Add("ESTADO_BUS_", OracleDbType.Int32, ParameterDirection.Input, codigo_despacho);



            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }

        public List<BUSES_DESPACHO> Buscar_Cabina_Bus(string placa, int codigo_despacho)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_BUSES.SP_BUSCAR_CABINA_BUS";

            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("BS_PLACA_", OracleDbType.Varchar2, ParameterDirection.Input, placa);
            bdParameters.Add("ESTADO_BUS_", OracleDbType.Int32, ParameterDirection.Input, codigo_despacho);


            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }




        public List<BUSES_DESPACHO> Buscar_Formato_Placa(string placa, int codigo_despacho)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "SELECT DISTINCT BS_PLACA,BD_CONCESIONARIO,BD_DIRECCION,to_char(BD_FECHA, 'DD/MM/YYYY') AS BD_FECHA, to_char(BD_FECHA, 'hh:mm') AS HORA, BD_KM, USUREG,URL_FOTO URL_FOTO1,URL_FOTO2,URL_FOTO3,URL_FOTO4 FROM BUSES_DESPACHO where bs_placa ='" + placa + "' and BD_COD_DESPACHO=" + codigo_despacho + " AND ROWNUM<2";
            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query).ToList();
            return result;
        }


        //public List<CC_BUSES> Listar_Buses(CC_BUSES model)
        //{
        //    var bdParameters = new OracleDynamicParameters();

        //    var query = "";

        //    query = "select BS_ID ID,BS_FECINI_PT FECINIC_PT,BS_NOM_EMPRE NOM_EMPRE,BS_PLACA PLACA,BS_PROPIETARIO PROPIETARIO,BS_PAQUETE_CONCESION AS PAQUETE_CONCESION,BS_PAQUETE_SERVICIO PAQUETE_SERVICIO,BS_TIPO_SERVICIO TIPO_SERVICIO,BS_ESTADO ESTADO,BS_MARCA MARCA,BS_MODELO MODELO,BS_AÑO_FABRICACION AÑO_FABRICACION,BS_COMBUSTIBLE COMBUSTIBLE,BS_TECNOLOGIA_EURO TECNOLOGIA_EURO,BS_POTENCIA_MOTOR POTENCIA_MOTOR,BS_SERIE_MOTOR SERIE_MOTOR,BS_SERIE_CHASIS SERIE_CHASIS,BS_COLOR_VEHICULO COLOR_VEHICULO,BS_LONGITUD LONGUITUD,BS_ASIENTOS ASIENTOS,BS_AREA_PASILLO AREA_PASILLO,BS_PESO_NETO PESO_NETO, BS_PESO_BRUTO PESO_BRUTO,BS_ALTURA ALTURA, BS_ANCHO ANCHO,BS_CARGA_UTIL CARGA_UTIL, BS_PUERTA_IZQUIERDA PUERTA_IZQUIERDA,BS_PARTIDA_REGISTRAL PARTIDA_REGISTRAL, BS_CODIGO_CVS CODIGO_CVS,BS_CVS_FEC_INIC CVS_FEC_INIC, BS_CVS_FEC_FIN CVS_FEC_FIN,BS_SOAT_FEC_INIC SOAT_FEC_INIC, BS_SOAT_FEC_FIN SOAT_FEC_FIN,BS_POLIZA_VEHICULOS POLIZA_VEHICULOS, BS_VEHICULOS_INIC VEHICULOS_INIC,BS_VEHICULOS_FIN VEHICULOS_FIN, BS_POLIZA_CIVIL AS POLIZA_RESPONSABILIDAD_CIVIL, BS_RC_INICIO RC_INICIO,BS_RC_FIN RC_FIN, BS_POLIZA_ACCI_COLECTIVOS    POLIZA_ACCIDENTES_COLECTIVOS,BS_APCP_INIC APCP_INICIO, BS_APCP_FIN APCP_VIGENCIA,BS_POLIZA_MULTIRIESGOS POLIZA_MULTIRIESGOS, BS_MULTIRESGOS_INIC   MULTIRESGOS_INICIO,BS_MULTIRESGOS_FIN MULTIRESGOS_VIGENCIA, BS_RTV_INIC RTV_INIC,BS_RTV_FIN RTV_FIN, BS_REVI_ANUAL_GNV_INIC REVISION_ANUAL_INIC,BS_REVI_ANUAL_GNV_FIN REVISION_ANUAL_FIN, BS_REVISION_CILINDROS_GNV_INIC REVISION_CILINDRO_INIC,BS_REVISION_CILINDROS_GNV_FIN REVISION_CILINDRO_FIN,ESTADO_VEHICULO,CORREDOR from cc_buses  where   ESTREG='1' AND ESTADO_VEHICULO IN ('NORMAL')   order by 1 ASC";


        //    var result = SqlMapper.Query<CC_BUSES>(conn, query).ToList();
        //    return result;
        //}



        public List<CC_BUSES> Listar_Conductores_Afectados()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select BS_ID ID,BS_FECINI_PT FECINIC_PT,BS_NOM_EMPRE NOM_EMPRE,BS_PLACA PLACA,BS_PROPIETARIO PROPIETARIO,BS_PAQUETE_CONCESION AS PAQUETE_CONCESION,BS_PAQUETE_SERVICIO PAQUETE_SERVICIO,BS_TIPO_SERVICIO TIPO_SERVICIO,BS_ESTADO ESTADO,BS_MARCA MARCA,BS_MODELO MODELO,BS_AÑO_FABRICACION AÑO_FABRICACION,BS_COMBUSTIBLE COMBUSTIBLE,BS_TECNOLOGIA_EURO TECNOLOGIA_EURO,BS_POTENCIA_MOTOR POTENCIA_MOTOR,BS_SERIE_MOTOR SERIE_MOTOR,BS_SERIE_CHASIS SERIE_CHASIS,BS_COLOR_VEHICULO COLOR_VEHICULO,BS_LONGITUD LONGUITUD,BS_ASIENTOS ASIENTOS,BS_AREA_PASILLO AREA_PASILLO,BS_PESO_NETO PESO_NETO, BS_PESO_BRUTO PESO_BRUTO,BS_ALTURA ALTURA, BS_ANCHO ANCHO,BS_CARGA_UTIL CARGA_UTIL, BS_PUERTA_IZQUIERDA PUERTA_IZQUIERDA,BS_PARTIDA_REGISTRAL PARTIDA_REGISTRAL, BS_CODIGO_CVS CODIGO_CVS,BS_CVS_FEC_INIC CVS_FEC_INIC, BS_CVS_FEC_FIN CVS_FEC_FIN,BS_SOAT_FEC_INIC SOAT_FEC_INIC, BS_SOAT_FEC_FIN SOAT_FEC_FIN,BS_POLIZA_VEHICULOS POLIZA_VEHICULOS, BS_VEHICULOS_INIC VEHICULOS_INIC,BS_VEHICULOS_FIN VEHICULOS_FIN, BS_POLIZA_CIVIL AS POLIZA_RESPONSABILIDAD_CIVIL, BS_RC_INICIO RC_INICIO,BS_RC_FIN RC_FIN, BS_POLIZA_ACCI_COLECTIVOS    POLIZA_ACCIDENTES_COLECTIVOS,BS_APCP_INIC APCP_INICIO, BS_APCP_FIN APCP_VIGENCIA,BS_POLIZA_MULTIRIESGOS POLIZA_MULTIRIESGOS, BS_MULTIRESGOS_INIC   MULTIRESGOS_INICIO,BS_MULTIRESGOS_FIN MULTIRESGOS_VIGENCIA, BS_RTV_INIC RTV_INIC,BS_RTV_FIN RTV_FIN, BS_REVI_ANUAL_GNV_INIC REVISION_ANUAL_INIC,BS_REVI_ANUAL_GNV_FIN REVISION_ANUAL_FIN, BS_REVISION_CILINDROS_GNV_INIC REVISION_CILINDRO_INIC,BS_REVISION_CILINDROS_GNV_FIN REVISION_CILINDRO_FIN ,PLACA_REEMPLAZADA,ESTADO_VEHICULO from cc_buses where ESTADO_VEHICULO='DESAFECTAR' AND ESTREG='1' order by 1 ASC ";
            var result = SqlMapper.Query<CC_BUSES>(conn, query).ToList();
            return result;
        }

        public CC_BUSES Buscar_Placas_id(int id)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select BS_ID ID,CORREDOR, BS_FECINI_PT FECINIC_PT,BS_NOM_EMPRE NOM_EMPRE, BS_PLACA PLACA,BS_PROPIETARIO PROPIETARIO, BS_PAQUETE_CONCESION AS PAQUETE_CONCESION, BS_PAQUETE_SERVICIO PAQUETE_SERVICIO,BS_TIPO_SERVICIO TIPO_SERVICIO, BS_ESTADO ESTADO,BS_MARCA MARCA, BS_MODELO MODELO,BS_AÑO_FABRICACION AÑO_FABRICACION, BS_COMBUSTIBLE COMBUSTIBLE,BS_TECNOLOGIA_EURO TECNOLOGIA_EURO, BS_POTENCIA_MOTOR POTENCIA_MOTOR,BS_SERIE_MOTOR SERIE_MOTOR, BS_SERIE_CHASIS SERIE_CHASIS,BS_COLOR_VEHICULO COLOR_VEHICULO, BS_LONGITUD LONGUITUD,BS_ASIENTOS ASIENTOS, BS_AREA_PASILLO AREA_PASILLO,BS_PESO_NETO PESO_NETO, BS_PESO_BRUTO PESO_BRUTO,BS_ALTURA ALTURA, BS_ANCHO ANCHO,BS_CARGA_UTIL CARGA_UTIL, BS_PUERTA_IZQUIERDA PUERTA_IZQUIERDA,BS_PARTIDA_REGISTRAL PARTIDA_REGISTRAL, BS_CODIGO_CVS CODIGO_CVS,BS_CVS_FEC_INIC CVS_FEC_INIC, BS_CVS_FEC_FIN CVS_FEC_FIN,BS_SOAT_FEC_INIC SOAT_FEC_INIC, BS_SOAT_FEC_FIN SOAT_FEC_FIN,BS_POLIZA_VEHICULOS POLIZA_VEHICULOS, BS_VEHICULOS_INIC VEHICULOS_INIC,BS_VEHICULOS_FIN VEHICULOS_FIN, BS_POLIZA_CIVIL AS POLIZA_RESPONSABILIDAD_CIVIL, BS_RC_INICIO RC_INICIO,BS_RC_FIN RC_FIN, BS_POLIZA_ACCI_COLECTIVOS    POLIZA_ACCIDENTES_COLECTIVOS,BS_APCP_INIC APCP_INICIO, BS_APCP_FIN APCP_VIGENCIA,BS_POLIZA_MULTIRIESGOS POLIZA_MULTIRIESGOS, BS_MULTIRESGOS_INIC   MULTIRESGOS_INICIO,BS_MULTIRESGOS_FIN MULTIRESGOS_VIGENCIA, BS_RTV_INIC RTV_INIC,BS_RTV_FIN RTV_FIN, BS_REVI_ANUAL_GNV_INIC REVISION_ANUAL_INIC,BS_REVI_ANUAL_GNV_FIN REVISION_ANUAL_FIN, BS_REVISION_CILINDROS_GNV_INIC REVISION_CILINDRO_INIC,BS_REVISION_CILINDROS_GNV_FIN REVISION_CILINDRO_FIN,ESTADO_VEHICULO from cc_buses where BS_ID = " + id + " AND ESTREG='1' ";
            var result = SqlMapper.QueryFirst<CC_BUSES>(conn, query);
            return result;
        }





        public CC_BUSES Ultimo_placas_buses()
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_BUSES.SP_ULTIMO_REGISTRO_BUS";
            var result = SqlMapper.QueryFirst<CC_BUSES>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }


        public List<CC_CONDUCTORES> Listar_Placa()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select BS_PLACA PLACA from CC_BUSES WHERE ID_ESTADO='1' ";
            var result = SqlMapper.Query<CC_CONDUCTORES>(conn, query).ToList();
            return result;
        }

        public List<BUSES_DESPACHO> Listar_Informes_Despacho()
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select  DISTINCT bs_placa,BD_COD_DESPACHO,to_char(BD_FECHA)BD_FECHA FROM BUSES_DESPACHO";
            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query).ToList();
            return result;
        }

        public void Cambiar_Estado_Bus(string placa, int codigo_despacho)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "update buses_despacho set ESTADO_BUS='NO PROGRAMABLE' WHERE BS_PLACA='" + placa + "' and BD_COD_DESPACHO=" + codigo_despacho + "";
            var result = SqlMapper.Query(conn, query);
        }

        public void Eliminar_Bus(int id)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "update cc_buses set ESTREG='0'  where BS_ID='" + id + "'";
            var result = SqlMapper.Query(conn, query);
        }


        public void Afectar_Bus(int id, string placa_nueva)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "update cc_buses set ESTADO_VEHICULO='DESAFECTAR',PLACA_REEMPLAZADA='" + placa_nueva + "' where BS_ID='" + id + "'";
            var result = SqlMapper.Query(conn, query);
        }


        public List<BUSES_DESPACHO> FIltrar_Archivo_Despacho(string placa, string fecha, string estado)
        {

            var bdParameters = new OracleDynamicParameters();

            var query = "";

            if (placa == "" && estado == null)
            {
                query = "select  DISTINCT bs_placa,BD_COD_DESPACHO,to_char(BD_FECHA)BD_FECHA,BD_LONGUITUD,BD_LATITUD,ESTADO_BUS,USUREG FROM BUSES_DESPACHO where to_char(BD_FECHA, 'DD.MM.YYYY')='" + fecha + "'";

            }
            else if (fecha == "01.01.0001" && estado == null)
            {
                query = "select  DISTINCT bs_placa,BD_COD_DESPACHO,to_char(BD_FECHA)BD_FECHA,BD_LONGUITUD,BD_LATITUD,ESTADO_BUS,USUREG FROM BUSES_DESPACHO where BS_PLACA='" + placa + "'";

            }
            else if (fecha == "01.01.0001" && placa == "")
            {
                query = "select DISTINCT B.bs_placa,B.BD_COD_DESPACHO,to_char(B.BD_FECHA)BD_FECHA,B.BD_LONGUITUD,B.BD_LATITUD,B.ESTADO_BUS,USUREG  FROM BUSES_DESPACHO B  WHERE B.ESTADO_BUS = '" + estado + "'";
            }

            else if (fecha == "01.01.0001")
            {
                query = "select  DISTINCT B.bs_placa,B.BD_COD_DESPACHO,to_char(B.BD_FECHA)BD_FECHA,B.BD_LONGUITUD,B.BD_LATITUD,B.ESTADO_BUS,USUREG  FROM BUSES_DESPACHO  B  WHERE B.BS_PLACA='" + placa + "' AND B.ESTADO_BUS='" + estado + "'";

            }
            else if (placa == "")
            {
                query = "select  DISTINCT B.bs_placa,B.BD_COD_DESPACHO,to_char(B.BD_FECHA)BD_FECHA,B.BD_LONGUITUD,B.BD_LATITUD,B.ESTADO_BUS,USUREG FROM BUSES_DESPACHO  B  WHERE B.ESTADO_BUS='" + estado + "' AND to_char(BD_FECHA, 'DD.MM.YYYY')='" + fecha + "'";

            }
            else if (estado == null || fecha == "01.01.0001")
            {
                query = "select  DISTINCT B.bs_placa,B.BD_COD_DESPACHO,to_char(B.BD_FECHA)BD_FECHA,B.BD_LONGUITUD,B.BD_LATITUD,B.ESTADO_BUS,USUREG FROM BUSES_DESPACHO  B WHERE B.BS_PLACA='" + placa + "'";

            }

            else
            {
                query = "select  DISTINCT B.bs_placa,B.BD_COD_DESPACHO,to_char(B.BD_FECHA)BD_FECHA,B.BD_LONGUITUD,B.BD_LATITUD,B.ESTADO_BUS,USUREG FROM BUSES_DESPACHO  B  WHERE B.BS_PLACA='" + placa + "' AND B.ESTADO_BUS='" + estado + "'AND to_char(BD_FECHA, 'DD.MM.YYYY')='" + fecha + "'";

            }


            var result = SqlMapper.Query<BUSES_DESPACHO>(conn, query).ToList();
            return result;
        }




        public RPTA_GENERAL Insertar_buses_conductor(int ID, string FECINIC_PT, string NOM_EMPRE, string PLACA, string PROPIETARIO, string PAQUETE_CONCESION, string PAQUETE_SERVICIO, string TIPO_SERVICIO, string ESTADO, string MARCA, string MODELO, string AÑO_FABRICACION,
                            string COMBUSTIBLE, string TECNOLOGIA_EURO, string POTENCIA_MOTOR, string SERIE_MOTOR, string SERIE_CHASIS, string COLOR_VEHICULO, string LONGUITUD,
                            string ASIENTOS, string AREA_PASILLO, string PESO_NETO, string PESO_BRUTO, string ALTURA, string ANCHO, string CARGA_UTIL,
                            string PUERTA_IZQUIERDA, string PARTIDA_REGISTRAL, string CODIGO_CVS, string CVS_FEC_INIC, string CVS_FEC_FIN, string SOAT_FEC_INIC, string SOAT_FEC_FIN,
                            string POLIZA_VEHICULOS, string VEHICULOS_INIC, string VEHICULOS_FIN, string POLIZA_RESPONSABILIDAD_CIVIL, string RC_INICIO, string RC_FIN, string POLIZA_ACCIDENTES_COLECTIVOS, string APCP_INICIO,
                             string APCP_VIGENCIA, string POLIZA_MULTIRIESGOS, string MULTIRESGOS_INICIO, string MULTIRESGOS_VIGENCIA, string RTV_INIC, string RTV_FIN, string REVISION_ANUAL_INIC, string REVISION_ANUAL_FIN, string REVISION_CILINDRO_INIC, string REVISION_CILINDRO_FIN, string USUARIO, string ESTADO_VEHICULO, string CORREDOR)
        {
            DateTime fecha_actual = DateTime.Now;
            string fecha = fecha_actual.ToString("dd/MM/yyyy");

            RPTA_GENERAL r = new RPTA_GENERAL();

            var bdParameters = new OracleDynamicParameters();
            try
            {
                var query = "insert into CC_BUSES values(" + ID +
                                                  ",'" + FECINIC_PT +
                                                 "','" + NOM_EMPRE +
                                                 "','" + PLACA +
                                                 "','" + PROPIETARIO +
                                                 "','" + PAQUETE_CONCESION +
                                                 "','" + PAQUETE_SERVICIO +
                                                 "','" + TIPO_SERVICIO +
                                                 "','" + ESTADO +
                                                 "','" + MARCA +
                                                 "','" + MODELO +
                                                 "','" + AÑO_FABRICACION +
                                                 "','" + COMBUSTIBLE +
                                                 "','" + TECNOLOGIA_EURO +
                                                 "','" + POTENCIA_MOTOR +
                                                 "','" + SERIE_MOTOR +
                                                 "','" + SERIE_CHASIS +
                                                 "','" + COLOR_VEHICULO +
                                                 "','" + LONGUITUD +
                                                 "','" + ASIENTOS +
                                                 "','" + AREA_PASILLO +
                                                 "','" + PESO_NETO +
                                                 "','" + PESO_BRUTO +
                                                 "','" + ALTURA +
                                                 "','" + ANCHO +
                                                 "','" + CARGA_UTIL +
                                                 "','" + PUERTA_IZQUIERDA +
                                                 "','" + PARTIDA_REGISTRAL +
                                                 "','" + CODIGO_CVS +
                                                 "','" + CVS_FEC_INIC +
                                                 "','" + CVS_FEC_FIN +
                                                 "','" + SOAT_FEC_INIC +
                                                 "','" + SOAT_FEC_FIN +
                                                 "','" + POLIZA_VEHICULOS +
                                                 "','" + VEHICULOS_INIC +
                                                 "','" + VEHICULOS_FIN +
                                                 "','" + POLIZA_RESPONSABILIDAD_CIVIL +
                                                 "','" + RC_INICIO +
                                                 "','" + RC_FIN +
                                                 "','" + POLIZA_ACCIDENTES_COLECTIVOS +
                                                 "','" + APCP_INICIO +
                                                 "','" + APCP_VIGENCIA +
                                                  "','" + POLIZA_MULTIRIESGOS +
                                                 "','" + MULTIRESGOS_INICIO +
                                                 "','" + MULTIRESGOS_VIGENCIA +
                                                 "','" + RTV_INIC +
                                                 "','" + RTV_FIN +
                                                 "','" + REVISION_ANUAL_INIC +
                                                 "','" + REVISION_ANUAL_FIN +
                                                 "','" + REVISION_CILINDRO_INIC +
                                                 "','" + REVISION_CILINDRO_FIN +
                                                 "','" + ESTADO_VEHICULO + "','" +
                                                 "','" + USUARIO +
                                                 "','" + fecha +
                                                 "','1'" +
                                                 ",'" + CORREDOR + "')";
                SqlMapper.Query(conn, query);

                r.COD_ESTADO = 1;
                r.DES_ESTADO = "Se registró correctamente";
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }




        public int ActualizarDapper(CC_DISTANCIA_PARADEROS modelo)
        {
            throw new NotImplementedException();
        }

        public int BorrarDapper(CC_DISTANCIA_PARADEROS modelo)
        {
            throw new NotImplementedException();
        }

        public int InsertarDapper(CC_DISTANCIA_PARADEROS modelo)
        {
            throw new NotImplementedException();
        }

        IEnumerable<CC_DISTANCIA_PARADEROS> IRepositorio<CC_DISTANCIA_PARADEROS>.ObtenerListadoDapper()
        {
            throw new NotImplementedException();
        }

        CC_DISTANCIA_PARADEROS IRepositorio<CC_DISTANCIA_PARADEROS>.ObtenerPorCodigoDapper(int id)
        {
            throw new NotImplementedException();
        }
    }
}
