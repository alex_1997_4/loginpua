﻿using DA;
using Dapper;
using Entidades;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace AD.EntidadesAD
{
    //public class EmpresaAD : Repositorio<TM_EMPRESA>, IEmpresaRepositorio
    //{
    //    string cadenaConexion = string.Empty;
    //    //private readonly OracleConnection conn;
    //    public EmpresaAD()
    //    {
    //        this.cadenaConexion = Configuracion.GetConectionSting("sConexionLOGINPUA");
    //        //conn = new OracleConnection(Configuracion.GetConectionSting("sConexionLOGINPUA"));
    //    }

    //    public IEnumerable<TM_EMPRESA> Datos()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public TM_EMPRESA Crear(TM_EMPRESA objeto, ref string mensaje, ref int tipo)
    //    {
    //        try
    //        {
    //            OracleParameter[] bdParameters = new OracleParameter[5];
    //            bdParameters[0] = new OracleParameter("P_EMPUSE", OracleDbType.Varchar2) { Value = objeto.EMPUSE };
    //            bdParameters[1] = new OracleParameter("P_EMPPAS", OracleDbType.Varchar2) { Value = objeto.EMPPAS };
    //            bdParameters[2] = new OracleParameter("P_USUREG", OracleDbType.Varchar2) { Value = SessionHelper.user_login };
    //            bdParameters[3] = new OracleParameter("P_EMPEMP", OracleDbType.Int32, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_EMPRESA.SP_INSERTAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    bdCmd.ExecuteNonQuery();
    //                    objeto.EMPEMP = int.Parse(bdCmd.Parameters["P_EMPEMP"].Value.ToString());
    //                    tipo = 1;
    //                    mensaje = "Registro creado";
    //                    return objeto;
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            return null;
    //        }
    //    }

    //    public TM_EMPRESA Modificar(TM_EMPRESA objeto, ref string mensaje, ref int tipo)
    //    {
    //        try
    //        {
    //            OracleParameter[] bdParameters = new OracleParameter[5];
    //            bdParameters[0] = new OracleParameter("P_EMPIDE", OracleDbType.Int32) { Value = objeto.EMPIDE };
    //            bdParameters[1] = new OracleParameter("P_EMPEMP", OracleDbType.Int32) { Value = objeto.EMPEMP };
    //            bdParameters[2] = new OracleParameter("P_EMPUSE", OracleDbType.Varchar2) { Value = objeto.EMPUSE };
    //            bdParameters[3] = new OracleParameter("P_EMPPAS", OracleDbType.Varchar2) { Value = objeto.EMPPAS };
    //            bdParameters[4] = new OracleParameter("P_USUMOD", OracleDbType.Varchar2) { Value = SessionHelper.user_login };

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_EMPRESA.SP_MODIFICAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    bdCmd.ExecuteNonQuery();
    //                    tipo = 1;
    //                    mensaje = "Registro modificado";
    //                    return objeto;
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            return null;
    //        }
    //    }

    //    public void Eliminar(TM_EMPRESA objeto, ref string mensaje, ref int tipo)
    //    {
    //        try
    //        {
    //            OracleParameter[] bdParameters = new OracleParameter[2];
    //            bdParameters[0] = new OracleParameter("P_EMPIDE", OracleDbType.Int32) { Value = objeto.EMPIDE };
    //            bdParameters[0] = new OracleParameter("P_EMPEMP", OracleDbType.Int32) { Value = objeto.EMPEMP };
    //            bdParameters[1] = new OracleParameter("P_USUMOD", OracleDbType.Varchar2) { Value = SessionHelper.user_login };

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_EMPRESA.SP_ELIMINAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    bdCmd.ExecuteNonQuery();
    //                    tipo = 1;
    //                    mensaje = "Registro eliminado";
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //        }
    //    }

    //    public TM_EMPRESA Datos(TM_EMPRESA objeto)
    //    {
    //        try
    //        {
    //            TM_EMPRESA resultado;
    //            OracleParameter[] bdParameters = new OracleParameter[3];
    //            bdParameters[0] = new OracleParameter("P_EMPIDE", OracleDbType.Int32) { Value = objeto.EMPIDE };
    //            bdParameters[1] = new OracleParameter("P_EMPEMP", OracleDbType.Int32) { Value = objeto.EMPEMP };
    //            bdParameters[2] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_EMPRESA.SP_DATOS", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
    //                    {
    //                        if (bdRd.HasRows)
    //                        {
    //                            bdRd.Read();
    //                            resultado = new TM_EMPRESA();
    //                            if (!DBNull.Value.Equals(bdRd["EMPIDE"])) { resultado.EMPIDE = Convert.ToInt32(bdRd["EMPIDE"]); }
    //                            if (!DBNull.Value.Equals(bdRd["EMPEMP"])) { resultado.EMPEMP = Convert.ToInt32(bdRd["EMPEMP"]); }
    //                            if (!DBNull.Value.Equals(bdRd["EMPUSE"])) { resultado.EMPUSE = Convert.ToString(bdRd["EMPUSE"]); }
    //                            if (!DBNull.Value.Equals(bdRd["EMPPAS"])) { resultado.EMPPAS = Convert.ToString(bdRd["EMPPAS"]); }
    //                            return resultado;
    //                        }
    //                    }
    //                }
    //            }
    //            return null;
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }
    //    /*public TM_EMPRESA Datos(TM_EMPRESA model)
    //    {
    //        var bdParameters = new OracleDynamicParameters();
    //        bdParameters.Add("EMPIDE", OracleDbType.Int32, ParameterDirection.Input, 1);
    //        bdParameters.Add("EMPEMP", OracleDbType.Int32, ParameterDirection.Input, 2);
    //        bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
    //        var query = "PKG_EMPRESA.SP_DATOS";
    //        var result = SqlMapper.QueryFirstOrDefault<TM_EMPRESA>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
    //        return result;
    //    }*/



    //    public List<TM_EMPRESA> Buscar(TM_EMPRESA objeto, ref string mensaje, ref int tipo, string orden = null)
    //    {
    //        try
    //        {
    //            List<TM_EMPRESA> resultado = new List<TM_EMPRESA>();
    //            OracleParameter[] bdParameters = new OracleParameter[5];
    //            bdParameters[0] = new OracleParameter("P_EMPIDE", OracleDbType.Int32) { Value = objeto.EMPIDE };
    //            bdParameters[1] = new OracleParameter("P_EMPEMP", OracleDbType.Int32) { Value = objeto.EMPEMP };
    //            bdParameters[2] = new OracleParameter("P_EMPUSE", OracleDbType.Varchar2) { Value = objeto.EMPUSE };
    //            bdParameters[3] = new OracleParameter("P_EMPPAS", OracleDbType.Varchar2) { Value = objeto.EMPPAS };
    //            bdParameters[4] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_EMPRESA.SP_BUSCAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
    //                    {
    //                        if (bdRd.HasRows)
    //                        {
    //                            while (bdRd.Read())
    //                            {
    //                                var item = new TM_EMPRESA();
    //                                if (!DBNull.Value.Equals(bdRd["EMPIDE"])) { item.EMPIDE = Convert.ToInt32(bdRd["EMPIDE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["EMPEMP"])) { item.EMPEMP = Convert.ToInt32(bdRd["EMPEMP"]); }
    //                                if (!DBNull.Value.Equals(bdRd["EMPUSE"])) { item.EMPUSE = Convert.ToString(bdRd["EMPUSE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["EMPPAS"])) { item.EMPPAS = Convert.ToString(bdRd["EMPPAS"]); }
    //                                resultado.Add(item);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            mensaje = "Lista obtenida";
    //            tipo = 1;
    //            return resultado;
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            return null;
    //        }
    //    }

    //    public List<TM_EMPRESA> BuscarPag(TM_EMPRESA objeto, string orden, out int totalReg, out int totalPag, ref string mensaje, ref int tipo, int pagina = 1, int registros = 50)
    //    {
    //        try
    //        {
    //            List<TM_EMPRESA> resultado = new List<TM_EMPRESA>();
    //            totalPag = 1;
    //            totalReg = 0;
    //            OracleParameter[] bdParameters = new OracleParameter[10];
    //            bdParameters[0] = new OracleParameter("P_EMPIDE", OracleDbType.Int32) { Value = objeto.EMPIDE };
    //            bdParameters[1] = new OracleParameter("P_EMPEMP", OracleDbType.Int32) { Value = objeto.EMPEMP };
    //            bdParameters[2] = new OracleParameter("P_EMPUSE", OracleDbType.Varchar2) { Value = objeto.EMPUSE };
    //            bdParameters[3] = new OracleParameter("P_EMPPAS", OracleDbType.Varchar2) { Value = objeto.EMPPAS };
    //            bdParameters[4] = new OracleParameter("P_ORDEN", OracleDbType.Varchar2) { Value = orden };
    //            bdParameters[5] = new OracleParameter("P_NUMPAG", OracleDbType.Int32) { Value = pagina };
    //            bdParameters[6] = new OracleParameter("P_NUMREG", OracleDbType.Int32) { Value = registros };
    //            bdParameters[7] = new OracleParameter("P_TOTPAG", OracleDbType.Int32, direction: ParameterDirection.Output);
    //            bdParameters[8] = new OracleParameter("P_TOTREG", OracleDbType.Int32, direction: ParameterDirection.Output);
    //            bdParameters[9] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_EMPRESA.SP_BUSCAR_PAG", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
    //                    {
    //                        if (bdRd.HasRows)
    //                        {
    //                            totalPag = Convert.ToInt32(bdCmd.Parameters["P_TOTPAG"].Value.ToString());
    //                            totalReg = Convert.ToInt32(bdCmd.Parameters["P_TOTREG"].Value.ToString());
    //                            while (bdRd.Read())
    //                            {
    //                                var item = new TM_EMPRESA();
    //                                if (!DBNull.Value.Equals(bdRd["EMPIDE"])) { item.EMPIDE = Convert.ToInt32(bdRd["EMPIDE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["EMPEMP"])) { item.EMPEMP = Convert.ToInt32(bdRd["EMPEMP"]); }
    //                                if (!DBNull.Value.Equals(bdRd["EMPUSE"])) { item.EMPUSE = Convert.ToString(bdRd["EMPUSE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["EMPPAS"])) { item.EMPPAS = Convert.ToString(bdRd["EMPPAS"]); }
    //                                resultado.Add(item);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            mensaje = "Lista obtenida";
    //            tipo = 1;
    //            return resultado;
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            totalPag = 1;
    //            totalReg = 0;
    //            return null;
    //        }
    //    }

    //    public TM_EMPRESA Insertar(TM_EMPRESA model)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public dynamic Modificar(TM_EMPRESA model)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Eliminar(int id)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
    public class EmpresaAD : Repositorio<TB_EMPRESA>, InterfaceAD<TB_EMPRESA>
    {
        private readonly OracleConnection conn;
        public EmpresaAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public IEnumerable<TB_EMPRESA> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_EMPRESA.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TB_EMPRESA>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TB_EMPRESA Insertar(TB_EMPRESA model)
        {
            var param = new
            {
                P_EMPCOD = model.EMPCOD,
                P_EMPNOM = model.EMPNOM,
                P_EMPPAG = model.EMPPAG,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_USUMOD = model.USUMOD,
                P_FECMOD = model.FECMOD,
                P_ESTREG = model.ESTREG,
            };
            var packages = "PKG_EMPRESA.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TB_EMPRESA>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TB_EMPRESA model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_EMPCOD", OracleDbType.Int32, ParameterDirection.Input, model.EMPCOD);
            bdParameters.Add("P_EMPNOM", OracleDbType.Varchar2, ParameterDirection.Input, model.EMPNOM);
            bdParameters.Add("P_EMPPAG", OracleDbType.Varchar2, ParameterDirection.Input, model.EMPPAG);
            bdParameters.Add("P_USUREG", OracleDbType.Varchar2, ParameterDirection.Input, model.USUREG);
            bdParameters.Add("P_FECREG", OracleDbType.Date, ParameterDirection.Input, model.FECREG);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, model.ESTREG);
            var packages = "PKG_EMPRESA.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TB_EMPRESA ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }
}