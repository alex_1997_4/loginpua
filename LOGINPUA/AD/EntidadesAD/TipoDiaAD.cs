﻿using DA;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System.Data;

namespace AD.EntidadesAD
{
    public class TipoDiaAD
    {
        private readonly OracleConnection conn;

        public TipoDiaAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }
        public List<CC_TIPO_DIA> getTipoDias() //cabecera de la programación
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_PROGRAMACION_BUSES.GET_TIPO_DIAS";
            var result = SqlMapper.Query<CC_TIPO_DIA>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }
    }
}
