﻿using DA;
using Dapper;
using ENTIDADES;
using ENTIDADES.InnerJoin;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD.EntidadesAD
{
    public class UsuariosEmpresaAD : Repositorio<TM_USUARIOS_EMPRESA>, InterfaceAD<TM_USUARIOS_EMPRESA>
    {
        private readonly OracleConnection conn;
        public UsuariosEmpresaAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public IEnumerable<TM_USUARIOS_EMPRESA> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_USUARIOS_EMPRESA.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TM_USUARIOS_EMPRESA>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TM_USUARIOS_EMPRESA Insertar(TM_USUARIOS_EMPRESA model)
        {
            var param = new
            {
                P_USUEMPCOD = model.USUEMPCOD,
                P_USUEMPUSU = model.USUEMPUSU,
                P_USUEMPCON = model.USUEMPCON,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_ESTREG = model.ESTREG,
                P_EMPCOD = model.EMPCOD,
                P_LOGCOD = model.LOGCOD
            };
            var packages = "PKG_USUARIOS_EMPRESA.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TM_USUARIOS_EMPRESA>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TM_USUARIOS_EMPRESA model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_USUEMPCOD", OracleDbType.Int32, ParameterDirection.Input, model.USUEMPCOD);
            bdParameters.Add("P_USUEMPUSU", OracleDbType.Varchar2, ParameterDirection.Input, model.USUEMPUSU);
            bdParameters.Add("P_USUEMPCON", OracleDbType.Varchar2, ParameterDirection.Input, model.USUEMPCON);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_EMPCOD", OracleDbType.Int32, ParameterDirection.Input, model.EMPCOD);
            bdParameters.Add("P_LOGCOD", OracleDbType.Int32, ParameterDirection.Input, model.LOGCOD);
            var packages = "PKG_USUARIOS_EMPRESA.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TM_USUARIOS_EMPRESA ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }

        public TM_USUARIOS_EMPRESA ObtenerPorCodigo(int id,int empresacodigo)
        {
            var query = "select * from TM_USUARIOS_EMPRESA WHERE LOGCOD= "+id+" AND EMPCOD="+empresacodigo;
            var result = conn.QueryFirstOrDefault<TM_USUARIOS_EMPRESA>(query);
            return result;
        }

        public IEnumerable<InnerJoinUsuariosAsignados> ListaUsuariosPorAsignacion(int activo, int codigo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_LOGCOD", OracleDbType.Int32, ParameterDirection.Input, codigo);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_USUARIOS_EMPRESA.SP_DATOS_USUARIO_ASIGNADO";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<InnerJoinUsuariosAsignados>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public List<InnerJoinValidacion> Validacion(TM_USUARIOS_EMPRESA model)
        {
            //var query = "select * from TM_USUARIOS_EMPRESA WHERE USUEMPUSU = '"+model.USUEMPUSU+ "' AND EMPCOD = " + model.EMPCOD + "";
            var query = "select LO.LOGUSU, UE.USUEMPUSU, US.USUNOM from TM_USUARIOS_EMPRESA UE " +
                        "INNER JOIN TB_LOGIN LO ON UE.LOGCOD = LO.LOGCOD "+
                        "INNER JOIN TB_USUARIO US ON LO.USUCOD = US.USUCOD " +
                        "WHERE UE.USUEMPUSU = '" + model.USUEMPUSU + "' AND UE.EMPCOD = "+ model.EMPCOD +"";
            var result = conn.Query<InnerJoinValidacion>(query).ToList();
            return result;
        }
    }
}