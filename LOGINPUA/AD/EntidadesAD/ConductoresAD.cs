﻿using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;
using ENTIDADES;

namespace AD.EntidadesAD
{
    public class ConductoresAD : Repositorio<CC_CONDUCTORES>
    {
        private readonly OracleConnection conn;
        public ConductoresAD(ref Object _bdConn)
        {
            _bdConn = Conexion.iniciar(ref conn, _bdConn);
        }


        public List<CC_CONDUCTORES> Listar_Conductores()
        {
            List<CC_CONDUCTORES> resultado = new List<CC_CONDUCTORES>();

            OracleParameter[] bdParameters = new OracleParameter[1];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            using (var bdCmd = new OracleCommand("PKG_CONDUCTORES.SP_LISTAR_CONDUCTORES", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_CONDUCTORES();
                            if (!DBNull.Value.Equals(bdRd["CODIGO"])) { item.CODIGO = Convert.ToInt32(bdRd["CODIGO"]); }
                            if (!DBNull.Value.Equals(bdRd["EMPRESA"])) { item.EMPRESA = bdRd["EMPRESA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["APELLIDOS"])) { item.APELLIDOS = bdRd["APELLIDOS"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["NOMBRES"])) { item.NOMBRES = bdRd["NOMBRES"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CONTIPLIC"])) { item.CONTIPLIC = bdRd["CONTIPLIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CONNUMLIC"])) { item.CONNUMLIC = bdRd["CONNUMLIC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CONFECNAC"])) { item.CONFECNAC = bdRd["CONFECNAC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["NUMDOC"])) { item.NUMDOC = bdRd["NUMDOC"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["INICIO"])) { item.INICIO = bdRd["INICIO"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["VIGENCIA"])) { item.VIGENCIA = bdRd["VIGENCIA"].ToString(); }

                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }





        //public void Insertar_ruta(int ID, string TIPO_DIA, string RUTA, string OPERADOR, string RECORDID, string ROUTE,
        //                    string BLK, string POG, string POT, string FNODE, string FTIME, string TTIME, string TNODE
        //                   , string PIG, string PIT, string LAY, string TRIP_TIME, string DISTANCIA, string ACUMULADO
        //                   , string PATS, string SENTIDO, string OBSERVACIONES, string PLACA, string PAQUETE)
        //{
        //    var bdParameters = new OracleDynamicParameters();
        //    var query = "insert into PROGRAMACION_RUTA values('" + ID + "'" +
        //                                         ",'" + TIPO_DIA +
        //                                         "','" + RUTA +
        //                                         "','" + OPERADOR +
        //                                         "','" + RECORDID +
        //                                         "','" + ROUTE +
        //                                         "','" + BLK +
        //                                         "','" + POG +
        //                                         "','" + POT +
        //                                         "','" + FNODE +
        //                                         "','" + FTIME +
        //                                         "','" + TTIME +
        //                                         "','" + TNODE +
        //                                         "','" + PIG +
        //                                         "','" + PIT +
        //                                         "','" + LAY +
        //                                         "','" + TRIP_TIME +
        //                                         "','" + DISTANCIA +
        //                                         "','" + ACUMULADO +
        //                                         "','" + PATS +
        //                                         "','" + SENTIDO +
        //                                         "','" + OBSERVACIONES +
        //                                         "','" + PLACA +
        //                                         "','" + PAQUETE + "')";
        //    SqlMapper.Query(conn, query);
        //}

        //public void Insertar_ruta_conductor(int ID_PLACAS_CONDUCTORES, string ITEM, string FECHA, string RUTA, string SERVICIO, string RAZON_SOCIAL, string PAQUETE, string PLACA, string TURNO, string PUNTO_INICIO, string HORA_PRESENTACION,
        //                           string HORA_SALE_PATIO, string HORA_CABECERA, string HORA_TRABAJO, string VIAJES, string CONDUCTOR, int DNI, int CODIGO_CAC)
        //{
        //    var bdParameters = new OracleDynamicParameters();
        //    var query = "insert into PLACAS_CONDUCTORES values(" + ID_PLACAS_CONDUCTORES +
        //                                         ",'" + ITEM +
        //                                         "','" + FECHA +
        //                                         "','" + RUTA +
        //                                         "','" + SERVICIO +
        //                                         "','" + RAZON_SOCIAL +
        //                                         "','" + PAQUETE +
        //                                         "','" + PLACA +
        //                                         "','" + TURNO +
        //                                         "','" + PUNTO_INICIO +
        //                                         "','" + HORA_PRESENTACION +
        //                                         "','" + HORA_SALE_PATIO +
        //                                         "','" + HORA_CABECERA +
        //                                         "','" + HORA_TRABAJO +
        //                                         "','" + VIAJES +
        //                                        "','" + CONDUCTOR +
        //                                         "'," + DNI +
        //                                         "," + CODIGO_CAC + ")";
        //    SqlMapper.Query(conn, query);
        //}


        //public CC_PROGRAMA_RUTA Ultimo_Registro_Rutas()
        //{
        //    var bdParameters = new OracleDynamicParameters();
        //    //string codigo = Encriptador.Encriptar(model.LOGPAS);  
        //    bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
        //    var query = "PKG_CONDUCTORES.SP_ULTIMO_REGISTRO_RUTA";
        //    var result = SqlMapper.QueryFirst<CC_PROGRAMA_RUTA>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
        //    return result;
        //}    


        //public CC_RUTAS_CONDUCTORES Ultimo_placas_conductores()
        //{
        //    var bdParameters = new OracleDynamicParameters();
        //    //string codigo = Encriptador.Encriptar(model.LOGPAS);  
        //    bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
        //    var query = "PKG_CONDUCTORES.SP_ULTIMO_PLACAS_CONDUCTORES";
        //    var result = SqlMapper.QueryFirst<CC_RUTAS_CONDUCTORES>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
        //    return result;
        //}

        //public List<CC_CONDUCTORES> Buscar_Conductores(CC_CONDUCTORES modelo)
        //{
        //    var bdParameters = new OracleDynamicParameters();
        //    //string codigo = Encriptador.Encriptar(model.LOGPAS);  
        //    bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
        //    var query = "PKG_CONDUCTORES.SP_BUSCAR_PLACA";
        //    bdParameters.Add("PLACA_", OracleDbType.Varchar2, ParameterDirection.Input, modelo.PLACA);
        //    var result = SqlMapper.Query<CC_CONDUCTORES>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
        //    return result;
        //}


        //public List<CC_CONDUCTORES> Listar_Placa()
        //{
        //    var bdParameters = new OracleDynamicParameters();
        //    var query = "select BS_PLACA PLACA from CC_BUSES";      
        //    var result = SqlMapper.Query<CC_CONDUCTORES>(conn, query).ToList();
        //    return result;
        //}


        //public int ActualizarDapper(CC_DISTANCIA_PARADEROS modelo)
        //{
        //    throw new NotImplementedException();
        //}

        //public int BorrarDapper(CC_DISTANCIA_PARADEROS modelo)
        //{
        //    throw new NotImplementedException();
        //}

        //public int InsertarDapper(CC_DISTANCIA_PARADEROS modelo)
        //{
        //    throw new NotImplementedException();
        //}

        //IEnumerable<CC_DISTANCIA_PARADEROS> IRepositorio<CC_DISTANCIA_PARADEROS>.ObtenerListadoDapper()
        //{
        //    throw new NotImplementedException();
        //}

        //CC_DISTANCIA_PARADEROS IRepositorio<CC_DISTANCIA_PARADEROS>.ObtenerPorCodigoDapper(int id)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
