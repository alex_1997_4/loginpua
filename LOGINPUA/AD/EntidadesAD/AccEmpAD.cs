﻿using DA;
using Dapper;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD.EntidadesAD
{
    public class AccEmpAD : Repositorio<TV_ACC_EMP>, InterfaceAD<TV_ACC_EMP>
    {
        private readonly OracleConnection conn;
        public AccEmpAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }
        public IEnumerable<TV_ACC_EMP> Datos(int activo)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<TV_ACC_EMP> Datos()
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_ACC_EMP.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TV_ACC_EMP>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TV_ACC_EMP Insertar(TV_ACC_EMP model)
        {
            var param = new
            {
                P_ACCEMPCOD = model.ACCEMPCOD,
                P_ACCCOD = model.ACCCOD,
                P_EMPCOD = model.EMPCOD,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_ESTREG = model.ESTREG
            };
            var packages = "PKG_ACC_EMP.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TV_ACC_EMP>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TV_ACC_EMP model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ACCEMPCOD", OracleDbType.Int32, ParameterDirection.Input, model.ACCEMPCOD);
            bdParameters.Add("P_ACCCOD", OracleDbType.Int32, ParameterDirection.Input, model.ACCCOD);
            bdParameters.Add("P_EMPCOD", OracleDbType.Int32, ParameterDirection.Input, model.EMPCOD);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, model.ESTREG);
            var packages = "PKG_ACC_EMP.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }
        public IEnumerable<TV_ACC_EMP> ObtenerListaPorId(string id)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select * from "+
                        "TV_LOG_ACC LA "+
                        "INNER JOIN TM_ACCESO AC ON LA.ACCCOD = AC.ACCCOD "+
                        "INNER JOIN TV_ACC_EMP AE ON AC.ACCCOD = AE.ACCCOD "+
                        "INNER JOIN TB_EMPRESA EM ON AE.EMPCOD = EM.EMPCOD "+
                        "WHERE LA.LOGCOD = "+ id;
            var result = SqlMapper.Query<TV_ACC_EMP>(conn, query);
            return result;
        }
        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TV_ACC_EMP ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }
}