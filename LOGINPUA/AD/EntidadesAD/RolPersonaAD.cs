﻿

using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;
using ENTIDADES;

namespace AD.EntidadesAD
{
    public class RolPersonaAD
    {
        private readonly OracleConnection conn;
        public RolPersonaAD(ref Object _bdConn)
        {
            _bdConn = Conexion.iniciar(ref conn, _bdConn);
            //conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public RPTA_GENERAL AgregarRol(string nombre, string usuario)
        {
            DateTime dateTime = DateTime.UtcNow.Date;
            string fecha_actual = dateTime.ToString("dd/MM/yyyy");
            int id_perfil = 0;
            RPTA_GENERAL r = new RPTA_GENERAL();
            OracleParameter[] bdParameters = new OracleParameter[3];
            bdParameters[0] = new OracleParameter("P_NOMBRE", OracleDbType.Varchar2) { Value = nombre };
            bdParameters[1] = new OracleParameter("P_USU_REG", OracleDbType.Varchar2) { Value = usuario };
            bdParameters[2] = new OracleParameter("P_FECHA_REG", OracleDbType.Varchar2) { Value = fecha_actual };
            

            try
            {
                using (var bdCmd = new OracleCommand("PKG_ROL.registrarRol", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se registró correctamente";
                }
            }
            catch (Exception ex)
            {
                r.AUX = 0;
                r.COD_ESTADO = 0;
                r.DES_ESTADO = "Ya existe un perfil con ese nombre";
            }
            return r;
        }


        public List<CC_ROL> ListarRol()
        {
            List<CC_ROL> resultado = new List<CC_ROL>();

            OracleParameter[] bdParameters = new OracleParameter[1];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            

            using (var bdCmd = new OracleCommand("PKG_ROL.listarRol", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_ROL();
                            if (!DBNull.Value.Equals(bdRd["ID_ROLPERSONA"])) { item.ID_ROLPERSONA = Convert.ToInt32(bdRd["ID_ROLPERSONA"]); }
                            if (!DBNull.Value.Equals(bdRd["NOMBRE"])) { item.NOMBRE = bdRd["NOMBRE"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["USU_REG"])) { item.USU_REG = bdRd["USU_REG"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["FECHA_REG"])) { item.FECHA_REG = bdRd["FECHA_REG"].ToString(); }

                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }

        public RPTA_GENERAL AnularRol(int idrol, string session_usuario)
        {
            DateTime dateTime = DateTime.UtcNow.Date;
            string fecha_actual = dateTime.ToString("dd/MM/yyyy");
            RPTA_GENERAL r = new RPTA_GENERAL();
            OracleParameter[] bdParameters = new OracleParameter[3];
            bdParameters[0] = new OracleParameter("P_ID_ROLPERSONA", OracleDbType.Int32) { Value = idrol };
            bdParameters[1] = new OracleParameter("P_USU_ANULA", OracleDbType.Varchar2) { Value = session_usuario };
            bdParameters[2] = new OracleParameter("P_FECHA_ANULA", OracleDbType.Varchar2) { Value = fecha_actual };

            try
            {
                using (var bdCmd = new OracleCommand("PKG_ROL.anularRol", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se elimino correctamente";
                }
            }
            catch (Exception ex)
            {
                r.AUX = 0;
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }

        public RPTA_GENERAL ModificarRol(int idrol, string nombre, string session_usuario)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();

            OracleParameter[] bdParameters = new OracleParameter[4];
            //bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var fecha_actual = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff");

            bdParameters[0] = new OracleParameter("P_ID_ROLPERSONA", OracleDbType.Int32) { Value = idrol };
            bdParameters[1] = new OracleParameter("P_NOMBRE", OracleDbType.Varchar2) { Value = nombre };
            bdParameters[2] = new OracleParameter("P_FECHA_MODIF", OracleDbType.Varchar2) { Value = fecha_actual };
            bdParameters[3] = new OracleParameter("P_USU_MODIF", OracleDbType.Varchar2) { Value = session_usuario };

            try
            {
                using (var bdCmd = new OracleCommand("PKG_ROL.modificarRol", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    //BS_ID = int.Parse(bdCmd.Parameters["BS_ID"].Value.ToString());
                    //r.AUX = BS_ID;
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se modificó correctamente";
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }




        //public List<CC_MODALIDAD> ListarModalidad()
        //{
        //    List<CC_MODALIDAD> resultado = new List<CC_MODALIDAD>();

        //    OracleParameter[] bdParameters = new OracleParameter[1];
        //    bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

        //    using (var bdCmd = new OracleCommand("PKG_TBUSUARIO.listarModalidad", conn))
        //    {
        //        bdCmd.CommandType = CommandType.StoredProcedure;
        //        bdCmd.Parameters.AddRange(bdParameters);
        //        using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
        //        {
        //            if (bdRd.HasRows)
        //            {
        //                while (bdRd.Read())
        //                {
        //                    var item = new CC_MODALIDAD();

        //                    if (!DBNull.Value.Equals(bdRd["ID_MODALIDAD_TRANS"])) { item.ID_MODALIDAD_TRANS = Convert.ToInt32(bdRd["ID_MODALIDAD_TRANS"]); }
        //                    if (!DBNull.Value.Equals(bdRd["NOMBRE"])) { item.NOMBRE = bdRd["NOMBRE"].ToString(); }
        //                    resultado.Add(item);
        //                }
        //            }
        //        }
        //    }
        //    return resultado;
        //}
        //public List<CC_PERSONA> ListarPersona()
        //{
        //    List<CC_PERSONA> resultado = new List<CC_PERSONA>();

        //    OracleParameter[] bdParameters = new OracleParameter[1];
        //    bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

        //    using (var bdCmd = new OracleCommand("PKG_TBUSUARIO.listarPersona", conn))
        //    {
        //        bdCmd.CommandType = CommandType.StoredProcedure;
        //        bdCmd.Parameters.AddRange(bdParameters);
        //        using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
        //        {
        //            if (bdRd.HasRows)
        //            {
        //                while (bdRd.Read())
        //                {
        //                    var item = new CC_PERSONA();

        //                    if (!DBNull.Value.Equals(bdRd["ID_PERSONA"])) { item.ID_PERSONA = Convert.ToInt32(bdRd["ID_PERSONA"]); }
        //                    if (!DBNull.Value.Equals(bdRd["NOMBRES"])) { item.NOMBRES = bdRd["NOMBRES"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["APEPAT"])) { item.APEPAT = bdRd["APEPAT"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["APEMAT"])) { item.APEMAT = bdRd["APEMAT"].ToString(); }
        //                    resultado.Add(item);
        //                }
        //            }
        //        }
        //    }
        //    return resultado;

        //}

        //public List<CC_USUARIO_PERSONA> ListarUsuarios()
        //{
        //    List<CC_USUARIO_PERSONA> resultado = new List<CC_USUARIO_PERSONA>();

        //    OracleParameter[] bdParameters = new OracleParameter[1];
        //    bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

        //    using (var bdCmd = new OracleCommand("PKG_TBUSUARIO.listarUsuario", conn))
        //    {
        //        bdCmd.CommandType = CommandType.StoredProcedure;
        //        bdCmd.Parameters.AddRange(bdParameters);
        //        using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
        //        {
        //            if (bdRd.HasRows)
        //            {
        //                while (bdRd.Read())
        //                {
        //                    var item = new CC_USUARIO_PERSONA();

        //                    if (!DBNull.Value.Equals(bdRd["USUARIO"])) { item.USUARIO = bdRd["USUARIO"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["CLAVE"])) { item.CLAVE = bdRd["CLAVE"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["MODALIDAD"])) { item.MODALIDAD = bdRd["MODALIDAD"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["PERFIL"])) { item.PERFIL = bdRd["PERFIL"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["NOMBRES"])) { item.NOMBRE = bdRd["NOMBRES"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["APEPAT"])) { item.APEPAT = bdRd["APEPAT"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["APEMAT"])) { item.APEMAT = bdRd["APEMAT"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["USU_REG"])) { item.USU_REG = bdRd["USU_REG"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["FECHA_REG"])) { item.FECHA_REG = bdRd["FECHA_REG"].ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["ID_USUARIO"])) { item.ID_USUARIO = Convert.ToInt32(bdRd["ID_USUARIO"]); }
        //                    if (!DBNull.Value.Equals(bdRd["ID_ESTADO"])) { item.ID_ESTADO = (bdRd["ID_ESTADO"]).ToString(); }
        //                    if (!DBNull.Value.Equals(bdRd["ID_MODALIDAD_TRANS"])) { item.ID_MODALIDAD_TRANS = Convert.ToInt32(bdRd["ID_MODALIDAD_TRANS"]); }
        //                    if (!DBNull.Value.Equals(bdRd["ID_PERFIL"])) { item.ID_PERFIL = Convert.ToInt32(bdRd["ID_PERFIL"]); }



        //                    resultado.Add(item);
        //                }
        //            }
        //        }
        //    }
        //    return resultado;

        //}


        //public RPTA_GENERAL ModificarPerfil(int idmodalidad, int id_modalidad_perfil, string nombre_perfil, string session_usuario)
        //{
        //    DateTime dateTime = DateTime.UtcNow.Date;
        //    string fecha_actual = dateTime.ToString("dd/MM/yyyy");
        //    RPTA_GENERAL r = new RPTA_GENERAL();
        //    OracleParameter[] bdParameters = new OracleParameter[5];
        //    bdParameters[0] = new OracleParameter("P_ID_PERFIL_MODALIDAD", OracleDbType.Int32) { Value = id_modalidad_perfil };
        //    bdParameters[1] = new OracleParameter("P_ID_MODALIDAD_TRANS", OracleDbType.Int32) { Value = idmodalidad };
        //    bdParameters[2] = new OracleParameter("P_NOMBRE", OracleDbType.Varchar2) { Value = nombre_perfil };
        //    bdParameters[3] = new OracleParameter("P_FECHA_MODIF", OracleDbType.Varchar2) { Value = fecha_actual };
        //    bdParameters[4] = new OracleParameter("P_USU_MODIF", OracleDbType.Varchar2) { Value = session_usuario };


        //    try
        //    {
        //        using (var bdCmd = new OracleCommand("PKG_PERFIL.modificarPerfil", conn))
        //        {
        //            bdCmd.CommandType = CommandType.StoredProcedure;
        //            bdCmd.Parameters.AddRange(bdParameters);
        //            bdCmd.ExecuteNonQuery();
        //            r.COD_ESTADO = 1;
        //            r.DES_ESTADO = "Se modifico correctamente";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        r.AUX = 0;
        //        r.COD_ESTADO = 0;
        //        r.DES_ESTADO = ex.Message;
        //    }
        //    return r;
        //}


        //public List<CC_PERFIL_USUARIO> Validar_Perfil(int idperfil)
        //{
        //    List<CC_PERFIL_USUARIO> resultado = new List<CC_PERFIL_USUARIO>();

        //    OracleParameter[] bdParameters = new OracleParameter[2];
        //    bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
        //    bdParameters[1] = new OracleParameter("P_ID_PERFIL", OracleDbType.Int32) { Value = idperfil };

        //    using (var bdCmd = new OracleCommand("PKG_PERFIL.consultarPerfil", conn))
        //    {
        //        bdCmd.CommandType = CommandType.StoredProcedure;
        //        bdCmd.Parameters.AddRange(bdParameters);
        //        using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
        //        {
        //            if (bdRd.HasRows)
        //            {
        //                while (bdRd.Read())
        //                {
        //                    var item = new CC_PERFIL_USUARIO();

        //                    if (!DBNull.Value.Equals(bdRd["ID_PERFIL"])) { item.ID_PERFIL = Convert.ToInt32(bdRd["ID_PERFIL"]); }
        //                    resultado.Add(item);
        //                }
        //            }
        //        }
        //    }
        //    return resultado;

        //}

    }
}








