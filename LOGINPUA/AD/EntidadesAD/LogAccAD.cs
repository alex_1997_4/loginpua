﻿using DA;
using Dapper;
using ENTIDADES;
using ENTIDADES.InnerJoin;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD.EntidadesAD
{
    public class LogAccAD : Repositorio<TV_LOG_ACC>, InterfaceAD<TV_LOG_ACC>
    {
        private readonly OracleConnection conn;
        public LogAccAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }
        public IEnumerable<TV_LOG_ACC> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_LOG_ACC.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TV_LOG_ACC>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TV_LOG_ACC Insertar(TV_LOG_ACC model)
        {
            var param = new
            {
                P_LOGACCCOD = model.LOGACCCOD,
                P_LOGCOD = model.LOGCOD,
                P_ACCCOD = model.ACCCOD,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_ESTREG = model.ESTREG
            };
            var packages = "PKG_LOG_ACC.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TV_LOG_ACC>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TV_LOG_ACC model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_LOGACCCOD", OracleDbType.Int32, ParameterDirection.Input, model.LOGACCCOD);
            bdParameters.Add("P_LOGCOD", OracleDbType.Int32, ParameterDirection.Input, model.LOGCOD);
            bdParameters.Add("P_ACCCOD", OracleDbType.Int32, ParameterDirection.Input, model.ACCCOD);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, model.ESTREG);
            var packages = "PKG_LOG_ACC.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TV_LOG_ACC ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<InnerJoinAccesos> ObtenerListadoDeAccesosPorLogCod(int id, int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "select LA.ACCCOD,AE.EMPCOD,LA.ESTREG,LA.LOGACCCOD from " +
                        "TV_LOG_ACC LA "+
                        "INNER JOIN TM_ACCESO AC ON LA.ACCCOD = AC.ACCCOD "+
                        "INNER JOIN TV_ACC_EMP AE ON AC.ACCCOD = AE.ACCCOD "+
                        "INNER JOIN TB_EMPRESA EM ON AE.EMPCOD = EM.EMPCOD "+
                        "WHERE LA.LOGCOD ="+ id +" AND AE.ESTREG = 1";
            if (activo == 1)
            {
                query += " AND LA.ESTREG = " + activo;
            }
            var result = SqlMapper.Query<InnerJoinAccesos>(conn, query);
            return result;
        }
    }
}
