﻿using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;

namespace AD.EntidadesAD
{
    public class ImagenAD : Repositorio<TM_PROGRAMACION_IMG>, IPrueba_Imagen
    {
        private readonly OracleConnection conn;
        public ImagenAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public TM_PROGRAMACION_IMG Insertar(TM_PROGRAMACION_IMG model)
        {
            throw new NotImplementedException();
        }

        public TM_PROGRAMACION_IMG insertar_imagen(TM_PROGRAMACION_IMG model)
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);

            var query = "PKG_PROGRAMACION_FOTOS.SP_INSERT_IMAGEN";
            bdParameters.Add("ID_PROGRAMACION_FOTOS", OracleDbType.Int32, ParameterDirection.Input, model.ID_PROGRAMACION_FOTOS);
            bdParameters.Add("FOTOS_URL", OracleDbType.Varchar2, ParameterDirection.Input, model.FOTOS_URL);
            bdParameters.Add("FOTOS_FECHA", OracleDbType.Date, ParameterDirection.Input, model.FOTOS_FECHA);
            bdParameters.Add("FOTOS_DESCRIPCION", OracleDbType.Varchar2, ParameterDirection.Input, model.FOTOS_DESCRIPCION);
            bdParameters.Add("ID_CORREDOR", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);

            var result = SqlMapper.QueryFirstOrDefault<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }



        public List<TM_PROGRAMACION_IMG> listado_Imagen()
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var query = "PKG_PROGRAMACION_FOTOS.SP_LISTADO_IMAGEN";
            var result = SqlMapper.Query<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }


        public TM_PROGRAMACION_IMG Obtener_Ultimo_Registro(TM_PROGRAMACION_IMG model)
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("ID_CORREDOR_", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);
            var query = "PKG_PROGRAMACION_FOTOS.SP_ULTIMO_REGISTRO_TIPO";
            var result = SqlMapper.QueryFirst<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }






        public TM_PROGRAMACION_IMG Obtener_Corredor(TM_PROGRAMACION_IMG model)
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_PROGRAMACION_FOTOS.SP_FILTRO_CORREDOR";
            bdParameters.Add("ID_CORREDOR_NOMBRE", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);

            var result = SqlMapper.QueryFirst<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public TM_PROGRAMACION_IMG Buscar_Foto_Corredor(TM_PROGRAMACION_IMG model, string fecha)
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_PROGRAMACION_FOTOS.SP_BUSCAR_CORREDOR_FOTO";
            bdParameters.Add("FOTOS_FECHA_IN", OracleDbType.Varchar2, ParameterDirection.Input, fecha);
            bdParameters.Add("ID_CORREDOR_NOMBRE", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);
            var result = SqlMapper.QueryFirst<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }


        public TM_PROGRAMACION_IMG Buscar_x_ID(int id)
        {

            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_PROGRAMACION_FOTOS.SP_BUSCAR_ID";
            bdParameters.Add("ID_BUSCAR", OracleDbType.Int32, ParameterDirection.Input, id);
            var result = SqlMapper.QueryFirst<TM_PROGRAMACION_IMG>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }
        public void Editar_Fotos(TM_PROGRAMACION_IMG model)
        {
            DateTime fecha = DateTime.Now;

            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            var query = "PKG_PROGRAMACION_FOTOS.SP_EDITAR_PROGRAMACION_FOTO";
            bdParameters.Add("ID", OracleDbType.Int32, ParameterDirection.Input, model.ID_PROGRAMACION_FOTOS);
            bdParameters.Add("FOTOS_URL_", OracleDbType.Varchar2, ParameterDirection.Input, model.FOTOS_URL);
            bdParameters.Add("FOTOS_DESCRIPCION_", OracleDbType.Varchar2, ParameterDirection.Input, model.FOTOS_DESCRIPCION);
            bdParameters.Add("ID_CORREDOR_", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);
            bdParameters.Add("FOTOS_FECHA_", OracleDbType.Date, ParameterDirection.Input, fecha);

            SqlMapper.Execute(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);


        }





        public void Listar_Conductores(int id, string aula, string pais)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "insert into prueba values('"+id+"'"+",'"+aula+"','"+pais+"')";
            SqlMapper.Query(conn, query).ToList();
        }

    }
}
