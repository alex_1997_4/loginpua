﻿using DA;
using Dapper;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace AD.EntidadesAD
{
    //public class LogAD : Repositorio<TL_LOG>, ILogRepositorio
    //{
    //    string cadenaConexion = string.Empty;
    //    public LogAD()
    //    {
    //        this.cadenaConexion = Configuracion.GetConectionSting("sConexionLOGINPUA");
    //    }

    //    public TL_LOG Crear(TL_LOG objeto)
    //    {
    //        try
    //        {
    //            OracleParameter[] bdParameters = new OracleParameter[7];
    //            bdParameters[0] = new OracleParameter("P_LOGIP", OracleDbType.Varchar2) { Value = objeto.LOGIP };
    //            bdParameters[1] = new OracleParameter("P_LOGFEC", OracleDbType.Varchar2) { Value = objeto.LOGFEC };
    //            bdParameters[2] = new OracleParameter("P_LOGUSE", OracleDbType.Varchar2) { Value = objeto.LOGUSE };
    //            bdParameters[3] = new OracleParameter("P_LOGACT", OracleDbType.Varchar2) { Value = objeto.LOGACT };
    //            bdParameters[4] = new OracleParameter("P_LOGMAC", OracleDbType.Varchar2) { Value = objeto.LOGMAC };
    //            bdParameters[5] = new OracleParameter("P_USUREG", OracleDbType.Varchar2) { Value = SessionHelper.user_login };
    //            bdParameters[6] = new OracleParameter("P_LOGIDE", OracleDbType.Int32, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_LOG.SP_INSERTAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    bdCmd.ExecuteNonQuery();
    //                    objeto.LOGIDE = int.Parse(bdCmd.Parameters["P_LOGIDE"].Value.ToString());
    //                    return objeto;
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            return null;
    //        }
    //    }

    //    public TL_LOG Modificar(TL_LOG objeto, ref string mensaje, ref int tipo)
    //    {
    //        try
    //        {
    //            OracleParameter[] bdParameters = new OracleParameter[6];
    //            bdParameters[0] = new OracleParameter("P_LOGIDE", OracleDbType.Int32) { Value = objeto.LOGIDE };
    //            bdParameters[1] = new OracleParameter("P_LOGIP", OracleDbType.Varchar2) { Value = objeto.LOGIP };
    //            bdParameters[2] = new OracleParameter("P_LOGFEC", OracleDbType.Varchar2) { Value = objeto.LOGFEC };
    //            bdParameters[3] = new OracleParameter("P_LOGUSE", OracleDbType.Varchar2) { Value = objeto.LOGUSE };
    //            bdParameters[4] = new OracleParameter("P_LOGACT", OracleDbType.Varchar2) { Value = objeto.LOGACT };
    //            bdParameters[5] = new OracleParameter("P_USUMOD", OracleDbType.Varchar2) { Value = SessionHelper.user_login };

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_LOG.SP_MODIFICAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    bdCmd.ExecuteNonQuery();
    //                    tipo = 1;
    //                    mensaje = "Registro modificado";
    //                    return objeto;
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            return null;
    //        }
    //    }

    //    public void Eliminar(TL_LOG objeto, ref string mensaje, ref int tipo)
    //    {
    //        try
    //        {
    //            OracleParameter[] bdParameters = new OracleParameter[2];
    //            bdParameters[0] = new OracleParameter("P_LOGIDE", OracleDbType.Int32) { Value = objeto.LOGIDE };
    //            bdParameters[1] = new OracleParameter("P_USUMOD", OracleDbType.Varchar2) { Value = SessionHelper.user_login };

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_LOG.SP_ELIMINAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    bdCmd.ExecuteNonQuery();
    //                    tipo = 1;
    //                    mensaje = "Registro eliminado";
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //        }
    //    }

    //    public TL_LOG Datos(TL_LOG objeto, ref string mensaje, ref int tipo)
    //    {
    //        try
    //        {
    //            TL_LOG resultado;
    //            OracleParameter[] bdParameters = new OracleParameter[2];
    //            bdParameters[0] = new OracleParameter("P_LOGIDE", OracleDbType.Int32) { Value = objeto.LOGIDE };
    //            bdParameters[1] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_LOG.SP_DATOS", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
    //                    {
    //                        if (bdRd.HasRows)
    //                        {
    //                            bdRd.Read();
    //                            resultado = new TL_LOG();
    //                            if (!DBNull.Value.Equals(bdRd["LOGIDE"])) { resultado.LOGIDE = Convert.ToInt32(bdRd["LOGIDE"]); }
    //                            if (!DBNull.Value.Equals(bdRd["LOGIP"])) { resultado.LOGIP = Convert.ToString(bdRd["LOGIP"]); }
    //                            if (!DBNull.Value.Equals(bdRd["LOGFEC"])) { resultado.LOGFEC = Convert.ToString(bdRd["LOGFEC"]); }
    //                            if (!DBNull.Value.Equals(bdRd["LOGUSE"])) { resultado.LOGUSE = Convert.ToString(bdRd["LOGUSE"]); }
    //                            if (!DBNull.Value.Equals(bdRd["LOGACT"])) { resultado.LOGACT = Convert.ToString(bdRd["LOGACT"]); }
    //                            mensaje = "Datos Obtenidos";
    //                            tipo = 1;
    //                            return resultado;
    //                        }
    //                    }
    //                }
    //            }
    //            mensaje = "No se encrontraró registro";
    //            tipo = 0;
    //            return null;
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            return null;
    //        }
    //    }

    //    public List<TL_LOG> Buscar(TL_LOG objeto, ref string mensaje, ref int tipo, string orden = null)
    //    {
    //        try
    //        {
    //            List<TL_LOG> resultado = new List<TL_LOG>();
    //            OracleParameter[] bdParameters = new OracleParameter[6];
    //            bdParameters[0] = new OracleParameter("P_LOGIDE", OracleDbType.Int32) { Value = objeto.LOGIDE };
    //            bdParameters[1] = new OracleParameter("P_LOGIP", OracleDbType.Varchar2) { Value = objeto.LOGIP };
    //            bdParameters[2] = new OracleParameter("P_LOGFEC", OracleDbType.Varchar2) { Value = objeto.LOGFEC };
    //            bdParameters[3] = new OracleParameter("P_LOGUSE", OracleDbType.Varchar2) { Value = objeto.LOGUSE };
    //            bdParameters[4] = new OracleParameter("P_LOGACT", OracleDbType.Varchar2) { Value = objeto.LOGACT };
    //            bdParameters[5] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_LOG.SP_BUSCAR", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
    //                    {
    //                        if (bdRd.HasRows)
    //                        {
    //                            while (bdRd.Read())
    //                            {
    //                                var item = new TL_LOG();
    //                                if (!DBNull.Value.Equals(bdRd["LOGIDE"])) { item.LOGIDE = Convert.ToInt32(bdRd["LOGIDE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGIP"])) { item.LOGIP = Convert.ToString(bdRd["LOGIP"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGFEC"])) { item.LOGFEC = Convert.ToString(bdRd["LOGFEC"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGUSE"])) { item.LOGUSE = Convert.ToString(bdRd["LOGUSE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGACT"])) { item.LOGACT = Convert.ToString(bdRd["LOGACT"]); }
    //                                resultado.Add(item);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            mensaje = "Lista obtenida";
    //            tipo = 1;
    //            return resultado;
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            return null;
    //        }
    //    }

    //    public List<TL_LOG> BuscarPag(TL_LOG objeto, string orden, out int totalReg, out int totalPag, ref string mensaje, ref int tipo, int pagina = 1, int registros = 50)
    //    {
    //        try
    //        {
    //            List<TL_LOG> resultado = new List<TL_LOG>();
    //            totalPag = 1;
    //            totalReg = 0;
    //            OracleParameter[] bdParameters = new OracleParameter[11];
    //            bdParameters[0] = new OracleParameter("P_LOGIDE", OracleDbType.Int32) { Value = objeto.LOGIDE };
    //            bdParameters[1] = new OracleParameter("P_LOGIP", OracleDbType.Varchar2) { Value = objeto.LOGIP };
    //            bdParameters[2] = new OracleParameter("P_LOGFEC", OracleDbType.Varchar2) { Value = objeto.LOGFEC };
    //            bdParameters[3] = new OracleParameter("P_LOGUSE", OracleDbType.Varchar2) { Value = objeto.LOGUSE };
    //            bdParameters[4] = new OracleParameter("P_LOGACT", OracleDbType.Varchar2) { Value = objeto.LOGACT };
    //            bdParameters[5] = new OracleParameter("P_ORDEN", OracleDbType.Varchar2) { Value = orden };
    //            bdParameters[6] = new OracleParameter("P_NUMPAG", OracleDbType.Int32) { Value = pagina };
    //            bdParameters[7] = new OracleParameter("P_NUMREG", OracleDbType.Int32) { Value = registros };
    //            bdParameters[8] = new OracleParameter("P_TOTPAG", OracleDbType.Int32, direction: ParameterDirection.Output);
    //            bdParameters[9] = new OracleParameter("P_TOTREG", OracleDbType.Int32, direction: ParameterDirection.Output);
    //            bdParameters[10] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

    //            using (var bdConn = new OracleConnection(this.cadenaConexion))
    //            {
    //                using (var bdCmd = new OracleCommand("PKG_LOG.SP_BUSCAR_PAG", bdConn))
    //                {
    //                    bdCmd.CommandType = CommandType.StoredProcedure;
    //                    bdCmd.Parameters.AddRange(bdParameters);
    //                    bdConn.Open();
    //                    using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
    //                    {
    //                        if (bdRd.HasRows)
    //                        {
    //                            totalPag = Convert.ToInt32(bdCmd.Parameters["P_TOTPAG"].Value.ToString());
    //                            totalReg = Convert.ToInt32(bdCmd.Parameters["P_TOTREG"].Value.ToString());
    //                            while (bdRd.Read())
    //                            {
    //                                var item = new TL_LOG();
    //                                if (!DBNull.Value.Equals(bdRd["LOGIDE"])) { item.LOGIDE = Convert.ToInt32(bdRd["LOGIDE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGIP"])) { item.LOGIP = Convert.ToString(bdRd["LOGIP"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGFEC"])) { item.LOGFEC = Convert.ToString(bdRd["LOGFEC"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGUSE"])) { item.LOGUSE = Convert.ToString(bdRd["LOGUSE"]); }
    //                                if (!DBNull.Value.Equals(bdRd["LOGACT"])) { item.LOGACT = Convert.ToString(bdRd["LOGACT"]); }
    //                                resultado.Add(item);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            mensaje = "Lista obtenida";
    //            tipo = 1;
    //            return resultado;
    //        }
    //        catch (Exception ex)
    //        {
    //            mensaje = ex.Message;
    //            tipo = 0;
    //            totalPag = 1;
    //            totalReg = 0;
    //            return null;
    //        }
    //    }

    //    public TL_LOG Insertar(TL_LOG model)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public dynamic Modificar(TL_LOG model)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Eliminar(int id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public List<TL_LOG> Datos()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IEnumerable<TL_LOG> ILogRepositorio.Datos()
    //    {
    //        throw new NotImplementedException();
    //    }

    //}
    public class LogAD : Repositorio<TL_LOG>, InterfaceAD<TL_LOG>
    {
        private readonly OracleConnection conn;
        public LogAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public IEnumerable<TL_LOG> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_LOG.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TL_LOG>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TL_LOG Insertar(TL_LOG model)
        {
            var param = new
            {
                P_LOGCOD = model.LOGCOD,
                P_LOGIP = model.LOGIP,
                P_LOGFEC = model.LOGFEC,
                P_LOGUSE = model.LOGUSE,
                P_LOGDES = model.LOGDES,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_USUMOD = model.USUMOD,
                P_FECMOD = model.FECMOD,
                P_ESTREG = model.ESTREG,
                P_LOGMAC = model.LOGMAC
            };
            var packages = "PKG_LOG.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TL_LOG>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TL_LOG model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_LOGCOD", OracleDbType.Int32, ParameterDirection.Input, model.LOGCOD);
            bdParameters.Add("P_LOGIP", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGIP);
            bdParameters.Add("P_LOGFEC", OracleDbType.Date, ParameterDirection.Input, model.LOGFEC);
            bdParameters.Add("P_LOGUSE", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGUSE);
            bdParameters.Add("P_LOGDES", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGDES);
            bdParameters.Add("P_USUREG", OracleDbType.Varchar2, ParameterDirection.Input, model.USUREG);
            bdParameters.Add("P_FECREG", OracleDbType.Date, ParameterDirection.Input, model.FECREG);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, model.ESTREG);
            bdParameters.Add("P_LOGMAC", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGMAC);
            var packages = "PKG_LOG.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TL_LOG ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }
}