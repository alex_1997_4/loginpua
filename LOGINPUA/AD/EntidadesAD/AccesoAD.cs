﻿using DA;
using Dapper;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD.EntidadesAD
{
    public class AccesoAD : Repositorio<TM_ACCESO>, InterfaceAD<TM_ACCESO>
    {
        private readonly OracleConnection conn;
        public AccesoAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public IEnumerable<TM_ACCESO> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_ACCESO.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TM_ACCESO>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }
        public IEnumerable<TM_ACCESO> DatosDe(int codigo)
        {
            var query = "select AC.* from "+
                    "TV_ACC_EMP AE "+
                    "INNER JOIN TM_ACCESO AC ON AE.ACCCOD = AC.ACCCOD "+
                    "WHERE AE.EMPCOD = " + codigo;
            var result = SqlMapper.Query<TM_ACCESO>(conn, query);
            return result;
        }
        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TM_ACCESO Insertar(TM_ACCESO model)
        {
            var param = new
            {
                P_ACCCOD = model.ACCCOD,
                P_ACCNOM = model.ACCNOM,
                P_ACCICO = model.ACCICO,
                P_ACCDIR = model.ACCDIR,
                P_ACCGRU = model.ACCGRU,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_ESTREG = model.ESTREG
            };
            var packages = "PKG_ACCESO.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TM_ACCESO>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TM_ACCESO model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ACCCOD", OracleDbType.Int32, ParameterDirection.Input, model.ACCCOD);
            bdParameters.Add("P_ACCNOM", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCNOM);
            bdParameters.Add("P_ACCDIR", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCDIR);
            bdParameters.Add("P_ACCICO", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCICO);
            bdParameters.Add("P_ACCGRU", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCGRU);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            var packages = "PKG_ACCESO.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public dynamic ModificarSinImagen(TM_ACCESO model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ACCCOD", OracleDbType.Int32, ParameterDirection.Input, model.ACCCOD);
            bdParameters.Add("P_ACCNOM", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCNOM);
            bdParameters.Add("P_ACCDIR", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCDIR);
            //bdParameters.Add("P_ACCICO", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCICO);
            bdParameters.Add("P_ACCGRU", OracleDbType.Varchar2, ParameterDirection.Input, model.ACCGRU);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            var packages = "PKG_ACCESO.SP_MODIFICARNOIMAGEN";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }
        public TM_ACCESO ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }
}
