﻿using DA;
using Dapper;
using Entidades;
using ENTIDADES;
using ENTIDADES.InnerJoin;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace AD.EntidadesAD
{
    //public class LoginAD : Repositorio<TB_LOGIN>, ILoginRepositorio
    //{
    //    private readonly OracleConnection conn;
    //    public LoginAD()
    //    {
    //        conn = new OracleConnection(Configuracion.GetConectionSting("sConexionLOGINPUA"));
    //    }

    //    public TB_LOGIN Datos(TB_LOGIN model)
    //    {
    //        var bdParameters = new OracleDynamicParameters();
    //        //string codigo = Encriptador.Encriptar(model.LOGPAS);
    //        bdParameters.Add("P_LOGUSU", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGUSU);
    //        bdParameters.Add("P_LOGPAS", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGPAS);
    //        bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
    //        var query = "PKG_LOGIN.SP_LOGIN";
    //        var result = SqlMapper.QueryFirstOrDefault<TB_LOGIN>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
    //        return result;
    //    }
    //    public IEnumerable<TB_LOGIN> DatosColores()
    //    {
    //        var bdParameters = new OracleDynamicParameters();
    //        bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
    //        var query = "PKG_LOGIN.SP_DATOS";
    //        var result = SqlMapper.Query<TB_LOGIN>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
    //        return result;
    //    }

    //    public int Eliminar(int id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public TB_LOGIN Insertar(TB_LOGIN model)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public dynamic Modificar(TB_LOGIN model)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    IEnumerable<TB_LOGIN> ILoginRepositorio.Datos()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class LoginAD : Repositorio<TB_LOGIN>, InterfaceAD<TB_LOGIN>
    {
        private readonly OracleConnection conn;
        public LoginAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }


        public DataSet IngresarIn(string usuario, string clave)
        {
            DataSet ds = new DataSet();


            using (OracleCommand command = new OracleCommand())
            {

                command.CommandText = "PKG_LOGIN.SP_INGRESAR_LOGIN_REAL";
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = conn;

                OracleParameter comparativoA = new OracleParameter();
                comparativoA.ParameterName = "P_LOGUSU";
                comparativoA.OracleDbType = OracleDbType.Varchar2;
                comparativoA.Direction = ParameterDirection.Input;
                comparativoA.Value = usuario;

                OracleParameter comparativoB = new OracleParameter();
                comparativoB.ParameterName = "P_LOGCON";
                comparativoB.OracleDbType = OracleDbType.Varchar2;
                comparativoB.Direction = ParameterDirection.Input;
                comparativoB.Value = clave;

                OracleParameter cursor_A = new OracleParameter();
                cursor_A.ParameterName = "P_CURSOR_A";
                cursor_A.OracleDbType = OracleDbType.RefCursor;
                cursor_A.Direction = ParameterDirection.Output;

                OracleParameter cursor_B = new OracleParameter();
                cursor_B.ParameterName = "P_CURSOR_B";
                cursor_B.OracleDbType = OracleDbType.RefCursor;
                cursor_B.Direction = ParameterDirection.Output;

                OracleParameter cursor_C = new OracleParameter();
                cursor_C.ParameterName = "P_CURSOR_C";
                cursor_C.OracleDbType = OracleDbType.RefCursor;
                cursor_C.Direction = ParameterDirection.Output;

                OracleParameter cursor_D = new OracleParameter();
                cursor_D.ParameterName = "P_CURSOR_D";
                cursor_D.OracleDbType = OracleDbType.RefCursor;
                cursor_D.Direction = ParameterDirection.Output;

                OracleParameter cursor_E = new OracleParameter();
                cursor_E.ParameterName = "P_CURSOR_E";
                cursor_E.OracleDbType = OracleDbType.RefCursor;
                cursor_E.Direction = ParameterDirection.Output;

                OracleParameter cursor_F = new OracleParameter();
                cursor_F.ParameterName = "P_CURSOR_F";
                cursor_F.OracleDbType = OracleDbType.RefCursor;
                cursor_F.Direction = ParameterDirection.Output;
                
                command.Parameters.Add(comparativoA);
                command.Parameters.Add(comparativoB);
                command.Parameters.Add(cursor_A);
                command.Parameters.Add(cursor_B);
                command.Parameters.Add(cursor_C);
                command.Parameters.Add(cursor_D);
                command.Parameters.Add(cursor_E);
                command.Parameters.Add(cursor_F);


                OracleDataAdapter da = new OracleDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds);

            }
            return ds;

        }

        public CC_USUARIO_PERSONA Verificar_Usuario(string usuario, string contraseña)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_LOGUSU", OracleDbType.Varchar2, ParameterDirection.Input, usuario);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_LOGIN.SP_AUTENTICAR_USUARIO";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault<CC_USUARIO_PERSONA>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }



        public InnerJoinLogin LoginIn(TB_LOGIN model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_LOGUSU", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGUSU);
            bdParameters.Add("P_LOGCON", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGCON);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_LOGIN.SP_AUTENTICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault<InnerJoinLogin>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }
        public IEnumerable<InnerJoinUsuario> Accesos(string LOGUSU, string LOGCON)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_LOGUSU", OracleDbType.Varchar2, ParameterDirection.Input, LOGUSU);
            bdParameters.Add("P_LOGCON", OracleDbType.Varchar2, ParameterDirection.Input, LOGCON);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_LOGIN.SP_ACCESOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<InnerJoinUsuario>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public IEnumerable<TB_LOGIN> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_LOGIN.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TB_LOGIN>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TB_LOGIN Insertar(TB_LOGIN model)
        {
            var param = new
            {
                P_USUCOD = model.USUCOD,
                P_LOGUSU = model.LOGUSU,
                P_LOGCOD = model.LOGCOD,
                P_LOGCON = model.LOGCON,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_USUMOD = model.USUMOD,
                P_FECMOD = model.FECMOD,
                P_ESTREG = model.ESTREG
            };
            var packages = "PKG_LOGIN.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TB_LOGIN>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TB_LOGIN model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_USUCOD", OracleDbType.Int32, ParameterDirection.Input, model.USUCOD);
            bdParameters.Add("P_LOGUSU", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGUSU);
            bdParameters.Add("P_LOGCOD", OracleDbType.Int32, ParameterDirection.Input, model.LOGCOD);
            bdParameters.Add("P_LOGCON", OracleDbType.Varchar2, ParameterDirection.Input, model.LOGCON);
            bdParameters.Add("P_USUREG", OracleDbType.Varchar2, ParameterDirection.Input, model.USUREG);
            bdParameters.Add("P_FECREG", OracleDbType.Date, ParameterDirection.Input, model.FECREG);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, model.ESTREG);
            var packages = "PKG_LOGIN.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TB_LOGIN ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }
}
