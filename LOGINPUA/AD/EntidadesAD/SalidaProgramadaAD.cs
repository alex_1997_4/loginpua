﻿using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;
using ENTIDADES;

namespace AD.EntidadesAD
{
    public class SalidaProgramadaAD
    {
        private readonly OracleConnection conn;
        public SalidaProgramadaAD(ref Object _bdConn)
        {
            _bdConn = Conexion.iniciar(ref conn, _bdConn);
            //conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public RPTA_GENERAL registrarMaestroSalidaProgramada(int idTipoDia, int idRutaTipoServicio, string fechaProgramacion, string nomUsuario)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();
            var id_maestro_salida_prog = 0;
            OracleParameter[] bdParameters = new OracleParameter[6];

            bdParameters[0] = new OracleParameter("P_ID_TIPO_DIA", OracleDbType.Int32) { Value = idTipoDia };
            bdParameters[1] = new OracleParameter("P_ID_RUTA_TIPO_SERVICIO", OracleDbType.Int32) { Value = idRutaTipoServicio };
            bdParameters[2] = new OracleParameter("P_FECHA_PROGRAMACION", OracleDbType.Varchar2) { Value = fechaProgramacion };
            bdParameters[3] = new OracleParameter("P_USU_REG", OracleDbType.Varchar2) { Value = nomUsuario };
            bdParameters[4] = new OracleParameter("P_FECHA_REG", OracleDbType.Varchar2) { Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") };
            bdParameters[5] = new OracleParameter("P_ID_MAESTRO_SALIDA_PROG", OracleDbType.Int32, direction: ParameterDirection.Output);

            try
            {
                using (var bdCmd = new OracleCommand("PKG_GESTION_SALIDA.guardarMaestroSalidaProgramada", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    id_maestro_salida_prog = int.Parse(bdCmd.Parameters["P_ID_MAESTRO_SALIDA_PROG"].Value.ToString());
                    r.AUX = id_maestro_salida_prog;
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se registró correctamente";
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }

        public RPTA_GENERAL verifica_maestro_prog(int idRutaTipoServicio, string fechaProgramacion)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();
            var id_maestro_salida_prog = 0;
            OracleParameter[] bdParameters = new OracleParameter[3];

            bdParameters[0] = new OracleParameter("P_ID_R_TIPO", OracleDbType.Int32) { Value = idRutaTipoServicio };
            bdParameters[1] = new OracleParameter("P_FECHA_PROG", OracleDbType.Varchar2) { Value = fechaProgramacion };
             bdParameters[2] = new OracleParameter("P_ID_MAESTRO_PROG", OracleDbType.Int32, direction: ParameterDirection.Output);
            try
            {
                using (var bdCmd = new OracleCommand("PKG_PROGRAMACION_BUSES.VERIFICA_DATA_MAESTRO_PROG", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    id_maestro_salida_prog = int.Parse(bdCmd.Parameters["P_ID_MAESTRO_PROG"].Value.ToString());
                    r.AUX = id_maestro_salida_prog;
                    r.COD_ESTADO = 1;
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }


        public RPTA_GENERAL anular_maestro_prog(int maestro_salida)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();
            var id_cantidad = 0;
            OracleParameter[] bdParameters = new OracleParameter[2];

            bdParameters[0] = new OracleParameter("P_MAESTRO_SALIDA_PROG", OracleDbType.Int32) { Value = maestro_salida };
            bdParameters[1] = new OracleParameter("P_CANTIDAD_REGISTROS", OracleDbType.Int32, direction: ParameterDirection.Output);

            try
            {
                using (var bdCmd = new OracleCommand("PKG_PROGRAMACION_BUSES.ANULA_MAESTRO_PROG", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    id_cantidad = int.Parse(bdCmd.Parameters["P_CANTIDAD_REGISTROS"].Value.ToString());
                    r.AUX = id_cantidad;
                    r.COD_ESTADO = 1;
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }

        public List<CC_RESUMEN_PROGRAMADO> getResumenProgramado(int idServicio)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ID_SERVICIO", OracleDbType.Int32, ParameterDirection.Input, idServicio);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var query = "PKG_PROGRAMACION_BUSES.GET_RESUMEN_PROGRAMADO";
            var result = SqlMapper.Query<CC_RESUMEN_PROGRAMADO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();

            return result;
        }

        public List<CC_RUTA_TIPO_SERVICIO> getTipoServicioByCorredor(int idCorredor)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ID_CORREDOR", OracleDbType.Int32, ParameterDirection.Input, idCorredor);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var query = "PKG_PROGRAMACION_BUSES.GET_TIPO_SERV";
            var result = SqlMapper.Query<CC_RUTA_TIPO_SERVICIO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();

            return result;
        }

        public List<CC_DATA_VIAJE_PROGRAMADO> getDataViajesProgramacion(int idMaestroSalidaProg)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ID_MAESTRO_SALIDA_PROG", OracleDbType.Int32, ParameterDirection.Input, idMaestroSalidaProg);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var query = "PKG_PROGRAMACION_BUSES.GET_VIAJES_PROGRAMACION";
            var result = SqlMapper.Query<CC_DATA_VIAJE_PROGRAMADO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();

            return result;
        }

        public RPTA_GENERAL guardarMSalidaProgramadaDet(int idMaestroSalidaProg, string tipodia, string servicio,
                                                        string pog, string pot, string fnode, string hSalida,
                                                        string hLlegada, string tNode, string PIG, double layover, string acumulado,
                                                        string sentido, string turno, string tipoServicio, string placa,
                                                        string cacConductor, string usuarioRegistro)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();
            OracleParameter[] bdParameters = new OracleParameter[19];

            bdParameters[0] = new OracleParameter("P_ID_MAESTRO_SALIDA_PROG", OracleDbType.Int32) { Value = idMaestroSalidaProg };
            bdParameters[1] = new OracleParameter("P_TIPO_DIA", OracleDbType.Varchar2) { Value = tipodia };
            bdParameters[2] = new OracleParameter("P_SERVICIO", OracleDbType.Varchar2) { Value = servicio };
            bdParameters[3] = new OracleParameter("P_POG", OracleDbType.Varchar2) { Value = pog };
            bdParameters[4] = new OracleParameter("P_POT", OracleDbType.Varchar2) { Value = pot };
            bdParameters[5] = new OracleParameter("P_FNODE", OracleDbType.Varchar2) { Value = fnode };
            bdParameters[6] = new OracleParameter("P_HSALIDA", OracleDbType.Varchar2) { Value = hSalida };
            bdParameters[7] = new OracleParameter("P_HLLEGADA", OracleDbType.Varchar2) { Value = hLlegada };
            bdParameters[8] = new OracleParameter("P_TNODE", OracleDbType.Varchar2) { Value = tNode };
            bdParameters[9] = new OracleParameter("P_PIG", OracleDbType.Varchar2) { Value = PIG };
            bdParameters[10] = new OracleParameter("P_LAYOVER", OracleDbType.Decimal) { Value = layover };
            bdParameters[11] = new OracleParameter("P_ACUMULADO", OracleDbType.Varchar2) { Value = acumulado };
            bdParameters[12] = new OracleParameter("P_SENTIDO", OracleDbType.Varchar2) { Value = sentido };
            bdParameters[13] = new OracleParameter("P_TURNO", OracleDbType.Varchar2) { Value = turno };
            bdParameters[14] = new OracleParameter("P_TIPO_SERVICIO", OracleDbType.Varchar2) { Value = tipoServicio };
            bdParameters[15] = new OracleParameter("P_PLACA", OracleDbType.Varchar2) { Value = placa };
            bdParameters[16] = new OracleParameter("P_CAC_CONDUCTOR", OracleDbType.Varchar2) { Value = cacConductor };
            bdParameters[17] = new OracleParameter("P_USU_REG", OracleDbType.Varchar2) { Value = usuarioRegistro };
            bdParameters[18] = new OracleParameter("P_FECHA_REG", OracleDbType.Varchar2) { Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") };

            try
            {
                using (var bdCmd = new OracleCommand("PKG_GESTION_SALIDA.guardarMSalidaProgramadaDet", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();
                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se registró correctamente";
                }
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }
    }
}
