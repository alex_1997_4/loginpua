﻿using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;
using ENTIDADES;


namespace AD.EntidadesAD
{
    public class RegistroPicoPlacaAD
    {
        private readonly OracleConnection conn;
        public RegistroPicoPlacaAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public CC_RECORRIDO getKmPorLadoYRecorrido(int idRuta, string lado) {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("P_ID_RUTA", OracleDbType.Int32, ParameterDirection.Input, idRuta);
            bdParameters.Add("P_LADO", OracleDbType.Varchar2, ParameterDirection.Input, lado);

            var query = "PKG_PROGRAMACION_BUSES.GET_KM_POR_LADO";
            var result = SqlMapper.QueryFirst<CC_RECORRIDO>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);

            return result;
        }

        public List<CC_REPORTE_PICO_PLACA> getReportePicoPlacaByFechas(string finicio, string ffin, int idRuta) {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("P_FECHA_INI", OracleDbType.Varchar2, ParameterDirection.Input, finicio);
            bdParameters.Add("P_FECHA_FIN", OracleDbType.Varchar2, ParameterDirection.Input, ffin);
            bdParameters.Add("P_ID_RUTA", OracleDbType.Int32, ParameterDirection.Input, idRuta);

            var query = "PKG_PROGRAMACION_BUSES.GET_DATA_REGISTROS_PICO_PLACA";
            var result = SqlMapper.Query<CC_REPORTE_PICO_PLACA>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();

            return result;
        }
        
        public List<CC_REPORTE_PICO_PLACA> getReporte_Comparativo(string fecha, int id_ruta, string turno, string ida, string vuelta)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_FECHA", OracleDbType.Varchar2, ParameterDirection.Input, fecha);
            bdParameters.Add("P_TURNO", OracleDbType.Varchar2, ParameterDirection.Input, turno);
            bdParameters.Add("IDRUTA", OracleDbType.Int32, ParameterDirection.Input, id_ruta);

            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("IDA", OracleDbType.Varchar2, ParameterDirection.Input, ida);
            bdParameters.Add("VUELTA", OracleDbType.Varchar2, ParameterDirection.Input, vuelta);
            
            var query = "PKG_PROGRAMACION_BUSES.GET_REPORTE_COMPARATIVO";
            var result = SqlMapper.Query<CC_REPORTE_PICO_PLACA>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();

            return result;
        }
        
        public RPTA_GENERAL registrarPicoPlaca( int idRuta, int idEstado, string turno, 
                                                string fechaRegistro, string horaInicio, string horaFin, 
                                                double velocidadPromedio_AB, double velocidadPromedio_BA,
                                                double distancia_A, double distancia_B,
                                                double tiempo_A, double tiempo_B,
                                                string observacion, string nomUsuario) {
            RPTA_GENERAL r = new RPTA_GENERAL();
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_PROGRAMACION_BUSES.REGISTRO_PICO_PLACA";
            try
            {
                bdParameters.Add("P_ID_RUTA", OracleDbType.Int32, ParameterDirection.Input, idRuta);
                bdParameters.Add("P_ID_ESTADO", OracleDbType.Int32, ParameterDirection.Input, idEstado);
                bdParameters.Add("P_TURNO", OracleDbType.Varchar2, ParameterDirection.Input, turno);
                bdParameters.Add("P_FECHA_REGISTRO", OracleDbType.Varchar2, ParameterDirection.Input, fechaRegistro);
                bdParameters.Add("P_HINICIO", OracleDbType.Varchar2, ParameterDirection.Input, horaInicio);
                bdParameters.Add("P_HFIN", OracleDbType.Varchar2, ParameterDirection.Input, horaFin);
                bdParameters.Add("P_VEL_PROMEDIO_AB", OracleDbType.Decimal, ParameterDirection.Input, velocidadPromedio_AB);
                bdParameters.Add("P_VEL_PROMEDIO_BA", OracleDbType.Decimal, ParameterDirection.Input, velocidadPromedio_BA);
                bdParameters.Add("P_DISTANCIA_A", OracleDbType.Decimal, ParameterDirection.Input, distancia_A);
                bdParameters.Add("P_DISTANCIA_B", OracleDbType.Decimal, ParameterDirection.Input, distancia_B);
                bdParameters.Add("P_TIEMPO_PROM_A", OracleDbType.Decimal, ParameterDirection.Input, tiempo_A);
                bdParameters.Add("P_TIEMPO_PROM_B", OracleDbType.Decimal, ParameterDirection.Input, tiempo_B);
                bdParameters.Add("P_OBS", OracleDbType.Varchar2, ParameterDirection.Input, observacion);
                bdParameters.Add("P_FECHA_REG", OracleDbType.Varchar2, ParameterDirection.Input, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                bdParameters.Add("P_USU_REG", OracleDbType.Varchar2, ParameterDirection.Input, nomUsuario);
                SqlMapper.QueryFirstOrDefault(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);

                r.COD_ESTADO = 1;
                r.DES_ESTADO = "Se registró correctamente";
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
                throw;
            }
            return r;
        }

        public RPTA_GENERAL anularRegistrosPorFecha( string fechaRegistro, int idRuta)
        {

            RPTA_GENERAL r = new RPTA_GENERAL();
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_PROGRAMACION_BUSES.ELIMINAR_REGISTROS";
            try
            {
                bdParameters.Add("P_FECHA_REGISTRO", OracleDbType.Varchar2, ParameterDirection.Input, fechaRegistro);
                bdParameters.Add("P_ID_RUTA", OracleDbType.Int32, ParameterDirection.Input, idRuta);
                SqlMapper.QueryFirstOrDefault(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
                r.COD_ESTADO = 1;
                r.DES_ESTADO = "Se anuló los registros correctamente";
            }
            catch (Exception ex)
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }
        public List<CC_REPORTE_PICO_PLACA> getHora_Comparativo(string fecha, int id_ruta, string turno)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_FECHA", OracleDbType.Varchar2, ParameterDirection.Input, fecha);
            bdParameters.Add("P_TURNO", OracleDbType.Varchar2, ParameterDirection.Input, turno);
            bdParameters.Add("IDRUTA", OracleDbType.Int32, ParameterDirection.Input, id_ruta);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            
            var query = "PKG_PROGRAMACION_BUSES.GET_HORA_RECORRIDO";
            var result = SqlMapper.Query<CC_REPORTE_PICO_PLACA>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();

            return result;
        }

    }
}
