﻿using DA;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System.Data;
using System;

namespace AD.EntidadesAD
{
    public class ProgramacionAD
    {
        private readonly OracleConnection conn;

        public ProgramacionAD(ref Object _bdConn)
        {
            _bdConn = Conexion.iniciar(ref conn, _bdConn);
        }
        public List<CC_DATOS_CORREDOR> getDataCorredor(int idCorredor) //cabecera de la programación
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters.Add("P_ID_CORREDOR", OracleDbType.Int32, ParameterDirection.Input, idCorredor);
            var query = "PKG_PROGRAMACION_BUSES.getParametrosCorredor";
            var result = SqlMapper.Query<CC_DATOS_CORREDOR>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }
        public RPTA_GENERAL ValidarFecha_Programados(string fecha, int idServicio)
        {
            var item = new RPTA_GENERAL();

            OracleParameter[] bdParameters = new OracleParameter[3];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters[1] = new OracleParameter("P_FECHA", OracleDbType.Varchar2) { Value = fecha };
            bdParameters[2] = new OracleParameter("P_IDSERVICIO", OracleDbType.Int32) { Value = idServicio };

            using (var bdCmd = new OracleCommand("PKG_PROGRAMACION_BUSES.VALIDARFECHA_PROGRAMACION", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            if (!DBNull.Value.Equals(bdRd["AUX"])) { item.DES_ESTADO = bdRd["AUX"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["ID"])) { item.AUX = Convert.ToInt32(bdRd["ID"]); }

                        }
                    }
                }
            }
            return item;

        }

        public List<CC_PROGRAMA_RUTA> getViajes_ProgrUnidades(string fecha, int id_ruta, string sentido)
        {
            List<CC_PROGRAMA_RUTA> resultado = new List<CC_PROGRAMA_RUTA>();
            string horaactual = DateTime.Now.ToString("HH:mm:ss");

            OracleParameter[] bdParameters = new OracleParameter[5];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters[1] = new OracleParameter("P_FECHA", OracleDbType.Varchar2) { Value = fecha }; ;
            bdParameters[2] = new OracleParameter("P_ID_RUTA", OracleDbType.Int32) { Value = id_ruta }; ;
            bdParameters[3] = new OracleParameter("P_SENTIDO", OracleDbType.Varchar2) { Value = sentido }; ;
            bdParameters[4] = new OracleParameter("P_HORA_ACTUAL", OracleDbType.Varchar2) { Value = horaactual }; ;



            using (var bdCmd = new OracleCommand("PKG_PROGRAMACION_BUSES.GET_VIAJES_PROGR_UNIDADES", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_PROGRAMA_RUTA();

                            if (!DBNull.Value.Equals(bdRd["ID_MSALIDA_PROG_DET"])) { item.ID_MSALIDA_PROG_DET = Convert.ToInt32(bdRd["ID_MSALIDA_PROG_DET"]); }
                            if (!DBNull.Value.Equals(bdRd["SERVICIO"])) { item.SERVICIO = Convert.ToInt32(bdRd["SERVICIO"]); }
                            if (!DBNull.Value.Equals(bdRd["HSALIDA"])) { item.HSALIDA = bdRd["HSALIDA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["PLACA"])) { item.PLACA = bdRd["PLACA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CAC_CONDUCTOR"])) { item.CAC_CONDUCTOR = bdRd["CAC_CONDUCTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["HORA_SALIDA_REAL"])) { item.HSALIDA_REAL = bdRd["HORA_SALIDA_REAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["PLACA_TIEMPOREAL"])) { item.PLACA_TIEMPOREAL = bdRd["PLACA_TIEMPOREAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CAC_TIEMPOREAL"])) { item.CAC_TIEMPOREAL = Convert.ToInt32(bdRd["CAC_TIEMPOREAL"]); }


                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }

        public List<CC_PROGRAMA_RUTA> getviajesProgrServ(string fecha, int id_ruta, string sentido, int idservicio)
        {
            List<CC_PROGRAMA_RUTA> resultado = new List<CC_PROGRAMA_RUTA>();

            OracleParameter[] bdParameters = new OracleParameter[5];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters[1] = new OracleParameter("P_ID_SERVICIO", OracleDbType.Int32) { Value = idservicio };
            bdParameters[2] = new OracleParameter("P_FECHA", OracleDbType.Varchar2) { Value = fecha }; ;
            bdParameters[3] = new OracleParameter("P_ID_RUTA", OracleDbType.Int32) { Value = id_ruta }; ;
            bdParameters[4] = new OracleParameter("P_SENTIDO", OracleDbType.Varchar2) { Value = sentido }; ;


            using (var bdCmd = new OracleCommand("PKG_PROGRAMACION_BUSES.GET_VIAJES_PROGR_X_SERV", conn))
            {
                bdCmd.CommandType = CommandType.StoredProcedure;
                bdCmd.Parameters.AddRange(bdParameters);
                using (var bdRd = bdCmd.ExecuteReader(CommandBehavior.SingleResult))
                {
                    if (bdRd.HasRows)
                    {
                        while (bdRd.Read())
                        {
                            var item = new CC_PROGRAMA_RUTA();

                            if (!DBNull.Value.Equals(bdRd["ID_MSALIDA_PROG_DET"])) { item.ID_MSALIDA_PROG_DET = Convert.ToInt32(bdRd["ID_MSALIDA_PROG_DET"]); }
                            if (!DBNull.Value.Equals(bdRd["SERVICIO"])) { item.SERVICIO = Convert.ToInt32(bdRd["SERVICIO"]); }
                            if (!DBNull.Value.Equals(bdRd["HSALIDA"])) { item.HSALIDA = bdRd["HSALIDA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["PLACA"])) { item.PLACA = bdRd["PLACA"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CAC_CONDUCTOR"])) { item.CAC_CONDUCTOR = bdRd["CAC_CONDUCTOR"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["HORA_SALIDA_REAL"])) { item.HSALIDA_REAL = bdRd["HORA_SALIDA_REAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["PLACA_TIEMPOREAL"])) { item.PLACA_TIEMPOREAL = bdRd["PLACA_TIEMPOREAL"].ToString(); }
                            if (!DBNull.Value.Equals(bdRd["CAC_TIEMPOREAL"])) { item.CAC_TIEMPOREAL = Convert.ToInt32(bdRd["CAC_TIEMPOREAL"]); }

                            resultado.Add(item);
                        }
                    }
                }
            }
            return resultado;
        }


        public RPTA_GENERAL Actulizar_ViajeTiempoReal(int idsalida, string unidad, int cac_temporal, string hora_salida, string usureg_real)
        {
            DateTime dateTime = DateTime.UtcNow.Date;
            string fecha_actual = dateTime.ToString("dd/MM/yyyy");
            RPTA_GENERAL r = new RPTA_GENERAL();
            var id_usuario = 0;

            OracleParameter[] bdParameters = new OracleParameter[6];
            bdParameters[0] = new OracleParameter("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            bdParameters[1] = new OracleParameter("P_ID_MSALIDA_PROG_DET", OracleDbType.Int32) { Value = idsalida };
            bdParameters[2] = new OracleParameter("P_UNIDAD", OracleDbType.Varchar2) { Value = unidad };
            bdParameters[3] = new OracleParameter("P_CAC_TIEMPOREAL", OracleDbType.Int32) { Value = cac_temporal };
            bdParameters[4] = new OracleParameter("P_HORA_SALIDA", OracleDbType.Varchar2) { Value = hora_salida };
            bdParameters[5] = new OracleParameter("P_USU_REG_REAL", OracleDbType.Varchar2) { Value = usureg_real };


            try
            {
                using (var bdCmd = new OracleCommand("PKG_PROGRAMACION_BUSES.P_ACTUALIZAR_VIAJETIEMPOREAL", conn))
                {
                    bdCmd.CommandType = CommandType.StoredProcedure;
                    bdCmd.Parameters.AddRange(bdParameters);
                    bdCmd.ExecuteNonQuery();

                    r.COD_ESTADO = 1;
                    r.DES_ESTADO = "Se registró correctamente";
                }
            }
            catch (Exception ex)
            {
                r.AUX = 0;
                r.COD_ESTADO = 0;
                r.DES_ESTADO = ex.Message;
            }
            return r;
        }


    }
}
