﻿using DA;
using Dapper;
using ENTIDADES;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AD.EntidadesAD
{
    public class UsuarioAD : Repositorio<TB_USUARIO>, InterfaceAD<TB_USUARIO>
    {
        private readonly OracleConnection conn;
        public UsuarioAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

        public IEnumerable<TB_USUARIO> Datos(int activo)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, activo);
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var packages = "PKG_USUARIO.SP_DATOS";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.Query<TB_USUARIO>(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public int Eliminar(int id)
        {
            throw new NotImplementedException();
        }

        public TB_USUARIO Insertar(TB_USUARIO model)
        {
            var param = new
            {
                P_USUCOD = model.USUCOD,
                P_NDOCUMENTO = model.NDOCUMENTO,
                P_USUNOM = model.USUNOM,
                P_USUAPEPAT = model.USUAPEPAT,
                P_USUAPEMAT = model.USUAPEMAT,
                P_PERFIL = model.PERFIL,
                P_USUREG = model.USUREG,
                P_FECREG = model.FECREG,
                P_USUMOD = model.USUMOD,
                P_FECMOD = model.FECMOD,
                P_ESTREG = model.ESTREG
            };
            var packages = "PKG_USUARIO.SP_INSERTAR";
            var SP = CommandType.StoredProcedure;
            var result = conn.QueryFirstOrDefault<TB_USUARIO>(packages, param, commandType: SP);
            return result;
        }

        public dynamic Modificar(TB_USUARIO model)
        {
            var bdParameters = new OracleDynamicParameters();
            bdParameters.Add("P_USUCOD", OracleDbType.Int32, ParameterDirection.Input, model.USUCOD);
            bdParameters.Add("P_NDOCUMENTO", OracleDbType.Varchar2, ParameterDirection.Input, model.NDOCUMENTO );
            bdParameters.Add("P_USUNOM", OracleDbType.Varchar2, ParameterDirection.Input, model.USUNOM);
            bdParameters.Add("P_USUAPEPAT", OracleDbType.Varchar2, ParameterDirection.Input, model.USUAPEPAT);
            bdParameters.Add("P_USUAPEMAT", OracleDbType.Varchar2, ParameterDirection.Input, model.USUAPEMAT);
            bdParameters.Add("P_PERFIL", OracleDbType.Varchar2, ParameterDirection.Input, model.PERFIL);
            bdParameters.Add("P_USUREG", OracleDbType.Varchar2, ParameterDirection.Input, model.USUREG);
            bdParameters.Add("P_FECREG", OracleDbType.Date, ParameterDirection.Input, model.FECREG);
            bdParameters.Add("P_USUMOD", OracleDbType.Varchar2, ParameterDirection.Input, model.USUMOD);
            bdParameters.Add("P_FECMOD", OracleDbType.Date, ParameterDirection.Input, model.FECMOD);
            bdParameters.Add("P_ESTREG", OracleDbType.Int32, ParameterDirection.Input, model.ESTREG);
            var packages = "PKG_USUARIO.SP_MODIFICAR";
            var SP = CommandType.StoredProcedure;
            var result = SqlMapper.QueryFirstOrDefault(conn, packages, param: bdParameters, commandType: SP);
            return result;
        }

        public TB_USUARIO ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }
}
