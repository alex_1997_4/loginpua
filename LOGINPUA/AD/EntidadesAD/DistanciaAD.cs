﻿using DA;
using Dapper;
using Entidades;
using Oracle.DataAccess.Client;
using Repositorio.EntidadesRepositorio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositorio;
using ENTIDADES;

namespace AD.EntidadesAD
{
    public class DistanciaAD : Repositorio<CC_DISTANCIA_PARADEROS>, IDistancia_Paraderos
    {
        private readonly OracleConnection conn;
        public DistanciaAD()
        {
            conn = new OracleConnection(Configuracion.GetConectionSting("sConexionSISCC"));
        }

       
      

         
        public CC_DISTANCIA_PARADEROS Consultar_Distancias(string ida, string vuelta)
        {

            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_DISTANCIA_PARADEROS.CONSULTAR_DISTANCIAS";
            bdParameters.Add("DIS_PARADERO_IDA_", OracleDbType.Varchar2, ParameterDirection.Input, ida);
            bdParameters.Add("DIS_PARADERO_VUELTA_", OracleDbType.Varchar2, ParameterDirection.Input, vuelta);

            var result = SqlMapper.QueryFirst<CC_DISTANCIA_PARADEROS>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public CC_DISTANCIA_PARADEROS Insertar(CC_DISTANCIA_PARADEROS model)
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);

            var query = "PKG_DISTANCIA_PARADEROS.SP_INSERTAR_DISTANCIA";
            bdParameters.Add("DIS_ID", OracleDbType.Int32, ParameterDirection.Input, model.DIS_ID);
            bdParameters.Add("DIS_PARADERO_IDA", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_PARADERO_IDA);
            bdParameters.Add("DIS_PARADERO_VUELTA", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_PARADERO_VUELTA);
            bdParameters.Add("DIS_RECORRIDO_KM", OracleDbType.BinaryFloat, ParameterDirection.Input, model.DIS_RECORRIDO_KM);
            bdParameters.Add("ID_CORREDOR", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);
            bdParameters.Add("SALIDA_BUS", OracleDbType.Varchar2, ParameterDirection.Input, model.SALIDA_BUS);
            bdParameters.Add("DIS_DISTANCIA_PROGRAMADA", OracleDbType.BinaryFloat, ParameterDirection.Input, model.DIS_DISTANCIA_PROGRAMADA);
            bdParameters.Add("DIS_DIA", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_DIA);
            bdParameters.Add("DIS_RUTA", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_RUTA);
            bdParameters.Add("DIS_OPERADOR", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_OPERADOR);
            bdParameters.Add("CC_SENTIDO", OracleDbType.Varchar2, ParameterDirection.Input, model.CC_SENTIDO);

            var datos = SqlMapper.QueryFirstOrDefault<CC_DISTANCIA_PARADEROS>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);

            return datos;
        }

        public List<CC_DISTANCIA_PARADEROS> Lista_Distancia()
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_DISTANCIA_PARADEROS.SP_LISTAR_DISTANCIAS";
            var result = SqlMapper.Query<CC_DISTANCIA_PARADEROS>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure).ToList();
            return result;
        }


        public CC_DISTANCIA_PARADEROS Buscar_x_ID(int id)
        {

            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);
            var query = "PKG_DISTANCIA_PARADEROS.SP_BUSCAR_ID";
            bdParameters.Add("DIS_ID_", OracleDbType.Int32, ParameterDirection.Input, id);
            var result = SqlMapper.QueryFirst<CC_DISTANCIA_PARADEROS>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public void Editar_Distancia(CC_DISTANCIA_PARADEROS model)
        {

            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            var query = "PKG_DISTANCIA_PARADEROS.SP_EDITAR_DISTANCIA";
            bdParameters.Add("DIS_ID_", OracleDbType.Int32, ParameterDirection.Input, model.DIS_ID);
            bdParameters.Add("DIS_PARADERO_IDA_ ", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_PARADERO_IDA);
            bdParameters.Add("DIS_PARADERO_VUELTA_", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_PARADERO_VUELTA);
            bdParameters.Add("DIS_RECORRIDO_KM_", OracleDbType.BinaryFloat, ParameterDirection.Input, model.DIS_RECORRIDO_KM);
            bdParameters.Add("ID_CORREDOR_", OracleDbType.Varchar2, ParameterDirection.Input, model.ID_CORREDOR);
            bdParameters.Add("SALIDA_BUS_", OracleDbType.Varchar2, ParameterDirection.Input, model.SALIDA_BUS);
            bdParameters.Add("DIS_DISTANCIA_PROGRAMADA_", OracleDbType.BinaryFloat, ParameterDirection.Input, model.DIS_DISTANCIA_PROGRAMADA);
            bdParameters.Add("DIS_DIA_", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_DIA);
            bdParameters.Add("DIS_RUTA_", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_RUTA);
            bdParameters.Add("DIS_OPERADOR_", OracleDbType.Varchar2, ParameterDirection.Input, model.DIS_OPERADOR);
            bdParameters.Add("CC_SENTIDO_", OracleDbType.Varchar2, ParameterDirection.Input, model.CC_SENTIDO);

            SqlMapper.Execute(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);


        }

        public CC_DISTANCIA_PARADEROS Ultimo_Registro()
        {
            var bdParameters = new OracleDynamicParameters();
            //string codigo = Encriptador.Encriptar(model.LOGPAS);  
            bdParameters.Add("P_CURSOR", OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var query = "PKG_DISTANCIA_PARADEROS.SP_ULTIMO_REGISTRO";
            var result = SqlMapper.QueryFirst<CC_DISTANCIA_PARADEROS>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
            return result;
        }


        public void Eliminar_x_ID(int id)
        {
            var bdParameters = new OracleDynamicParameters();
            var query = "PKG_DISTANCIA_PARADEROS.SP_ELIMINAR_X_ID";
            bdParameters.Add("DIS_ID_", OracleDbType.Int32, ParameterDirection.Input, id);
            SqlMapper.QueryFirstOrDefault<CC_DISTANCIA_PARADEROS>(conn, query, param: bdParameters, commandType: CommandType.StoredProcedure);
        }

    }
}
