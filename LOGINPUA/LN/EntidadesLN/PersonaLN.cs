﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class PersonaLN
    {
        private Object bdConn;
        private readonly PersonaAD persona;
 
        public PersonaLN()
        {
            PersonaAD paraderoRepositorio = new PersonaAD(ref bdConn);
            persona = paraderoRepositorio;
        }

        public List<CC_TIPODOCUMENTO> ListarTipoDocumento()
        {
            var datos = persona.ListarTipoDocumento();
            return datos;
        }

        public List<CC_ROL> listarTipoRol()
        {
            var datos = persona.listarTipoRol();
            return datos;
        }

        public RPTA_GENERAL registrarPersona(string nombre, string apepaterno, string apematerno, string numdocu, int tipodocu, string correo, int tiporol, string usuario)
        {

            var datos = persona.registrarPersona(nombre, apepaterno, apematerno, numdocu, tipodocu, correo, tiporol, usuario);
            return datos;
        }

        public List<CC_PERSONA> getlistaPersonas()
        {

            var datos = persona.getlistaPersonas();
            return datos;
        }
        public RPTA_GENERAL ModificarPersonas(int idpersona, string nombre, string apepaterno, string apematerno, int numdocu, int tipodocu, string correo, int tiporol, string usuario)
        {

            var datos = persona.ModificarPersonas(idpersona,nombre,apepaterno,apematerno,numdocu,tipodocu,correo,tiporol,usuario);
            return datos;
        }

        public RPTA_GENERAL EliminarPersonas(int idpersona, string usuario)
        {
            var datos = persona.EliminarPersonas(idpersona, usuario);
            return datos;
        }

        public RPTA_GENERAL EditarDatosPersonas(int idpersona, string nombre, string apepaterno, string apematerno, string numdocu, string correo,string usuario)
        {
            var datos = persona.EditarDatosPersonas(idpersona, nombre, apepaterno, apematerno, numdocu, correo, usuario);
            return datos;
        }

    }
}

