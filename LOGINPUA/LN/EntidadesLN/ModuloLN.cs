﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class ModuloLN
    {
        private Object bdConn;
        private readonly ModuloAD _perfil;


        public ModuloLN()
        {
            ModuloAD usuarioRepositorio = new ModuloAD(ref bdConn);
            _perfil = usuarioRepositorio;
        }
        public List<CC_MODULO> ListarModulo(int idmodalidad)
        {
            var datos = _perfil.ListarModulo(idmodalidad);
            return datos;
        }
        public RPTA_GENERAL AnularModulo(int idperfil, string session_usuario)
        {
            var datos = _perfil.AnularModulo(idperfil, session_usuario);
            return datos;
        }
        public List<CC_MODALIDAD> ListarModalidad()
        {
            var listar = _perfil.ListarModalidad();
             return listar;
        }

        public RPTA_GENERAL modificarModulo(int idmodulo, int id_modalidad, string nombre, string url, string session_usuario)
        {
            var listar = _perfil.modificarModulo(idmodulo,id_modalidad,nombre,url,session_usuario);
            return listar;
        }
    

        public RPTA_GENERAL AgregarModulo(int idmodalidad, string nombre, string url, string img, string session_usuario)
        {
            var datos = _perfil.AgregarModulo(idmodalidad, nombre, url, img, session_usuario);
            return datos;
        }
        public RPTA_GENERAL modificarModulo_IMG(int idmodalidad, string nombre,string session_usuario)
        {
            var datos = _perfil.modificarModulo_IMG(idmodalidad, nombre,session_usuario);
            return datos;
        }

        



    }
}

