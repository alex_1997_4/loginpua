﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class TipoServicioLN
    {
        private Object bdConn;
        private readonly TipoServicioAD tipoServicio;
        public TipoServicioLN()
        {
            TipoServicioAD tipoServicioRepositorio = new TipoServicioAD(ref bdConn);
            tipoServicio = tipoServicioRepositorio;
        }

        public List<CC_RUTA_TIPO_SERVICIO> getRutaTipoDeServicioOper(int id_ruta)
        {
            var datos = tipoServicio.getRutaTipoDeServicioOper(id_ruta);
            return datos;
        }

        public List<CC_RUTA_TIPO_SERVICIO> getTiposServicio()
        {
            var datos = tipoServicio.getTiposServicio();
            return datos;
        }

        public List<CC_RUTA_TIPO_SERVICIO> getTiposServicioOperacional()
        {
            var datos = tipoServicio.getTiposServicioOperacional();
            return datos;
        }

        public RPTA_GENERAL registrarRutaTipoServicio(int idRuta, int idTipoServicio, int idTipoServicioOper, string nombre, string usuarioRegistra)
        {
            var datos = tipoServicio.registrarRutaTipoServicio(idRuta, idTipoServicio, idTipoServicioOper, nombre, usuarioRegistra);
            return datos;
        }

        public RPTA_GENERAL editarRutaTservic(int idRutaTipoServ, int idTipoServicio, int idTipoServicioOper, string nombre, string usuarioModifica)
        {
            var rpta = tipoServicio.editarRutaTservic(idRutaTipoServ, idTipoServicio, idTipoServicioOper, nombre, usuarioModifica);
            return rpta;
        }

        public RPTA_GENERAL anularRutaTipoServicio(int idRutaServicioOperacional, string usu_anula)
        {
            var datos = tipoServicio.anularRutaTipoServicio(idRutaServicioOperacional, usu_anula);
            return datos;
        }

        public RPTA_GENERAL registraRecorridoServicioOperacional(int idRutaTipoServicio, string lado, string sentido, string usuarioReg)
        {
            var datos = tipoServicio.registraRecorridoServicioOperacional(idRutaTipoServicio, lado, sentido, usuarioReg);
            return datos;
        }

        public RPTA_GENERAL actualizarParaderoTservic(int idParaderoTipoServicio, int idEstado, string usu_modif)
        {
            var datos = tipoServicio.actualizarParaderoTservic(idParaderoTipoServicio, idEstado, usu_modif);
            return datos;
        }
        
        public List<CC_PARADERO> getParaderoByIdRecorridTServ(int idRecorridoTipoServicio)
        {
            var datos = tipoServicio.getParaderoByIdRecorridTServ(idRecorridoTipoServicio);
            return datos;
        }
    }
}
