﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;
using ENTIDADES.InnerJoin;

namespace LN.EntidadesLN
{
    public class LogAccLN : InterfaceLN<TV_LOG_ACC>
    {
        string mensaje = "";
        int tipo = 0;
        private readonly LogAccAD _LogAccAD;
        public LogAccLN()
        {
            LogAccAD LogAccAD = new LogAccAD();
            _LogAccAD = LogAccAD;
        }

        public List<TV_LOG_ACC> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TV_LOG_ACC> listaLogAcc = new List<TV_LOG_ACC>();
            try
            {
                listaLogAcc = _LogAccAD.Datos(activo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaLogAcc = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaLogAcc;
        }

        public int Modificar(TV_LOG_ACC modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _LogAccAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }

        public int Insertar(TV_LOG_ACC modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _LogAccAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return modelo == null ? 0 : 1;
        }

        public int Eliminar(TV_LOG_ACC modelo)
        {
            throw new NotImplementedException();
        }

        public TV_LOG_ACC ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TV_LOG_ACC TV_LOG_ACC = new TV_LOG_ACC();
            try
            {
                TV_LOG_ACC = _LogAccAD.ObtenerPorTablaFiltroId("TV_LOG_ACC", "LOGCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TV_LOG_ACC = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TV_LOG_ACC;
        }
        public List<TV_LOG_ACC> ObtenerListadoPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TV_LOG_ACC> TV_LOG_ACC = new List<TV_LOG_ACC>();
            try
            {
                TV_LOG_ACC = _LogAccAD.ObtenerListaPorTablaFiltroId("TV_LOG_ACC", "LOGCOD", id).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TV_LOG_ACC = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TV_LOG_ACC;
        }

        public List<InnerJoinAccesos> ObtenerListadoDeAccesosPorLogCod(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<InnerJoinAccesos> InnerJoinAccesos = new List<InnerJoinAccesos>();
            try
            {
                InnerJoinAccesos = _LogAccAD.ObtenerListadoDeAccesosPorLogCod(id,0).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                InnerJoinAccesos = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return InnerJoinAccesos;
        }
    }

}
