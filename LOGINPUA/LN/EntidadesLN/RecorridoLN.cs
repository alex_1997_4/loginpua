﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class RecorridoLN
    {
        private Object bdConn;
        private readonly RecorridoAD recorrido;

        public RecorridoLN()
        {
            RecorridoAD recorridoRepositorio = new RecorridoAD(ref bdConn);
            recorrido = recorridoRepositorio;
        }

        public List<CC_RECORRIDO> getRecorridosByRuta(int idRuta)
        {
            var datos = recorrido.getRecorridosByRuta(idRuta);
            return datos;
        }
    }
}
