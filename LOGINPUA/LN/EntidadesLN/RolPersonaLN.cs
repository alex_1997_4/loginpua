﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class RolPersonaLN
    {
        private Object bdConn;
        private readonly RolPersonaAD _rolPersona;


        public RolPersonaLN()
        {
            RolPersonaAD usuarioRepositorio = new RolPersonaAD(ref bdConn);
            _rolPersona = usuarioRepositorio;
        }

        public List<CC_ROL> ListarRol()
        {
            var datos = _rolPersona.ListarRol();
            return datos;
        }

        public RPTA_GENERAL AgregarRolPersona(string nombre, string usuario)
        {
            var datos = _rolPersona.AgregarRol(nombre, usuario);
            return datos;
        }

        public RPTA_GENERAL AnularRol(int idrol, string session_usuario)
        {
            var rpta = _rolPersona.AnularRol(idrol, session_usuario);
            return rpta;
        }

        public RPTA_GENERAL ModificarRol(int idrol, string nombre, string session_usuario)
        {
            var datos = _rolPersona.ModificarRol(idrol, nombre, session_usuario);
            return datos;
        }



        //public RPTA_GENERAL AgregarPerfil_modalidad(int id_perfil, int id_modalidad, string session_usuario)
        //{
        //    var datos = _perfil.AgregarPerfil_modalidad(id_perfil, id_modalidad, session_usuario);
        //    return datos;
        //}

        //public List<CC_PERFIL_USUARIO> Validar_Perfil(int idperfil)
        //{
        //    var rpta = _perfil.Validar_Perfil(idperfil);
        //    return rpta;
        //}


    }
}

