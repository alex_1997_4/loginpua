﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class PlacaFiscalizacionLN
    {
        private Object bdConn;
        private readonly PlacaFiscalizacion placaFiscalizacion;

        public PlacaFiscalizacionLN()
        {
            PlacaFiscalizacion placaFiscalizacionRepositorio = new PlacaFiscalizacion(ref bdConn);
            placaFiscalizacion = placaFiscalizacionRepositorio;
        }

        public List<CONADISPLACA> getPlacasConadis()
        {
            var datos = placaFiscalizacion.getPlacasConadis();
            return datos;
        }

        public List<VECINOSPLACA> getplacasVecinosEmp()
        {
            var datos = placaFiscalizacion.getplacasVecinosEmp();
            return datos;
        }
    }
}
