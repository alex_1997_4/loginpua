﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class EmpresaLN : InterfaceLN<TB_EMPRESA>
    {
        string mensaje = "";
        int tipo = 0;
        bool valor = true;
        private readonly EmpresaAD _EmpresaAD;
        public EmpresaLN()
        {
            EmpresaAD EmpresaAD = new EmpresaAD();
            _EmpresaAD = EmpresaAD;
        }

        public List<TB_EMPRESA> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TB_EMPRESA> listaEmpresa = new List<TB_EMPRESA>();
            try
            {
                listaEmpresa = _EmpresaAD.Datos(activo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaEmpresa = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaEmpresa;
        }

        public int Modificar(TB_EMPRESA modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _EmpresaAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor == null ? tipo : tipo;
        }

        public int Insertar(TB_EMPRESA modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _EmpresaAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }

        public int Eliminar(TB_EMPRESA modelo)
        {
            bool ValorLN = true;
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                ValorLN = _EmpresaAD.ModificarActivo("TB_EMPRESA", 0, "EMPCOD", modelo.EMPCOD, modelo.USUMOD);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                valor = false;
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                valor = ValorLN;
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }

        public TB_EMPRESA ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TB_EMPRESA TB_EMPRESA = new TB_EMPRESA();
            try
            {
                TB_EMPRESA = _EmpresaAD.ObtenerPorTablaFiltroId("TB_EMPRESA", "EMPCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TB_EMPRESA = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TB_EMPRESA;
        }
    }

}
