﻿using System;
using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class CorredoresLN
    {
        private Object bdConn;

        private readonly CorredorAD corredorAD;
        public CorredoresLN()
        {
            CorredorAD corredorRepositorio = new CorredorAD(ref bdConn);
            corredorAD = corredorRepositorio;
        }

        public List<CC_CORREDOR> obtenerListaCorredores()
        {
            var datos = corredorAD.getListaCorredor();
            return datos;
        }
        public List<CC_CORREDOR> getRutaCorredor()
        {
            var datos = corredorAD.getRutaCorredor();
            return datos;
        }
    }
}
