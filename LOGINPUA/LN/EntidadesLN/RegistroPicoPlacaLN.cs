﻿using System;
using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class RegistroPicoPlacaLN
    {
        private readonly RegistroPicoPlacaAD registroPicoplaca = new RegistroPicoPlacaAD();
        public RegistroPicoPlacaLN()
        {
            RegistroPicoPlacaAD registroPicoplacaRepositorio = new RegistroPicoPlacaAD();
            registroPicoplaca = registroPicoplacaRepositorio;
        }


        public List<CC_REPORTE_PICO_PLACA> getReporte_Comparativo(string fechaInicio, int id_ruta, string turno, string ida, string vuelta)
        {
            var datos = registroPicoplaca.getReporte_Comparativo(fechaInicio, id_ruta, turno, ida, vuelta);
            return datos;
        }

        public List<CC_REPORTE_PICO_PLACA> getHora_Comparativo(string fechaInicio, int id_ruta, string turno)
        {
            var datos = registroPicoplaca.getHora_Comparativo(fechaInicio, id_ruta, turno);
            return datos;
        }


        public RPTA_GENERAL registrarVelocidadPPlaca(int idRuta, 
                                                    int idEstado, 
                                                    string turno, 
                                                    string fechaRegistro, 
                                                    string horaInicio, 
                                                    string horaFin, 
                                                    double velocidadPromedio_ab,
                                                    double velocidadPromedio_ba,
                                                    double  distancia_A,
                                                    double distancia_B,
                                                    double tiempo_A, 
                                                    double tiempo_B,
                                                    string observacion, 
                                                    string nomUsuario)
        {
            RPTA_GENERAL datos = new RPTA_GENERAL();
            datos = registroPicoplaca.registrarPicoPlaca(   idRuta,
                                                                idEstado,
                                                                turno,
                                                                fechaRegistro,
                                                                horaInicio,
                                                                horaFin,
                                                                velocidadPromedio_ab,
                                                                velocidadPromedio_ba,
                                                                distancia_A,
                                                                distancia_B,
                                                                tiempo_A, 
                                                                tiempo_B,
                                                                observacion,
                                                                nomUsuario);
            return datos;
        }

        public RPTA_GENERAL anularRegistrosPorFecha(string fechaRegistro, int idRuta) {
            var datos = registroPicoplaca.anularRegistrosPorFecha(fechaRegistro, idRuta);
            return datos;
        }

        public CC_RECORRIDO getKmPorLadoYRecorrido(int idRuta, string lado) {
            var datos = registroPicoplaca.getKmPorLadoYRecorrido(idRuta, lado);
            return datos;
        }

        public List<CC_REPORTE_PICO_PLACA> getReportePicoPlacaByFechas(string fechaInicio, string fechaFin, int idRuta)
        {
            var datos = registroPicoplaca.getReportePicoPlacaByFechas(fechaInicio, fechaFin, idRuta);
            return datos;
        }
    }
}
