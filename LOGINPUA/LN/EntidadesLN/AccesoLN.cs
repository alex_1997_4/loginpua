﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class AccesoLN : InterfaceLN<TM_ACCESO>
    {
        string mensaje = "";
        int tipo = 0;
        bool valor = true;
        private readonly AccesoAD _AccesoAD;
        private readonly AccEmpAD _AccEmpAD;
        public AccesoLN()
        {
            AccEmpAD AccEmpAD = new AccEmpAD();
            _AccEmpAD = AccEmpAD;
            AccesoAD AccesoAD = new AccesoAD();
            _AccesoAD = AccesoAD;
        }
        public List<TM_ACCESO> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TM_ACCESO> listaAcceso = new List<TM_ACCESO>();
            try
            {
                listaAcceso = _AccesoAD.Datos(activo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaAcceso = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaAcceso;
        }
        public List<TM_ACCESO> DatosDe(int codigo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TM_ACCESO> listaAcceso = new List<TM_ACCESO>();
            try
            {
                listaAcceso = _AccesoAD.DatosDe(codigo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaAcceso = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaAcceso;
        }
        public string InsertarAcceso(TM_ACCESO modelo, TV_ACC_EMP modelo1)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                
                _AccesoAD.Insertar(modelo);
                modelo.ACCCOD = _AccesoAD.CodigoDeUltimoRegistro("TM_ACCESO", "ACCCOD", "ACCCOD");
                //_AccesoAD.Insertar(modelo);
                modelo1.ACCEMPCOD = _AccEmpAD.CodigoDeUltimoRegistro("TV_ACC_EMP", "ACCEMPCOD", "ACCEMPCOD");
                modelo1.ACCCOD = modelo.ACCCOD;
                _AccEmpAD.Insertar(modelo1);
                tipoLN = 1;
                mensajeLN = "Valido1";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
        public string ModificarAcceso(TM_ACCESO modelo, TV_ACC_EMP modelo1)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                if (modelo.ACCICO == "Util/Image/NoExisteImagen")
                {
                    valor = _AccesoAD.ModificarSinImagen(modelo);
                    mensajeLN = "Valido1";
                }
                else if (modelo.ACCICO != "Util/Image/NoExisteImagen")
                {
                    valor = _AccesoAD.Modificar(modelo);
                    mensajeLN = "Valido";
                }
                valor = _AccEmpAD.Modificar(modelo1);
                tipoLN = 1;
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
        public string EliminarAcceso(TM_ACCESO modelo, TV_ACC_EMP modelo1)
        {
            bool ValorLN = true;
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                ValorLN = _AccesoAD.ModificarActivo("TM_ACCESO", 0, "ACCCOD", modelo.ACCCOD, modelo.USUMOD);
                ValorLN = _AccEmpAD.ModificarActivo("TV_ACC_EMP", 0, "ACCEMPCOD", modelo1.ACCEMPCOD, modelo1.USUMOD);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                valor = false;
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                valor = ValorLN;
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
        public int Modificar(TM_ACCESO modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _AccesoAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }
        public int Insertar(TM_ACCESO modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _AccesoAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipoLN;
        }
        public int Eliminar(TM_ACCESO modelo)
        {
            bool ValorLN = true;
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                ValorLN = _AccesoAD.ModificarActivo("TM_ACCESO",0, "ACCCOD",modelo.ACCCOD, modelo.USUMOD);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                valor = false;
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                valor = ValorLN;
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }
        public TM_ACCESO ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TM_ACCESO TM_ACCESO = new TM_ACCESO();
            try
            {
                TM_ACCESO = _AccesoAD.ObtenerPorTablaFiltroId("TM_ACCESO", "ACCCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TM_ACCESO = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TM_ACCESO;
        }
    }

}
