﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class ModalidadTransporteLN
    {
        private Object bdConn;
        private readonly ModalidadTransporteAD modalidadTransporte;
        public ModalidadTransporteLN()
        {
            ModalidadTransporteAD modalidadTransporteRepositorio = new ModalidadTransporteAD(ref bdConn);
            modalidadTransporte = modalidadTransporteRepositorio;
        }
        public List<CC_MODALIDAD_TRANSPORTE> getModalidadTransporte()
        {
            var datos = modalidadTransporte.getModalidadTransporte();
            return datos;
        }
    }
}
