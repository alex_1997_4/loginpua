﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;

namespace LN.EntidadesLN
{
    public class SalidaProgramadaLN
    {
        private Object bdConn;
        private readonly SalidaProgramadaAD salida;
        public SalidaProgramadaLN()
        {
            SalidaProgramadaAD salidaRepositorio = new SalidaProgramadaAD(ref bdConn);
            salida = salidaRepositorio;
        }

        public RPTA_GENERAL registrarMaestroSalidaProgramada(int idTipoDia, int idRutaTipoServicio, string fechaProgramacion, string nomUsuario)
        {
            var rpta = salida.registrarMaestroSalidaProgramada(idTipoDia, idRutaTipoServicio, fechaProgramacion, nomUsuario);
            return rpta;
        }

        public RPTA_GENERAL guardarMSalidaProgramadaDet(int idMaestroSalidaProg, string tipodia, string servicio,
                                                        string pog, string pot, string fnode, string hSalida,
                                                        string hLlegada, string tNode, string PIG, double layover, string acumulado,
                                                        string sentido, string turno, string tipoServicio, string placa,
                                                        string cacConductor, string usuarioRegistro)
        {
            var rpta = salida.guardarMSalidaProgramadaDet( idMaestroSalidaProg, tipodia, servicio, pog, 
                                                           pot, fnode, hSalida, hLlegada, tNode, PIG, layover,
                                                           acumulado, sentido, turno, tipoServicio, placa,
                                                           cacConductor, usuarioRegistro);
            return rpta;
        }

        public RPTA_GENERAL verifica_maestro_prog(int idTipoDia, string fechaProgramacion)
        {
            var rpta = salida.verifica_maestro_prog(idTipoDia, fechaProgramacion);
            return rpta;
        }

        
         public RPTA_GENERAL anular_maestro_prog(int idmaestro)
        {
            var rpta = salida.anular_maestro_prog(idmaestro);
            return rpta;
        }

        public List<CC_RESUMEN_PROGRAMADO> getResumenProgramado(int idServicio)
        {
            var rpta = salida.getResumenProgramado(idServicio);
            return rpta;
        }
        public List<CC_RUTA_TIPO_SERVICIO> getTipoServicioByCorredor(int idCorredor)
        {
            var rpta = salida.getTipoServicioByCorredor(idCorredor);
            return rpta;
        }

        public List<CC_DATA_VIAJE_PROGRAMADO> getDataViajesProgramacion(int idMaestroSalidaProg)
        {
            var rpta = salida.getDataViajesProgramacion(idMaestroSalidaProg);
            return rpta;
        }
    }
}
