﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;
using ENTIDADES.InnerJoin;

namespace LN.EntidadesLN
{
    public class UsuariosEmpresaLN : InterfaceLN<TM_USUARIOS_EMPRESA>
    {
        string mensaje = "";
        int tipo = 0;
        bool valor = true;
        private readonly UsuariosEmpresaAD _UsuariosEmpresaAD;
        private readonly EmpresaAD _EmpresaAD;
        private readonly LoginAD _LoginAD;
        public UsuariosEmpresaLN()
        {
            UsuariosEmpresaAD UsuariosEmpresaAD = new UsuariosEmpresaAD();
            _UsuariosEmpresaAD = UsuariosEmpresaAD;
            EmpresaAD EmpresaAD = new EmpresaAD();
            _EmpresaAD = EmpresaAD;
            LoginAD LoginAD = new LoginAD();
            _LoginAD = LoginAD;
        }

        public List<TM_USUARIOS_EMPRESA> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TM_USUARIOS_EMPRESA> listaUsuariosEmpresa = new List<TM_USUARIOS_EMPRESA>();
            try
            {
                listaUsuariosEmpresa = _UsuariosEmpresaAD.Datos(activo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaUsuariosEmpresa = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaUsuariosEmpresa;
        }

        public object ListaUsuariosPorAsignacion(int activo, int codigo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            object objeto = new object();
            try
            {
                objeto = new
                {
                    TB_EMPRESA = _EmpresaAD.Datos(1),
                    TM_USUARIOS_EMPRESA = _UsuariosEmpresaAD.ObtenerListaPorTablaFiltroId("TM_USUARIOS_EMPRESA", "LOGCOD",codigo)
                };
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                objeto = new
                {
                    mensajeLN
                };
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return objeto;
        }

        public string CrearUsuarioEmpresa(TM_USUARIOS_EMPRESA modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                int id = _UsuariosEmpresaAD.CodigoDeUltimoRegistro("TM_USUARIOS_EMPRESA", "USUEMPCOD", "USUEMPCOD") + 1;

                _UsuariosEmpresaAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }

        public int ModificarUsuarioEmpresa(TM_USUARIOS_EMPRESA modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _UsuariosEmpresaAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }

        public int Modificar(TM_USUARIOS_EMPRESA modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _UsuariosEmpresaAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }

        public int Insertar(TM_USUARIOS_EMPRESA modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _UsuariosEmpresaAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }

        public int Eliminar(TM_USUARIOS_EMPRESA modelo)
        {
            throw new NotImplementedException();
        }

        public bool Validacion(List<TM_USUARIOS_EMPRESA> modelo, ref string mensajeAviso)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            bool valor = false;
            TB_LOGIN modelo2 = new TB_LOGIN();
            List<InnerJoinValidacion> modelo1 = new List<InnerJoinValidacion>();
            List<TM_USUARIOS_EMPRESA> modelo3 = new List<TM_USUARIOS_EMPRESA>();
            try
            {
                for (int i = 0; i < modelo.Count; i++)
                {
                    if (modelo[i].USUEMPUSU != null)
                    {
                        modelo2 = _LoginAD.ObtenerPorTablaFiltroId("TB_LOGIN", "LOGCOD", modelo[i].LOGCOD);
                        modelo3 = _UsuariosEmpresaAD.ObtenerListaPorTablaFiltroId("TM_USUARIOS_EMPRESA", "LOGCOD", modelo[i].LOGCOD).ToList();

                        modelo1 = _UsuariosEmpresaAD.Validacion(modelo[i]);
                        if (modelo3[i].USUEMPUSU != modelo1[0].USUEMPUSU)
                        {
                            if (modelo1.Count >= 1)
                            {

                                mensajeAviso = "El Usuario '" + modelo2.LOGUSU + "' " +
                                          "no se le puede asignar el usuario de Acceso '" + modelo1[0].USUEMPUSU + "'" +
                                          ", por que ya se le encuentra asignado al usuario '" + modelo1[0].USUNOM + "'";
                                i = modelo.Count;
                                valor = true;
                            }
                        }
                    }
                }
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = false;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }

        public TM_USUARIOS_EMPRESA ObtenerPorCodigo(int id, int empresacod)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TM_USUARIOS_EMPRESA UsuariosEmpresa = new TM_USUARIOS_EMPRESA();
            try
            {
                UsuariosEmpresa = _UsuariosEmpresaAD.ObtenerPorCodigo(id,empresacod);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                UsuariosEmpresa = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return UsuariosEmpresa;
        }

        public TM_USUARIOS_EMPRESA ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TM_USUARIOS_EMPRESA TM_USUARIOS_EMPRESA = new TM_USUARIOS_EMPRESA();
            try
            {
                TM_USUARIOS_EMPRESA = _UsuariosEmpresaAD.ObtenerPorTablaFiltroId("TM_USUARIOS_EMPRESA", "USUEMPCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TM_USUARIOS_EMPRESA = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TM_USUARIOS_EMPRESA;
        }

        public string AsignarUsuario(List<TM_USUARIOS_EMPRESA> modelo) {
            int tipoLN = 0;
            string mensajeLN = "";
            int count = 0;
            List<TM_USUARIOS_EMPRESA> TM_USUARIOS_EMPRESA = new List<TM_USUARIOS_EMPRESA>();
            try
            {
                TM_USUARIOS_EMPRESA = _UsuariosEmpresaAD.ObtenerListaPorTablaFiltroId("TM_USUARIOS_EMPRESA", "LOGCOD", modelo[0].LOGCOD).OrderBy(x => x.EMPCOD).ToList();
                modelo = modelo.OrderBy(x => x.EMPCOD).ToList();
                for (int i = 0; i < modelo.Count; i++)
                {
                    try
                    {
                        if (modelo[i].EMPCOD == TM_USUARIOS_EMPRESA[i].EMPCOD)
                        {
                            modelo[i].ESTREG = 1;
                            modelo[i].FECMOD = DateTime.Now;
                            modelo[i].USUEMPCOD = TM_USUARIOS_EMPRESA[i].USUEMPCOD;
                            _UsuariosEmpresaAD.Modificar(modelo[i]);
                        }
                        else
                        {
                            modelo[i].ESTREG = 1;
                            modelo[i].FECREG = DateTime.Now;
                            modelo[i].USUEMPCOD = _UsuariosEmpresaAD.CodigoDeUltimoRegistro("TM_USUARIOS_EMPRESA", "USUEMPCOD", "USUEMPCOD") + 1;
                            _UsuariosEmpresaAD.Insertar(modelo[i]);
                        }
                    }
                    catch (Exception)
                    {

                        modelo[i].ESTREG = 1;
                        modelo[i].FECREG = DateTime.Now;
                        modelo[i].USUEMPCOD = _UsuariosEmpresaAD.CodigoDeUltimoRegistro("TM_USUARIOS_EMPRESA", "USUEMPCOD", "USUEMPCOD") + 1;
                        _UsuariosEmpresaAD.Insertar(modelo[i]);
                    }
                }
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
    }

}
