﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;
using ENTIDADES.InnerJoin;

namespace LN.EntidadesLN
{
    public class UsuarioLN : InterfaceLN<TB_USUARIO>
    {
        string mensaje = "";
        int tipo = 0;
        bool valor = true;
        private readonly UsuarioAD _UsuarioAD;
        private readonly LogAccAD _LogAccAD;
        private readonly LoginAD _LoginAD;
        private readonly UsuariosEmpresaAD _UsuariosEmpresaAD;
        public UsuarioLN()
        {
            UsuarioAD UsuarioAD = new UsuarioAD();
            _UsuarioAD = UsuarioAD;
            LogAccAD LogAccAD = new LogAccAD();
            _LogAccAD = LogAccAD;
            LoginAD LoginAD = new LoginAD();
            _LoginAD = LoginAD;
            UsuariosEmpresaAD UsuariosEmpresaAD = new UsuariosEmpresaAD();
            _UsuariosEmpresaAD = UsuariosEmpresaAD;
        }

        public string CrearUsuario(TB_USUARIO modelo, TB_LOGIN modelo1, List<TV_LOG_ACC> modelo2)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TM_USUARIOS_EMPRESA modelo3 = new TM_USUARIOS_EMPRESA();
            try
            {
                int id = _UsuarioAD.CodigoDeUltimoRegistro("TB_USUARIO", "USUCOD", "USUCOD") + 1;
                _UsuarioAD.Insertar(modelo);
                modelo1.LOGCOD = id;
                modelo1.ESTREG = modelo.ESTREG;
                modelo1.USUREG = modelo.USUREG;
                modelo1.FECREG = modelo.FECREG;
                modelo1 = _LoginAD.Insertar(modelo1);
                for (int i = 0; i < modelo2.Count; i++)
                {
                    modelo2[i].LOGCOD = id;
                    modelo2[i].ESTREG = modelo.ESTREG;
                    modelo2[i].USUREG = modelo.USUREG;
                    modelo2[i].FECREG = modelo.FECREG;
                    _LogAccAD.Insertar(modelo2[i]);
                }
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
        public string ModificarUsuario(TB_USUARIO modelo, TB_LOGIN modelo1, List<TV_LOG_ACC> modelo2)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 0;
            List<InnerJoinAccesos> InnerJoinAccesos = new List<InnerJoinAccesos>();
            try
            {
                valor = _UsuarioAD.Modificar(modelo);
                valor = _LoginAD.Modificar(modelo1);
                InnerJoinAccesos = _LogAccAD.ObtenerListadoDeAccesosPorLogCod(modelo.USUCOD, 0).ToList();
                if (InnerJoinAccesos.Count >= modelo2.Count)
                {
                    for (int i = 0; i < InnerJoinAccesos.Count; i++)
                    {
                        if (InnerJoinAccesos[i].ACCCOD == modelo2[i].ACCCOD)
                        {
                            modelo2[i].LOGACCCOD = InnerJoinAccesos[i].LOGACCCOD;
                            modelo2[i].USUMOD = modelo.USUMOD;
                            modelo2[i].FECMOD = modelo.FECMOD;
                            valor = _LogAccAD.Modificar(modelo2[i]);
                        }
                    }
                }
                else if (InnerJoinAccesos.Count < modelo2.Count)
                {
                    for (int i = 0; i < modelo2.Count; i++)
                    {
                        try
                        {
                            if (InnerJoinAccesos[i].ACCCOD == modelo2[i].ACCCOD)
                            {
                                modelo2[i].LOGACCCOD = InnerJoinAccesos[i].LOGACCCOD;
                                modelo2[i].USUMOD = modelo.USUMOD;
                                modelo2[i].FECMOD = modelo.FECMOD;
                                valor = _LogAccAD.Modificar(modelo2[i]);
                            }
                        }
                        catch (Exception)
                        {

                            modelo2[i].USUREG = modelo.USUMOD;
                            modelo2[i].FECREG = modelo.FECMOD;
                            modelo2[i].LOGCOD = modelo.USUCOD;
                            valor = _LogAccAD.Insertar(modelo2[i]);
                        }
                        //if (InnerJoinAccesos[i].ACCCOD == modelo2[i].ACCCOD)
                        //{
                        //    modelo2[i].LOGACCCOD = InnerJoinAccesos[i].LOGACCCOD;
                        //    modelo2[i].USUMOD = modelo.USUMOD;
                        //    modelo2[i].FECMOD = modelo.FECMOD;
                        //    valor = _LogAccAD.Modificar(modelo2[i]);
                        //}
                        //else
                        //{
                        //    modelo2[i].USUREG = modelo.USUMOD;
                        //    modelo2[i].FECREG = modelo.FECMOD;
                        //    modelo2[i].LOGCOD = modelo.USUCOD;
                        //    valor = _LogAccAD.Insertar(modelo2[i]);
                        //}
                    }
                }
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
        public string EliminarUsuario(TB_USUARIO modelo, TB_LOGIN modelo1, List<TV_LOG_ACC> modelo2)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            bool valorLN = true;
            TV_LOG_ACC modelo3 = new TV_LOG_ACC();
            try
            {
                valorLN = _UsuarioAD.ModificarActivo("TB_USUARIO", 0, "USUCOD", modelo.USUCOD, modelo.USUMOD);
                valorLN = _LoginAD.ModificarActivo("TB_LOGIN", 0, "LOGCOD", modelo1.LOGCOD, modelo1.USUMOD);
                for (int i = 0; i < modelo2.Count; i++)
                {
                    valorLN = _LoginAD.ModificarActivo("TV_LOG_ACC", 0, "LOGACCCOD", modelo2[i].LOGACCCOD, modelo1.USUMOD);
                }
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "" + e;
                valorLN = false;
                modelo = null;
            }
            finally
            {
                valor = valorLN;
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return mensaje;
        }
        public List<TB_USUARIO> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TB_USUARIO> listaUsuario = new List<TB_USUARIO>();
            try
            {
                listaUsuario = _UsuarioAD.Datos(activo).ToList();

                foreach (var item in listaUsuario)
                {
                    item.FECHA_REGISTRO=Convert.ToDateTime(item.FECHA_REGISTRO).ToString("dd/M/yyyy");
                }

                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaUsuario = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaUsuario;
        }

        public int Modificar(TB_USUARIO modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _UsuarioAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }

        public int Insertar(TB_USUARIO modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _UsuarioAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return modelo == null ? 0 : 1;
        }

        public int Eliminar(TB_USUARIO modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            bool valorLN = true;
            TV_LOG_ACC modelo3 = new TV_LOG_ACC();
            try
            {
                valorLN = _UsuarioAD.ModificarActivo("TB_USUARIO", 0, "USUCOD", modelo.USUCOD, modelo.USUMOD);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valorLN = false;
                modelo = null;
            }
            finally
            {
                valor = valorLN;
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return tipo;
        }

        public List<TB_USUARIO> Listar_Usuarios_CC(int empresa)
        {
            var result = _UsuarioAD.Listar_Usuarios_CC(empresa);

            return result;
        }

        public List<TB_USUARIO> Listar_Usuarios_Hacom()
        {
            var result = _UsuarioAD.Listar_Usuarios_Hacom();

            return result;
        }


        
        public TB_USUARIO ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TB_USUARIO TB_USUARIO = new TB_USUARIO();
            try
            {
                TB_USUARIO = _UsuarioAD.ObtenerPorTablaFiltroId("TB_USUARIO", "USUCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TB_USUARIO = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TB_USUARIO;
        }



    }

}
