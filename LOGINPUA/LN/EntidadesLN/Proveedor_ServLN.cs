﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class Proveedor_ServLN
    {
        private Object bdConn;
        private readonly Proveedor_ServAD _Proovedor_Serv;
        private readonly Usuario_PersonaAD Usuario_PersonaAD;


        public Proveedor_ServLN()
        {
            Proveedor_ServAD usuarioRepositorio = new Proveedor_ServAD(ref bdConn);
            _Proovedor_Serv = usuarioRepositorio;
        }
        //public List<CC_PERFIL_USUARIO> ListarPerfil(int idmodalidad)
        //{
        //    var datos = _perfil.ListarPerfil(idmodalidad);
        //    return datos;
        //}

        public List<CC_CORREDOR> ListCorredores(int id_prov_serv)
        {
            var datos = _Proovedor_Serv.ListCorredores(id_prov_serv);
            return datos;
        }

        public List<CC_PROVUSU_CORREDOR> ListCorredor_X_Usuprov(int id_prov_corredor)
        {
            var datos = _Proovedor_Serv.ListCorredor_X_Usuprov(id_prov_corredor);
            return datos;

        }
        public List<CC_PROVEEDORSERV> ListarProveedorServ()
        {
            var datos = _Proovedor_Serv.ListarProveedorServ();
            return datos;
        }
        public List<CC_PROVEEDORSERV_USUARIO> ListarProvServ_Usuario()
        {
            var datos = _Proovedor_Serv.ListarProvServ_Usuario();
            return datos;
        }
        public List<CC_USUARIO_PERSONA> ListarUsuarios()
        {
            var datos = _Proovedor_Serv.ListarUsuarios();
            return datos;
        }
        public RPTA_GENERAL Desactivar_ProvUsuarios(int idpersona, string session_usuario)
        {
            var datos = _Proovedor_Serv.Desactivar_Usuarios(idpersona, session_usuario);
            return datos;
        }
        public RPTA_GENERAL Desactivar_Cuenta_Usuario(int idpersona, string session_usuario)
        {
            var datos = _Proovedor_Serv.Desactivar_Cuenta_Usuario(idpersona, session_usuario);
            return datos;
        }

        public RPTA_GENERAL Actualizar_ContraseñaAnti(int idprov_usu)
        {
            var datos = _Proovedor_Serv.Actualizar_ContraseñaAnti(idprov_usu);
            return datos;
        }

        public CC_USUARIO_PERSONA Buscar_x_idusuario(int idusuario)
        {
            var datos = _Proovedor_Serv.Buscar_x_idusuario(idusuario);
            return datos;
        }
        public RPTA_GENERAL AgregarUsuario_Proveedor(int idprov, string usuario, string contraseña, string usuario_session)
        {
            var datos = _Proovedor_Serv.AgregarUsuario_Proveedor(idprov, usuario, contraseña, usuario_session);
            return datos;
        }
        public RPTA_GENERAL AgregarUsuario_Corredor(int idcorredores, int idusuario, string session_usuario)
        {
            var datos = _Proovedor_Serv.AgregarUsuario_Corredor(idcorredores, idusuario, session_usuario);
            return datos;
        }

        public RPTA_GENERAL Actualizar_Usuario_Prov(int idcorredores, int idusuario, string session_usuario)
        {
            var datos = _Proovedor_Serv.Actualizar_Usuario_Prov(idcorredores, idusuario, session_usuario);
            return datos;
        }
        public RPTA_GENERAL Rpta_Usuario_Existente(int idcorredores, int idusuario, int id_prov_serv)
        {
            var datos = _Proovedor_Serv.Rpta_Usuario_Existente(idcorredores, idusuario, id_prov_serv);
            return datos;
        }
        public RPTA_GENERAL Rpta_Cuenta_Existente(int idprov, string cuenta)
        {
            var datos = _Proovedor_Serv.Rpta_Cuenta_Existente(idprov, cuenta);
            return datos;
        }

        

    }
}

