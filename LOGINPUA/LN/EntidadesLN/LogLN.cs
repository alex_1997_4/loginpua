﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class LogLN : InterfaceLN<TL_LOG>
    {
        string mensaje = "";
        int tipo = 0;
        bool valor = true;
        private readonly LogAD _LogAD;
        public LogLN()
        {
            LogAD LogAD = new LogAD();
            _LogAD = LogAD;
        }

        public List<TL_LOG> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TL_LOG> listaLog = new List<TL_LOG>();
            try
            {
                listaLog = _LogAD.Datos(activo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaLog = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaLog;
        }

        public int Modificar(TL_LOG modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _LogAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }

        public int Insertar(TL_LOG modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _LogAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return modelo == null ? 0 : 1;
        }

        public int Eliminar(TL_LOG modelo)
        {
            throw new NotImplementedException();
        }

        public TL_LOG ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
    }

}
