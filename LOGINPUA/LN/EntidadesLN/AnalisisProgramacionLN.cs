﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class AnalisisProgramacionLN
    {
        private Object bdConn;
        private readonly AnalisisProgramacionAD analisisProgramacion;

        public AnalisisProgramacionLN()
        {
            AnalisisProgramacionAD analisisProgRepositorio = new AnalisisProgramacionAD(ref bdConn);
            analisisProgramacion = analisisProgRepositorio;
        }
        public List<CC_RUTA_TIPO_SERVICIO> getRutasByModalidadTransporte(int idModalidad)
        {
            var datos = analisisProgramacion.getRutasByModalidadTransporte(idModalidad);
            return datos;
        }

        public List<CC_DATA_ANALISIS_PROG_COSAC> getDataMuestraViajesCOSAC(int idRutatipoServicio, string fechaConsultaIni, string fechaConsultaFin)
        {
            var datos = analisisProgramacion.getDataMuestraViajesCOSAC(idRutatipoServicio, fechaConsultaIni, fechaConsultaFin);
            return datos;
        }

        public List<CC_DATA_ANALISIS_PROG_CORREDORES> getDataMuestraViajesCORREDORES(int idRutatipoServicio, string fechaConsultaIni, string fechaConsultaFin)
        {
            var datos = analisisProgramacion.getDataMuestraViajesCORREDORES(idRutatipoServicio, fechaConsultaIni, fechaConsultaFin);
            return datos;
        }

        

        public RPTA_GENERAL registrarMaestroViajeCOSAC(int idRutaTipoServicio, string fecha, string usuarioRegistra)
        {
            var datos = analisisProgramacion.registrarMaestroViajeCOSAC(idRutaTipoServicio, fecha, usuarioRegistra);
            return datos;
        }

        public RPTA_GENERAL registrarMDetalleViajeCOSAC(int idMaestroViajeCosac, string hProgramada, string hEjecutada, string estacion, string abrevEstacion,
                                                        string nomRuta, int nroBloque, string sentido, int seq_viaje, int idBus, int idViaje, string fechaHoraPasoRegistro, string usuarioRegistra)
        {
            var datos = analisisProgramacion.registrarMDetalleViajeCOSAC(idMaestroViajeCosac, hProgramada, hEjecutada, estacion, abrevEstacion, nomRuta, nroBloque, sentido, seq_viaje, idBus, idViaje, fechaHoraPasoRegistro, usuarioRegistra);
            return datos;
        }

        public List<CC_RECORRIDO_TSERVICIO> getRecorridosTServByRutaServ(int idTipoServicioOper)
        {
            var datos = analisisProgramacion.getRecorridosTServByRutaServ(idTipoServicioOper);
            return datos;
        }

        public List<CC_PARADERO> getParaderoTServByRecTserv(int idRecorridoTipoServicio)
        {
            var datos = analisisProgramacion.getParaderoTServByRecTserv(idRecorridoTipoServicio);
            return datos;
        }

        public List<CC_DATA_ANALISIS_PROG_COSAC> verificarDataParaAnalisis(string fechaConsulta, int idRutaTipoServicio)
        {
            var datos = analisisProgramacion.verificarDataParaAnalisis(fechaConsulta, idRutaTipoServicio);
            return datos;
        }

    }
}
