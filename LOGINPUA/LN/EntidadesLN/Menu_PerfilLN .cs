﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class Menu_PerfilLN
    {
        private Object bdConn;
        private readonly MenuPerfilAD _menuperfil;


        public Menu_PerfilLN()
        {
            MenuPerfilAD usuarioRepositorio = new MenuPerfilAD(ref bdConn);
            _menuperfil = usuarioRepositorio;
        }




        public List<CC_MENU_PERFIL> ListarPermisos(int idperfil)
        {
            var datos = _menuperfil.ListarPermisos(idperfil);
            return datos;
        }

        
              public List<CC_MENUPERFIL_ACCION> listarAcceso()
        {
            var datos = _menuperfil.listarAcceso();
            return datos;
        }

        public List<CC_MENUPERFIL_ACCION> accesoslist_menu_idperfil(int idperfil)
        {
            var datos = _menuperfil.accesoslist_menu_idperfil(idperfil);
            return datos;
        }
        public List<CC_MENUPERFIL_ACCION> accesoslist_X_idmenuperfil(int idmenuperfil)
        {
            var datos = _menuperfil.accesoslist_X_idmenuperfil(idmenuperfil);
            return datos;
        }

        
        public List<CC_MENU_PERFIL> ListarMenu_x_ID(int idmenu, int idperfil)
        {
            var datos = _menuperfil.ListarMenu_x_ID(idmenu, idperfil);
            return datos;
        }

        public RPTA_GENERAL AgregarPermiso_Perfil(int idperfil, int idmenu, string session_usuario)
        {
            var datos = _menuperfil.AgregarPermiso_Perfil(idperfil, idmenu, session_usuario);
            return datos;
        }
        public RPTA_GENERAL Agregar_AccionMenu(int id_menuperfil, int id_accion, string session_usuario)
        {
            var datos = _menuperfil.Agregar_AccionMenu(id_menuperfil, id_accion, session_usuario);
            return datos;
        }




        public RPTA_GENERAL Desactivar_Permisos(int idmenu, int idestado, int id_perfil, string session_usuario)
        {
            var datos = _menuperfil.Desactivar_Permisos(idmenu, idestado, id_perfil, session_usuario);
            return datos;
        }

        public RPTA_GENERAL Desactivar_Acciones(int idmenu)
        {
            var datos = _menuperfil.Desactivar_Acciones(idmenu);
            return datos;
        }


 


        public List<CC_MENU_PERFIL> listarModalidad_Perfiles()
        {
            var listar = _menuperfil.listarModalidad_Perfiles();
            return listar;
        }






    }
}

