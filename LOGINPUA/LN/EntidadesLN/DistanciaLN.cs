﻿using AD.EntidadesAD;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LN.EntidadesLN
{
    public class DistanciaLN : InterfaceLN<CC_DISTANCIA_PARADEROS>
    {
        private readonly DistanciaAD _loginRepositorio;

        public DistanciaLN()
        {
            DistanciaAD loginRepositorio = new DistanciaAD();
            _loginRepositorio = loginRepositorio;
        }


        public void Datos(int activo)
        {
            throw new NotImplementedException();
        }



      

        public int Eliminar(CC_DISTANCIA_PARADEROS modelo)
        {
            throw new NotImplementedException();
        }

        public CC_DISTANCIA_PARADEROS Insertar(CC_DISTANCIA_PARADEROS modelo)
        {
            CC_DISTANCIA_PARADEROS datos = _loginRepositorio.Insertar(modelo);
            return datos;

        }

        public CC_DISTANCIA_PARADEROS Consultar_Distancias(string ida, string vuelta)
        {
            CC_DISTANCIA_PARADEROS datos = _loginRepositorio.Consultar_Distancias(ida, vuelta);
            return datos;

        }

        public List<CC_DISTANCIA_PARADEROS> ObtenerListado()
        {
            List<CC_DISTANCIA_PARADEROS> datos = _loginRepositorio.Lista_Distancia();
            return datos;
        }


        public void Eliminar_x_ID(int id)
        {
            _loginRepositorio.Eliminar_x_ID(id);

        }




        public CC_DISTANCIA_PARADEROS Buscar_x_ID(int id)
        {
            CC_DISTANCIA_PARADEROS dat = _loginRepositorio.Buscar_x_ID(id);
            return dat;
        }
        public void Editar_Distancia(CC_DISTANCIA_PARADEROS model)
        {
            _loginRepositorio.Editar_Distancia(model);
        }

        public CC_DISTANCIA_PARADEROS Ultimo_Registro()
        {
            CC_DISTANCIA_PARADEROS dat = _loginRepositorio.Ultimo_Registro();
            return dat;
        }



        public int Modificar(CC_DISTANCIA_PARADEROS modelo)
        {
            throw new NotImplementedException();
        }

        public CC_DISTANCIA_PARADEROS ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }

        int InterfaceLN<CC_DISTANCIA_PARADEROS>.Insertar(CC_DISTANCIA_PARADEROS modelo)
        {
            throw new NotImplementedException();
        }

        List<CC_DISTANCIA_PARADEROS> InterfaceLN<CC_DISTANCIA_PARADEROS>.Datos(int activo)
        {
            throw new NotImplementedException();
        }
    }
}
