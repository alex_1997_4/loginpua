﻿using AD.EntidadesAD;
using ENTIDADES;
using System;
using System.Collections.Generic;

namespace LN.EntidadesLN
{
    public class BUSESLN : InterfaceLN<CC_BUSES>
    {
        private readonly BusesAD _loginRepositorio;
        private Object bdConn;

        public BUSESLN()
        {
            BusesAD loginRepositorio = new BusesAD(ref bdConn);
            _loginRepositorio = loginRepositorio;


        }
 
        public List<CC_BUSES> Listar_Buses()
        {
            var datos = _loginRepositorio.Listar_Buses();
            return datos;
        }

        public List<CC_BUSES> Listar_Buses_by_Id(int idCorredor)
        {
            var datos = _loginRepositorio.Listar_Buses_by_Id(idCorredor);
            return datos;
        }

        public RPTA_GENERAL Insertar_Buses_Nuevos(CC_BUSES modelo)
        {

            var datos = _loginRepositorio.Insertar_Buses_Nuevos(modelo);
            return datos;
        }

        public RPTA_GENERAL anularBus(int idBus, string usuarioAnula)
        {
            var datos = _loginRepositorio.anularBus(idBus, usuarioAnula);
            return datos;
        }

        public RPTA_GENERAL Modificar_Bus(CC_BUSES modelo)
        {
            var datos = _loginRepositorio.Modificar_Bus(modelo);
            return datos;
        }

        public RPTA_GENERAL desafectarBus(int id_placa_reemplazada, string placa_nueva)
        {
            var datos = _loginRepositorio.desafectarBus(id_placa_reemplazada, placa_nueva);
            return datos;
        }

        public List<CC_BUSES> Listar_Buses_Desafectados()
        {
            var datos = _loginRepositorio.Listar_Buses_Desafectados();
            return datos;
        }

        public RPTA_GENERAL Insertar_Buses_Afectados(CC_BUSES modelo)
        {

            var datos = _loginRepositorio.Insertar_Buses_Afectados(modelo);
            return datos;
        }

        public List<CC_CONDUCTORES> Listar_Placa()
        {
            var datos = _loginRepositorio.Listar_Placa();
            return datos;
        }


        public List<BUSES_DESPACHO> Listar_Informes_Despacho()
        {
            var datos = _loginRepositorio.Listar_Informes_Despacho();
            return datos;
        }


        public List<BUSES_DESPACHO> FIltrar_Archivo_Despacho(string placa, string fecha, string estado)
        {
            var datos = _loginRepositorio.FIltrar_Archivo_Despacho(placa, fecha, estado);
            return datos;
        }
 
        public void Cambiar_Estado_Bus(string placa, int cod_despacho)
        {

            _loginRepositorio.Cambiar_Estado_Bus(placa, cod_despacho);

        }
        public void Insertar_Buses_Despacho(List<BUSES_DESPACHO> lista)
        {

            foreach (BUSES_DESPACHO item in lista)
            {

                _loginRepositorio.Insertar_Buses_Despacho(item.BD_ID, item.CD_ID, item.BS_PLACA, item.BD_ESTADO, item.BD_CALIDAD, item.BD_OBSERVACION, item.BD_DIRECCION, item.BD_KM, item.BD_CONCESIONARIO, item.BD_FECHA, item.USUREG, item.ESTREG, item.BD_LONGUITUD, item.BD_LATITUD, item.BD_COD_DESPACHO, item.URL_FOTO1, item.URL_FOTO2, item.URL_FOTO3, item.URL_FOTO4, item.ESTADO_BUS,item.COMENTARIO);

            }

        }

        public RPTA_GENERAL Verifica_Placa_Existente(string placa)
        {
            var datos = _loginRepositorio.Verifica_Placa_Existente(placa);
            return datos;
        }
        

        public List<BUSES_DESPACHO> Lista_Conceptos_Dentro()
        {
            var datos = _loginRepositorio.Lista_Conceptos_Dentro();
            return datos;
        }
        public List<BUSES_DESPACHO> Lista_Cabina_Vehiculo()
        {
            var datos = _loginRepositorio.Lista_Cabina_Vehiculo();
            return datos;
        }

        public List<BUSES_DESPACHO> Lista_Conceptos_Exterior()
        {
            var datos = _loginRepositorio.Lista_Conceptos_Exterior();
            return datos;
        }
 

        public BUSES_DESPACHO Ultimo_Buses_Despacho()
        {
            var datos = _loginRepositorio.Ultimo_Buses_Despacho();
            return datos;
        }



        public BUSES_DESPACHO Ultimo_Codigo_Archivo(string placa)
        {
            var datos = _loginRepositorio.Ultimo_Codigo_Archivo(placa);
            return datos;
        }

        public int Eliminar(CC_BUSES modelo)
        {
            throw new NotImplementedException();
        }

        public int Insertar(CC_BUSES modelo)
        {
            throw new NotImplementedException();
        }

        public int Modificar(CC_BUSES modelo)
        {
            throw new NotImplementedException();
        }

        List<CC_BUSES> InterfaceLN<CC_BUSES>.Datos(int activo)
        {
            throw new NotImplementedException();
        }

        CC_BUSES InterfaceLN<CC_BUSES>.ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }
        public CC_BUSES Estado_Vigencias(CC_BUSES model)
        {
            var datos = _loginRepositorio.Estado_Vigencias(model);
            return datos;
        } 
    }
}
