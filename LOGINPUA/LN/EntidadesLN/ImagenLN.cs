﻿using AD.EntidadesAD;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LN.EntidadesLN
{
    public class ImagenLN : InterfaceLN<TM_PROGRAMACION_IMG>
    {
        private readonly ImagenAD _loginRepositorio;

        public ImagenLN()
        {
            ImagenAD loginRepositorio = new ImagenAD();
            _loginRepositorio = loginRepositorio;
        }

        public int Actualizar(TM_PROGRAMACION_IMG modelo)
        {
            throw new NotImplementedException();
        }

        

        public int Borrar(TM_PROGRAMACION_IMG modelo)
        {
            throw new NotImplementedException();
        }

        public TM_PROGRAMACION_IMG Buscar()
        {
            throw new NotImplementedException();
        }

        public int Insertar(TM_PROGRAMACION_IMG modelo)
        {
            throw new NotImplementedException();
        }

        public TM_PROGRAMACION_IMG insertar_imagen(TM_PROGRAMACION_IMG modelo)
        {
            TM_PROGRAMACION_IMG dat = _loginRepositorio.insertar_imagen(modelo);
            return dat;
        }

         
        public TM_PROGRAMACION_IMG Obtener_Corredor(TM_PROGRAMACION_IMG modelo)
        {
            TM_PROGRAMACION_IMG dat = _loginRepositorio.Obtener_Corredor(modelo);
            return dat;
        }

        public TM_PROGRAMACION_IMG Buscar_x_ID(int id)
        {
            TM_PROGRAMACION_IMG dat = _loginRepositorio.Buscar_x_ID(id);
            return dat;
        }

        public void Editar_Fotos(TM_PROGRAMACION_IMG model)
        {
           _loginRepositorio.Editar_Fotos(model);
        }

        

        public TM_PROGRAMACION_IMG Buscar_Foto_Corredor(TM_PROGRAMACION_IMG modelo,string fecha)
        {
            TM_PROGRAMACION_IMG dat = _loginRepositorio.Buscar_Foto_Corredor(modelo,fecha);
            return dat;
        }
        
        

        public List<TM_PROGRAMACION_IMG> ObtenerListado()
        {
            List<TM_PROGRAMACION_IMG> dat = _loginRepositorio.listado_Imagen();
            return dat;
        }

        public TM_PROGRAMACION_IMG Obtener_Ultimo_Registro(TM_PROGRAMACION_IMG model)
        {
            TM_PROGRAMACION_IMG dat = _loginRepositorio.Obtener_Ultimo_Registro(model);
            return dat;
        }

        public TM_PROGRAMACION_IMG ObtenerPorCodigo(int id)
        {
            throw new NotImplementedException();
        }

        //List<TM_PROGRAMACION_IMG> InterfaceLN<TM_PROGRAMACION_IMG>.ObtenerListado()
        //{
        //    throw new NotImplementedException();
        //}

        public List<TM_PROGRAMACION_IMG> Datos(int activo)
        {
            throw new NotImplementedException();
        }

        public int Eliminar(TM_PROGRAMACION_IMG modelo)
        {
            throw new NotImplementedException();
        }

        public int Modificar(TM_PROGRAMACION_IMG modelo)
        {
            throw new NotImplementedException();
        }
    }
}
