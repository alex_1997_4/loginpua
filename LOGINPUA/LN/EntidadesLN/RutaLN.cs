﻿using System;
using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class RutaLN
    {
        private readonly RutaAD rutaAD;
        private Object bdConn;

        public RutaLN()
        {
            RutaAD rutaRepositorio = new RutaAD(ref bdConn);
            rutaAD = rutaRepositorio;
        }

        public List<CC_RUTA> obtenerRutasPorCorredor(int idCorredor)
        {
            var datos = rutaAD.getRutaxCorredor(idCorredor);
            return datos;
        }

        public List<CC_RUTA_CORREDOR> getRutaCorredor()
        {
            var datos = rutaAD.getRutaCorredor();
            return datos;
        }

        public List<CC_RUTA_CORREDOR> getRuta_X_modalidad(int modalidad)
        {
            var datos = rutaAD.getRuta_X_modalidad(modalidad);
            return datos;
        }

        public List<CC_RUTA_TIPO_SERVICIO> getRutaSerOpe_X_modalidad(int id_rtuta)
        { 
            var datos = rutaAD.getRutaSerOpe_X_modalidad(id_rtuta);
            return datos;
        }
        public List<CC_RECORRIDO> getLado_X_Ruta(int id_rtuta)
        {
            var datos = rutaAD.getLado_X_Ruta(id_rtuta);
            return datos;
        }

        


    }
}
