﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using AD.EntidadesAD_SQL;

namespace LN.EntidadesLN
{
    public class ViajeCOSAC_LN
    {
        string cadenaConexionCOSAC = "ConexionORBCAD";
        public DataSet consultaViajesCOSAC(string etiqueta_ruta, string fechaConsulta)
        {
            return new ViajesCOSAC_AD().consultaViajesCOSAC(etiqueta_ruta, fechaConsulta, cadenaConexionCOSAC);
        }
    }
}
