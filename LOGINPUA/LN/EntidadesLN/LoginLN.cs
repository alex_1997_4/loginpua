﻿using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;
using AD.EntidadesAD;
using ENTIDADES.InnerJoin;
using System.Data;

namespace LN.EntidadesLN
{
    //public class LoginLN : InterfaceLN<TM_LOGIN>
    //{
    //    private readonly LoginAD _loginRepositorio;
    //    private readonly LogAD _logRepositorio;
    //    private readonly EmpresaAD _empresaRepositorio;
    //    public LoginLN()
    //    {
    //        LoginAD loginRepositorio = new LoginAD();
    //        _loginRepositorio = loginRepositorio;


    //        LogAD logRepositorio = new LogAD();
    //        _logRepositorio = logRepositorio;


    //        EmpresaAD empresaRepositorio = new EmpresaAD();
    //        _empresaRepositorio = empresaRepositorio;


    //    }
    //    public TM_LOGIN Autentica(TM_LOGIN modelo)
    //    {
    //        TM_LOGIN dat = _loginRepositorio.Datos(modelo);
    //        return dat;
    //    }

    //    public TL_LOG RegistraLog(TL_LOG modelo)
    //    {
    //        TL_LOG dat = _logRepositorio.Crear(modelo);
    //        return dat;
    //    }


    //    public TM_EMPRESA ObtieneUsuarioEmpresa(TM_EMPRESA modelo)
    //    {
    //        TM_EMPRESA dat = _empresaRepositorio.Datos(modelo);
    //        return dat;
    //    }

    //    public object ObtieneUsuarioEmpresa(object empresamodel)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Actualizar(TM_LOGIN modelo)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Borrar(TM_LOGIN modelo)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public TM_LOGIN Buscar()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Insertar(TM_LOGIN modelo)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public List<TM_LOGIN> ObtenerListado()
    //    {
    //        List<TM_LOGIN> listaColores = new List<TM_LOGIN>();
    //        listaColores = _loginRepositorio.DatosColores().ToList();
    //        return listaColores;
    //    }

    //    public TM_LOGIN ObtenerPorCodigo(int id)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    List<TM_LOGIN> InterfaceLN<TM_LOGIN>.Datos(int activo)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Eliminar(TM_LOGIN modelo)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public int Modificar(TM_LOGIN modelo)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}

    public class LoginLN : InterfaceLN<TB_LOGIN>
    {
        string mensaje = "";
        int tipo = 0;
        bool valor = true;
        private readonly LoginAD _LoginAD;
        public LoginLN()
        {
            LoginAD LoginAD = new LoginAD();
            _LoginAD = LoginAD;
        }


        public DataSet IngresarIn(string usuario, string clave)
        {
            var rpta = _LoginAD.IngresarIn(usuario, clave);
            return rpta;
        }
        public CC_USUARIO_PERSONA Verificar_Usuario(string usuario, string clave)
        {
            var rpta = _LoginAD.Verificar_Usuario(usuario, clave);
            return rpta;
        }

         

        

        //public CC_INGRESOUSUARIO IngresoIn(string usuario, string clave,ref string mensaje,ref int tipo)
        //{
        //    int tipoLN = 0;
        //    string mensajeLN = "";
        //    int alerta = 0;
        //    CC_INGRESOUSUARIO InnerJoinLogin = new CC_INGRESOUSUARIO();
        //    try
        //    {
        //        //InnerJoinLogin = _LoginAD.IngresarIn(usuario,clave);

        //        alerta = 1;
        //        if (InnerJoinLogin != null)
        //        {
        //            alerta = 2;
        //            if (InnerJoinLogin.ESTREG == 1)
        //            {
        //                alerta = 3;
        //            }
        //        }
        //        switch (alerta)
        //        {
        //            case 1:
        //                mensajeLN = "No Existe el Usuario";
        //                break;
        //            case 2:
        //                mensajeLN = "Usuario Deshabilitado";
        //                break;
        //            case 3:
        //                mensajeLN = "Ingreso al Sistema";
        //                break;
        //            default:
        //                mensajeLN = "";
        //                break;
        //        }
        //        tipoLN = 1;
        //    }
        //    catch (Exception e)
        //    {
        //        tipoLN = 0;
        //        mensajeLN = "Fallido";
        //        InnerJoinLogin = new CC_INGRESOUSUARIO();
        //    }
        //    finally
        //    {
        //        mensaje = mensajeLN;
        //        tipo = tipoLN;
        //    }
        //    return InnerJoinLogin;
        //}




        public InnerJoinLogin LoginIn(TB_LOGIN model, ref string mensaje, ref int tipo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            int alerta = 0;
            InnerJoinLogin InnerJoinLogin = new InnerJoinLogin();
            try
            {
                InnerJoinLogin = _LoginAD.LoginIn(model);
                alerta = 1;
                if (InnerJoinLogin != null)
                {
                    alerta = 2;
                    if (InnerJoinLogin.ESTREG == 1)
                    {
                        alerta = 99;
                    }
                }
                switch (alerta)
                {
                    case 1:
                        mensajeLN = "No Existe el Usuario";
                        break;
                    case 2:
                        mensajeLN = "Usuario Deshabilitado";
                        break;
                    case 99:
                        mensajeLN = "Ingreso al Sistema";
                        break;
                    default:
                        mensajeLN = "";
                        break;
                }
                tipoLN = 1;
                //mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                InnerJoinLogin = new InnerJoinLogin();
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return InnerJoinLogin;
        }
        public List<InnerJoinUsuario> Accesos(string nombre, string password, ref string mensaje)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<InnerJoinUsuario> ListInnerJoinUsuario = new List<InnerJoinUsuario>();
            try
            {
                ListInnerJoinUsuario = _LoginAD.Accesos(nombre, password).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                ListInnerJoinUsuario = new List<InnerJoinUsuario>();
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return ListInnerJoinUsuario;
        }

        public List<TB_LOGIN> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TB_LOGIN> listaLogin = new List<TB_LOGIN>();
            try
            {
                listaLogin = _LoginAD.Datos(activo).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaLogin = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaLogin;
        }

        public int Modificar(TB_LOGIN modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _LoginAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }

        public int Insertar(TB_LOGIN modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _LoginAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return modelo == null ? 0 : 1;
        }

        public int Eliminar(TB_LOGIN modelo)
        {
            throw new NotImplementedException();
        }

        public TB_LOGIN ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TB_LOGIN TB_LOGIN = new TB_LOGIN();
            try
            {
                TB_LOGIN = _LoginAD.ObtenerPorTablaFiltroId("TB_LOGIN", "LOGCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TB_LOGIN = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TB_LOGIN;
        }
    }
}
