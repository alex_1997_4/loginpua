﻿using System;
using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;

namespace LN.EntidadesLN
{
    public class TipoDiaLN
    {
        private readonly TipoDiaAD tipoDiaAD;
        public TipoDiaLN()
        {
            TipoDiaAD tipoDiaRepositorio = new TipoDiaAD();
            tipoDiaAD = tipoDiaRepositorio;
        }

        public List<CC_TIPO_DIA> obtenertipoDias()
        {
            var datos = tipoDiaAD.getTipoDias();
            return datos;
        }
    }
}
