﻿using AD.EntidadesAD;
using ENTIDADES;
using System;
using System.Collections.Generic;

namespace LN.EntidadesLN
{
    public class IncidenciasLN 
    {
        private readonly IncidenciasAD _loginRepositorio;
        private Object bdConn;

        public IncidenciasLN()
        {
            IncidenciasAD loginRepositorio = new IncidenciasAD(ref bdConn);
            _loginRepositorio = loginRepositorio;

        }

        public List<CC_INCIDENCIA> Listar_Incidencias()
        {
            var datos = _loginRepositorio.Listar_Incidencias();
            return datos;
        }

        public RPTA_GENERAL Registrar_Incidencia(CC_INCIDENCIA Model_Incidencia)
        {

            var datos = _loginRepositorio.Registrar_Incidencia(Model_Incidencia);
            return datos;
        }

        public RPTA_GENERAL AnularIncidencia(int idIncidencia, string usuarioAnula)
        {
            var datos = _loginRepositorio.AnularIncidencia(idIncidencia, usuarioAnula);
            return datos;
        }


        public List<CC_INFRACCION> Listar_Infracciones()
        {
            var datos = _loginRepositorio.Listar_Infracciones();
            return datos;
        }
        
        public List<CC_PERSONA_INCIDENCIA> Listar_Persona_Incidencia()
        {
            var datos = _loginRepositorio.Listar_Persona_Incidencia();
            return datos;
        }

        public RPTA_GENERAL Registrar_Infraccion(CC_INFRACCION Model_Infraccion)
        {

            var datos = _loginRepositorio.Registrar_Infraccion(Model_Infraccion);
            return datos;
        }


        public RPTA_GENERAL Editar_Infraccion(CC_INFRACCION Model_Infraccion)
        {
            var datos = _loginRepositorio.Editar_Infraccion(Model_Infraccion);
            return datos;
        }

 
        public void Cambiar_Estado_Bus(string placa, int cod_despacho)
        {

            _loginRepositorio.Cambiar_Estado_Bus(placa, cod_despacho);

        }
        public void Insertar_Buses_Despacho(List<BUSES_DESPACHO> lista)
        {

            foreach (BUSES_DESPACHO item in lista)
            {

                _loginRepositorio.Insertar_Buses_Despacho(item.BD_ID, item.CD_ID, item.BS_PLACA, item.BD_ESTADO, item.BD_CALIDAD, item.BD_OBSERVACION, item.BD_DIRECCION, item.BD_KM, item.BD_CONCESIONARIO, item.BD_FECHA, item.USUREG, item.ESTREG, item.BD_LONGUITUD, item.BD_LATITUD, item.BD_COD_DESPACHO, item.URL_FOTO1, item.URL_FOTO2, item.URL_FOTO3, item.URL_FOTO4, item.ESTADO_BUS,item.COMENTARIO);

            }

        }

        public RPTA_GENERAL Verifica_Placa_Existente(string placa)
        {
            var datos = _loginRepositorio.Verifica_Placa_Existente(placa);
            return datos;
        }
        

        public List<BUSES_DESPACHO> Lista_Conceptos_Dentro()
        {
            var datos = _loginRepositorio.Lista_Conceptos_Dentro();
            return datos;
        }

        public BUSES_DESPACHO Ultimo_Codigo_Archivo(string placa)
        {
            var datos = _loginRepositorio.Ultimo_Codigo_Archivo(placa);
            return datos;
        }

        public int Eliminar(CC_BUSES modelo)
        {
            throw new NotImplementedException();
        }

        public int Insertar(CC_BUSES modelo)
        {
            throw new NotImplementedException();
        }

        public int Modificar(CC_BUSES modelo)
        {
            throw new NotImplementedException();
        }

    }
}
