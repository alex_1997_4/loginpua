﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class RutasLN
    {
        private Object bdConn;
        private readonly RutasAD rutas;
        public RutasLN()
        {
            RutasAD salidaRepositorio = new RutasAD(ref bdConn);
            rutas = salidaRepositorio;
        }

        public RPTA_GENERAL registrarRuta(int idCorredor, string nombre, string nroRuta, string distritos, string usuarioRegistra)
        {
            var rpta = rutas.registrarRuta(idCorredor, nombre, nroRuta, distritos, usuarioRegistra);
            return rpta;
        }

        public RPTA_GENERAL registrarRecorrido(int idRuta, string sentido, string lado, string usuarioRegistra)
        {
            var rpta = rutas.registrarRecorrido (idRuta, sentido, lado, usuarioRegistra);
            return rpta;
        }
        
        public List<CC_RUTA> listarRutasByIdCorredor(int idCorredor)
        {
            var rpta = rutas.listarRutasByIdCorredor(idCorredor);
            return rpta;
        }

        public RPTA_GENERAL editarRuta(int idRuta, string nroRuta, string nombre, string distritos, string usuarioModifica)
        {
            var rpta = rutas.editar_Ruta(idRuta, nroRuta, nombre, distritos, usuarioModifica);
            return rpta;
        }

        public RPTA_GENERAL anularRuta(int idRuta, string usuarioRegistra)
        {
            var rpta = rutas.anularRuta(idRuta, usuarioRegistra);
            return rpta;
        }
        
    }
}
