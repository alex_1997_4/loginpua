﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class ViaLN
    {
        private Object bdConn;
        private readonly ViaAD via;
        public ViaLN()
        {
            ViaAD viaRepositorio = new ViaAD(ref bdConn);
            via = viaRepositorio;
        }

        public List<CC_VIA> getVias()
        {
            var datos = via.getVias();
            return datos;
        }
    }
}
