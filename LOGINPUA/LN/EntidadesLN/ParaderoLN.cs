﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class ParaderoLN
    {
        private Object bdConn;
        private readonly ParaderoAD paradero;

        public ParaderoLN()
        {
            ParaderoAD paraderoRepositorio = new ParaderoAD(ref bdConn);
            paradero = paraderoRepositorio;
        }

        public List<CC_PARADERO> getParaderosByIdRuta(int id_ruta)
        {
            var datos = paradero.getParaderosByIdRuta(id_ruta);
            return datos;
        }
        public List<CC_PARADERO> getParaderosByStrRecorrido(string strRecorrido)
        {
            var datos = paradero.getParaderosByStrRecorrido(strRecorrido);
            return datos;
        }
        public RPTA_GENERAL registrarParadero( int idRecorrido, int idTipoParadero, int idVia, string nombre, string nombreEtiqueta, 
                                                    double distanciaParcial, double latitud, double longitud, int nroOrden, string usuarioReg)
        {
            var datos = paradero.registrarParadero(idRecorrido, idTipoParadero, idVia, nombre, nombreEtiqueta,
                                                        distanciaParcial, latitud, longitud, nroOrden, usuarioReg);
            return datos;
        }

        public RPTA_GENERAL anularParadero(int idParadero, string usuarioReg)
        {
            var datos = paradero.anularParadero(idParadero, usuarioReg);
            return datos;
        }

        public RPTA_GENERAL modificarParadero(int idParadero, string nombre, string etiquetaNombre,
                                                int idTipoParadero, double distanciaParcial,
                                                double latitud, double longitud, int idVia,int numero_orden,
                                                string usuarioModif)
        {
            var datos = paradero.modificarParadero( idParadero, nombre, etiquetaNombre,
                                                    idTipoParadero, distanciaParcial,
                                                    latitud, longitud, idVia, numero_orden,
                                                    usuarioModif);
            return datos;
        }
    }
}

