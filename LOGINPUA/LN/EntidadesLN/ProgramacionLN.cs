﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;

namespace LN.EntidadesLN
{
    public class ProgramacionLN
    {
        private readonly ProgramacionAD programacionAD;
        private Object bdConn;

        public ProgramacionLN() {
            ProgramacionAD programacionRepositorio = new ProgramacionAD(ref bdConn);
            programacionAD = programacionRepositorio;
        }
        public List<CC_DATOS_CORREDOR> obtenerDataCorredor(int idCorredor)
        {
            var datos = programacionAD.getDataCorredor(idCorredor);
            return datos;
        }


        public RPTA_GENERAL ValidarFecha_Programados(string fecha,int idServicio)
        {
            var datos = programacionAD.ValidarFecha_Programados(fecha, idServicio);
            return datos;
        }
        public List<CC_PROGRAMA_RUTA> getViajes_ProgrUnidades(string fecha, int id_ruta, string sentido)
        {
            var datos = programacionAD.getViajes_ProgrUnidades(fecha,id_ruta,sentido);
            return datos;
        }
        public List<CC_PROGRAMA_RUTA> getviajesProgrServ(string fecha, int id_ruta, string sentido,int idservicio)
        {
            var datos = programacionAD.getviajesProgrServ(fecha, id_ruta, sentido, idservicio);
            return datos;
        }

        public RPTA_GENERAL Actulizar_ViajeTiempoReal(int idsalida, string unidad, int cac_temporal, string hora_salida, string usureg_real)
        {
            var datos = programacionAD.Actulizar_ViajeTiempoReal(idsalida, unidad, cac_temporal, hora_salida, usureg_real);
            return datos;
        }


    }
}
