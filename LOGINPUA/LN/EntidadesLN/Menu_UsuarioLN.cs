﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class Menu_UsuarioLN
    {
        private Object bdConn;
        private readonly Menu_UsuarioAD menus;


        public Menu_UsuarioLN()
        {
            Menu_UsuarioAD usuarioRepositorio = new Menu_UsuarioAD(ref bdConn);
            menus = usuarioRepositorio;
        }


        public List<CC_MENU_USUARIO> ListarModulos(int id_modalidad)
        {
            var datos = menus.ListarModulos(id_modalidad);
            return datos;
        }

        public List<CC_MENU_USUARIO> listarMenuPadre(int id_modalidad)
        {
            var datos = menus.listarMenuPadre(id_modalidad);
            return datos;
        }

        public List<CC_MENU_USUARIO> listarMODULOS_X_ID(int id, int idmodalidad)
        {
            var datos = menus.listarMODULOS_X_ID(id, idmodalidad);
            return datos;
        }


        public RPTA_GENERAL AgregarMenu(string nombre, string url, string icono, int tipomenu, int menupadre, int modulo, int idmodalidad, string session_usuario)
        {
            var datos = menus.AgregarMenu(nombre, url, icono, tipomenu, menupadre, modulo, idmodalidad, session_usuario);
            return datos;
        }

        public RPTA_GENERAL AnularMenu(int idmenu, string session_usuario)
        {
            var datos = menus.AnularMenu(idmenu, session_usuario);
            return datos;
        }


        public RPTA_GENERAL EditarMenu(int idmenu, string nombre, string session_usuario)
        {
            var datos = menus.EditarMenu(idmenu,nombre, session_usuario);
            return datos;
        }

        



    }
}

