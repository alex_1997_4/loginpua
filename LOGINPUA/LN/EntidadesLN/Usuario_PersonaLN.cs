﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class Usuario_PersonaLN
    {
        private Object bdConn;
        private readonly Usuario_PersonaAD usuario;


        public Usuario_PersonaLN()
        {
            Usuario_PersonaAD usuarioRepositorio = new Usuario_PersonaAD(ref bdConn);
            usuario = usuarioRepositorio;
        }

        public List<CC_PERFIL_USUARIO> ListarPerfil_modalidad()
        {
            var datos = usuario.ListarPerfil_modalidad();
            return datos;
        }

        public List<CC_PERFIL_USUARIO> ListarUsuario_x_ID(int id)
        {
            var List = usuario.ListarUsuario_x_ID(id);
            return List;
        }
        public List<CC_PROVEEDORSERV_USUARIO> ListCuentas_x_id(int id_prov)
        {
            var List = usuario.ListCuentas_x_id(id_prov);
            return List;
        }

        
        public List<CC_PERSONA> ListarPersona()
        {
            var datos = usuario.ListarPersona();
            return datos;
        }

        public List<CC_USUARIO_PERSONA> ListarUsuarios()
        {
            var datos = usuario.ListarUsuarios();
            return datos;
        }


        public RPTA_GENERAL AgregarUsuario(int idpersona, string usuarios, string contraseña, string session_usuario)
        {
            var datos = usuario.AgregarUsuario(idpersona, usuarios, contraseña, session_usuario);
            return datos;
        }
        public RPTA_GENERAL registrarUsuario_Perfil(int idusuario, int idperfil, string session_usuario)
        {
            var datos = usuario.registrarUsuario_Perfil(idusuario, idperfil, session_usuario);
            return datos;
        }



        public RPTA_GENERAL Desactivar_Usuarios(int idpersona, string session_usuario)
        {
            var datos = usuario.Desactivar_Usuarios(idpersona, session_usuario);
            return datos;
        }

        public RPTA_GENERAL ModificarUsuarios_Perfil(int idusuario, string idperfil, string clave, int estado_usuario, string session_usuario)
        {
            var datos = usuario.ModificarUsuarios_Perfil(idusuario, idperfil, clave, estado_usuario, session_usuario);
            return datos;
        }







    }
}

