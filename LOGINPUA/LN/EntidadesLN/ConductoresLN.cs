﻿using AD.EntidadesAD;
using Entidades;
using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LN.EntidadesLN
{
    public class ConductoresLN  
    {
        private readonly ConductoresAD _loginRepositorio;
        private Object bdConn;


        public ConductoresLN()
        {
            ConductoresAD loginRepositorio = new ConductoresAD(ref bdConn);
            _loginRepositorio = loginRepositorio;
        }

        public List<CC_CONDUCTORES> Listar_Conductores()
        {
            var datos = _loginRepositorio.Listar_Conductores();
            return datos;
        }

        //public CC_PROGRAMA_RUTA Ultimo_id_rutas()
        //{
        //    var datos = _loginRepositorio.Ultimo_Registro_Rutas();
        //    return datos;
        //}

        //public CC_RUTAS_CONDUCTORES Ultimo_placas_conductores()
        //{
        //    var datos = _loginRepositorio.Ultimo_placas_conductores();
        //    return datos;
        //}





        //public void Insertar_ruta(int ID, string TIPO_DIA, string RUTA, string OPERADOR, string RECORDID, string ROUTE,
        //                     string BLK, string POG, string POT, string FNODE, string FTIME, string TTIME, string TNODE
        //                    , string PIG, string PIT, string LAY, string TRIP_TIME, string DISTANCIA, string ACUMULADO
        //                    , string PATS, string SENTIDO, string OBSERVACIONES, string PLACA, string PAQUETE)
        //{
        //    _loginRepositorio.Insertar_ruta(ID, TIPO_DIA, RUTA, OPERADOR, RECORDID, ROUTE, BLK, POG, POT, FNODE, FTIME, TTIME, TNODE, PIG, PIT, LAY, TRIP_TIME, DISTANCIA, ACUMULADO, PATS, SENTIDO, OBSERVACIONES, PLACA, PAQUETE);
        //}


        //public void Insertar_ruta_conductor(int ID_PLACAS_CONDUCTORES, string ITEM, string FECHA, string RUTA, string SERVICIO, string RAZON_SOCIAL, string PAQUETE, string PLACA, string TURNO, string PUNTO_INICIO, string HORA_PRESENTACION,
        //                              string HORA_SALE_PATIO, string HORA_CABECERA, string HORA_TRABAJO, string VIAJES, string CONDUCTOR, int DNI, int CODIGO_CAC)
        //{
        //    _loginRepositorio.Insertar_ruta_conductor(ID_PLACAS_CONDUCTORES, ITEM, FECHA, RUTA, SERVICIO, RAZON_SOCIAL, PAQUETE, PLACA, TURNO, PUNTO_INICIO, HORA_PRESENTACION, HORA_SALE_PATIO, HORA_CABECERA, HORA_TRABAJO, VIAJES, CONDUCTOR, DNI, CODIGO_CAC);

        //}







        //public List<CC_CONDUCTORES> Buscar_Conductores(CC_CONDUCTORES modelo)
        //{
        //    var datos = _loginRepositorio.Buscar_Conductores(modelo);
        //    return datos;
        //}

        //public List<CC_CONDUCTORES> Datos(int activo)
        //{
        //    throw new NotImplementedException();
        //}

        //public int Eliminar(CC_CONDUCTORES modelo)
        //{
        //    throw new NotImplementedException();
        //}

        //public int Insertar(CC_CONDUCTORES modelo)
        //{
        //    throw new NotImplementedException();
        //}

        //public int Modificar(CC_CONDUCTORES modelo)
        //{
        //    throw new NotImplementedException();
        //}

        //public CC_CONDUCTORES ObtenerPorCodigo(int id)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
