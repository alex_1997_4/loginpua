﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;
namespace LN.EntidadesLN
{
    public class PerfilLN
    {
        private Object bdConn;
        private readonly PerfilAD _perfil;


        public PerfilLN()
        {
            PerfilAD usuarioRepositorio = new PerfilAD(ref bdConn);
            _perfil = usuarioRepositorio;
        }
        public List<CC_PERFIL_USUARIO> ListarPerfil(int idmodalidad)
        {
            var datos = _perfil.ListarPerfil(idmodalidad);
            return datos;
        }


        public List<CC_MODALIDAD> ListarModalidad()
        {
            var datos = _perfil.ListarModalidad();
            return datos;
        }

        //public List<CC_PERSONA> ListarPersona()
        //{
        //    var datos = usuario.ListarPersona();
        //    return datos;
        //}

        //public List<CC_USUARIO_PERSONA> ListarUsuarios()
        //{
        //    var datos = usuario.ListarUsuarios();
        //    return datos;
        //}


        //public RPTA_GENERAL AgregarUsuario(int idpersona, int idmodalidad, int idperfil, string usuarios, string contraseña, string session_usuario)
        //{
        //    var datos = usuario.AgregarUsuario(idpersona,idmodalidad,idperfil, usuarios, contraseña,session_usuario);
        //    return datos;
        //}

        public RPTA_GENERAL AnularPerfil(int idperfil, string session_usuario)
        {
            var rpta = _perfil.AnularPerfil(idperfil, session_usuario);
            return rpta;
        }

        public List<CC_PERFIL_USUARIO> Validar_Perfil(int idperfil)
        {
            var rpta = _perfil.Validar_Perfil(idperfil);
            return rpta;
        }
        
        public RPTA_GENERAL ModificarPerfil(int idmodalidad, int id_modalidad_perfil, string nombre_perfil, string session_usuario)
        {
            var datos = _perfil.ModificarPerfil(idmodalidad, id_modalidad_perfil, nombre_perfil, session_usuario);
            return datos;
        }

        public RPTA_GENERAL AgregarPerfil(string nombre, string session_usuario)
        {
            var datos = _perfil.AgregarPerfil(nombre, session_usuario);
            return datos;
        }

        public RPTA_GENERAL AgregarPerfil_modalidad(int id_perfil, int id_modalidad, string session_usuario)
        {
            var datos = _perfil.AgregarPerfil_modalidad(id_perfil, id_modalidad, session_usuario);
            return datos;
        }




    }
}

