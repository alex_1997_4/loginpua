﻿using AD.EntidadesAD;
using ENTIDADES;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LN.EntidadesLN
{
    public class AccEmpLN : InterfaceLN<TV_ACC_EMP>
    {
        string mensaje = "";
        int tipo = 0;
        private readonly AccEmpAD _AccEmpAD;
        public AccEmpLN()
        {
            AccEmpAD AccEmpAD = new AccEmpAD();
            _AccEmpAD = AccEmpAD;
        }

        public List<TV_ACC_EMP> Datos(int activo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TV_ACC_EMP> listaAccEmp = new List<TV_ACC_EMP>();
            try
            {
                listaAccEmp = _AccEmpAD.Datos().ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                listaAccEmp = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return listaAccEmp;
        }

        public int Modificar(TV_ACC_EMP modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            dynamic valor = 1;
            try
            {
                valor = _AccEmpAD.Modificar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                valor = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return valor;
        }

        public int Insertar(TV_ACC_EMP modelo)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            try
            {
                modelo = _AccEmpAD.Insertar(modelo);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                modelo = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return modelo == null ? 0 : 1;
        }

        public int Eliminar(TV_ACC_EMP modelo)
        {
            throw new NotImplementedException();
        }

        public TV_ACC_EMP ObtenerPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            TV_ACC_EMP TV_ACC_EMP = new TV_ACC_EMP();
            try
            {
                TV_ACC_EMP = _AccEmpAD.ObtenerPorTablaFiltroId("TV_ACC_EMP", "ACCCOD", id);
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TV_ACC_EMP = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TV_ACC_EMP;
        }
        public List<TV_ACC_EMP> ObtenerListadoPorCodigo(int id)
        {
            int tipoLN = 0;
            string mensajeLN = "";
            List<TV_ACC_EMP> TV_ACC_EMP = new List<TV_ACC_EMP>();
            try
            {
                TV_ACC_EMP = _AccEmpAD.ObtenerListaPorTablaFiltroId("TV_ACC_EMP", "ACCEMPCOD", id).ToList();
                tipoLN = 1;
                mensajeLN = "Valido";
            }
            catch (Exception e)
            {
                tipoLN = 0;
                mensajeLN = "Fallido";
                TV_ACC_EMP = null;
            }
            finally
            {
                mensaje = mensajeLN;
                tipo = tipoLN;
            }
            return TV_ACC_EMP;
        }
    }
}
