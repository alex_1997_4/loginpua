﻿using System.Collections.Generic;
using ENTIDADES;
using AD.EntidadesAD;
using System;
using System.Data;

namespace LN.EntidadesLN
{
    public class SalidaLN
    {
        private Object bdConn;
        private readonly SalidaAD salida;
        public SalidaLN()
        {
            SalidaAD salidaRepositorio = new SalidaAD(ref bdConn);
            salida = salidaRepositorio;
        }

        public List<CC_PARADERO> getParaderosByIdRuta(int id_ruta)
        {
            var datos = salida.getParaderosByIdRuta(id_ruta);
            return datos;
        }

        public RPTA_GENERAL VerificarMaestroSalida(int idRuta, string fecha)
        {
            var rpta = salida.VerificarMaestroSalida(idRuta, fecha);
            return rpta;
        }
        public RPTA_GENERAL registrarMaestroSalida(int idRuta, string fecha, string nomUsuario)
        {
            var rpta = salida.registrarMaestroSalida(idRuta, fecha, nomUsuario);
            return rpta;
        }

        public RPTA_GENERAL registrarSalidaEjecutada(int idMaestroSalida, string padron,
                                                        string placa, string sentido, string horaSalida,
                                                        string horaLlegada, int nroServicio, string conductor,
                                                        string dniConductor, string comentario, string estadoViaje,
                                                        string usuarioRegistra)
        {
            var rpta = salida.registrarSalidaEjecutada(idMaestroSalida, padron,
                                                        placa, sentido, horaSalida,
                                                        horaLlegada, nroServicio, conductor,
                                                        dniConductor, comentario, estadoViaje,
                                                        usuarioRegistra);
            return rpta;
        }

        public RPTA_GENERAL registrarSalidaHoraPasoParadero(int idSalidaEjecutada, int idParadero,
                                                    string horaPaso, string lado, string usuarioRegistra)
        {
            var rpta = salida.registrarSalidaHoraPasoParadero(idSalidaEjecutada, idParadero, horaPaso, lado, usuarioRegistra);
            return rpta;
        }

        public DataSet getDataComparativos(string fechaComparativoA, string fechaComparativoB, int idRuta)
        {
            var rpta = salida.getDataComparativos(fechaComparativoA, fechaComparativoB, idRuta);
            return rpta;
        }
        public DataSet getViajesPorRuta(int _idRuta, string _fechaInicio, string _fechaFin)
        {
            var rpta = salida.getViajesPorRuta(_idRuta, _fechaInicio, _fechaFin);
            return rpta;
        }

        public RPTA_GENERAL AnularMaestroSalida(int Id_maestro_salida)
        {
            var rpta = salida.AnularMaestroSalida(Id_maestro_salida);
            return rpta;
        }
        public List<CC_REPORTE_DESPACHO> Consultar_DespachoFecha(int mes, int año, string idruta)
        {
            var rpta = salida.Consultar_DespachoFecha(mes, año, idruta);
            return rpta;
        }
    }
}
