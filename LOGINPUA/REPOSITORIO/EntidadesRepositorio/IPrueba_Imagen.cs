﻿using Entidades;
using System.Collections.Generic;

namespace Repositorio.EntidadesRepositorio
{
    public interface IPrueba_Imagen : IRepositorio<TM_PROGRAMACION_IMG>
    {
        TM_PROGRAMACION_IMG Insertar(TM_PROGRAMACION_IMG model);
      
    }
}
