﻿using Entidades;
using System.Collections.Generic;

namespace Repositorio.EntidadesRepositorio
{
    public interface IEmpresaRepositorio : IRepositorio<TM_EMPRESA>
    {
        TM_EMPRESA Insertar(TM_EMPRESA model);
        dynamic Modificar(TM_EMPRESA model);
        int Eliminar(int id);
        IEnumerable<TM_EMPRESA> Datos();
 
    }
}
