﻿using Entidades;
using ENTIDADES;
using System.Collections.Generic;

namespace Repositorio.EntidadesRepositorio
{
    public interface ILogRepositorio : IRepositorio<TL_LOG>
    {
        TL_LOG Insertar(TL_LOG model);
        dynamic Modificar(TL_LOG model);
        int Eliminar(int id);
        IEnumerable<TL_LOG> Datos();
    }
}
