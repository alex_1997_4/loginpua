﻿using Entidades;
using System.Collections.Generic;

namespace Repositorio.EntidadesRepositorio
{
    public interface IDistancia_Paraderos : IRepositorio<CC_DISTANCIA_PARADEROS>
    {
        CC_DISTANCIA_PARADEROS Insertar(CC_DISTANCIA_PARADEROS model);
      
    }
}
