﻿using Entidades;
using System.Collections.Generic;

namespace Repositorio.EntidadesRepositorio
{
    public interface ILoginRepositorio : IRepositorio<TM_LOGIN>
    {
        TM_LOGIN Insertar(TM_LOGIN model);
        dynamic Modificar(TM_LOGIN model);
        int Eliminar(int id);
        IEnumerable<TM_LOGIN> Datos();
    }
}
