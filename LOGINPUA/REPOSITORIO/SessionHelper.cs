﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LOGINPUA.Repositorio
{
    public static class SessionHelper
    {
        
        public static String accesoRojo
        {
            get { return HttpContext.Current.Session["accesoRojo"] == null ? null : HttpContext.Current.Session["accesoRojo"].ToString(); }
            set { HttpContext.Current.Session["accesoRojo"] = value; }
        }
        public static String accesoAmarillo
        {
            get { return HttpContext.Current.Session["accesoAmarillo"] == null ? null : HttpContext.Current.Session["accesoAmarillo"].ToString(); }
            set { HttpContext.Current.Session["accesoAmarillo"] = value; }
        }
        public static String accesoVerde
        {
            get { return HttpContext.Current.Session["accesoVerde"] == null ? null : HttpContext.Current.Session["accesoVerde"].ToString(); }
            set { HttpContext.Current.Session["accesoVerde"] = value; }
        }
        public static String accesoMorado
        {
            get { return HttpContext.Current.Session["accesoMorado"] == null ? null : HttpContext.Current.Session["accesoMorado"].ToString(); }
            set { HttpContext.Current.Session["accesoMorado"] = value; }
        }
        public static String accesoAzul
        {
            get { return HttpContext.Current.Session["accesoAzul"] == null ? null : HttpContext.Current.Session["accesoAzul"].ToString(); }
            set { HttpContext.Current.Session["accesoAzul"] = value; }
        }

        public static String accesoBlanco
        {
            get { return HttpContext.Current.Session["accesoBlanco"] == null ? null : HttpContext.Current.Session["accesoBlanco"].ToString(); }
            set { HttpContext.Current.Session["accesoBlanco"] = value; }
        }
        
        public static String userId
        {
            get { return HttpContext.Current.Session["userId"] == null ? null : HttpContext.Current.Session["userId"].ToString(); }
            set { HttpContext.Current.Session["userId"] = value; }
        }
        
        public static String codigo
        {
            get { return HttpContext.Current.Session["accesoCodigo"] == null ? null : HttpContext.Current.Session["accesoCodigo"].ToString(); }
            set { HttpContext.Current.Session["accesoCodigo"] = value; }
        }
        public static String user_login
        {
            get { return HttpContext.Current.Session["user_login"] == null ? null : HttpContext.Current.Session["user_login"].ToString(); }
            set { HttpContext.Current.Session["user_login"] = value; }
        }
        public static String user_clave
        {
            get { return HttpContext.Current.Session["user_clave"] == null ? null : HttpContext.Current.Session["user_clave"].ToString(); }
            set { HttpContext.Current.Session["user_clave"] = value; }
        }
        public static Object menu
        {
            get { return HttpContext.Current.Session["opcionesMenuUsuario"]; }
            set { HttpContext.Current.Session["opcionesMenuUsuario"] = value; }
        }
        public static Int32? cod_con
        {
            get { if (HttpContext.Current.Session["cod_con"] == null) return null; else return Convert.ToInt32(HttpContext.Current.Session["cod_con"]); }
            set { HttpContext.Current.Session["cod_con"] = value; }
        }
        public static Int32? cod_sup
        {
            get { if (HttpContext.Current.Session["cod_sup"] == null) return null; else return Convert.ToInt32(HttpContext.Current.Session["cod_sup"]); }
            set { HttpContext.Current.Session["cod_sup"] = value; }
        }

        public static bool sesion_valida(ref string mensaje, ref int tipo)
        {
            //HttpContext.Current.Session["user_login"] = "SDIAZ";
            //return true;
            if (HttpContext.Current.Session["accesoCodigo"] != null)
            {
                return true;
            }
            else
            {
                mensaje = "La sesión ha caducado";
                tipo = -1;
                return false;
            }
        }
    }
}
