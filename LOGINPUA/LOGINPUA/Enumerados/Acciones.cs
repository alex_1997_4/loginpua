﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LOGINPUA.Enumerados
{
    public class Acciones
    {
        public enum EnumAcciones
        {
            REGISTRAR,
            MODIFICAR,
            ANULAR,
            IMPRIMIR
        }
    }
}