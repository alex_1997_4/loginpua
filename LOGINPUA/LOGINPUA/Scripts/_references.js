/// <autosync enabled="true" />
/// <reference path="../angularjs/config/myconfig.js" />
/// <reference path="../angularjs/controller/accesoscontroller.js" />
/// <reference path="../angularjs/controller/administradorcontroller.js" />
/// <reference path="../angularjs/controller/asignarcontroller.js" />
/// <reference path="../angularjs/controller/empresacontroller.js" />
/// <reference path="../angularjs/controller/hacomcontroller.js" />
/// <reference path="../angularjs/controller/logincontroller.js" />
/// <reference path="../angularjs/controller/permisoscontroller.js" />
/// <reference path="../angularjs/controller/usuariocontroller.js" />
/// <reference path="../angularjs/directive/mydirective.js" />
/// <reference path="../angularjs/library/angular.min.js" />
/// <reference path="../angularjs/library/angular-datatables.bootstrap.min.js" />
/// <reference path="../angularjs/library/angular-datatables.columnfilter.min.js" />
/// <reference path="../angularjs/library/angular-datatables.light-columnfilter.js" />
/// <reference path="../angularjs/library/angular-datatables.min.js" />
/// <reference path="../angularjs/library/angular-resource.min.js" />
/// <reference path="../angularjs/library/angular-ui-router.min.js" />
/// <reference path="../angularjs/library/datatables.columnfilter.js" />
/// <reference path="../angularjs/library/datatables.lightcolumnfilter.min.js" />
/// <reference path="../angularjs/library/ngbreadcrumb.js" />
/// <reference path="../angularjs/library/ui-bootstrap-tpls.min.js" />
/// <reference path="../angularjs/module/myapp.js" />
/// <reference path="../angularjs/module/myapplogin.js" />
/// <reference path="../angularjs/service/myservices.js" />
/// <reference path="../angularjs/service/myservicesgeneral.js" />
/// <reference path="../angularjs/service/myserviceslogin.js" />
/// <reference path="../angularjs/utilidad/datatable.js" />
/// <reference path="../angularjs/utilidad/funciones.js" />
/// <reference path="../angularjs/utilidad/funcioneslogin.js" />
/// <reference path="../angularjs/utilidad/html.js" />
/// <reference path="../content/datapruebas/viajescorredores.js" />
/// <reference path="../content/js/all.min.js" />
/// <reference path="../content/js/bootstrap.min.js" />
/// <reference path="../content/js/bootstrap-datepicker.min.js" />
/// <reference path="../content/js/bootstrap-datetimepicker.min.js" />
/// <reference path="../content/js/bootstrap-datetimepicker.pt-br.js" />
/// <reference path="../content/js/bootstrap-select.js" />
/// <reference path="../content/js/bostrap.bundle.min.js" />
/// <reference path="../content/js/cargar_pagina.js" />
/// <reference path="../content/js/jquery.easing.min.js" />
/// <reference path="../content/js/jquery.min.js" />
/// <reference path="../content/js/js_buses/accordion.js" />
/// <reference path="../content/js/js_buses/js_registro_buses.js" />
/// <reference path="../content/js/js_calendario/bootstrap-datepicker.min.js" />
/// <reference path="../content/js/js_conductor/formulario.js" />
/// <reference path="../content/js/js_conductor/js_conductor.js" />
/// <reference path="../content/js/js_distancia/datatables.js" />
/// <reference path="../content/js/js_incidencias/accordion.js" />
/// <reference path="../content/js/js_incidencias/js_registro_incidencias.js" />
/// <reference path="../content/js/js_incidencias/js_registro_infracciones.js" />
/// <reference path="../content/js/js_mantenimiento_menu/registro_menus.js" />
/// <reference path="../content/js/js_mantenimiento_menu_perfil/registro_menu_perfil.js" />
/// <reference path="../content/js/js_mantenimiento_perfil/registro_perfil.js" />
/// <reference path="../content/js/js_mantenimiento_persona/registro_persona.js" />
/// <reference path="../content/js/js_mantenimiento_proveedor_serv/registro_proveedor_serv.js" />
/// <reference path="../content/js/js_mantenimiento_usuario/registro_usuario.js" />
/// <reference path="../content/js/js_pico_placa/pico_placa.js" />
/// <reference path="../content/js/js_programacion/alertify.min.js" />
/// <reference path="../content/js/js_programacion/full_screem.js" />
/// <reference path="../content/js/js_programacion/jquery.mcustomscrollbar.concat.min.js" />
/// <reference path="../content/js/js_programacion/jquery-3.3.1.min.js" />
/// <reference path="../content/js/js_programacion/modulo_admin.js" />
/// <reference path="../content/js/js_programacion/programacion.js" />
/// <reference path="../content/js/js_programacion_bus/js_conductor/formulario.js" />
/// <reference path="../content/js/js_programacion_bus/js_importarprogramacion.js" />
/// <reference path="../content/js/js_programacion_bus/js_prgdetall.js" />
/// <reference path="../content/js/js_registro_despacho/js_registrodespacho.js" />
/// <reference path="../content/js/js_registro_pico_placa/js_registropicoplaca.js" />
/// <reference path="../content/js/js_registro_pico_placa/js_rerporte_comparativo.js" />
/// <reference path="../content/js/js_rol_persona/accordion.js" />
/// <reference path="../content/js/js_rol_persona/js_registro_rol_persona.js" />
/// <reference path="../content/js/mobiscroll.jquery.min.js" />
/// <reference path="../content/js/monthpiker.js" />
/// <reference path="../content/plugins/datepicker/bootstrap-datepicker.min.js" />
/// <reference path="../content/plugins/daterangepicker/daterangepicker.min.js" />
/// <reference path="../content/plugins/fancytable/fancytable.js" />
/// <reference path="../content/plugins/lodash/lodash.min.js" />
/// <reference path="../content/plugins/moment/moment.js" />
/// <reference path="../content/plugins/numbervalidador/numbervalidador.js" />
/// <reference path="../content/plugins/raphael/raphael.js" />
/// <reference path="../content/plugins/sweetalert/sweetalert2.js" />
/// <reference path="../content/plugins/tooltipster/jquery.tooltipster.min.js" />
/// <reference path="../content/plugins/treeview/bootstrap-treeview.js" />
/// <reference path="../content/plugins/uitablefilter/jquery.uitablefilter.js" />
/// <reference path="../content/sweetalert/promise-polyfill.js" />
/// <reference path="../content/sweetalert/sweetalert2@8.js" />
/// <reference path="bootstrap.js" />
/// <reference path="exportar_excel/blob.js" />
/// <reference path="exportar_excel/filesaver.js" />
/// <reference path="exportar_excel/jhxlsx.js" />
/// <reference path="googlemapsutil/richmarker.js" />
/// <reference path="googlemapsutil/utilgooglemap.js" />
/// <reference path="jquery.datatables.min.js" />
/// <reference path="jquery.infinitecarousel3.min.js" />
/// <reference path="jquery.unobtrusive-ajax.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="jquery-3.1.1.min.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="scripts_admin/bootstrap-4.2.1.min.js" />
/// <reference path="scripts_admin/jquery.datatables.min.js" />
/// <reference path="scripts_admin/jquery.mcustomscrollbar.concat.min.js" />
/// <reference path="scripts_admin/jquery-3.3.1.min.js" />
/// <reference path="utilidades/util.js" />
