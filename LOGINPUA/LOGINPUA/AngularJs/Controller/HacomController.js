﻿'use strict';
MyApp.controller('HacomController', HacomController);
function HacomController(
    $scope, MyServices, $q, Datatable, Funciones
    ) {

    $scope.UsuarioHacom = function () {


        var defer = $q.defer();
        MyServices.Lista("Usuarios_Hacom", "Usuarios_Hacoms")
            .then(function (result) {
                 $scope.datas = result.data;
                defer.resolve(result.data);
            });
        return defer.promise;
    }

      
    $scope.UsuarioHacom();

};