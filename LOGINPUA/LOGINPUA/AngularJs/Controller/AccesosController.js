﻿'use strict';
MyApp.controller('AccesosController', AccesosController);
function AccesosController(
    $scope, DTOptionsBuilder, DTColumnBuilder, $q, $compile,
    Datatable, Funciones, Html, MyServices
    ) {
    var vm = this;
    var Controller = "Accesos";
    vm.dtInstance = {};
    $scope.ListaGrupos = [{ GRUNOM: 'Corredor' }, { GRUNOM: 'Programacion' }, { GRUNOM: 'Otros' }]
    vm.Html = function (accnom, accgru, accico, empcod) {
        var Model = {
            FaltaIngresarElNombreDelAcceso: accnom,
            FaltaSeleccionarElGrupo: accgru,
            FaltaIngresaElIconoDelAcceso: accico,
            FalaSeleccionarLaEmpresa : empcod
        }
        return Model;
    }
    vm.Servidor = function (Nivel, accnom, accico, accicofile, accdir, accgru, empcod) {
        var Model = {
            Nivel: Nivel,
            ACCNOM: accnom,
            ACCICO: accico,
            ACCICOFILE: accicofile,
            ACCDIR: accdir,
            ACCGRU: accgru,
            EMPCOD: empcod
        }
        return Model;
    }
    vm.Data = function () {
        var defer = $q.defer();
        MyServices.Lista(Controller, "Acceso")
            .then(function (result) {
                $scope.data = result.data;
                defer.resolve(result.data);
            });
        return defer.promise;
    }
    vm.DataTable = function () {
        vm.dtOptions = DTOptionsBuilder.fromFnPromise(vm.Data)
            .withPaginationType('full_numbers')
            .withOption('createdRow', vm.createdRow)
            .withLanguage(Datatable.language);
        vm.dtColumns = [
            DTColumnBuilder.newColumn('ACCCOD').withTitle('N°'),
            DTColumnBuilder.newColumn('ACCNOM').withTitle('Nombre del Acceso'),
            DTColumnBuilder.newColumn('ACCGRU').withTitle('Grupo'),
            DTColumnBuilder.newColumn(null).withTitle('Habilitado').renderWith(vm.checkboxAcciones),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').renderWith(vm.Acciones)
        ];
    }
    vm.createdRow = function (row, data, dataIndex) {
        $compile(angular.element(row).contents())($scope);
    }
    vm.Acciones = function (data, type, full, meta) {
        return '<div class="">' +
                    '<div class="">' +
                    '<div class="col">' +
                      '<button type="button" class="btn btn-success" ng-click="OpenModal(' + data.ACCCOD + ',' + 2 + ')"><i class="fas fa-edit"></i></button>' +
                     '<button type="button" class="btn btn-danger" ng-click="BorrarBtn(' + data.ACCCOD + ',' + 3 + ')"><i class="fas fa-trash-alt"></i></button>' +
                    '</div>' +
                  '</div>' +
                '</div>';
    }
    vm.checkboxAcciones = function (data, type, full, meta) {
        return '<input type="checkbox" class="form-control" id="04" ng-checked="' + data.ESTREG + '" disabled>';
    }
    $scope.OpenModal = function (Codigo, Nivel) {
        $scope.modalTipoStilo = "modal-dialog-grande";
        $scope.UrlPage = "";
        $scope.UrlUp = false;
        $scope.Objeto = [];
        MyServices.Lista("Empresa", "Empresas").then(function (result) {
            $scope.ListaEmpresas = result.data;
        });
        $scope.Objeto.accico = [];
        $scope.Items = [{}];
        if (Codigo != null) {
            for (var i = 0; i < $scope.data.length; i++) {
                if ($scope.data[i].ACCCOD == Codigo) {
                    MyServices.IdPor(Controller, "Acceso", $scope.data[i])
                        .then(function (result) {
                            $scope.Objeto = result.data;
                            $scope.Objeto.EMPCOD = result.data.EMPCOD.toString();
                        });
                }
            }
        }
        $scope.Nivel = Nivel;
        Html.OpenModal('Mantenimiento');
        $scope.Titulo = "Mantenimiento de Accesos";
        $scope.UrlUp = true;
        $scope.UrlPage = Funciones.UrlTipo("/" + Controller + "/", Nivel);
        vm.ActivarBotones(Nivel);
    }
    $scope.CrearBtn = function (objeto) {
        objeto.Nivel = $scope.Nivel;
        var HtmlModel = new vm.Html(objeto.accnom, objeto.accgru, objeto.accico, objeto.empcod);
        if (Funciones.validator(HtmlModel)) {
            var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.accnom, objeto.accico.name, objeto.accico,
                objeto.accdir, objeto.accgru, objeto.empcod);

            //var nuevoObjeto = [];
            //for (var i = 0; i < $scope.Items.length; i++) {
            //    nuevoObjeto.push({
            //        Nivel: ServidorModel.Nivel,
            //        ACCNOM: ServidorModel.ACCNOM,
            //        ACCICO: ServidorModel.ACCICO,
            //        ACCICOFILE: ServidorModel.ACCICOFILE,
            //        ACCDIR: ServidorModel.ACCDIR,
            //        ACCGRU: ServidorModel.ACCGRU,
            //    });
            //}
            MyServices.InsertarConArchivos(Controller, Controller, ServidorModel)
                .then(function (result) {
                    Html.CloseModal("Mantenimiento");
                    Funciones.Sweet("Correcto", "Acceso Registrado", 1, "Guardado");
                    vm.dtInstance.reloadData(true);
                    vm.DataTable();
                });
        }
    }
    $scope.ModificarBtn = function (objeto) {
        objeto.Nivel = $scope.Nivel;
        var HtmlModel = new vm.Html(objeto.ACCNOM, objeto.ACCGRU, "NoExisteImagen", objeto.EMPCOD);
        if (Funciones.validator(HtmlModel)) {
            if (objeto.accico != undefined) {
                var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.ACCNOM, objeto.accico.name, objeto.accico,
                objeto.ACCDIR, objeto.ACCGRU, objeto.EMPCOD);
            } else {
                var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.ACCNOM, "NoExisteImagen", "NoExisteImagen",
                objeto.ACCDIR, objeto.ACCGRU, objeto.EMPCOD);
            }
            ServidorModel.ACCCOD = objeto.ACCCOD;
            ServidorModel.ACCEMPCOD = objeto.ACCEMPCOD;
            MyServices.ModificarConArchivos(Controller, Controller, ServidorModel)
                .then(function (result) {
                    Html.CloseModal("Mantenimiento");
                    Funciones.Sweet("Correcto", "Acceso Actualizado", 1, "Guardado");
                    vm.dtInstance.reloadData(true);
                    vm.DataTable();
                });
        }
    }
    $scope.BorrarBtn = function (Codigo, Nivel) {
        var objeto = {};
        for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].ACCCOD == Codigo) {
                MyServices.IdPor(Controller, "Acceso", $scope.data[i])
                    .then(function (result) {
                        objeto = result.data;
                        objeto.Nivel = Nivel;
                        var nombre = "El Acceso a " + result.data.ACCNOM;
                        Funciones.SweetBorrar(result.data.ACCNOM, nombre, "Acceso").then((result) => {
                            if (result.value) {
                                MyServices.Mantenimiento(Controller, Controller, objeto).then(function (result) {
                                    Funciones.Sweet("Correcto", "Acceso Borrado", 1, "Guardado");
                                    vm.dtInstance.reloadData(true);
                                    vm.DataTable();
                                });
                            } else if (result.dismiss == "cancel") {

                            }
                        });
                    });
            }
        }
    }
    vm.ActivarBotones = function (Nivel) {
        switch (Nivel) {
            case 1:
                $scope.Crear = true;
                $scope.Modificar = false;
                $scope.Borrar = false;
                break;
            case 2:
                $scope.Crear = false;
                $scope.Modificar = true;
                $scope.Borrar = false;
                break;
            case 3:
                $scope.Crear = false;
                $scope.Modificar = false;
                $scope.Borrar = true;
                break;
            default:
                $scope.Crear = false;
                $scope.Modificar = false;
                $scope.Borrar = false;
                console.log('Detalle');
        }
    }
    $scope.addItemsCreate = function () {
        $scope.Obligatorio = true;
        var newItemNo = $scope.Items.length + 1;
        $scope.Items.push({
            val1: "N° " + newItemNo
        });
    }
    $scope.addItems = function () {
        var newItemNo = $scope.Items.length + 1;
        $scope.Items.push({
            val1: "N° " + newItemNo
        });
    }
    $scope.removeItemsCreate = function (index) {
        if (index !== 0) {
            $scope.Items.splice(index, 1);
            //$scope.Objeto.EMPCOD.splice(index, 1);
            //$scope.Objeto.ACCCOD.splice(index, 1);
        }
    }
    $scope.removeItems = function (index) {
        if (index !== 0) {
            $scope.Items.splice(index, 1);
            $scope.Objeto.ESTREG[index] = 0;
        }
    }

    vm.DataTable();
};