﻿'use strict';
MyApp.controller('UsuarioController', UsuarioController);
function UsuarioController(
    $scope, DTOptionsBuilder, DTColumnBuilder, $q, $compile,
    Datatable, Funciones, Html, MyServices, MyServicesGeneral,
    $location
    ) {
    var vm = this;
    var Controller = "Usuarios";
    $scope.Items = {};
    vm.dtInstance = {};
    $scope.Obligatorio = false;
    vm.Html = function (ndocumento, usunom, usuapepat, usuapemat, logcon) {
        var Model = {
            FaltaIngresarNumeroDocumento: ndocumento,
            FaltaIngresarElNombre: usunom,
            FaltaIngresarElApellidoPaterno: usuapepat,
            FaltaIngresarElApellidoMaterno: usuapemat,
            FaltaIngresarLaContraseña: logcon,
        }
        return Model;
    }
    vm.Servidor = function (Nivel, ndocumento, usunom, usuapepat, usuapemat, perfil, logcon) {
        var Model = {
            Nivel: Nivel,
            NDOCUMENTO: ndocumento,
            USUNOM: usunom,
            USUAPEPAT: usuapepat,
            USUAPEMAT: usuapemat,
            PERFIL: perfil,
            LOGUSU: usunom[0] + usuapepat,
            LOGCON: logcon,

        }
        return Model;
    }
    vm.Data = function () {
        var defer = $q.defer();
        MyServices.Lista(Controller, "Usuarios")
            .then(function (result) {
                $scope.data = result.data;
                 

                defer.resolve(result.data);
            });
        return defer.promise;
    }
    vm.DataTable = function () {
        vm.dtOptions = DTOptionsBuilder.fromFnPromise(vm.Data)
            .withPaginationType('full_numbers')
            .withOption('createdRow', vm.createdRow)
            .withLanguage(Datatable.language);
        vm.dtColumns = [
            DTColumnBuilder.newColumn('USUCOD').withTitle('Codigo'),
            DTColumnBuilder.newColumn('NDOCUMENTO').withTitle('Número de Documento'),
            DTColumnBuilder.newColumn('USUNOM').withTitle('Nombre'),
            DTColumnBuilder.newColumn('USUAPEPAT').withTitle('Apellido Paterno'),
            DTColumnBuilder.newColumn('USUAPEMAT').withTitle('Apellido Materno'),
            DTColumnBuilder.newColumn('LOGCON').withTitle('Contraseña Encriptada'),
            DTColumnBuilder.newColumn('PERFIL').withTitle('Perfil Usuario'),
            DTColumnBuilder.newColumn('FECHA_REGISTRO').withTitle('Fecha Registro').withOption('type', 'date'),

            DTColumnBuilder.newColumn('USUREG').withTitle('Usuario Registro'),

            DTColumnBuilder.newColumn(null).withTitle('Acciones').renderWith(vm.Acciones)
        ];
    }
    vm.createdRow = function (row, data, dataIndex) {
        $compile(angular.element(row).contents())($scope);
    }
    vm.Acciones = function (data, type, full, meta) {
        return '<div class="">' +
                    '<div class="">' +
                    '<div>' +
                      '<button type="button" class="btn btn-success"  style="padding:4px;" ng-click="OpenModal(' + data.USUCOD + ',' + 2 + ')"><i class="fas fa-edit"></i></button>' +
                      '<button type="button" class="btn btn-danger"  style="padding:4px;"  ng-click="BorrarBtn(' + data.USUCOD + ',' + 3 + ')"><i class="fas fa-trash-alt"></i></button>' +
                      '<button type="button" class="btn btn-primary" style="padding:4px;"  ng-click="OpenAsignacion(' + data.USUCOD + ')"><i class="fas fa-edit"></i></button>' +
                    '</div>' +
                  '</div>' +
                '</div>';
    }
    vm.checkboxAcciones = function (data, type, full, meta) {
        return '<input type="checkbox" class="form-control" id="04" ng-checked="' + data.ESTREG + '" disabled>';
    }
    $scope.OpenModal = function (Codigo, Nivel) {
        $scope.Asignacion = false;
        $scope.Obligatorio = false;
        $scope.modalTipoStilo = "modal-dialog-grande";
        $scope.UrlPage = "";
        $scope.UrlUp = false;
        $scope.Objeto = [];
        $scope.Objeto.EMPCOD = [];
        $scope.Objeto.ACCCOD = [];
        $scope.Items = [];
        MyServices.Lista(Controller, "Empresas").then(function (result) {
            $scope.ListaEmpresas = result.data;
        });
        if (Codigo != null) {
            for (var x = 0; x < $scope.data.length; x++) {
                if ($scope.data[x].USUCOD == Codigo) {
                    MyServices.IdPor(Controller, "Usuario", $scope.data[x])
                        .then(function (result) {
                            $scope.Objeto = result.data;
                            $scope.Objeto.EMPCOD = [];
                            $scope.Objeto.ACCCOD = [];
                            $scope.Objeto.ESTREG = [];
                            $scope.Objeto.LOGACCCOD = [];
                            for (var i = 0; i < $scope.Objeto.length; i++) {
                                $scope.addItems();
                                $scope.Objeto.USUCOD = $scope.Objeto[i].USUCOD;
                                $scope.Objeto.NDOCUMENTO = $scope.Objeto[i].NDOCUMENTO;
                                $scope.Objeto.USUNOM = $scope.Objeto[i].USUNOM;
                                $scope.Objeto.USUAPEPAT = $scope.Objeto[i].USUAPEPAT;
                                $scope.Objeto.USUAPEMAT = $scope.Objeto[i].USUAPEMAT;
                                $scope.Objeto.PERFIL = $scope.Objeto[i].PERFIL;
                                $scope.Objeto.LOGCOD = $scope.Objeto[i].LOGCOD;
                                $scope.Objeto.LOGUSU = $scope.Objeto[i].LOGUSU;
                                $scope.Objeto.LOGCON = $scope.Objeto[i].LOGCON;
                                $scope.Objeto.EMPCOD[i] = result.data[i].EMPCOD.toString();
                                $scope.Objeto.ACCCOD[i] = result.data[i].ACCCOD.toString();
                                $scope.Objeto.LOGACCCOD[i] = result.data[i].LOGACCCOD;
                                if (result.data[i].ESTREG == 1) {
                                    $scope.Objeto.ESTREG[i] = true;
                                } else {
                                    $scope.Objeto.ESTREG[i] = false;
                                }
                                $scope.BuscarAccesos($scope.Objeto.EMPCOD, i);
                            }
                            $scope.Nivel = Nivel;
                            Html.OpenModal('Mantenimiento');
                            $scope.Titulo = "Mantenimiento de Usuarios";
                            $scope.UrlUp = true;
                            $scope.UrlPage = Funciones.UrlTipo("/" + Controller + "/", Nivel);
                            vm.ActivarBotones(Nivel);
                        });
                }
            }
        } else {
            $scope.Nivel = Nivel;
            Html.OpenModal('Mantenimiento');
            $scope.Titulo = "Mantenimiento de Usuarios";
            $scope.UrlUp = true;
            $scope.UrlPage = Funciones.UrlTipo("/" + Controller + "/", Nivel);
            vm.ActivarBotones(Nivel);
        }
    }
    $scope.CrearBtn = function (objeto) {
        var conteo = 0;
        for (var i = 0; i < objeto.ACCCOD.length; i++) {
            conteo = 0;
            for (var x = 0; x < objeto.ACCCOD.length; x++) {
                if (objeto.ACCCOD[i] == objeto.ACCCOD[x]) {
                    conteo++;
                    if (conteo == 2) {
                        return Funciones.Sweet("Error", "Tiene Accesos Duplicados", 2, "Guardado");
                    }
                }
            }
        }
        objeto.Nivel = $scope.Nivel;
        var HtmlModel = new vm.Html(objeto.ndocumento, objeto.usunom, objeto.usuapepat,
            objeto.usuapemat, objeto.perfil, objeto.logcon);
        if (Funciones.validator(HtmlModel)) {
            var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.ndocumento, objeto.usunom, objeto.usuapepat,
            objeto.usuapemat, objeto.perfil, objeto.logcon);
            if ($scope.Obligatorio == false) {
                return Funciones.Sweet("Error", "Minimo debe tener un Acceso el Usuario Registrado", 2, "Ok");
            }
            if (objeto.EMPCOD == undefined) {
                return Funciones.Sweet("Error", "Falta Seleccionar una Empresa Nro 1", 2, "Ok");
            } else if (objeto.ACCCOD == undefined || objeto.ACCCOD == "") {
                return Funciones.Sweet("Error", "Falta Seleccionar su Acceso Nro 1", 2, "Ok");
            }
            for (var x = 0; x < $scope.Items.length; x++) {
                if (objeto.EMPCOD[x] == undefined) {
                    return Funciones.Sweet("Error", "Falta Seleccionar una Empresa Nro " + (x + 1), 2, "Ok");
                } else if (objeto.ACCCOD[x] == undefined || objeto.ACCCOD[x] == "") {
                    return Funciones.Sweet("Error", "Falta Seleccionar su Acceso Nro " + (x + 1), 2, "Ok");
                }
            }

            var nuevoObjeto = [];
            ServidorModel.LOGUSU = ServidorModel.LOGUSU.replace(/ /g, "");
            for (var i = 0; i < $scope.Items.length; i++) {
                nuevoObjeto.push({
                    Nivel: ServidorModel.Nivel,
                    NDOCUMENTO: ServidorModel.NDOCUMENTO.toUpperCase(),
                    USUNOM: ServidorModel.USUNOM.toUpperCase(),
                    USUAPEPAT: ServidorModel.USUAPEPAT.toUpperCase(),
                    USUAPEMAT: ServidorModel.USUAPEMAT.toUpperCase(),
                    PERFIL: ServidorModel.PERFIL.toUpperCase(),
                    LOGUSU: ServidorModel.LOGUSU.toUpperCase(),
                    LOGCON: ServidorModel.LOGCON,
                    EMPCOD: objeto.EMPCOD[i],
                    ACCCOD: objeto.ACCCOD[i],
                });
            }

            MyServices.Mantenimiento(Controller, Controller, nuevoObjeto)
                .then(function (result) {
                    Html.CloseModal("Mantenimiento");
                    Funciones.Sweet("Correcto", "Usuario Registrado", 1, "Guardado");
                    vm.dtInstance.reloadData(true);
                    vm.DataTable();
                });
        }
    }
    $scope.ModificarBtn = function (objeto) {
        var conteo = 0;
        for (var i = 0; i < objeto.ACCCOD.length; i++) {
            conteo = 0;
            for (var x = 0; x < objeto.ACCCOD.length; x++) {
                if (objeto.ACCCOD[i] == objeto.ACCCOD[x]) {
                    conteo++;
                    if (conteo == 2) {
                        return Funciones.Sweet("Error", "Tiene Accesos Duplicados", 2, "Guardado");
                    }
                }
            }
        }
        objeto.Nivel = $scope.Nivel;
        var HtmlModel = new vm.Html(objeto.NDOCUMENTO, objeto.USUNOM, objeto.USUAPEPAT,
            objeto.USUAPEMAT, objeto.PERFIL, objeto.LOGCON);
        if (Funciones.validator(HtmlModel)) {
            var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.NDOCUMENTO, objeto.USUNOM, objeto.USUAPEPAT,
            objeto.USUAPEMAT, objeto.PERFIL, objeto.LOGCON);

            if (objeto.EMPCOD == undefined) {
                return Funciones.Sweet("Error", "Falta Seleccionar una Empresa Nro 1", 2, "Ok");
            } else if (objeto.ACCCOD == undefined || objeto.ACCCOD == "") {
                return Funciones.Sweet("Error", "Falta Seleccionar su Acceso Nro 1", 2, "Ok");
            }
            for (var x = 0; x < $scope.Items.length; x++) {
                if (objeto.EMPCOD[x] == undefined) {
                    return Funciones.Sweet("Error", "Falta Seleccionar una Empresa Nro " + (x + 1), 2, "Ok");
                } else if (objeto.ACCCOD[x] == undefined || objeto.ACCCOD[x] == "") {
                    return Funciones.Sweet("Error", "Falta Seleccionar su Acceso Nro " + (x + 1), 2, "Ok");
                }
            }
            var nuevoObjeto = [];
            ServidorModel.LOGUSU = ServidorModel.LOGUSU.replace(/ /g, "");
            for (var i = 0; i < $scope.Objeto.ACCCOD.length; i++) {
                nuevoObjeto.push({
                    Nivel: ServidorModel.Nivel,
                    USUCOD: objeto.USUCOD,
                    NDOCUMENTO: ServidorModel.NDOCUMENTO.toUpperCase(),
                    USUNOM: ServidorModel.USUNOM.toUpperCase(),
                    USUAPEPAT: ServidorModel.USUAPEPAT.toUpperCase(),
                    USUAPEMAT: ServidorModel.USUAPEMAT.toUpperCase(),
                    PERFIL: ServidorModel.PERFIL.toUpperCase(),
                    LOGCOD: objeto.LOGCOD,
                    LOGUSU: ServidorModel.LOGUSU.toUpperCase(),
                    LOGCON: ServidorModel.LOGCON,
                    EMPCOD: objeto.EMPCOD[i],
                    ACCCOD: objeto.ACCCOD[i],
                    LOGACCCOD: objeto.LOGACCCOD[i],
                    USUREG: objeto.USUREG,
                    FECREG: objeto.FECREG,
                    ESTREG: objeto.ESTREG[i]
                });
            }

            MyServices.Mantenimiento(Controller, Controller, nuevoObjeto)
                .then(function (result) {
                    Html.CloseModal("Mantenimiento");
                    Funciones.Sweet("Correcto", "Usuario Actualizado", 1, "Guardado");
                    vm.dtInstance.reloadData(true);
                    vm.DataTable();
                });
        }
    }
    $scope.BorrarBtn = function (Codigo, Nivel) {
        var objeto = [{}];
        for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].USUCOD == Codigo) {
                MyServices.IdPor(Controller, "Usuario", $scope.data[i])
                    .then(function (result) {
                        for (var i = 0; i < result.data.length; i++) {
                            objeto[i] = result.data[i];
                            objeto[i].Nivel = Nivel;
                            var nombre = "el Usuario " + result.data[0].LOGUSU;
                        }
                        Funciones.SweetBorrar(result.data[0].USUNOM, nombre, "Usuario").then((result) => {
                            if (result.value) {
                                MyServices.Mantenimiento(Controller, Controller, objeto).then(function (result) {
                                    Funciones.Sweet("Correcto", "Usuario Borrada", 1, "Guardado");
                                    vm.dtInstance.reloadData(true);
                                    vm.DataTable();
                                });
                            } else if (result.dismiss == "cancel") {

                            }
                        });
                    });
            }
        }
    }
    vm.ActivarBotones = function (Nivel) {
        switch (Nivel) {
            case 1:
                $scope.Crear = true;
                $scope.Modificar = false;
                $scope.Borrar = false;
                break;
            case 2:
                $scope.Crear = false;
                $scope.Modificar = true;
                $scope.Borrar = false;
                break;
            case 3:
                $scope.Crear = false;
                $scope.Modificar = false;
                $scope.Borrar = true;
                break;
            default:
                $scope.Crear = false;
                $scope.Modificar = false;
                $scope.Borrar = false;
                console.log('Detalle');
        }
    }
    $scope.addItemsCreate = function () {
        $scope.Obligatorio = true;
        var newItemNo = $scope.Items.length + 1;
        $scope.Items.push({
            val1: "N° " + newItemNo
        });
    }
    $scope.addItems = function () {
        var newItemNo = $scope.Items.length + 1;
        $scope.Items.push({
            val1: "N° " + newItemNo
        });
    }
    $scope.removeItemsCreate = function (index) {
        if (index !== 0) {
            $scope.Items.splice(index, 1);
            $scope.Objeto.EMPCOD.splice(index, 1);
            $scope.Objeto.ACCCOD.splice(index, 1);
        }
    }
    $scope.removeItems = function (index) {
        if (index !== 0) {
            $scope.Items.splice(index, 1);
            $scope.Objeto.ESTREG[index] = 0;
        }
    }
    $scope.ListaAccesos = [];
    $scope.BuscarAccesos = function (Codigo, index) {
        for (var i = 0; i < $scope.ListaEmpresas.length; i++) {
            if ($scope.ListaEmpresas[i].EMPCOD == Codigo[index]) {
                $scope.ListaAccesos[index] = {};
                MyServices.ListaDe(Controller, "Accesos", $scope.ListaEmpresas[i]).then(function (r) {
                    $scope.ListaAccesos[index] = r.data;
                })
            }
        }
    }
    $scope.habilitar = function (value, index) {
        if (value == true) {
            return true;
        }
        else {
            return false;
        }
    }
    $scope.OpenAsignacion = function (Codigo) {
        var obj = {};
        $scope._codigoAsignador = Codigo;
        obj.LOGCOD = Codigo;
        $scope.ListaInput = [];
        $scope.Objeto = [];
        MyServices.ListaDe("Asignar", "UsuariosPorAsignacion", obj)
        .then(function (result) {
            $scope.ListaInput = result.data.TB_EMPRESA;
            $scope.Objeto = angular.copy(result.data.TB_EMPRESA);
            if (result.data.TM_USUARIOS_EMPRESA.length > 0) {
                for (var i = 0; i < $scope.ListaInput.length; i++) {
                    for (var x = 0; x < result.data.TM_USUARIOS_EMPRESA.length; x++) {
                        if (result.data.TM_USUARIOS_EMPRESA[x].EMPCOD == $scope.ListaInput[i].EMPCOD) {
                            $scope.Objeto[i] = [];
                            $scope.Objeto[i] = {
                                EMPCOD: $scope.ListaInput[i].EMPCOD,
                                EMPNOM: $scope.ListaInput[i].EMPNOM,
                                USUEMPUSU: result.data.TM_USUARIOS_EMPRESA[x].USUEMPUSU,
                                USUEMPCON: result.data.TM_USUARIOS_EMPRESA[x].USUEMPCON,
                                LOGCOD: Codigo
                            };
                        }
                    }
                }
            } else {
                for (var i = 0; i < $scope.ListaInput.length; i++) {
                    $scope.Objeto[i] = [];
                    $scope.Objeto[i] = {
                        EMPCOD: $scope.ListaInput[i].EMPCOD,
                        EMPNOM: $scope.ListaInput[i].EMPNOM,
                        USUEMPUSU: "",
                        USUEMPCON: "",
                        LOGCOD: Codigo
                    };
                }
            }
        });
        Html.OpenModal('Asignacion');
        $scope.Titulo = "Asignacion de Usuarios";
        $scope.UrlUp = true;
        $scope.UrlPage = MyServicesGeneral.Url() + "/Usuarios/Asignar";
        $scope.Asignacion = true;
    }
    $scope.BtnAsignacion = function (objeto) {
        var datos_objeto = [];
        for (var i = 0; i < objeto.length; i++) {
            debugger;
            datos_objeto.push({
                LOGCOD: $scope._codigoAsignador,
                USUEMPUSU: objeto[i].USUEMPUSU,
                USUEMPCON: objeto[i].USUEMPCON,
                EMPCOD: objeto[i].EMPCOD,
                EMPNOM: objeto[i].EMPNOM,
                EMPPAG: objeto[i].EMPPAG,
                ESTREG: objeto[i].ESTREG,
            });
        }
        MyServices.Mantenimiento("Asignar", "Asignacion", datos_objeto)
        .then(function (result) {
            if (result.data.tipo == 1) {
                Funciones.Sweet("Aviso", result.data.msj, 3, "Aviso");
            }
            else {
                Funciones.Sweet("Correcto", "Actualizacion de Asignacion", 1, "Guardado");
                Html.CloseModal('Asignacion');
                vm.dtInstance.reloadData(true);
                vm.DataTable();
            }
        });
    }
    vm.DataTable();
};