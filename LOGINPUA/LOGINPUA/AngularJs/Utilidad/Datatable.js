﻿'use strict';
MyApp.service('Datatable', Datatable);
function Datatable($http, $q, $resource, $location) {
    //Datatable
    var vm = this;
    vm.language = {
        "processing": "Procesando...",
        "search": "Buscar:",
        "lengthMenu": "Mostrar _MENU_ elementos",
        "info": "Mostrando desde _START_ al _END_ de _TOTAL_ elementos",
        "infoEmpty": "Mostrando ning&uacute;n elemento.",
        "infoFiltered": "(filtrado _MAX_ elementos total)",
        "infoPostFix": "",
        "loadingRecords": "Cargando registros...",
        "zeroRecords": "No se encontraron registros",
        "emptyTable": "No hay datos disponibles en la tabla",

        "paginate": {
            "first": "Primero",
            "previous": "Anterior",
            "next": "Siguiente",
            "last": "Último"
        },
        "aria": {
            "sortAscending": ": Activar para ordenar la tabla en orden ascendente",
            "sortDescending": ": Activar para ordenar la tabla en orden descendente"
        }
    }
};