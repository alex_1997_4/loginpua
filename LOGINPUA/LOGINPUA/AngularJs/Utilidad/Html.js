﻿'use strict';
MyApp.service('Html', Html);
function Html() {
    var vm = this;
    var data;
    vm.Opentoast = function () {
        var toastDirective = $('#toast');
        toastDirective.toast('show');
    }
    vm.Closetoast = function () {
        var toastDirective = $('#toast');
        toastDirective.toast('hide');
    }
    vm.OpenModal = function (NombreConcatenado) {
        var modalDirective = $('#Modal' + NombreConcatenado + "");
        modalDirective.modal('show');
    }
    vm.CloseModal = function (NombreConcatenado) {
        var modalDirective = $('#Modal' + NombreConcatenado + "");
        modalDirective.modal('hide');
    }
};