﻿'use strict';
MyApp
.directive('datatable', function () {
    return {
        restrict: 'E',
        template: '<div class="table-responsive">' +
                    '<table style="width:100%" datatable="" dt-options="showCase.dtOptions" dt-columns="showCase.dtColumns" dt-instance="showCase.dtInstance" class="table table-hover"></table>' +
                  '</div>'
    };
})
.directive('toast', function () {
    return {
        restrict: 'E',
        template: '<div class="{{htmlClass}}" position-absolute w-100 d-flex flex-column p-4">' +
                '<div class="toast ml-auto" role="alert" id="toast" data-delay="700" data-autohide="false">' +
                    '<div class="toast-header" style="background: #17a2b8;color: white;">' +
                        '<strong class="mr-auto text-primary" style="color: white!important;">{{htmlTitulo}}</strong>' +
                        '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">' +
                            '<span aria-hidden="true">×</span>' +
                        '</button>' +
                    '</div>' +
                    '<div class="toast-body">{{htmlPalabra}}</div>' +
                '</div>' +
            '</div>'
    };
})
.directive('modalMantenimiento', function () {
    return {
        restrict: 'E',
        template: '<div class="modal" id="ModalMantenimiento" aria-labelledby="ModalMantenimiento" aria-hidden="true">' +
                    '<div ng-class="modalTipoStilo" class="modal-dialog" role="document">' +
                    '<div class="modal-content container-fluid">' +
                      '<div class="modal-header">' +
                        '<h5 class="modal-title">{{Titulo}}</h5>' +
                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                          '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                      '</div>' +
                      '<div class="modal-body" ng-if="UrlUp == true">' +
                        '<div ng-include="UrlPage"></div>' +
                      '</div>' +
                      '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-success" ng-if="Crear == true" ng-click="CrearBtn(Objeto)">Crear</button>' +
                        '<button type="button" class="btn btn-primary" ng-if="Modificar == true" ng-click="ModificarBtn(Objeto)">Modificar</button>' +
                        '<button type="button" class="btn btn-warning" ng-if="Borrar == true" ng-click="BorrarBtn(Objeto)">Borrar</button>' +
                        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>'
    };
})
.directive('modalAsignacion', function () {
    return {
        restrict: 'E',
        template: '<div class="modal" id="ModalAsignacion" aria-labelledby="ModalAsignacion" aria-hidden="true">' +
                    '<div ng-class="modalTipoStilo" class="modal-dialog" role="document">' +
                    '<div class="modal-content container-fluid">' +
                      '<div class="modal-header">' +
                        '<h5 class="modal-title">{{Titulo}}</h5>' +
                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                          '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                      '</div>' +
                      '<div class="modal-body" ng-if="UrlUp == true">' +
                        '<div ng-include="UrlPage"></div>' +
                      '</div>' +
                      '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-success" ng-if="Asignacion == true" ng-click="BtnAsignacion(Objeto)">Asignar</button>' +
                        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>'
    };
})
.directive('myBreadcrumb', function () {
    return {
        restrict: 'E',
        template: '<nav aria-label="breadcrumb">' +
                      '<ol class="breadcrumb">' +
                        '<li class="breadcrumb-item" ng-repeat-start="item in myBreadcrumb" ng-repeat-end><a href="#!/{{item.Url}}">{{item.Page}}</a></li>' +
                      '</ol>' +
                    '</nav>'
    };
})
.directive('myTarget', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var href = element.href;
            if (true) {  // replace with your condition
                element.attr("target", "_blank");
            }
        }
    };
})
//.directive('fileModel', [function () {
//        return {
//            controller: ['$parse', '$element', '$attrs', '$scope', function ($parse, $element, $attrs, $scope) {
//                var exp = $parse($attrs.filesModel);
//                $element.on('change', function () {
//                    exp($scope, this.files);
//                    $scope.Objeto.accico = this.files[0];
//                    console.log($scope.Objeto.accico);
//                    $scope.$apply();
//                });
//            }]
//        }
//}])
.directive("akFileModel", ["$parse",
                function ($parse) {
                    return {
                        restrict: "A",
                        link: function (scope, element, attrs) {
                            var model = $parse(attrs.akFileModel);
                            var modelSetter = model.assign;
                            element.bind("change", function () {
                                scope.$apply(function () {
                                    scope.Objeto.accico = modelSetter(scope, element[0].files[0]);
                                });
                            });
                        }
                    }
                }]);