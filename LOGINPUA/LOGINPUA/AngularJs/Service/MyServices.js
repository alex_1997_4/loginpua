﻿'use strict';
MyApp.service('MyServices', MyServices);
function MyServices($http, MyServicesGeneral) {
    var vm = this;
    var data;
    vm.Lista = function (Controllador, Tipos) {
        data = MyServicesGeneral.Post("/" + Controllador + "/Lista" + Tipos + "");
        data.then(function Funciono(respuesta) {
        }, function Fallo(respuesta) {
        });
        return data;
    }
    vm.IdPor = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostData("/" + Controllador + "/IdPor" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
        }, function Fallo(respuesta) {
        });
        return data;
    }
    vm.Mantenimiento = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostData("/" + Controllador + "/Mantenimiento" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
        }, function Fallo(respuesta) {
        });
        return data;
    }
    vm.Insertar = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostData("/" + Controllador + "/Insertar" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
        }, function Fallo(respuesta) {
        });
        return data;
    }
    vm.InsertarConArchivos = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostDataFile("/" + Controllador + "/Mantenimiento" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
            //console.log(respuesta);
        }, function Fallo(respuesta) {
            //console.log(respuesta.statusText);
        });
        return data;
    }
    vm.Modificar = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostData("/" + Controllador + "/Modificar" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
           // console.log(respuesta);
        }, function Fallo(respuesta) {
           // console.log(respuesta.statusText);
        });
        return data;
    }
    vm.ModificarConArchivos = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostDataFile("/" + Controllador + "/Mantenimiento" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
           // console.log(respuesta);
        }, function Fallo(respuesta) {
           // console.log(respuesta.statusText);
        });
        return data;
    }
    vm.Borrar = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostData("/" + Controllador + "/Borrar" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
           // console.log(respuesta);
        }, function Fallo(respuesta) {
          //  console.log(respuesta.statusText);
        });
        return data;
    }
    vm.ListaDe = function (Controllador, Tipo, Objeto) {
        data = MyServicesGeneral.PostData("/" + Controllador + "/ListaDe" + Tipo + "", Objeto);
        data.then(function Funciono(respuesta) {
          //  console.log(respuesta);
        }, function Fallo(respuesta) {
           // console.log(respuesta.statusText);
        });
        return data;
    }
};