﻿'use strict';
MyApp.service('MyServicesGeneral', MyServicesGeneral);
function MyServicesGeneral($http) {
    var vm = this;
    var data;
    vm.Url = function () {
        return urlSitio;
    }
    vm.FileData = function (data) {
        var FileDataValue = new FormData();
        angular.forEach(data, function (value, key) {
            FileDataValue.append(key, value);
        });
        return FileDataValue;
    };
    vm.PostDataFile = function (url, value) {
        data = $http({
            method: "POST",
            url: vm.Url() + url,
            data: vm.FileData(value),
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        });
        return data;
    }
    //Funciones Generales
    vm.Post = function (url) {
        data = $http({
            method: "POST",
            url: vm.Url() + url,
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
    vm.PostData = function (url, value) {
        var data = $http({
            method: "POST",
            url: vm.Url() + url,
            data: JSON.stringify(value),
            headers: { "Content-Type": "application/json; charset = utf-8;" },
        });
        return data;
    }
    vm.Get = function (url) {
        data = $http({
            method: "GET",
            url: vm.Url() + url,
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
    vm.GetData = function (url, value) {
        data = $http({
            method: "GET",
            url: vm.Url() + url,
            data: JSON.stringify(value),
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
};