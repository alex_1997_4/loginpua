﻿namespace LOGINPUA.Util.Knockout
{
  public enum KnockoutValueUpdateKind
  {
    Change, KeyUp, KeyPress, AfterKeyDown
  }
}
