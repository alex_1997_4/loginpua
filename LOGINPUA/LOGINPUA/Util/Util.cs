﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ENTIDADES;

namespace LOGINPUA.Util
{
    public class Util
    {
        public Boolean validaVistaxPerfil(String ActionController, DataTable lista)
        {
            if (lista == null) { return false; }
            Boolean flagAuxValida = false;
            //
            foreach (DataRow item in lista.Rows)
            {
                var urlAction = item[3].ToString();
                int index = urlAction.IndexOf('/');
                string txtController = "";
                string txtAction = "";
                //
                if (index != -1)
                {
                    String[] dataSplit = urlAction.Split('/');
                    txtController = dataSplit[0];
                    txtAction = dataSplit[1];
                    if (ActionController.ToUpper() == txtAction.ToUpper())
                    {
                        flagAuxValida = true;
                    }
                }
            }
            return flagAuxValida;
        }

        public List<CC_MENUPERFIL_ACCION> validadAccionMenu(List<CC_MENUPERFIL_ACCION> Lista_acciones, string nombreActionCurrent, string controller)
        {

            List<CC_MENUPERFIL_ACCION> Lista_acciones_ = new List<CC_MENUPERFIL_ACCION>();


            foreach (var item_acciones in Lista_acciones)
            {

                var urlAction = item_acciones.URL;
                String[] dataSplit = urlAction.Split('/');
                var txtAction = dataSplit[1];
                var txtController = dataSplit[0];

                if (nombreActionCurrent == txtAction && txtController == controller)
                {
                    CC_MENUPERFIL_ACCION acciones = new CC_MENUPERFIL_ACCION();
                    acciones.ID_ACCION = item_acciones.ID_ACCION;
                    acciones.NOMBRE = item_acciones.NOMBRE;
                    acciones.ICON_ACCION = item_acciones.ICON_ACCION;
                    acciones.MENU = item_acciones.MENU;
                    Lista_acciones_.Add(acciones);
                }
            }
            return Lista_acciones_;
        }
    }
}
