﻿var DATAJSONAUTOCOMPLETE = []
var SERVICIO = 0
var IDSALIDA_PROGDETA = 0
var IDSALIDA_ESPECIFICO = 0



$(document).ready(function () {



    getSelectRutaxCorredor($("#txtRuta").val());

    $('#txtRuta').on('change', function () {
        getSelectRutaxCorredor($("#txtRuta").val());
        $('#txtrutaserv-button').children('span').css('font-size', '11px')
        $('#txtrutaserv-button').children('span').css('margin-left', '-15px')
        getLado_X_Ruta($('#txtRuta').val());


    });

    $('#txtrutaserv').on('change', function () {
        $('#txtlado').empty();

        $('#txtrutaserv-button').children('span').css('font-size', '11px')
        $('#txtrutaserv-button').children('span').css('margin-left', '-15px')
    });

    $.each($('.modalhref'), function () {
        $(".modalhref").attr('href', "#standard");
    });
    getLado_X_Ruta($('#txtRuta').val());

    $('#txtrutaserv-button').css('height', '46px')


    getPlaca_Buses();
    getConductores();


    $('#txtDispo').on('click', function () {
        if ($(this).is(':checked')) {

            $('#idDespachoUnidad_Serv').children('tbody').children().each(function () {
                if ($(this).attr('data-vacio') == 0) {
                    $(this).css('display', 'none')
                }
            });

        } else {

            $('#idDespachoUnidad_Serv').children('tbody').children().each(function () {
                if ($(this).attr('data-vacio') == 0) {
                    $(this).removeAttr('style')
                }
            });
        }
    });

    //var cloned = $('#txtlado').clone();
    //console.log(cloned,'cloned')
    //cloned.attr('id', 'id_of_dup');
    //cloned.appendTo($("#id_of_form"));


    $("#autocomplete-4").keypress(function () {
        alert(1)
        $('#idtr').empty()

        if ($(this).val().length >= 3) {
            var textoIngresado = $(this).val();
             _.filter(DATAJSONAUTOCOMPLETE, function (obj) {
                var result = _.startsWith(obj.label, textoIngresado);
                if (result==true) {
                     
                    $('#idtr').append('<tr onclick="seleccionaFila($(this))" class="select_results__option"><td>' + obj.label + '</td> </tr>')
                }
 
            });
        }
        //$('#autocomplete-4').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children().css('z-index', '99999999')

    });

});









function getObtenerInformacion(elemet) {
    $('#txtlado_unidad2').empty()

    var cloned = $('#txtlado').clone();
    cloned.attr('id', 'txtlado_unidad');
    cloned.attr('class', 'form-control');
    cloned.appendTo($("#txtlado_unidad2"));


    $("#txtDispo").attr('checked', true);


    //$("#page").css("background", "rgba(0, 0, 0, 0.6);");


    $('#idDespachoUnidad_Serv tbody').empty();

    SERVICIO = 0;
    IDSALIDA_PROGDETA = 0;

    $('#txtTitle').empty()

    var ruta = $('#txtRuta :selected').text()
    var lado = $('#txtlado').val()

    SERVICIO = elemet.attr('data-servicio')
    IDSALIDA_PROGDETA = elemet.attr('data-id_msalida_prog_det')

    $('#txtservicio_unidad').val(SERVICIO)
    $('#txtlado_unidad').val(lado)
    var title_ = "Despacho Unidad" + " " + ruta;
    $('#txtTitle').text(title_)

    getListarViajesXServ(1);

}

function getSelectRutaxCorredor(id_ruta_) {
    $('#txtrutaserv').empty();
    $('#txtrutaserv-button').children('span').empty();

    $.ajax({
        url: URL_GET_RUTATIPO_SERVICIO,
        dataType: 'json',
        data: { id_ruta: id_ruta_ },
        success: function (result) {
            if (result.length == 0) { // si la lista esta vacia
                $('#txtrutaserv').append('<option value="0">' + 'No existen datos' + '</option>').selectmenu('refresh');
                $('#txtrutaserv-button').children('span').css('font-size', '11px')
                $('#txtrutaserv-button').children('span').css('margin-left', '-15px')
                return false;
            } else {
                $.each(result, function () {
                    $('#txtrutaserv').append('<option value="' + this.ID_RUTA_TIPO_SERVICIO + '">' + this.NOMBRE + '</option>').selectmenu('refresh');;
                });
            }

            $('#txtrutaserv-button').children('span').css('font-size', '11px')
            $('#txtrutaserv-button').children('span').css('margin-left', '-15px')

        }
    }, JSON);
}



function getLado_X_Ruta(id_ruta_) {
    $('#txtlado').empty();
    $('#txtlado-button').children('span').empty();

    $.ajax({
        url: URL_GET_LADO_X_RUTA,
        dataType: 'json',
        data: { id_ruta: id_ruta_ },
        success: function (result) {
            if (result.length == 0) { // si la lista esta vacia
                $('#txtlado').append('<option value="0">' + 'No existen datos' + '</option>').selectmenu('refresh');
                return false;
            } else {
                $.each(result, function () {
                    $('#txtlado').append('<option value="' + this.SENTIDO + '">' + this.LADO + '</option>').selectmenu('refresh');;
                });
            }
            //$("#txtlado").clone().prependTo("#txtlado_unidad2").attr('id', 'txtlado_unidad');


        }
    }, JSON);
}


function getPlaca_Buses() {
    $('#txtunidad_').empty();
    $('#txtunidad_-button').children('span').empty();

    $.ajax({
        url: URL_GET_PLACAS,
        dataType: 'json',
        success: function (result) {
            if (result.length == 0) { // si la lista esta vacia
                $('#txtunidad_').append('<option value="0">' + 'No existen datos' + '</option>')
                return false;
            } else {
                $.each(result, function () {
                    $('#txtunidad_').append('<option value="' + this.BS_PLACA + '">' + this.BS_PLACA + '</option>')
                });
            }
            //$('.selectpicker').selectpicker('refresh');

            //$('.selectpicker').selectmenu('refresh');

        }
    }, JSON);
}

function getConductores() {
    DATAJSONAUTOCOMPLETE = [];

    $('#txtconductor_').empty();
    $('#txtconductor_-button').children('span').empty();

    $.ajax({
        url: URL_GET_CONDUCTORES,
        dataType: 'json',
        success: function (result) {


            $.each(result, function () {
                var item = {
                    label: this.CODIGO + '-' + this.APELLIDOS,
                    value: this.CODIGO
                }
                DATAJSONAUTOCOMPLETE.push(item);
            }) 
        }
    }, JSON);
}


function getListarProgramacionUnidades() {
    var id_ruta_ = $('#txtrutaserv').val();
    var sentido_ = $('#txtlado').val();

    var i = 0;
    var strHTML = "";
    $('#tbProDet tbody').empty();
    block();
    $.ajax({
        url: URL_GET_GETVIAJES_PROGRUNIDADES,
        dataType: 'json',
        data: {
            id_ruta: id_ruta_,
            sentido: sentido_
        },
        success: function (result) {



            if (result.length == 0) {
                strHTML += '<tr><td colspan="5" class="text-center">No hay información para mostrar</td></tr>';
            } else {
                $.each(result, function () {
                    i++;


                    strHTML += '<tr data-servicio="' + this.SERVICIO + '" data-id_msalida_prog_det="' + this.ID_MSALIDA_PROG_DET + '" class="modalhref" style="cursor:pointer" onclick="getObtenerInformacion($(this))">' +
                       '<td> ' + i + '</td>' +
                       '<td>' + this.SERVICIO + '</td>' +
                       '<td>' + this.HSALIDA + '</td>' +
                       '<td>' + (this.PLACA_TIEMPOREAL == null ? "<span style='color:blue'>DISP</span>" : this.PLACA_TIEMPOREAL) + '</td>' +
                       '<td>' + (this.CAC_TIEMPOREAL == 0 ? "<span style='color:blue'>DISP</span>" : this.CAC_TIEMPOREAL) + '</td>' +
                       '</tr>';
                });
            }
            $('#tbProDet tbody').append(strHTML);
            $('#tbProDet tr').click(function () {


                var accounta = "";
                accounta = $('#modal_RegistroProgDetall').mobiscroll().popup({
                    display: 'center'
                }).mobiscroll('getInst'),
                        scrollable = $('#scrollable').mobiscroll().popup({
                            display: 'center',
                            scrollLock: false,
                            cssClass: 'mbsc-no-padding md-content-scroll',
                            buttons: []
                        }).mobiscroll('getInst');

                accounta.show();
            });

            setTimeout(function () {
                unblock();
            }, 500);

        },
        error: function (xhr, status, error) {
            strHTML += '<tr><td colspan="5" class="text-center">No hay información para mostrar</td></tr>';
        },
    }, JSON);
}


function getListarViajesXServ(action) {



    var idservicio_ = "";
    var i = 0;
    var strHTML = "";

    $('#idDespachoUnidad_Serv tbody').empty();
    var id_ruta_ = $('#txtrutaserv').val();
    var sentido_ = $('#txtlado_unidad').val();
    var idsalida_prog_deta_ = IDSALIDA_PROGDETA

    //ACCION DE LISTAR POR LA TABLA O POR EL BUTTON
    if (action == 1) { idservicio_ = SERVICIO; } else if (action == 2) { idservicio_ = $('#txtservicio_unidad').val(); }

    block();

    $.ajax({
        url: URL_GET_GETVIAJES_PROGRX_SERVICIO,
        dataType: 'json',
        data: {
            id_ruta: id_ruta_,
            sentido: sentido_,
            idservicio: idservicio_
        },
        success: function (result) {


            if (result.length == 0) {
                strHTML += '<tr><td colspan="6" class="text-center">No hay información para mostrar</td></tr>';
            } else {


                $.each(result, function () {
                    var styleauto = "";
                    var vacios2 = 0;
                    var display = "";


                    if (this.CAC_TIEMPOREAL == 0) {
                        vacios2 = 1;
                        display = "display: "
                    } else {
                        display = "display: none "
                    }


                    (this.ID_MSALIDA_PROG_DET == idsalida_prog_deta_ ? styleauto = "background-color:#17a2b838" : styleauto = "")
                    if (result.length == 0) {
                        strHTML += '<tr><td colspan="5" class="text-center">No hay información para mostrar</td></tr>';
                    } else {
                        i++;
                        strHTML += '<tr  data-vacio="' + vacios2 + '" style="' + styleauto + ';' + display + '" data-id_msalida_prog_det="' + this.ID_MSALIDA_PROG_DET + '">' +
                            '<td> ' + i + '</td>' +
                            '<td>' + this.HSALIDA + '</td>' +
                            '<td>' + (this.HSALIDA_REAL == null ? "<span style='color:blue'>DISP</span>" : this.HSALIDA_REAL) + '</td>' +
                            '<td>' + (this.PLACA_TIEMPOREAL == null ? "<span style='color:blue'>DISP</span>" : this.PLACA_TIEMPOREAL) + '</td>' +
                            '<td>' + (this.CAC_TIEMPOREAL == 0 ? "<span style='color:blue'>DISP</span>" : this.CAC_TIEMPOREAL) + '</td>' +
                            '<td><a href="#modal_DetallSalida" onclick="ButtonActualizarViajeTemporal($(this))"> <svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></a></td>' +
                            '</tr>';
                    }
                });
            }
            $('#idDespachoUnidad_Serv tbody').append(strHTML);
            setTimeout(function () {
                unblock();
            }, 500);

        }

    }, JSON);
}


function ButtonActualizarViajeTemporal(element) {
    block();
    var account = MODAL_X_ID('#modal_DetallSalida');
    account.show();

    IDSALIDA_ESPECIFICO = 0;
    IDSALIDA_ESPECIFICO = element.parent().parent().attr('data-id_msalida_prog_det')

    setTimeout(function () {
        unblock();
    }, 500);

}


function Actualizar_ViajeTemporal() {


    var account = MODAL_X_ID('#modal_RegistrarResult');
    var id_salida_ = IDSALIDA_ESPECIFICO;
    var id_unidad_ = $('#txtunidad_').val();
    var id_cac_temporal = $('#txtconductor_').val();;
    var id_hora_salida = $('#txtHorasalida_').val();;

    $.ajax({
        url: URL_ACTUALIRZAR_VIAJES_PROGR,
        dataType: 'json',
        data: {
            idsalida: id_salida_,
            unidad: id_unidad_,
            cac_temporal: id_cac_temporal,
            hora_salida: id_hora_salida,
        },
        success: function (result) {
            if (result.COD_ESTADO == 1) {
                getListarProgramacionUnidades();

                account.show();
                getSelectRutaxCorredor($("#txtRuta").val())

                $('#modal_DetallSalida').parent().parent().parent().parent().parent().parent().hide()
                $('#modal_RegistroProgDetall').parent().parent().parent().parent().parent().parent().hide()

            }
        }
    }, JSON);
}


function MODAL_X_ID(modal) {
    var account = "";
    account = $(modal).mobiscroll().popup({
        display: 'center'
    }).mobiscroll('getInst'),
            scrollable = $('#scrollable').mobiscroll().popup({
                display: 'center',
                scrollLock: false,
                cssClass: 'mbsc-no-padding md-content-scroll',
                buttons: []
            }).mobiscroll('getInst');


    return account;
}






//BLOQUEO
function block() {
    var body = $('#panel-body');
    var w = body.css("width");
    var h = body.css("height");
    var trb = $('#throbber');
    var position = body.offset(); // top and left coord, related to document

    trb.css({
        width: w,
        height: h,
        opacity: 0.7,
        position: 'absolute',

    });
    trb.show();
}
function unblock() {
    var trb = $('#throbber');
    trb.hide();
}


function seleccionaFila(elementFila) {
    $.each($('#tbDetallSalida tbody > tr'), function () {
        console.log($(this)[0])
        $(this).css('background-color', '');
    });
    elementFila.css('background-color', '#17a2b838');
}