﻿$(document).ready(function () {


    getPersonaIncidencia();
    getListConductores();
    getListInfracciones();
    getListIncidencias();



    $('#selectInfraccion').on('change', function () {
        //console.log($("#selectInfraccion option:selected")[0], 'onchange');
        var tipo_infraccion = $("#selectInfraccion option:selected").attr('data-TIPO_INFRACCION');
        var tipo_sancion = $("#selectInfraccion option:selected").attr('data-SANCION');
        $('#txt_tipo_sancion').val(tipo_sancion)
        $('#txt_tipo_infraccion').val(tipo_infraccion)

        if (tipo_sancion == "Amonestación" || tipo_sancion == "Restricción") {
            $('#txt_num_dias').css({ "display": "block" });
            $('#txt_tipo_date').css({ "display": "block" });
        } else if (tipo_sancion == "Ninguno") {
            $('#txt_num_dias').css({ "display": "none" });
            $('#txt_tipo_date').css({ "display": "none" });
        }
    });



    $('#selectAgregarTipoPersonaIncidencia').on('change', function () {
        var person = $("#selectAgregarTipoPersonaIncidencia option:selected").text();

    });

    $('#selectAgregarPersonaIncidencia').on('change', function () {
        var person = $("#selectAgregarPersonaIncidencia option:selected").text();

    });


    $("#sectionParametrosAgregarIncidencia").on("submit", function (e) {

        e.preventDefault();

        //if ($('#BS_PLACA').val().length == 0) {
        //    Swal.fire({
        //        type: 'info',
        //        title: "Debe llenar los campos obligatorios para realizar el registro.",
        //        showConfirmButton: false,
        //    });
        //    return false;
        //}

        var ModelIncidenciaAgregar = $('#sectionParametrosAgregarIncidencia').serializeObject();
        $('#registrar_incid_btn').prop('disabled', true).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Guardando...');

        $.ajax({
            url: URL_REGISTRAR_INCIDENCIA,
            data: ModelIncidenciaAgregar,
            dataType: 'json',
            success: function (result) {
                Swal.fire({
                    type: result.COD_ESTADO == 0 ? 'error' : 'success',
                    title: result.DES_ESTADO,
                    showConfirmButton: false,
                    //timer: 2000
                })
                $('#registrar_incid_btn').prop('disabled', false).html('Guardar');

                if (result.COD_ESTADO == 1) {
                    $('#modal_agregar_inciden').modal('hide');
                }

                getListIncidencias();

            },
            error: function (xhr, status, error) {

            },

        }, JSON);

    })

    $("#sectionParametrosEditarIncidencia").on("submit", function (e) {

        e.preventDefault();
        var ModelInfraccionEditar = $('#sectionParametrosEditarIncidencia').serializeObject();
        $('#editar_incid_btn').prop('disabled', true).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Guardando...');

        $.ajax({
            url: URL_EDITAR_INCIDENCIA,
            data: ModelInfraccionEditar,
            dataType: 'json',
            success: function (result) {
                Swal.fire({
                    type: result.COD_ESTADO == 0 ? 'error' : 'success',
                    title: result.DES_ESTADO,
                    showConfirmButton: false,
                    //timer: 2000
                })

                $('#editar_incid_btn').prop('disabled', false).html('Guardar');
                $('#modal_editar_inciden').modal('hide');

                //getListInfracciones();

            },
            error: function (xhr, status, error) {

            },

        }, JSON);

    })
});


$("body").on("click", ".bootstrap-select", function () {
    var Selectstyle = $('#selectAgregarPersonaIncidencia').parent().children()[2]

    $(Selectstyle).removeAttr('style')

    $('.selectpicker').selectpicker('refresh');

    //$(Selectstyle).css('min-width', '444px');

});

//FUNCION QUE SE USARÁ PARA ENVIAR LOS INPUT EN UN MODEL
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function getPersonaIncidencia() {
    $.ajax({
        url: URL_LIST_PERSONA_INCIDENCIA,
        dataType: 'json',
        success: function (result) {
            $.each(result, function () {
                $('#selectAgregarTipoPersonaIncidencia').append('<option value="' + this.ID_PERSONA_INCIDENCIA + '">' + this.DESCRIPCION + '</option>');
                $('#selectEditarPersonaIncid').append('<option value="' + this.ID_PERSONA_INCIDENCIA + '">' + this.DESCRIPCION + '</option>');

                
            });

            $('.selectpicker').selectpicker('refresh');

        }
    }, JSON);
}


function getListConductores() {

    $.ajax({
        url: URL_LIST_CONDUCTORES,
        dataType: 'json',
        success: function (result) {
            $.each(result, function () {
                $('#selectAgregarPersonaIncidencia').append('<option value="' + this.CODIGO + '">' + this.CODIGO + ' - ' + this.APELLIDOS + ' ' + this.NOMBRES + '</option>');

            });


            $('.selectpicker').selectpicker('refresh');
            //var Selectstyle = $('#selectAgregarPersonaIncidencia').parent().children()[2]
            //Selectstyle = $(Selectstyle).css('color', 'red;')
            //console.log(Selectstyle[0], 'Selectstyle')
        }
    }, JSON);
}





function getListInfracciones() {

    $.ajax({
        url: URL_LIST_INFRACCIONES,
        dataType: 'json',
        success: function (result) {
            $.each(result, function () {
                $('#selectInfraccion').append('<option data-SANCION="' + this.SANCION + '" data-TIPO_INFRACCION="' + this.TIPO_INFRACCION + '" value="' + this.ID_INFRACCION + '">' + this.COD_INFRACCION + ' ' + this.DESCRIPCION + '</option>');

                //$('#selectSancion').append('<option value="' + this.ID_INFRACCION + '">' + this.SANCION + '</option>');

            });

            $('.selectpicker').selectpicker('refresh');

            var tipo_infraccion = $('#selectInfraccion').children().attr('data-TIPO_INFRACCION');
            $('#txt_tipo_infraccion').val(tipo_infraccion);
            var tipo_sancion = $("#selectInfraccion option:selected").attr('data-SANCION');
            $('#txt_tipo_sancion').val(tipo_sancion)




        }
    }, JSON);
}


function getListIncidencias() {


    // PERMISOS
    //1= EDITAR || 2==ELIMINAR 
    var permiso_edit = $('#permisoActualiza').val();
    var permiso_eliminar = $('#permisoEliminar').val();



    $('#Tbincidencias tbody').empty();
    //$('#mostrartodo_btn').prop('disabled', true).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Mostrando...');
    DATA_LISTA_BUSES = "";
    var strHTML = '';



    $.ajax({
        url: URL_LIST_INCIDENCIAS,
        dataType: 'json',
        success: function (result) {
            DATA_LISTA_BUSES = result
            //$('#listar_buses tbody').empty();
            //$('#mostrartodo_btn').prop('disabled', false).html('<i class="fas fa-bus-alt"></i>&nbsp; Mostrar Todo');
            if (result.length <= 0) {
                strHTML += '<tr><td colspan="100" class="text-left" style="padding-left: 15%">No hay información para mostrar</td></tr>';

            } else {

                $.each(result, function (i) {

                    var dateFecha_Incidencia = this.FECHA_INCIDENCIA.substring(0, 10);
                    var parts = dateFecha_Incidencia.split(/[- :]/);
                    var Fecha_Incidencia = parts[2] + '/' + parts[1] + '/' + parts[0];


                    strHTML += '<tr  onclick="seleccionaFila($(this))" ' +
                        ' data-ID_INCIDENCIA="' + (this.ID_INCIDENCIA == null ? "" : this.ID_INCIDENCIA) + '" ' +
                        ' data-FECHA_INCIDENCIA="' + (this.FECHA_INCIDENCIA == null ? "" : this.FECHA_INCIDENCIA) + '" ' +
                        ' data-PLACA_PADRON="' + (this.PLACA_PADRON == null ? "" : this.PLACA_PADRON) + '" ' +
                        ' data-COD_INFRACCION="' + (this.COD_INFRACCION == null ? "" : this.COD_INFRACCION) + '" ' +
                        ' data-DESCRIPCION_INFRACCION="' + (this.DESCRIPCION_INFRACCION == null ? "" : this.DESCRIPCION_INFRACCION) + '" ' +
                        ' data-SANCION="' + (this.SANCION == null ? "" : this.SANCION) + '" ' +
                        ' data-DESCRIPCION_INCIDENCIA="' + (this.DESCRIPCION_INCIDENCIA == null ? "" : this.DESCRIPCION_INCIDENCIA) + '" ' +
                        ' data-DESCARGO="' + (this.DESCARGO == null ? "" : this.DESCARGO) + '" ' + '>' + '>' +

                        '<td>' + (i + 1) + '</td>' +
                         
                        '<td class="text-center" >' + Fecha_Incidencia + '</td>' +
                        '<td class="text-center" >' + (this.FECHA_INCIDENCIA == null ? "" : this.FECHA_INCIDENCIA).substring(11, 16) + '</td>' +
                        '<td class="text-center" >' + (this.PLACA_PADRON == null ? "" : this.PLACA_PADRON) + '</td>' +
                        '<td class="text-center" >' + (this.COD_INFRACCION == null ? "" : this.COD_INFRACCION) + ' - ' + (this.APELLIDOS == null ? "" : this.APELLIDOS) + ' ' + (this.NOMBRES == null ? "" : this.NOMBRES) + '</td>' +
                        '<td class="text-center" >' + '<button type="button" class="btn btn-primary btn-sm" data-toggle="popover"  title="Descripcion" data-content="' + (this.DESCRIPCION_INFRACCION == null ? "No hay informacion" : this.DESCRIPCION_INFRACCION) + '">' + '<i class="fas fa-eye"></i>' + '</button>' + '</td>' +
                        '<td class="text-center" >' + (this.SANCION == null ? "" : this.SANCION).toUpperCase() + '</td>' +
                        '<td class="text-center" >' + '<button type="button" class="btn btn-primary btn-sm" data-toggle="popover"  title="Descripcion" data-content="' + (this.DESCRIPCION_INCIDENCIA == null ? "No hay informacion" : this.DESCRIPCION_INCIDENCIA) + '">' + '<i class="fas fa-eye"></i>' + '</button>' + '</td>' +
                        '<td class="text-center" >' + '<button type="button" class="btn btn-primary btn-sm" data-toggle="popover"  title="Descargo" data-content="' + (this.DESCARGO == null ? "No hay informacion" : this.DESCARGO) + '">' + '<i class="fas fa-eye"></i>' + '</button>' + '</td>' +
                        '<td class="text-center" >' + (this.USU_REG == null ? "" : this.USU_REG) + '</td>' +
                        '<td class="text-center" >' + (this.FECHA_REG == null ? "" : this.FECHA_REG) + '</td>' +
                        //'<td>' + '<span class="far fa-edit" aria-hidden="true" style="cursor:pointer;" onclick="abrirEditarIncidencia($(this).parent().parent());" ></span>' + '</td>'+
                        (permiso_eliminar == 1 ? '<td>' + '<span class="far fa-trash-alt" aria-hidden="true" style="cursor:pointer;"  onclick="confirmarAnulacionIncidencia(' + this.ID_INCIDENCIA + ');" ></span>' + '</td>' : '') +
                        '</tr>';

                });

            }
            $('#listar_incidencias').append(strHTML);
            util.activarEnumeradoTabla('#Tbincidencias', $('#btnBusquedaEnTabla'));

            $('[data-toggle="popover"]').popover({
                placement: 'right'
            });
        },

    }, JSON);

}


var tableDataExportar = [{
    "sheetName": "Hoja",
    "data": []
}];

function exportarTabla() {

    $('#exportar').prop('disabled', true).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Exportando...');
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '_' + mm + '_' + yyyy;


    tableDataExportar = [{
        "sheetName": "Hoja",
        "data": []
    }];
    var options = {
        fileName: "Reporte Buses " + today
    };


    var tHeadExcel = [{ "text": "N°" },
        { "text": "CORREDOR" },
        { "text": "FECHA DE INCIO A PT" },
        { "text": "NOMBRE DE LA EMPRESA" },
        { "text": "PLACA" },
        { "text": "PROPIETARIO" },
        { "text": "PAQUETE CONCESIÓN" },
        { "text": "PAQUETE DONDE BRINDA SERVICIO" },
        { "text": "TIPO_SERVICIO" },
        { "text": "ESTADO" },
        { "text": "MARCA" },
        { "text": "MODELO" },
        { "text": "AÑO FABRICACION" },
        { "text": "COMBUSTIBLE" },
        { "text": "TECNOLOGIA EURO" },
        { "text": "POTENCIA MOTOR" },
        { "text": "SERIE MOTOR" },
        { "text": "SERIE CHASIS" },
        { "text": "COLOR VEHICULO" },
        { "text": "LONGUITUD" },
        { "text": "ASIENTOS" },
        { "text": "AREA PASILLO" },
        { "text": "PESO NETO" },
        { "text": "PESO BRUTO" },
        { "text": "ALTURA" },
        { "text": "ANCHO" },
        { "text": "UTIL" },
        { "text": "PUERTA IZQUIERDA" },
        { "text": "PARTIDA REGISTRAL" },
        { "text": "CODIGO CVS" },
        { "text": "CVS INICIO" },
        { "text": "CVS VIGENCIA" },
        { "text": "SOAT INICIO" },
        { "text": "SOAT VIGENCIA" },
        { "text": "N° PÓLIZA VEHÍCULOS" },
        { "text": "VEHICULOS INICIO" },
        { "text": "VEHICULOS FIN" },
        { "text": "N° POLIZA RESPONSABILIDAD CIVIL" },
        { "text": "RC INICIO" },
        { "text": "RC VIGENCIA" },
        { "text": "N° POLIZA ACCIDENTES PERSONALES COLECTIVOS PARTICULARES" },
        { "text": "APCP INICIO" },
        { "text": "APCP VIGENCIA" },
        { "text": "N° POLIZA MULTIRIESGOS" },
        { "text": "MULTIRESGOS INICIO" },
        { "text": "MULTIRIESGOS VIGENCIA" },
        { "text": "RTV INICIO" },
        { "text": "RTV VIGENCIA" },
        { "text": "INICIO REVISION ANUAL GNV" },
        { "text": "VIGENCIA REVISION ANUAL GNV" },
        { "text": "INICIO REVISION CILINDROS GNV" },
        { "text": "VIGENCIA REVISION CILINDROS GNV" }
    ]

    tableDataExportar[0].data.push(tHeadExcel);

    $.each(DATA_LISTA_BUSES, function (i) {

        var objeto = this;
        var itemArr = [];
        itemArr.push({ "text": (i + 1) },
            { "text": objeto.ABREVIATURA },
            { "text": objeto.BS_FECINI_PT },
            { "text": objeto.BS_NOM_EMPRE },
            { "text": objeto.BS_PLACA },
            { "text": objeto.BS_PROPIETARIO },
            { "text": objeto.BS_PAQUETE_CONCESION },
            { "text": objeto.BS_PAQUETE_SERVICIO },
            { "text": objeto.BS_TIPO_SERVICIO },
            { "text": objeto.BS_ESTADO },
            { "text": objeto.BS_MARCA },
            { "text": objeto.BS_MODELO },
            { "text": objeto.BS_AÑO_FABRICACION },
            { "text": objeto.BS_COMBUSTIBLE },
            { "text": objeto.BS_TECNOLOGIA_EURO },
            { "text": objeto.BS_POTENCIA_MOTOR },
            { "text": objeto.BS_SERIE_MOTOR },
            { "text": objeto.BS_SERIE_CHASIS },
            { "text": objeto.BS_COLOR_VEHICULO },
            { "text": objeto.BS_LONGITUD },
            { "text": objeto.BS_ASIENTOS },
            { "text": objeto.BS_AREA_PASILLO },
            { "text": objeto.BS_PESO_NETO },
            { "text": objeto.BS_PESO_BRUTO },
            { "text": objeto.BS_ALTURA },
            { "text": objeto.BS_ANCHO },
            { "text": objeto.BS_CARGA_UTIL },
            { "text": objeto.BS_PUERTA_IZQUIERDA },
            { "text": objeto.BS_PARTIDA_REGISTRAL },
            { "text": objeto.BS_CODIGO_CVS },
            { "text": objeto.BS_CVS_FEC_INIC },
            { "text": objeto.BS_CVS_FEC_FIN },
            { "text": objeto.BS_SOAT_FEC_INIC },
            { "text": objeto.BS_SOAT_FEC_FIN },
            { "text": objeto.BS_POLIZA_VEHICULOS },
            { "text": objeto.BS_VEHICULOS_INIC },
            { "text": objeto.BS_VEHICULOS_FIN },
            { "text": objeto.BS_POLIZA_CIVIL },
            { "text": objeto.BS_RC_INICIO },
            { "text": objeto.BS_RC_FIN },
            { "text": objeto.BS_POLIZA_ACCI_COLECTIVOS },
            { "text": objeto.BS_APCP_INIC },
            { "text": objeto.BS_APCP_FIN },
            { "text": objeto.BS_POLIZA_MULTIRIESGOS },
            { "text": objeto.BS_MULTIRESGOS_INIC },
            { "text": objeto.BS_MULTIRESGOS_FIN },
            { "text": objeto.BS_RTV_INIC },
            { "text": objeto.BS_RTV_FIN },
            { "text": objeto.BS_REVI_ANUAL_GNV_INIC },
            { "text": objeto.BS_REVI_ANUAL_GNV_FIN },
            { "text": objeto.BS_REVISION_CILINDROS_GNV_INIC },
            { "text": objeto.BS_REVISION_CILINDROS_GNV_FIN }
        )
        tableDataExportar[0].data.push(itemArr);

    });
    Jhxlsx.export(tableDataExportar, options);
    $('#exportar').prop('disabled', false).html('Exportar');
}


function subirArchivoTemp(element) {
    var nombreArchivo = element.val().split("\\").pop();
    var extensionArchivo = nombreArchivo.split('.')[1];
    element.siblings(".custom-file-label").addClass("selected").html(nombreArchivo);
}


function abrirEditarIncidencia(element) {


    var ID_INCIDENCIA = element.attr('data-ID_INCIDENCIA');
    var PERSONA_INCIDENCIA = element.attr('data-COD_INFRACCION');
    var FECHA_INCIDENCIA = element.attr('data-FECHA_INCIDENCIA');
    var PLACA_PADRON = element.attr('data-PLACA_PADRON');
    var COD_INFRACCION = element.attr('data-COD_INFRACCION');
    var DESCRIPCION_INFRACCION = element.attr('data-DESCRIPCION_INFRACCION');
    var SANCION = element.attr('data-SANCION');
    var DESCRIPCION_INCIDENCIA = element.attr('data-DESCRIPCION_INCIDENCIA');
    var DESCARGO = element.attr('data-DESCARGO');


    $('#TXT_ID_INCIDENCIA').val(Number(ID_INCIDENCIA));
    $('#selectEditarPersonaIncid').val(Number(PERSONA_INCIDENCIA));
    $('#TXT_FECHA_INCIDENCIA_EDITAR').val(FECHA_INCIDENCIA);
    $('#TXT_PLACA_PADRON_EDITAR').val(PLACA_PADRON);
    $('#TXT_COD_INFRACCION').val(COD_INFRACCION);
    $('#TXT_DESCRIPCION_INFRACCION').val(DESCRIPCION_INFRACCION);
    $('#TXT_SANCION').val(SANCION);
    $('#TXT_DESCRIPCION_INCIDENCIA').val(DESCRIPCION_INCIDENCIA);
    $('#TXT_DESCARGO').val(DESCARGO);

    $('#modal_editar_inciden').modal('show');
}


function seleccionaFila(elementFila) {
    $.each($('#Tbincidencias tbody > tr'), function () {
        $(this).css('background-color', '');
    });
    elementFila.css('background-color', '#17a2b838');
}


$('#modal_agregar_inciden .save').click(function (e) {
    e.preventDefault();
    addImage(5);
    $('#modal_agregar_inciden').modal('hide');
    return false;
})

$('#modal_editar_inciden .save').click(function (e) {
    e.preventDefault();
    addImage(5);
    $('#modal_editar_inciden').modal('hide');
    return false;
})

function confirmarAnulacionIncidencia(idIncidencia) {
    Swal.fire({
        text: "Estas seguro que deseas eliminar la incidencia ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: URL_ANULA_INCIDENCIA,
                dataType: 'json',
                data: {
                    idIncidencia: idIncidencia
                },
                success: function (result) {
                    getListIncidencias();
                    Swal.fire({
                        type: (result.COD_ESTADO == 1 ? 'success' : 'error'),
                        title: result.DES_ESTADO,
                        showConfirmButton: false,
                        //timer: 2000
                    });
                }
            }, JSON);
        }
    });
}



function getCorredoresByModalidad() {
    var modalidadTransporteSelecionado = $('#mod').val();
    $('#selectCorredores, #selectRuta').empty();
    var cantidadRegistros = 0;
    //
    $.each(JSON_DATA_CORREDORES, function () {
        if (this.ID_MODALIDAD_TRANS == Number(modalidadTransporteSelecionado)) {
            cantidadRegistros++;
            $('#selectCorredores').append('<option value="' + this.ID_CORREDOR + '">' + this.ABREVIATURA + '</option>');
        }
    });
    if (cantidadRegistros == 0) {
        $('#selectCorredores, #selectRuta').append('<option value="0">' + '-- No hay información --' + '</option>');
        $('#tbRutatipoServicio tbody').empty();
        return false
    }
    getRutaPorCorredor();
}

function getRutaPorCorredor() {

    $.ajax({
        url: URL_GET_RUTA_X_CORREDOR,
        dataType: 'json',
        data: { idCorredor: $('#selectCorredores').val() },
        success: function (result) {
            $('#selectRutaCorredor').empty();
            if (result.length == 0) { // si la lista esta vacia
                $('#selectRutaCorredor').append('<option value="0">' + '--No hay información--' + '</option>');
                return false;
            } else {
                $.each(result, function () {
                    $('#selectRutaCorredor').append('<option value="' + this.ID_RUTA + '">' + this.NRO_RUTA + '</option>');
                });
            }
        }
    }, JSON);
}
