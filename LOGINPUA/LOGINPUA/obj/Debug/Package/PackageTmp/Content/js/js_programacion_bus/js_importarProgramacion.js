﻿var ARCHIVOS_PERMITIDOS = {
    EXCEL: 'xlsx'
}
var ACTION_REEMPLAZAR_DATOS = false;
$(document).ready(function () {
    //getTipoServicioByCorredor();
    getTipoDias();
    getDataCorredor();
    //
    verificarTipoDia($('#selectTipoDia'));
    $('.tooltip_descargar_excel').tooltipster();
    $("#formSubirProgramacion").on("submit", function (e) {
        e.preventDefault();
        var formularioDatos = new FormData(document.getElementById("formSubirProgramacion"));
        $('#btnSubirArchivo').prop('disabled', true).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Subiendo...');
        //
        $.ajax({
            //url: '@Url.Action("uploadProgramacionV2", "ProgramacionSalida")?txtRuta=' + $('#selectRuta').val() + '&fechaProgramacion=' + $('#txtfechaProg').val(),
            url: URL_ACTION_UPLOAD_FILE + '/?fecha=' + $('#txtDiasSeleccionados').val()
                                           + '&TipoDia=' + $('#selectTipoDia').val() +
                                           '&reemplazar=' + (ACTION_REEMPLAZAR_DATOS ? 1 : 0) +
                                            '&idServicio=' + $('#selectRutaTipoServicio').val() +
                                            '&ruta_ajax=' + $( "#selectRutaConsulta option:selected" ).text(),
            type: "post",
            dataType: "json",
            data: formularioDatos,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (respuesta) {

            console.log(respuesta, 'respuesta')
             //console.log(respuesta, 'respuesta');
            ACTION_REEMPLAZAR_DATOS = false;
            $('#btnSubirArchivo').prop('disabled', false).html('Subir archivo');
            if (respuesta.codResultado == 1) {
                Swal.fire({
                    type: respuesta.codResultado == 1 ? 'success' : 'error',
                    title: respuesta.desResultado,
                    showConfirmButton: false,
                    //timer: 2500
                });
                $('#modalImportarProgramacion').modal('hide');
                getResumenProgramacion();
            }
            if (respuesta.COD_ESTADO ==0) { //encontró fecha 
                     Swal.fire({
                         type: respuesta.COD_ESTADO == 1 ? 'success' : 'error',
                        title: respuesta.DES_ESTADO,
                        showConfirmButton: false,
                        //timer: 2500
                    });

            }

            if (respuesta.COD_ESTADO == 3) { //encontró fecha 

                var texto = respuesta.DES_ESTADO.bold();
                texto = texto.replace(/ /g, "");
                texto = texto.substring(0, texto.length - 1)
 

                Swal.fire({
                    title: 'ALERTA !',
                    html: "Se eliminará la programacion de la fecha " + texto,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si'
                }).then((result) => {
                    console.log(result,'result')
                    //console.log('resultado prop->>', result);
                    if (result.value) { //si responde que s   
                        ACTION_REEMPLAZAR_DATOS = true;
                        $("#formSubirProgramacion").submit();
                    } else {
                        ACTION_REEMPLAZAR_DATOS = false;
                    }
                });
            }
        });
    })
});


function AbrirModalImportar() {
    var textoDialog = '[' + $("#selectRutaConsulta option:selected").text() + '] ' +
                        $("#selectRutaTipoServicio option:selected").text();
    $('.modal-title').text(textoDialog);

    verificarTipoDia($('#selectTipoDia'))
    $('#txtDiasSeleccionados').val('');
    $('#archivoSubido').val('');
    $('#lblArchivoSubido').text('Seleccionar Archivo');
    $('#modalImportarProgramacion .modal-footer .btn-success').prop('disabled', true);

    getSelectRutaxCorredor($('#selectCorredores').val());

}

var CANTIDAD_DIAS_MAXIMO_SELECCION = 5;
function verificarTipoDia(elemento) {
    $('#txtDiasSeleccionados').val('');
    var codTipoDia = Number(elemento.val());
    var opcionDatePicker = null;
    $('#txtDiasSeleccionados').datepicker('destroy');
    switch (codTipoDia) {
        case 1: //Hábil
            opcionDatePicker = {
                startDate: new Date(),
                multidate: CANTIDAD_DIAS_MAXIMO_SELECCION,
                multidateSeparator: ' - ',
                //multidate: true,
                format: "dd/mm/yyyy",
                //daysOfWeekHighlighted: "0,6",
                daysOfWeekHighlighted: "1,2,3,4,5",
                datesDisabled: ['31/08/2017'],
                daysOfWeekDisabled: [0, 6],
                //daysOfWeekDisabled: [1, 2, 3, 4, 5],
                language: 'es'
            }
            break;
        case 2: //Sábado
            opcionDatePicker = {
                startDate: new Date(),
                multidate: CANTIDAD_DIAS_MAXIMO_SELECCION,
                multidateSeparator: ' - ',
                format: "dd/mm/yyyy",
                daysOfWeekHighlighted: "6",
                datesDisabled: ['31/08/2017'],
                daysOfWeekDisabled: [0, 1, 2, 3, 4, 5],
                language: 'es'
            }
            break;
        case 3: //Domingo
            opcionDatePicker = {
                startDate: new Date(),
                multidate: CANTIDAD_DIAS_MAXIMO_SELECCION,
                multidateSeparator: ' - ',
                format: "dd/mm/yyyy",
                daysOfWeekHighlighted: "0",
                daysOfWeekDisabled: [1, 2, 3, 4, 5, 6],
                language: 'es'
            }
            break;
        case 4: //Feriado
            opcionDatePicker = {
                startDate: new Date(),
                multidate: CANTIDAD_DIAS_MAXIMO_SELECCION,
                multidateSeparator: ' - ',
                format: "dd/mm/yyyy",
                //daysOfWeekHighlighted: "0",
                //daysOfWeekDisabled: [1, 2, 3, 4, 5, 6],
                language: 'es'
            }
            break;
        default:
            break;
    }
    $('#txtDiasSeleccionados').datepicker(opcionDatePicker).on('changeDate', function (e) {
        // `e` here contains the extra attributes
        //$(this).find('.input-group-addon .count').text(' ' + e.dates.length);
    });

}

function subirArchivoTemp(element) {
    var nombreArchivo = element.val().split("\\").pop();
    var extensionArchivo = nombreArchivo.split('.')[1];
    //console.log("extensionArchivo", extensionArchivo);
    element.siblings(".custom-file-label").html('');
    if (extensionArchivo == ARCHIVOS_PERMITIDOS.EXCEL) {
        $('#modalImportarProgramacion .modal-footer .btn-success').prop('disabled', false);
        element.siblings(".custom-file-label").addClass("selected").html(nombreArchivo);
    } else {
        element.siblings(".custom-file-label").addClass("selected").html("Seleccionar Archivo");
        $('#modalImportarProgramacion .modal-footer .btn-success').prop('disabled', true);
        Swal.fire({
            type: 'error',
            title: 'El archivo ' + nombreArchivo + ' no tiene el formato correcto.',
            showConfirmButton: false,
            timer: 2500
        })
    }
}

function getDataCorredor() {
    var txtCorredor = $("#selectCorredores option:selected").text();
    $('#txtNombreCorredor').text(txtCorredor);
}

function getSelectRutaxCorredor(idCorredor) {
    $.ajax({
        url: URL_GET_RUTA_X_CORREDOR,
        dataType: 'json',
        data: { idCorredor: idCorredor },
        success: function (result) {
            $('#selectRuta').empty();
            if (result.length == 0) { // si la lista esta vacia
                Swal.fire({
                    type: 'error',
                    title: 'No hay rutas para mostrar',
                    showConfirmButton: false,
                    timer: 2000
                })
            } else {
                $('#modalImportarProgramacion').modal('show');
                $.each(result, function () {
                    $('#selectRuta').append('<option value="' + this.ID_RUTA + '">' + this.NRO_RUTA + '</option>');
                });
            }
        }
    }, JSON);
}

function getTipoDias() {

    $.ajax({
        url: URL_GET_TIPO_DIAS,
        dataType: 'json',
        success: function (result) {
            $('#selectTipoDia').empty();
            $.each(result, function () {
                $('#selectTipoDia').append('<option value="' + this.ID_TIPO_DIA + '">' + this.NOMBRE + '</option>');
            });
        }
    }, JSON);
}

function getResumenProgramacion() {

    var strHTML = '';
    $.ajax({
        url: URL_GET_RESUMEN_PROGRAMACION,
        data: { idServicio: $('#selectRutaTipoServicio').val() },
        dataType: 'json',
        success: function (result) {

            $('#tbSalidaProgramada tbody').empty();
            if (result.length <= 0) {
                strHTML += '<tr><td colspan="13" class="text-center">No hay información para mostrar</td></tr>';
            } else {
                $.each(result, function (i) {
                    strHTML += '<tr>' +
                                    '<td>' + (i + 1) + '</td>' +
                                    '<td>' + this.FECHA_PROGRAMACION + '</td>' +
                                    '<td class="text-center" >' + this.ABREVIATURA + '</td>' +
                                    '<td class="text-center" >' + this.NRO_RUTA + '</td>' +
                                    '<td>' + this.TIPO_DIA + '</td>' +
                                    '<td>' + this.TIPO_SERVICIO + '</td>' +
                                    '<td>' + this.CANTIDAD_VIAJE + '</td>' +
                                    '<td>' + this.TIPO_OPERACIONAL + '</td>' +
                                    '<td>' + '<button type="button"  class="btn btn-info btn-sm" data-toggle="modal" style="font-size: 11px;" onclick="verPOG();"><span class="fa fa-eye"></span>&nbsp;&nbsp;POG</button>' + '</td>' +
                                    '<td>' + this.USU_REG + '</td>' +
                                    '<td class="text-center">' + this.FECHA_REG + '</td>' +
                               '</tr>';
                });
            }
            $('#tbSalidaProgramada tbody').append(strHTML);

        }, error: function (xhr, status, error) {
            $('#tbSalidaProgramada tbody').empty();

            strHTML += '<tr><td colspan="13" class="text-center">No hay información para mostrar</td></tr>';
            $('#tbSalidaProgramada tbody').append(strHTML);
            console.log(strHTML, 'strHTML')
        },

    }, JSON);
}

var FECHA_TEMP = '27/01/2020';
function verPOG() {
    $.ajax({
        url: URL_GET_DATA_VIAJES,
        data: { idMaestroSalidaProg: 265 },
        dataType: 'json',
        success: function (result) {
            var salto = 60;
            var dataFranjaHoras = util.obtenerHorasEnTimestamp(FECHA_TEMP + ' ' + '04:00:00', FECHA_TEMP + ' ' + '22:00:00', salto);
            //            
            var ultimosViajes = getViajesPIG(result);
            $.each(result, function () {//
                if (this.POG != null) {//solo los primeros viajes 
                    verificaViajePorFranjaHoraria(this, dataFranjaHoras);//
                }
            });
            //
            $.each(ultimosViajes, function () {//agregando los viajes PIG en cada franja (solo los viajes finales)
                setViajesPIGinFranja(this, dataFranjaHoras);
            });
            //
            console.log('dataFranjaHoras--->', dataFranjaHoras);
            return;
        }, error: function (xhr, status, error) {

        },
    }, JSON);
}

function setViajesPIGinFranja(objeto, franja) {//ultimos viajes
    $.each(franja, function () {
        //this.arrViajesFinal = [];
        var tmstmIniRango_ = util.convertDatetoTimeStamp(FECHA_TEMP + ' ' + this.hInicio);
        var tmstmFinRango_ = util.convertDatetoTimeStamp(FECHA_TEMP + ' ' + this.hFin);
        var tmstmFecSalida_ = util.convertDatetoTimeStamp(FECHA_TEMP + ' ' + objeto.HSALIDA);
        //
        if (tmstmFecSalida_ >= tmstmIniRango_ && tmstmFecSalida_ <= tmstmFinRango_) {
            var item = objeto;
            this.arrViajesFinal.push(item);
        }
    });
}

function getViajesPIG(dataJSON) {//ultimos viajes
    var rpta = [];
    $.each(dataJSON, function () {
        if (this.PIG != null) {
            rpta.push(this);
        }
    });
    return rpta;
}

function verificaViajePorFranjaHoraria(objSalida, dataFranjaHoraria) {
    //objSalida.arrViajesFinal = [];
    $.each(dataFranjaHoraria, function () {
        this.arrViajesFinal = [];
        var tmstmIniRango = util.convertDatetoTimeStamp(FECHA_TEMP + ' ' + this.hInicio);
        var tmstmFinRango = util.convertDatetoTimeStamp(FECHA_TEMP + ' ' + this.hFin);
        var tmstmFecSalida = util.convertDatetoTimeStamp(FECHA_TEMP + ' ' + objSalida.HSALIDA);
        //
        if (tmstmFecSalida >= tmstmIniRango && tmstmFecSalida <= tmstmFinRango) {
            var item = objSalida;
            this.arrViajes.push(item);
        }        
    });
}

function getTipoServicioByCorredor() {

    $.ajax({
        url: URL_GET_TIPO_SERVICIO_BY_CORREDOR,
        data: { idCorredor: $('#selectCorredores').val() },
        dataType: 'json',
        success: function (result) {
            $('#selectRutaTipoServicio').empty();
            if (result.length == 0) {
                $('#selectRutaTipoServicio').append('<option value="">No Existe Información</option>');
                return false;
            }
            else {
                $.each(result, function () {
                    $('#selectRutaTipoServicio').append('<option value="' + this.ID_RUTA_TIPO_SERVICIO + '">' +
                            this.TIPO_SERVICIO + ' ' + this.TIPO_OPERACIONAL + ' ' + this.NRO_RUTA + '</option>');
                });
            }
        },
    },
    JSON);
}