﻿'use strict';
MyAppLogin.service('FuncionesLogin', FuncionesLogin);
function FuncionesLogin(MyServicesLogin) {
    var vm = this;
    var data;
    vm.UrlTipo = function (Controlador, Nivel) {
        var UrlPagina = "";
        if (Nivel == 1) {
            UrlPagina = Controlador + "Crear";
        }
        else if (Nivel == 2) {
            UrlPagina = Controlador + "Modificar";
        }
        else if (Nivel == 3) {
            UrlPagina = Controlador + "Borrar";
        }
        else if (Nivel == 4) {
            UrlPagina = Controlador + "Detalle";
        }
        return MyServicesLogin.Url() + UrlPagina;
    }
    vm.fechaDatetimeAString = function (fechaHtml, Mostrar) {
        var fechafix = "";
        var dateString = fechaHtml.substr(6);
        var currentTime = new Date(parseInt(dateString));
        var month = currentTime.getMonth() + 1;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        var hour = currentTime.getHours();
        var min = currentTime.getMinutes();
        var sec = currentTime.getSeconds();
        day < 10 ? day = "0" + day : day;
        month < 10 ? month = "0" + month : month;
        if (Mostrar == 1) {
            hour < 10 ? hour = "0" + hour : hour;
            min < 10 ? min = "0" + min : min;
            sec < 10 ? sec = "0" + sec : sec;
            fechafix = day + "/" + month + "/" + year + " - " + hour + ":" + min + ":" + sec;
        } else {
            fechafix = day + "/" + month + "/" + year;
        }
        return fechafix;
    }
    vm.FechaServidorAHtml = function (fechaServidor) {
        return new Date(fechaServidor);
    }
    vm.Sweet = function (titleNom, textNom, typeNum, button) {
        var typeNom = "";
        switch (typeNum) {
            case 1: typeNom = "success";
                break;
            case 2: typeNom = "error";
                break;
            case 3: typeNom = "warning";
                break;
            case 4: typeNom = "info";
                break;
            default: typeNom = "question";
                break;
        }
        if (titleNom == "") {
            titleNom = "Ingrese Algun Titulo";
        }
        if (textNom == "") {
            textNom = "Ingrese Algun Texto";
        }
        if (button == "") {
            button = "Ingrese Algun Texto";
        }
        return Swal.fire({
            title: titleNom,
            text: textNom,
            type: typeNom,
            confirmButtonText: button
        })
    }
    vm.validator = function (objeto) {
        var Exit = true;
        var Notice = "";
        var Letter = "";
        for (var prop in objeto) {
            if (objeto.hasOwnProperty(prop)) {
                switch (typeof objeto[prop]) {
                    case 'string':
                        if (objeto[prop] == "" || objeto[prop] == undefined) {
                            for (var index = 0; index < prop.length; index++) {
                                var Letter = prop.charAt(index);
                                if (Upperletter(Letter)) {
                                    Notice += " " + Letter;
                                }
                                if (Lowerletter(Letter)) {
                                    Notice += Letter;
                                }
                            }
                            vm.Sweet("Error", Notice, 2, "Error");
                            return Exit = false;
                        }
                        break;
                    case 'number':
                        if (objeto[prop] == null || objeto[prop] == undefined) {                           
                            vm.Sweet("Error", "Falto Ingresar el " + prop + "", 2, "Error");
                            return Exit = false;
                        }
                        break;
                    case 'object':
                        if (objeto[prop] == null || objeto[prop] == undefined) {
                            vm.Sweet("Error", "Falto Ingresar el " + prop + "", 2, "Error");
                            return Exit = false;
                        }
                        break;
                    default:
                        if (objeto[prop] == null || objeto[prop] == undefined) {
                            for (var index = 0; index < prop.length; index++) {
                                var Letter = prop.charAt(index);
                                if (Upperletter(Letter)) {
                                    Notice += " " + Letter;
                                }
                                if (Lowerletter(Letter)) {
                                    Notice += Letter;
                                }
                            }
                            vm.Sweet("Error", Notice, 2, "Error");
                            return Exit = false;
                        }
                        break;
                }
            }
        }
        return Exit;
    }
    function Upperletter(letter) {
        return letter === letter.toUpperCase();
    }
    function Lowerletter(letter) {
        return letter === letter.toLowerCase();
    }
};