﻿'use strict';
MyApp.service('Funciones', function (MyServicesGeneral) {
    var vm = this;
    var data;
    var Fecha = {};
    vm.UrlTipo = function (Controller, Tipo) {
        var UrlPage = "";
        switch (Tipo) {
            case 1:
                UrlPage = Controller + "Crear";
                break;
            case 2:
                UrlPage = Controller + "Modificar";
                break;
            case 3:
                UrlPage = Controller + "Borrar";
                break;
            case 4:
                UrlPage = Controller + "Detalle";
                break;
            default:
                UrlPage = "";
                console.log("Error");
                break;
        }
        return MyServicesGeneral.Url() + UrlPage;
    }
    vm.FechaHtmlAServidor = function (fechaHtml) {
        Fecha.dateString = fechaHtml.substr(6);
        Fecha.currentTime = new Date(parseInt(dateString));
        Fecha.month = currentTime.getMonth() + 1;
        Fecha.day = currentTime.getDate();
        Fecha.year = currentTime.getFullYear();
        Fecha.hour = currentTime.getHours();
        Fecha.min = currentTime.getMinutes();
        Fecha.day < 10 ? Fecha.day = "0" + Fecha.day : Fecha.day;
        Fecha.month < 10 ? Fecha.month = "0" + Fecha.month : Fecha.month;
        Fecha.hour < 10 ? Fecha.hour = "0" + Fecha.hour : Fecha.hour;
        Fecha.min < 10 ? Fecha.min = "0" + Fecha.min : Fecha.min;
        return Fecha.day + "/" + Fecha.month + "/" + Fecha.year + " - " + Fecha.hour + ":" + Fecha.min;
    }
    vm.FechaServidorAHtml = function (fechaServidor) {
        return new Date(fechaServidor);
    }
    vm.Sweet = function (Titulo, SubTitulo, Tipo, PieTitulo) {
        var TipoNombre = "";
        var Objeto_Swat = {};
        Objeto_Swat = {
            type: "",
            title: "",
            text: "",
            footer: ""
        }
        switch (Tipo) {
            case 1:
                TipoNombre = "success";
                break;
            case 2:
                TipoNombre = "error";
                break;
            case 3:
                TipoNombre = "warning";
                break;
            case 4:
                TipoNombre = "info";
                break;
            default:
                TipoNombre = "question";
                break;
        }
        Objeto_Swat = {
            type: TipoNombre,
            title: Titulo,
            text: SubTitulo,
            footer: PieTitulo
        }
        Swal.fire(Objeto_Swat);
        //    {
        //    type: 'error',
        //    title: 'Oops...',
        //    text: 'Something went wrong!',
        //    footer: '<a href>Why do I have this issue?</a>'
        //}
    }
    vm.SweetBorrar = function (nombre, datorelevante, Objeto) {
        return Swal.fire({
            title: 'Eliminar a ' + nombre,
            text: "Se Eliminara a " + datorelevante,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar'
        })
    }
    vm.validator = function (objeto) {
        var Exit = true;
        var Notice = "";
        var Letter = "";
        for (var prop in objeto) {
            if (objeto.hasOwnProperty(prop)) {
                switch (typeof objeto[prop]) {
                    case 'string':
                        if (objeto[prop] == "" || objeto[prop] == undefined) {
                            for (var index = 0; index < prop.length; index++) {
                                var Letter = prop.charAt(index);
                                if (Upperletter(Letter)) {
                                    Notice += " " + Letter;
                                }
                                if (Lowerletter(Letter)) {
                                    Notice += Letter;
                                }
                            }
                            Exit = false;
                            return vm.Sweet("Error", Notice, 2, "Error");
                        }
                        break;
                    case 'number':
                        if (objeto[prop] == null || objeto[prop] == undefined) {
                            Exit = false;
                            return vm.Sweet("Error", "Falto Ingresar el " + prop + "", 2, "Error");
                        }
                        break;
                    case 'object':
                        if (objeto[prop] == null || objeto[prop] == undefined || objeto[prop].length == 0) {
                            for (var index = 0; index < prop.length; index++) {
                                var Letter = prop.charAt(index);
                                if (Upperletter(Letter)) {
                                    Notice += " " + Letter;
                                }
                                if (Lowerletter(Letter)) {
                                    Notice += Letter;
                                }
                            }
                            Exit = false;
                            return vm.Sweet("Error", Notice + "", 2, "Error");
                        }
                        break;
                    default:
                        if (objeto[prop] == null || objeto[prop] == undefined) {
                            for (var index = 0; index < prop.length; index++) {
                                var Letter = prop.charAt(index);
                                if (Upperletter(Letter)) {
                                    Notice += " " + Letter;
                                }
                                if (Lowerletter(Letter)) {
                                    Notice += Letter;
                                }
                            }
                            Exit = false;
                            return vm.Sweet("Error", Notice, 2, "Error");
                        }
                        break;

                }
            }
        }
        return Exit;
    }
    function Upperletter(letter) {
        return letter === letter.toUpperCase();
    }
    function Lowerletter(letter) {
        return letter === letter.toLowerCase();
    }
});