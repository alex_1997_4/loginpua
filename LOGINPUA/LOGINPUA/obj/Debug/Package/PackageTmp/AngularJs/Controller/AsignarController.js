﻿'use strict';
MyApp.controller('AsignarController', AsignarController);
function AsignarController(
    $scope, DTOptionsBuilder, DTColumnBuilder, $q, $compile,
    Datatable, Funciones, Html, MyServices
    ) {
    var vm = this;
    var Controller = "Asignar";
    vm.dtInstance = {};
    vm.Html = function (empnom, emppag) {
        var Model = {
            FaltaIngresarElNombreDeLaAsignado: empnom,
            FaltaIngresarLaPaginaDeLaEmpresa: emppag
        }
        return Model;
    }
    vm.Servidor = function (Nivel, empnom, emppag) {
        var Model = {
            Nivel: Nivel,
            EMPNOM: empnom,
            EMPPAG: emppag
        }
        return Model;
    }
    vm.Data = function () {
        var defer = $q.defer();
        MyServices.Lista(Controller, "Asignar")
            .then(function (result) {
                $scope.data = result.data;
                defer.resolve(result.data);
            });
        return defer.promise;
    }
    vm.DataTable = function () {
        vm.dtOptions = DTOptionsBuilder.fromFnPromise(vm.Data)
            .withPaginationType('full_numbers')
            .withOption('createdRow', vm.createdRow)
            .withLanguage(Datatable.language)
            .withOption('order', [[0, 'desc']]);;
        vm.dtColumns = [
            DTColumnBuilder.newColumn('LOGCOD').withTitle('N°'),
            DTColumnBuilder.newColumn('USUEMPUSU').withTitle('Nombre del Usuario'),
            DTColumnBuilder.newColumn(null).withTitle('Habilitado').renderWith(vm.checkboxAcciones),
            DTColumnBuilder.newColumn(null).withTitle('Acciones').renderWith(vm.Acciones)
        ];
    }
    vm.createdRow = function (row, data, dataIndex) {
        $compile(angular.element(row).contents())($scope);
    }
    vm.Acciones = function (data, type, full, meta) {
        return '<div class="">' +
                    '<div class="">' +
                    '<div class="col">' +
                      '<button type="button" class="btn btn-success" ng-click="OpenModal(' + data.LOGCOD + ',' + 2 + ')"><i class="fas fa-edit"></i></button>' +
                      '<button type="button" class="btn btn-primary" ng-click="Asignacion(' + data.LOGCOD + ')"><i class="fas fa-edit"></i></button>' +
                      '<button type="button" class="btn btn-danger" ng-click="BorrarBtn(' + data.LOGCOD + ',' + 3 + ')"><i class="fas fa-trash-alt"></i></button>' +
                    '</div>' +
                  '</div>' +
                '</div>';
    }
    vm.checkboxAcciones = function (data, type, full, meta) {
        return '<input type="checkbox" class="form-control" id="04" ng-checked="' + data.ESTREG + '" disabled>';
    }
    $scope.OpenModal = function (Codigo, Nivel) {
        $scope.modalTipoStilo = "modal-dialog-grande";
        $scope.UrlPage = "";
        $scope.UrlUp = false;
        $scope.Objeto = {};
        var obj = {};
        if (Codigo != null) {
            obj.LOGCOD = Codigo;
            MyServices.ListaDe(Controller, "UsuariosPorAsignacion", obj)
            .then(function (result) {
                $scope.Lista = result.data;
            });
            for (var i = 0; i < $scope.data.length; i++) {
                if ($scope.data[i].USUEMPCOD == Codigo) {
                    MyServices.IdPor(Controller, "Asignar", $scope.data[i])
                        .then(function (result) {
                            $scope.Objeto = result.data;
                        });
                }
            }
        }
        $scope.Nivel = Nivel;
        Html.OpenModal('Mantenimiento');
        $scope.Titulo = "Mantenimiento de Asignaciones";
        $scope.UrlUp = true;
        $scope.UrlPage = Funciones.UrlTipo("/" + Controller + "/", Nivel);
        vm.ActivarBotones(Nivel);
    }
    $scope.CrearBtn = function (objeto) {
        objeto.Nivel = $scope.Nivel;
        var HtmlModel = new vm.Html(objeto.empnom, objeto.emppag);
        if (Funciones.validator(HtmlModel)) {
            var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.empnom, objeto.emppag);
            MyServices.Mantenimiento(Controller, Controller, ServidorModel)
                .then(function (result) {
                    Html.CloseModal("Mantenimiento");
                    Funciones.Sweet("Correcto", "Asignacion Registrada", 1, "Guardado");
                    vm.dtInstance.reloadData(true);
                    vm.DataTable();
                });
        }
    }
    $scope.ModificarBtn = function (objeto) {
        objeto.Nivel = $scope.Nivel;
        var HtmlModel = new vm.Html(objeto.EMPNOM, objeto.EMPPAG);
        if (Funciones.validator(HtmlModel)) {
            var ServidorModel = new vm.Servidor(objeto.Nivel, objeto.EMPNOM, objeto.EMPPAG);
            MyServices.Mantenimiento(Controller, Controller, objeto)
                .then(function (result) {
                    Html.CloseModal("Mantenimiento");
                    Funciones.Sweet("Correcto", "Asignacion Actualizada", 1, "Guardado");
                    vm.dtInstance.reloadData(true);
                    vm.DataTable();
                });
        }
    }
    $scope.BorrarBtn = function (Codigo, Nivel) {
        var objeto = {};
        for (var i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].USUEMPCOD == Codigo) {
                MyServices.IdPor(Controller, "Asignar", $scope.data[i])
                    .then(function (result) {
                        objeto = result.data;
                        objeto.Nivel = Nivel;
                        var nombre = "l " + result.data.EMPNOM;
                        Funciones.SweetBorrar(result.data.EMPNOM, nombre, "Asignar").then((result) => {
                            if (result.value) {
                                MyServices.Mantenimiento(Controller, Controller, objeto).then(function (result) {
                                    Funciones.Sweet("Correcto", "Usuario con Asignacion Borrada", 1, "Guardado");
                                    vm.dtInstance.reloadData(true);
                                    vm.DataTable();
                                });
                            } else if (result.dismiss == "cancel") {

                            }
                        });
                    });
            }
        }
    }
    $scope.Asignacion = function (Codigo) {
        MyServices.ListaDe(Controller, "UsuariosPorAsignacion", obj)
        .then(function (result) {
            $scope.Lista = result.data;
        });
    }
    vm.ActivarBotones = function (Nivel) {
        switch (Nivel) {
            case 1:
                $scope.Crear = true;
                $scope.Modificar = false;
                $scope.Borrar = false;
                break;
            case 2:
                $scope.Crear = false;
                $scope.Modificar = true;
                $scope.Borrar = false;
                break;
            case 3:
                $scope.Crear = false;
                $scope.Modificar = false;
                $scope.Borrar = true;
                break;
            default:
                $scope.Crear = false;
                $scope.Modificar = false;
                $scope.Borrar = false;
                console.log('Detalle');
        }
    }

    vm.DataTable();
};