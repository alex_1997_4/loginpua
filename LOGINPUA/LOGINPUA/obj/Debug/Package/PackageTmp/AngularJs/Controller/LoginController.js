﻿MyAppLogin.controller('LoginController', LoginController);
function LoginController($scope, MyServicesLogin, FuncionesLogin) {
    var vm = this;
    vm.Html = function (LOGUSU, LOGPAS) {
        var Model = {
            FaltaIngresarSuUsuario: LOGUSU,
            FaltaIngresarSuContraseña: LOGPAS
        }
        return Model;
    }
    vm.Servidor = function (LOGUSU, LOGPAS) {
        var Model = {
            LOGUSU: LOGUSU,
            LOGPAS: LOGPAS
        }
        return Model;
    }
    var Url = MyServicesLogin.Url();
    $scope.usuarios = {};
    $scope.Cuerpo = {};

    $scope.btnIngresar = function (obj) {
        var HtmlModel = new vm.Html(obj.LOGUSU,obj.LOGPAS);
        if (FuncionesLogin.validator(HtmlModel)) {
            var ServidorModel = new vm.Servidor(obj.LOGUSU, obj.LOGPAS);
            MyServicesLogin.ValidaDataLogin(ServidorModel).then(function (r) {
                //console.log(r);
                if (r.alerta != "Fallido") {
                    if (r.objeto != null) {
                        if (r.tipo == 1) {
                            FuncionesLogin.Sweet(r.alerta, r.mensaje, 1, "Ok");
                            setTimeout(function () {
                                location.href = Url + "/Home/Sistemas";
                            }, 1000);
                        } else if (r.mensaje == "Usuario Deshabilitado") {
                            FuncionesLogin.Sweet(r.alerta, r.mensaje, 2, "Ok");
                            setTimeout(function () {
                                location.href = Url + "/Home/Index";
                            }, 1000);
                        }
                }
            } else {
                    FuncionesLogin.Sweet(r.alerta, r.mensaje, 2, "Ok");
                    location.href = "#";
        }
    });
}
}
}