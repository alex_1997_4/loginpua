﻿'use strict';
MyApp.controller('PermisosController', PermisosController);
function PermisosController($scope, MyServices, Funciones, MyServicesGeneral) {
    var vm = this;
    var Controllador = "Accesos"
    vm.Html = function (LOGUSU, LOGPAS) {
        var Model = {
            FaltaIngresarSuUsuario: LOGUSU,
            FaltaIngresarSuContraseña: LOGPAS
        }
        return Model;
    }
    vm.Servidor = function (LOGUSU, LOGPAS) {
        var Model = {
            LOGUSU: LOGUSU,
            LOGPAS: LOGPAS
        }
        return Model;
    }
    $scope.Acceso = {};
    $scope.Acceso.Corredor = [];
    $scope.Acceso.Otros = [];
    $scope.Acceso.Programacion = [];
    $scope.Acceso.CC_TV = [];
    $scope.Acceso.Modulos_Programacion = [];
    $scope.Acceso.Modulos_Supervisor = [];
    $scope.Acceso.Modulos_Buses = [];
    $scope.Acceso.Modulo_RegistroPicoPlaca = [];
    $scope.Acceso.Modulo_Registro_Despacho = [];
    $scope.Acceso.Mantenimiento = [];


    


    $scope.Activar = {};
    vm.Accesos = function () {
        MyServices.Lista(Controllador, "Accesos").then(function (r) {
            for (var i = 0; i < r.data.Accesos.length; i++) {
                if (r.data.Accesos[i].ACCGRU == "Corredor") {
                    $scope.Activar.Corredor = true;
                    $scope.Acceso.Corredor.push(r.data.Accesos[i]);
                } else if (r.data.Accesos[i].ACCGRU == "Otros") {
                    $scope.Activar.Otros = true;
                    $scope.Acceso.Otros.push(r.data.Accesos[i]);
                } else if (r.data.Accesos[i].ACCGRU == "Programacion") {
                    $scope.Activar.Programacion = true;
                    $scope.Acceso.Programacion.push(r.data.Accesos[i]);
                }
                else if (r.data.Accesos[i].ACCGRU == "CC_TV") {
                    $scope.Activar.CC_TV = true;
                    $scope.Acceso.CC_TV.push(r.data.Accesos[i]);
                }
                else if (r.data.Accesos[i].ACCGRU == "Modulos_Programacion") {
                    $scope.Activar.Modulos_Programacion = true;
                    $scope.Acceso.Modulos_Programacion.push(r.data.Accesos[i]);
                }
                else if (r.data.Accesos[i].ACCGRU == "Modulos_Supervisor") {
                    $scope.Activar.Modulos_Supervisor = true;
                    $scope.Acceso.Modulos_Supervisor.push(r.data.Accesos[i]);
                }
                else if (r.data.Accesos[i].ACCGRU == "Modulos_Buses") {
                    $scope.Activar.Modulos_Buses = true;
                    $scope.Acceso.Modulos_Buses.push(r.data.Accesos[i]);
                }
                else if (r.data.Accesos[i].ACCGRU == "Modulo_RegistroPicoPlaca") {
                    $scope.Activar.Modulo_RegistroPicoPlaca = true;
                    $scope.Acceso.Modulo_RegistroPicoPlaca.push(r.data.Accesos[i]);
                }   
                else if (r.data.Accesos[i].ACCGRU == "Modulo_Registro_Despacho") {
                    $scope.Activar.Modulo_Registro_Despacho = true;
                    $scope.Acceso.Modulo_Registro_Despacho.push(r.data.Accesos[i]);
                }
                else if (r.data.Accesos[i].ACCGRU == "Mantenimiento") {
                    $scope.Activar.Mantenimiento = true;
                    $scope.Acceso.Mantenimiento.push(r.data.Accesos[i]);
                }
            }
        });
    }
    vm.Accesos();
}