﻿'use strict';
MyApp.controller('AdministradorController', AdministradorController);
function AdministradorController(
    $scope, MyServices, $q,Datatable,Funciones
    ) {
    var vm = this;

    $scope.FiltrarEmpresa = function () {
        $scope.empresas;
        var objeto = {
            Empresa:$scope.empresas
        };

        var defer = $q.defer();
        MyServices.ListaDe("Administracion", "Empresa", objeto)
            .then(function (result) {
                $scope.datas = result.data;
                defer.resolve(result.data);
            });
        return defer.promise;
    }

    vm.createdRow = function (row, data, dataIndex) {
        $compile(angular.element(row).contents())($scope);
    }


    vm.DataTable = function () {
        vm.dtOptions = DTOptionsBuilder.fromFnPromise($scope.FiltrarEmpresa)
            .withPaginationType('full_numbers')
            .withOption('createdRow', vm.createdRow)
            .withLanguage(Datatable.language);
        vm.dtColumns = [
            DTColumnBuilder.newColumn('LOGUSU').withTitle('LOGUSU'),
             
        ];
    }
     



};