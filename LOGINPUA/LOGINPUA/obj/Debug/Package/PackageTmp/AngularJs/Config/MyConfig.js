﻿'use strict';
MyApp.config(function ($urlRouterProvider, $stateProvider) {
    var vm = this;
    var Url = urlSitio;
    var Usuarios = {
        name: 'Usuarios',
        url: '/Usuarios',
        //template: '<h3>hello world!</h3>'
        templateUrl: Url + "/Usuarios/Usuarios"
    }
    var Empresas = {
        name: 'Empresas',
        url: '/Empresas',
        //template: '<h3>hello world!</h3>' 
        templateUrl: Url + "/Empresa/Empresa"
    }
    var Accesos = {
        name: 'Accesos',
        url: '/Accesos',
        //template: '<h3>hello world!</h3>'
        templateUrl: Url + "/Accesos/Accesos"
    }
    var Usuario_CC = {
        name: 'Usuarios_CC',
        url: '/Usuario_Empresas',
        //template: '<h3>hello world!</h3>'
        templateUrl: Url + "/Administracion/Listar_Usuarios_CC"
    }
    var Usuario_Disponible_Hacom = {
        name: 'Usuario_Disponible_Hacom',
        url: '/Usuarios_Hacom',
        //template: '<h3>hello world!</h3>'
        templateUrl: Url + "/Usuarios_Hacom/Usuarios_Hacoms"
    }
    var Asignar = {
        name: 'Asignar',
        url: 'Asignar',
        templateUrl: Url + "/Asignar/Asignar"
    }
    $urlRouterProvider.otherwise('/');
    $stateProvider.state("Usuarios", Usuarios);
    $stateProvider.state("Empresas", Empresas);
    $stateProvider.state("Accesos", Accesos);
    $stateProvider.state("Usuarios_CC", Usuario_CC);
    $stateProvider.state("Asignar", Asignar);
    $stateProvider.state("Usuario_Disponible_Hacom", Usuario_Disponible_Hacom);

});