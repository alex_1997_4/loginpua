﻿MyAppLogin.service('MyServicesLogin', MyServicesLogin);
function MyServicesLogin($http) {
    var vm = this;
    var data;
    vm.Url = function () {
        return urlSitio;
    }
    //Solicitud para Loguearse
    //COMUNICA EL JS DE ANGULAR CON EL CONTROLLER DE C#
    vm.ValidaDataLogin = function (Objetos) {
        var data = PostData("/Login/AutenticaCredenciales", Objetos)
         .then(function (r) {
             return r.data;
         });
        return data;
    }
    function Post(url) {
        data = $http({
            method: "POST",
            url: vm.Url() + url,
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
    function PostData(url, value) {
        var data = $http({
            method: "POST",
            url: vm.Url() + url,
            data: JSON.stringify(value),
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
    function Get(url) {
        data = $http({
            method: "GET",
            url: vm.Url() + url,
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
    function GetData(url, value) {
        data = $http({
            method: "GET",
            url: vm.Url() + url,
            data: JSON.stringify(value),
            headers: { "Content-Type": "application/json; charset = utf-8;" }
        });
        return data;
    }
};