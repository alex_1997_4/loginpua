﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using LOGINPUA.Repositorio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class BusesController : Controller
    {

        private readonly PerfilLN _PerfilLN;
        Util.Util utilidades = new Util.Util();

        private readonly LoginLN _LoginLN;
        private readonly RutaLN _rutaLN;
        private readonly CorredoresLN _CorredoresLN;
        private readonly BUSESLN _BusesLN;
        private readonly AccesoLN _AccesoLN;
        private readonly AccEmpLN _AccEmpLN;
        public BusesController(LoginLN LoginLN, AccesoLN AccesoLN, AccEmpLN AccEmpLN, BUSESLN BusesLN, CorredoresLN CorredoresLN)
        {
            _LoginLN = LoginLN;
            _AccesoLN = AccesoLN;
            _AccEmpLN = AccEmpLN;
            _BusesLN = BusesLN;
            _CorredoresLN = CorredoresLN;
        }
        ReadExcel readExcel = new ReadExcel();

        public ActionResult Buses()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }

        }
        public ActionResult Buses_Desafectados(int? id_bus_desafec,string ObservacionBus)
        {
            if (id_bus_desafec != null)
            {
                ViewBag.placa_desafec = id_bus_desafec;
                 
            }
            return View();
        }
 
        public string getRutaCorredor()
        {
            var lista = _CorredoresLN.getRutaCorredor();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string ListarBuses()
        {
            var lista = _BusesLN.Listar_Buses();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string ListarBusesbyId(int idCorredor)
        {
            var lista = _BusesLN.Listar_Buses_by_Id(idCorredor);
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string Subir_Excel()
        {
            //RPTA_GENERAL e = new RPTA_GENERAL();
            RPTA_GENERAL rpta = new RPTA_GENERAL();

            RegistroPicoPlacaLN _registroPicoPlacaLN = new RegistroPicoPlacaLN();

            var data = "";
            string usuario = Session["user_login"].ToString();
            var ob = Request.Files;
            var archivoSubido = Request.Files[0];
            var nombreArchivo = Path.GetFileName(archivoSubido.FileName);
            var extensionArchivo = Path.GetExtension(archivoSubido.FileName);
            var arrFileName = nombreArchivo.Split('.');
            var nuevoNombreArchivoExcel = arrFileName[0] + "_FILE_" + DateTime.Now.ToString("dd_MM_yyyy_h_mm_ss") + extensionArchivo;
            var pathArchivo = Server.MapPath("~/Adjuntos/Files_Despacho/" + nuevoNombreArchivoExcel);
            string pathFinal = Path.Combine(pathArchivo);
            archivoSubido.SaveAs(pathFinal);

            data = readExcel.Matriz_Placas(pathFinal, usuario);

            return data;
        }


        public string Registro_Bus_Indiv(CC_BUSES Model_Bus)
        {
            RPTA_GENERAL r = new RPTA_GENERAL();
            var result = "";
            //Validar si Existe la placa
            var rpt_placa = _BusesLN.Verifica_Placa_Existente(Model_Bus.BS_PLACA);

            if (rpt_placa.AUX == 0)
            {

                string usuario = Session["user_login"].ToString();
                Model_Bus.ID_ESTADO = 1;
                Model_Bus.USU_REG = usuario;
                Model_Bus.ESTADO_VEHICULO = "NORMAL";

                var data = _BusesLN.Insertar_Buses_Nuevos(Model_Bus);
                result = JsonConvert.SerializeObject(data);
            }
            else
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = "Placa Existente";
                result = JsonConvert.SerializeObject(r);
            }
            return result;
        }

        public string anularBus(int idBus)
        {
            var anularBus = JsonConvert.SerializeObject(_BusesLN.anularBus(idBus, Session["user_login"].ToString()));
            return anularBus;
        }

        public string Modificar_Bus(CC_BUSES Model_Bus)
        {
            string usuario = Session["user_login"].ToString();

            Model_Bus.USU_MODIF = usuario;


            var data = _BusesLN.Modificar_Bus(Model_Bus);
            var result = JsonConvert.SerializeObject(data);
            return result;
        }

        public string listarBusesDesafectados()
        {
            var lista = _BusesLN.Listar_Buses_Desafectados();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string Registro_Bus_Afectado(CC_BUSES Model_Bus, int id_placa_reemplazada)
        {
            string usuario = Session["user_login"].ToString();
            Model_Bus.ID_ESTADO = 1;
            Model_Bus.USU_REG = usuario;
            Model_Bus.ESTADO_VEHICULO = "NORMAL";


            var data = _BusesLN.Insertar_Buses_Afectados(Model_Bus);

            //PLACA NUEVA DEL BUS
            var placa_nueva = data.DES_ESTADO;
            var datas = _BusesLN.desafectarBus(id_placa_reemplazada, placa_nueva);
            var result = JsonConvert.SerializeObject(datas);
            return result;
        }
    }

}