﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Util.Seguridad;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LOGINPUA.Util;
using System.Data;

namespace LOGINPUA.Controllers
{
    public class Usuario_PersonasController : Controller
    {
        private readonly Usuario_PersonaLN _Usuario_PersonaLN;
        private readonly Proveedor_ServLN _Proveedor_ServLN;

        private readonly LoginLN _LoginLN;
        Util.Util utilidades = new Util.Util();

        // GET: Usuario_Personas
        public Usuario_PersonasController(LoginLN LoginLN, Usuario_PersonaLN Usuario_PersonaLN, Proveedor_ServLN Proveedor_ServLN)
        {
            _Usuario_PersonaLN = Usuario_PersonaLN;
            _LoginLN = LoginLN;
            _Proveedor_ServLN = Proveedor_ServLN;
        }

        public ActionResult MantenimientoUsuario()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }

        public string ListarPersonas()
        {
            var listaPersona = _Usuario_PersonaLN.ListarPersona();
            var result = JsonConvert.SerializeObject(listaPersona); //para la lista principal
            return result;
        }

        public string ListarPerfil_modalidad()
        {
            var listar = _Usuario_PersonaLN.ListarPerfil_modalidad();
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }

        public string ListarUsuario_x_ID(int id)
        {
            var listar = _Usuario_PersonaLN.ListarUsuario_x_ID(id);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }
        public string ListCuentas_x_id(int id_prov_serv)
        {
            var listar = _Usuario_PersonaLN.ListCuentas_x_id(id_prov_serv);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }

        public string AgregarUsuario(int idpersona, string perfiles, string usuarios, string contraseña, int txtUsuarioProv, string idcorredores)
        {
            var listaPersona = _Usuario_PersonaLN.ListarPersona();
            ViewBag.listaPersona = listaPersona;

            var result = "";
            string usuario = Session["user_login"].ToString();
            var msj = _LoginLN.Verificar_Usuario(usuarios, contraseña);
            string[] s_perfiles = new string[10];
            s_perfiles = perfiles.Split('|');

            var msj_final = msj == null ? 0 : msj.ID_USUARIO;
            if (msj_final == 0)
            {
                var contraseña_ = Encriptador.Encriptar(contraseña);

                var rpta = _Usuario_PersonaLN.AgregarUsuario(idpersona, usuarios, contraseña_, usuario);

                //ID DEL USUARIO CREADO
                var id_usuario = rpta.AUX;

                if (txtUsuarioProv != 0 && idcorredores != null)
                {
                    //REGISTRAR EL CORREDOR POR CUENTA
                    string[] id_corredor_nuevo = idcorredores.Split('|');
                    foreach (var idcorredor in id_corredor_nuevo)
                    {
                        int id_corredores_final = Convert.ToInt32(idcorredor);
                        _Proveedor_ServLN.AgregarUsuario_Corredor(id_corredores_final, txtUsuarioProv, usuario);
                    }

                    //ACTUALIZAR EL USUARIO POR LA CUENTA
                    var rptaa = _Proveedor_ServLN.Actualizar_Usuario_Prov(txtUsuarioProv, id_usuario, usuario);

                }

                foreach (var id_perfil in s_perfiles)
                {
                    int id_perfil_ = Convert.ToInt32(id_perfil);
                    var rpta_ = _Usuario_PersonaLN.registrarUsuario_Perfil(id_usuario, id_perfil_, usuario);

                }



                result = JsonConvert.SerializeObject(rpta);
            }
            else
            {
                RPTA_GENERAL rpt = new RPTA_GENERAL();
                rpt.DES_ESTADO = "Usuario Existente Cambiar";
                rpt.COD_ESTADO = 0;
                result = JsonConvert.SerializeObject(rpt);
            }

            return result;
        }

        public string ListarUsuarios()
        {
            var listar = _Usuario_PersonaLN.ListarUsuarios();

            //Para Saber la clave desencriptada
            List<CC_USUARIO_PERSONA> Lista_Usuario = new List<CC_USUARIO_PERSONA>();
            //foreach (var item in listar)
            //{
            //    CC_USUARIO_PERSONA usuario = new CC_USUARIO_PERSONA();
            //    usuario.ID_USUARIO = item.ID_USUARIO;

            //    var Clave = Encriptador.Desencriptar(item.CLAVE);
            //    usuario.CLAVE = Clave;
            //    usuario.NOMBRE = item.NOMBRE;
            //    usuario.APEPAT = item.APEPAT;
            //    usuario.APEMAT = item.APEMAT;
            //    usuario.USU_REG = item.USU_REG;
            //    usuario.ID_ESTADO = item.ID_ESTADO;
            //    usuario.ID_USUARIO = item.ID_USUARIO;
            //    usuario.USUARIO = item.USUARIO;
            //    usuario.USU_REG = item.USU_REG;
            //    usuario.FECHA_REG = item.FECHA_REG;
            //    Lista_Usuario.Add(usuario);
            //}



            //var result = JsonConvert.SerializeObject(Lista_Usuario); //para la lista principal

            var result = JsonConvert.SerializeObject(listar); //para la lista principal


            return result;
        }
        public string Desactivar_Usuarios(int idusuario)
        {
            string usuario = Session["user_login"].ToString();
            var rpta = _Usuario_PersonaLN.Desactivar_Usuarios(idusuario, usuario);
            var result = JsonConvert.SerializeObject(rpta);
            return result;
        }
        public string ModificarUsuarios(int idusuario, string clave, string estado_contraseña, int estado_usuario, string idperfiles)
        {
            string contraseñafinal = "";
            RPTA_GENERAL rpta = new RPTA_GENERAL();
            string usuario_ssesion = Session["user_login"].ToString();
            if (estado_contraseña == "encriptado")
            {
                contraseñafinal = clave;
            }
            else
            {
                var clave_encriptada = Encriptador.Encriptar(clave);
                contraseñafinal = clave_encriptada;
            }

            rpta = _Usuario_PersonaLN.ModificarUsuarios_Perfil(idusuario, idperfiles, contraseñafinal, estado_usuario, usuario_ssesion);
            var result = JsonConvert.SerializeObject(rpta);
            return result;
        }
    }
}