﻿

using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Util.Seguridad;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class Menu_PerfilController : Controller
    {
        private readonly Menu_PerfilLN _Menu_perfilLN;
        private readonly PerfilLN _Perfil;
        private readonly Menu_UsuarioLN _MenuUsuario;
        Util.Util utilidades = new Util.Util();


        // GET: Usuario_Personas
        public Menu_PerfilController(Menu_PerfilLN Menu_perfilLN, PerfilLN Perfil, Menu_UsuarioLN MenuUsuario)
        {

            _Menu_perfilLN = Menu_perfilLN;
            _Perfil = Perfil;
            _MenuUsuario = MenuUsuario;
        }

        public ActionResult Asignar_MenuPerfi()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();

            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());


            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }
        public string listarMenuPadre(int idmodalidad)
        {
            var listar = _MenuUsuario.listarMenuPadre(idmodalidad);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }

        public string listarModalidad_Perfiles()
        {
            var listar = _Menu_perfilLN.listarModalidad_Perfiles();
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }

        public string AgregarPermiso_Perfil(int idperfil, int idmenu)
        {
            string usuario = Session["user_login"].ToString();
            var rpta = _Menu_perfilLN.AgregarPermiso_Perfil(idperfil, idmenu, usuario);
            var result = JsonConvert.SerializeObject(rpta);
            return result;
        }

        public string ListarPermisos(int idperfil)
        {
            var listar = _Menu_perfilLN.ListarPermisos(idperfil);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }
        public string ListarMenu_x_ID(int idmenu, int idperfil)
        {
            var listar = _Menu_perfilLN.ListarMenu_x_ID(idmenu, idperfil);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }
        public string Desactivar_Permisos(int idmenu, int idestado, int id_perfil)
        {
            string usuario = Session["user_login"].ToString();
            var rpta = _Menu_perfilLN.Desactivar_Permisos(idmenu, idestado, id_perfil, usuario);
            var result = JsonConvert.SerializeObject(rpta);
            return result;
        }
        public string Agregar_AccionMenu(int idmenu_perfil, string acciones)
        {
            string usuario = Session["user_login"].ToString();
            RPTA_GENERAL rpt = new RPTA_GENERAL();

            string[] array_accion = new string[40];
            array_accion = acciones.Split('|');

            rpt = _Menu_perfilLN.Desactivar_Acciones(idmenu_perfil);

            if (acciones != "")
            {
                foreach (var id_accion in array_accion)
                {
                    if (id_accion != "")
                    {
                        int id_accion_ = Convert.ToInt32(id_accion);
                        rpt = _Menu_perfilLN.Agregar_AccionMenu(idmenu_perfil, id_accion_, usuario);

                    }
                }
            }

            var result = JsonConvert.SerializeObject(rpt); //para la lista principal
            return result;
        }

        public string accesoslist_menu_idperfil(int idperfil)
        {
            var listar = _Menu_perfilLN.accesoslist_menu_idperfil(idperfil);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }

        public string accesoslist_X_idmenuperfil(int idmenuperfil)
        {
            var listar = _Menu_perfilLN.accesoslist_X_idmenuperfil(idmenuperfil);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }


        public string listarAcceso()
        {
            var listar = _Menu_perfilLN.listarAcceso();
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }


    }
}