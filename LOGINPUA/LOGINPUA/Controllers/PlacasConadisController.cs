﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpreadsheetLight;
using Newtonsoft.Json;
using System.Globalization;
using System.Transactions;
using System.IO.Compression;
using LN.Reportes;

namespace LOGINPUA.Controllers
{
    public class PlacasConadisController : Controller
    {
        private readonly PlacaFiscalizacionLN _PlacasFiscalizacion;
        // GET: PlacasConadis
        public PlacasConadisController(PlacaFiscalizacionLN PlacasFiscalizacion)
        {
            _PlacasFiscalizacion = PlacasFiscalizacion;
        }

        public ActionResult Index()
        {
            return View();
        }

        public string getplacasConadis()
        {
            var rpta = "";
            //var placasConadis = "";
            try
            {
                rpta = JsonConvert.SerializeObject(_PlacasFiscalizacion.getPlacasConadis());
            }
            catch (Exception ex )
            {
                rpta = ex.Message;
            }
            
            return rpta;
        }

        public string getplacasVecinosEmp()
        {
            var rpta = "";
            //var placasConadis = "";
            try
            {
                rpta = JsonConvert.SerializeObject(_PlacasFiscalizacion.getplacasVecinosEmp());
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }

            return rpta;
        }
    }
}