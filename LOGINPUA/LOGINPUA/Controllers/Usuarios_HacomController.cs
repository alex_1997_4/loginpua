﻿using LN.EntidadesLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class Usuarios_HacomController : Controller
    {
        private readonly UsuarioLN _UsuarioLN;
        public Usuarios_HacomController(UsuarioLN UsuarioLN)
        {
            _UsuarioLN = UsuarioLN;
        }
        // GET: Usuarios_Hacom
        public ActionResult index()
        {
            return View();
        }
        public ActionResult Usuarios_Hacoms()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ListaUsuarios_Hacoms()
        {
            var lista = _UsuarioLN.Listar_Usuarios_Hacom();
            return Json(lista);
        }
    }
}