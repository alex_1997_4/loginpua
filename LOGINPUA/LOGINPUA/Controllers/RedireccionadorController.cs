﻿using LOGINPUA.Util.Seguridad;
using System;
using System.Web.Mvc;
using System.Diagnostics;
using System.ComponentModel;
using System.IO;
using LOGINPUA.Models;
using LN.EntidadesLN;
using System.Net;
using Entidades;
using System.Net.NetworkInformation;
using ENTIDADES;

namespace LOGINPUA.Controllers
{
    public class RedireccionadorController : Controller
    {
        private readonly LoginLN _LoginLN;
        private readonly LogLN _LogLN;
        private readonly UsuariosEmpresaLN _UsuariosEmpresaLN;
        public RedireccionadorController(LoginLN LoginLN, LogLN LogLN, 
            UsuariosEmpresaLN UsuariosEmpresaLN)
        {
            _LogLN = LogLN;
            _LoginLN = LoginLN;
            _UsuariosEmpresaLN = UsuariosEmpresaLN;

        }

        // GET: Redireccionador
        public ActionResult Index()
        {
            return View();
        }

        //OBTENER MAC
        public static PhysicalAddress GetMacAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    return nic.GetPhysicalAddress();
                }
            }
            return null;
        }
        string MAC = GetMacAddress().ToString();

        public ActionResult Abexa(int id)
        {
            TB_EMPRESA TB_EMPRESA = new TB_EMPRESA();
            TL_LOG LOG = new TL_LOG();

            int id_usuario = Convert.ToInt32(Session["userId"]);
            //int empresa_usuario = 2;
            //TB_EMPRESA.EMPEMP = empresa_usuario;
            //TB_EMPRESA.EMPIDE = id_usuario;
            var usuarioEmpresa = _UsuariosEmpresaLN.ObtenerPorCodigo(id_usuario, 2);

            //Usuario Abexa
            string usuario = usuarioEmpresa.USUEMPUSU;
            //string usuario = "MHUAMAN";
            //Password
            string blanco = usuarioEmpresa.USUEMPCON;

            //Corredor Correspondiente
            int corredor = id;
            string corredorAbexa = "";
            string nombreCorredor = "";
            string url = "http://abexacloud.com/tgpsnw2/usuario/loginExterno/?arg=";
            if (corredor == 3)
            {
                nombreCorredor = "Corredor Morado";
                corredorAbexa = usuario + "@sjl";
            }
            else if (corredor == 4)
            {
                nombreCorredor = "Corredor Verde";
                corredorAbexa = usuario + "@cotranscar";
            }
            else if (corredor == 5)
            {
                nombreCorredor = "Corredor Azul";
                corredorAbexa = usuario + "@cta";
            }
            else if (corredor == 6)
            {
                nombreCorredor = "Ingreso a la Programacion";
                corredorAbexa = usuario + "@pruebas";
                url = "http://abexacloud.com/tgpstest/Usuario/generarProgramacion?arg=";
            }

            //Concatena datos y encripta
            string concatenado = corredorAbexa + "|" + Encriptador.Desencriptar(blanco);
            //string concatenado = "jhermitano@pruebas|123"; //+ Encriptador.Desencriptar(blanco);
            string codigoConcatenado = Encriptador.Encriptar(concatenado);

            //OBTENER IP LOCAL
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }


            //DATOS QUE SE GUARDARÁN EN EL LOG
            LOG.LOGIP = localIP;
            LOG.LOGFEC = DateTime.Now;
            LOG.FECREG = DateTime.Now;
            LOG.LOGUSE = usuario;
            LOG.LOGDES = "Ingresó al sistema Abexa - " + nombreCorredor;
            LOG.LOGMAC = MAC;
            LOG.ESTREG = 1;
            //SI HAY INGRESO A SISTEMA EXTERNO, LO REGISTRA
            var LogExitoso = _LogLN.Insertar(LOG);

            //REDIRECCIONA A ABEXA
            return Redirect(url + codigoConcatenado);
        }

        public ActionResult Hacom(int id)
        {
            TB_EMPRESA TB_EMPRESA = new TB_EMPRESA();
            TL_LOG LOG = new TL_LOG();

            int id_usuario = Convert.ToInt32(Session["userId"]);
            //int empresa_usuario = 1;

            //TB_EMPRESA.EMPEMP = empresa_usuario;
            //TB_EMPRESA.EMPIDE = id_usuario;
            var usuarioEmpresa = _UsuariosEmpresaLN.ObtenerPorCodigo(id_usuario, 1);

            //Usuario Hacom
            string usuario = usuarioEmpresa.USUEMPUSU;

            //Password
            string blanco = usuarioEmpresa.USUEMPCON;

            //PARA EL ENCRIPTADOR
            MensajeModel modelo = new MensajeModel();
            string output = "";
            string str = "";
            string usuarioH = "";
            string passwordH = "";


            int corredor = id;
            String corredorHacom = "";
            string nombreCorredor = "";
            if (corredor == 1)
            {
                nombreCorredor = "Corredor Rojo";
                corredorHacom = "20563472244";
                usuarioH = usuario;
                passwordH = blanco;
            }
            else if (corredor == 2)
            {
                nombreCorredor = "Corredor Amarillo";
                corredorHacom = "20566238927";
                usuarioH = usuario;
                passwordH = blanco;
            }

            //var ubijava = @"C:\Program Files\Java\jdk1.8.0_201\bin\";
            var ubiJar = "-jar \"" + Server.MapPath("~/Util/encriptador.jar") + "\" " + corredorHacom + " " + usuarioH + " " + passwordH;
            try
            {
                var proceso = new Process();
                proceso.EnableRaisingEvents = false;
                proceso.StartInfo.FileName = @"C:\Program Files\Java\jdk1.8.0_201\bin\java.exe";
                proceso.StartInfo.Arguments = ubiJar;
                proceso.StartInfo.UseShellExecute = false;
                proceso.StartInfo.RedirectStandardOutput = true;
                proceso.Start();
                StreamReader reader = proceso.StandardOutput;
                output = reader.ReadToEnd();
                str = output;
                str = str.Remove(str.Length - 2, 2);

                //OBTENER IP LOCAL
                IPHostEntry host;
                string localIP = "";
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if (ip.AddressFamily.ToString() == "InterNetwork")
                    {
                        localIP = ip.ToString();
                    }
                }


                //DATOS QUE SE GUARDARÁN EN EL LOG
                LOG.LOGIP = localIP;
                LOG.LOGFEC = DateTime.Now;
                LOG.FECREG = DateTime.Now;
                LOG.LOGUSE = usuario;
                LOG.LOGDES = "Ingresó al sistema Matrix - " + nombreCorredor;
                LOG.LOGMAC = MAC;
                LOG.ESTREG = 1;
                //SI HAY INGRESO A SISTEMA EXTERNO, LO REGISTRA
                var LogExitoso = _LogLN.Insertar(LOG);

 

                //REDIRECCIONA A MATRIX
                return Redirect("http://www.matrix.pe/matrix/?idEnc=" + str);
            }
            catch (Win32Exception w)
            {
                Console.WriteLine(w.Message);
                Console.WriteLine(w.ErrorCode.ToString());
                Console.WriteLine(w.NativeErrorCode.ToString());
                Console.WriteLine(w.StackTrace);
                Console.WriteLine(w.Source);
                Exception e = w.GetBaseException();
                Console.WriteLine(e.Message);
                modelo.mensaje = e.Message;
                return View(modelo);
            }
        }

        public ActionResult Otros(int id) {
            string url = "Home/mensaje_error";
            switch (id)
            {
                case 1:
                    url = "http://200.37.244.149:8002/avlcc/mapeo";
                    break;
                case 2:
                    url = "http://200.37.244.150:8003/BOE/BI";
                    break;
                default:
                    return View(url);
                    break;
            }
            return Redirect(url);
        }
    }
}