﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpreadsheetLight;
using Newtonsoft.Json;


namespace LOGINPUA.Controllers
{
    public class SupervisorController : Controller
    {
        private readonly PerfilLN _PerfilLN;
        Util.Util utilidades = new Util.Util();

        private readonly LoginLN _LoginLN;

        private readonly AccesoLN _AccesoLN;
        private readonly AccEmpLN _AccEmpLN;
         private readonly BUSESLN _BusesLN;
   
        CC_CONDUCTORES model = new CC_CONDUCTORES();
        BusesModel buse_model = new BusesModel();

        public SupervisorController(ConductoresLN ConductoresLN, LoginLN LoginLN, AccesoLN AccesoLN, AccEmpLN AccEmpLN, DistanciaLN DistanciaLN, BUSESLN BusesLN, CorredoresLN CorredorLN, RutaLN RutaLN, RegistroPicoPlacaLN registroVelocidadPPlaca)
        {
            _LoginLN = LoginLN;
            _AccesoLN = AccesoLN;
            _AccEmpLN = AccEmpLN;
             _BusesLN = BusesLN;
   
        }
        public ActionResult Index()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }



        public ActionResult Fuera_Campo()
        {
            List<CC_CONDUCTORES> lista = _BusesLN.Listar_Placa();
            ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");

            DateTime fecha_Actual = DateTime.Now;
            string fecha_nombre = fecha_Actual.ToString("dd/MM/yyyy");
            string hora_actual = fecha_Actual.ToString("HH:mm:ss");

            ViewBag.fecha_actual = fecha_nombre;
            ViewBag.Hora_actual = hora_actual;

            return View();
        }

        [HttpPost]
        public ActionResult Estado_Bus()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Estado_Bus_consulta(CC_BUSES model, string direccion, int? km, string latitud, string longitud)
        {
            string usuario = "";
            if (Session["user_login"] == null)
            {
                return View("../Home/Sistemas");
            }
            else
            {
                usuario = Session["user_login"].ToString();

            }

            if (latitud == "" && longitud == "")
            {
                ViewBag.GPS = "Debe Activar GPS";
                List<CC_CONDUCTORES> lista = _BusesLN.Listar_Placa();

                ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");
                ConductoresModel modelo_c = new ConductoresModel();

                DateTime fecha_Actual = DateTime.Now;
                string fecha_nombre = fecha_Actual.ToString("dd/MM/yyyy");
                string hora_actual = fecha_Actual.ToString("HH:mm:ss");

                ViewBag.fecha_actual = fecha_nombre;
                ViewBag.Hora_actual = hora_actual;
                return View("Fuera_Campo");
            }

            var Lista_Dentro_vehiculo = _BusesLN.Lista_Conceptos_Dentro();
            var Lista_Exterior_Vehiculo = _BusesLN.Lista_Conceptos_Exterior();
            var Lista_Cabina_Vehiculo = _BusesLN.Lista_Cabina_Vehiculo();


            BusesModel modelo_buses = new BusesModel();
            try
            {
                var data13 = _BusesLN.Estado_Vigencias(model);
            }
            catch (Exception)
            {
                ViewBag.Message = "Ingrese Placa";
                List<CC_CONDUCTORES> lista = _BusesLN.Listar_Placa();
                ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");
                ConductoresModel modelo_c = new ConductoresModel();

                DateTime fecha_Actual = DateTime.Now;
                string fecha_nombre = fecha_Actual.ToString("dd/MM/yyyy");
                string hora_actual = fecha_Actual.ToString("HH:mm:ss");

                ViewBag.fecha_actual = fecha_nombre;
                ViewBag.Hora_actual = hora_actual;
                return View("Fuera_Campo");
            }
            var vigencias_autos = _BusesLN.Estado_Vigencias(model);

            //LOGICA DE VIGENCIA
            DateTime fecha = DateTime.Now;
            string fecha_ = fecha.ToString("dd/MM/yyyy");



            string mensaje = "";

            //if (vigencias_autos.BS_CVS_FEC_FIN != null &&
            //    //vigencias_autos.COD_CAC != null &&
            //    vigencias_autos.BS_RTV_FIN  != null &&
            //    vigencias_autos.BS_SOAT_FEC_FIN != null &&
            //    //vigencias_autos.RC_FIN != null &&
            //    vigencias_autos.BS_VEHICULOS_FIN != null)
            //{

            //    if (Convert.ToDateTime(vigencias_autos.BS_CVS_FEC_FIN) < Convert.ToDateTime(fecha_) ||
            //        //Convert.ToDateTime(vigencias_autos.COD_CAC) < Convert.ToDateTime(fecha_) ||
            //        Convert.ToDateTime(vigencias_autos.BS_RTV_FIN) < Convert.ToDateTime(fecha_) ||
            //        Convert.ToDateTime(vigencias_autos.BS_SOAT_FEC_FIN) < Convert.ToDateTime(fecha_) 
            //        //|| Convert.ToDateTime(vigencias_autos.RC_FIN) < Convert.ToDateTime(fecha_) 
            //        || Convert.ToDateTime(vigencias_autos.BS_VEHICULOS_FIN) < Convert.ToDateTime(fecha_))
            //    {
            //        mensaje = "NO PROGRAMABLE";
            //    }
            //    else
            //    {
            //        mensaje = "PROGRAMABLE";

            //    }
            //}





            string[] vigencias = new string[] { vigencias_autos.BS_CVS_FEC_FIN, vigencias_autos.COD_CAC, vigencias_autos.BS_RTV_FIN, vigencias_autos.BS_SOAT_FEC_FIN, vigencias_autos.BS_RC_FIN, vigencias_autos.BS_VEHICULOS_FIN };
            ViewBag.vigencias = vigencias;

            modelo_buses.Lista_Dentro_Vehiculo = Lista_Dentro_vehiculo;
            modelo_buses.Lista_Exterior_Vehiculo = Lista_Exterior_Vehiculo;
            modelo_buses.Lista_Cabina_Vehiculo = Lista_Cabina_Vehiculo;


            modelo_buses.CC_BUSES = vigencias_autos;

            ViewBag.direccion = direccion;
            ViewBag.km = km;
            ViewBag.placa = vigencias_autos.BS_PLACA;
            ViewBag.concesionario = vigencias_autos.BS_NOM_EMPRE;
            ViewBag.latitud = latitud;
            ViewBag.longitud = longitud;
            ViewBag.Estado = "PROGRAMABLE";




            return View("Estado_Bus", modelo_buses);
        }


        [HttpPost]
        public ActionResult Insertar_Buses_Despacho(int[] Conceptos, string[] estado, string[] calidad, string[] observacion, string placa, string Concesionario, int? km, string direccion, string longitud, string latitud, HttpPostedFileBase Foto_Cargar1, HttpPostedFileBase Foto_Cargar2, HttpPostedFileBase Foto_Cargar3, HttpPostedFileBase Foto_Cargar4, string mensaje, string comentario)
        {
            string usuario = "";
            if (Session["user_login"] == null)
            {
                return View("../Home/Sistemas"); 
            }
            else
            {
                usuario = Session["user_login"].ToString();
            }

            List<BUSES_DESPACHO> lista = new List<BUSES_DESPACHO>();
            int a = 0;
            int id = 0;
            int codigo_archivo = 0;
            DateTime fecha_Actual = DateTime.Now;
            string fecha_Busqueda = fecha_Actual.ToString("dd/MM/yyyy hh:mm:ss ");
            string codigo_seudonimo = fecha_Actual.ToString("dd-MM-yyyy");



            /*CODIGO ALETORIO */


            try
            {
                var Ultimo_Codigo_Archivo = _BusesLN.Ultimo_Codigo_Archivo(placa);
                codigo_archivo = Ultimo_Codigo_Archivo.BD_COD_DESPACHO + 1;
            }
            catch (Exception)
            {
                codigo_archivo = 0001;
            }


            try
            {
                var id1 = _BusesLN.Ultimo_Buses_Despacho();
                id = id1.BD_ID;
            }
            catch (Exception)

            {
                id = 0;
            }


            /* IMAGEN */

            string ficheroNombre1 = "";
            string ficheroNombre2 = "";
            string ficheroNombre3 = "";
            string ficheroNombre4 = "";


            string urlArchivos = "~/Util/img_despacho";

            if (Foto_Cargar1 != null && Foto_Cargar2 == null && Foto_Cargar3 == null && Foto_Cargar4 == null)
            {
                ficheroNombre1 = "000" + codigo_archivo + "-1" + placa + "." + Path.GetFileName(Foto_Cargar1.ContentType);
                string r1 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre1);
                Foto_Cargar1.SaveAs(r1);
            }
            if (Foto_Cargar1 != null && Foto_Cargar2 != null && Foto_Cargar3 == null && Foto_Cargar4 == null)
            {
                ficheroNombre1 = "000" + codigo_archivo + "-1" + placa + "." + Path.GetFileName(Foto_Cargar1.ContentType);
                string r1 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre1);
                Foto_Cargar1.SaveAs(r1);

                ficheroNombre2 = "000" + codigo_archivo + "-2" + placa + "." + Path.GetFileName(Foto_Cargar2.ContentType);
                string r2 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre2);
                Foto_Cargar2.SaveAs(r2);

            }

            if (Foto_Cargar1 != null && Foto_Cargar2 != null && Foto_Cargar3 != null && Foto_Cargar4 == null)
            {
                ficheroNombre1 = "000" + codigo_archivo + "-1" + placa + "." + Path.GetFileName(Foto_Cargar1.ContentType);
                string r1 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre1);
                Foto_Cargar1.SaveAs(r1);

                ficheroNombre2 = "000" + codigo_archivo + "-2" + placa + "." + Path.GetFileName(Foto_Cargar2.ContentType);
                string r2 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre2);
                Foto_Cargar2.SaveAs(r2);

                ficheroNombre3 = "000" + codigo_archivo + "-3" + placa + "." + Path.GetFileName(Foto_Cargar3.ContentType);
                string r3 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre3);
                Foto_Cargar3.SaveAs(r3);

            }
            if (Foto_Cargar1 != null && Foto_Cargar2 != null && Foto_Cargar3 != null && Foto_Cargar4 != null)
            {
                ficheroNombre1 = "000" + codigo_archivo + "-1" + placa + "." + Path.GetFileName(Foto_Cargar1.ContentType);
                string r1 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre1);
                Foto_Cargar1.SaveAs(r1);

                ficheroNombre2 = "000" + codigo_archivo + "-2" + placa + "." + Path.GetFileName(Foto_Cargar2.ContentType);
                string r2 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre2);
                Foto_Cargar2.SaveAs(r2);

                ficheroNombre3 = "000" + codigo_archivo + "-3" + placa + "." + Path.GetFileName(Foto_Cargar3.ContentType);
                string r3 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre3);
                Foto_Cargar3.SaveAs(r3);

                ficheroNombre4 = "000" + codigo_archivo + "-4" + placa + "." + Path.GetFileName(Foto_Cargar4.ContentType);
                string r4 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre4);
                Foto_Cargar4.SaveAs(r4);

            }
            if (Foto_Cargar1 == null && Foto_Cargar2 != null && Foto_Cargar3 != null)
            {
                ficheroNombre2 = "000" + codigo_archivo + "-2" + placa + "." + Path.GetFileName(Foto_Cargar2.ContentType);
                string r2 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre2);
                Foto_Cargar2.SaveAs(r2); ;

                ficheroNombre3 = "000" + codigo_archivo + "-3" + placa + "." + Path.GetFileName(Foto_Cargar3.ContentType);
                string r3 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre3);
                Foto_Cargar3.SaveAs(r3);

            }
            else if (Foto_Cargar2 == null && Foto_Cargar1 != null && Foto_Cargar3 != null)
            {
                ficheroNombre1 = "000" + codigo_archivo + "-1" + placa + "." + Path.GetFileName(Foto_Cargar1.ContentType);
                string r1 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre1);
                Foto_Cargar1.SaveAs(r1);

                ficheroNombre3 = "000" + codigo_archivo + "-3" + placa + "." + Path.GetFileName(Foto_Cargar3.ContentType);
                string r3 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre3);
                Foto_Cargar3.SaveAs(r3);

            }
            else if (Foto_Cargar3 == null && Foto_Cargar2 != null && Foto_Cargar1 != null)
            {
                ficheroNombre1 = "000" + codigo_archivo + "-1" + placa + "." + Path.GetFileName(Foto_Cargar1.ContentType);
                string r1 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre1);
                Foto_Cargar1.SaveAs(r1);

                ficheroNombre2 = "000" + codigo_archivo + "-2" + placa + "." + Path.GetFileName(Foto_Cargar2.ContentType);
                string r2 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre2);
                Foto_Cargar2.SaveAs(r2);


            }
            else if (Foto_Cargar4 != null && Foto_Cargar3 == null && Foto_Cargar2 == null && Foto_Cargar1 == null)
            {

                ficheroNombre4 = "000" + codigo_archivo + "-4" + placa + "." + Path.GetFileName(Foto_Cargar4.ContentType);
                string r4 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre4);
                Foto_Cargar4.SaveAs(r4);
            }

            else if (Foto_Cargar3 != null && Foto_Cargar4 == null && Foto_Cargar2 == null && Foto_Cargar1 == null)
            {

                ficheroNombre3 = "000" + codigo_archivo + "-3" + placa + "." + Path.GetFileName(Foto_Cargar3.ContentType);
                string r3 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre3);
                Foto_Cargar3.SaveAs(r3);
            }
            else if (Foto_Cargar2 != null && Foto_Cargar3 == null && Foto_Cargar4 == null && Foto_Cargar1 == null)
            {

                ficheroNombre2 = "000" + codigo_archivo + "-2" + placa + "." + Path.GetFileName(Foto_Cargar2.ContentType);
                string r2 = Path.Combine(Server.MapPath(urlArchivos), ficheroNombre2);
                Foto_Cargar2.SaveAs(r2);
            }

            //RECORRER LISTAS DESPACHO

            try
            {
                for (int i = 0; i < 61; i++)
                {
                    BUSES_DESPACHO ruta = new BUSES_DESPACHO();

                    id = id + 1;
                    ruta.BD_ID = id;
                    ruta.CD_ID = Conceptos[i];
                    ruta.BS_PLACA = placa;
                    ruta.BD_ESTADO = estado[i];
                    if (i >= 7)
                    {
                        a = a + 1;
                        ruta.BD_CALIDAD = calidad[a];
                    }
                    ruta.BD_OBSERVACION = observacion[i];
                    ruta.BD_DIRECCION = direccion;
                    ruta.BD_KM = km;
                    ruta.BD_CONCESIONARIO = Concesionario;
                    ruta.BD_FECHA = fecha_Busqueda;
                    ruta.USUREG = usuario;
                    ruta.ESTREG = 1;
                    ruta.BD_LATITUD = latitud;
                    ruta.BD_LONGUITUD = longitud;
                    ruta.BD_COD_DESPACHO = codigo_archivo;
                    ruta.URL_FOTO1 = urlArchivos + "/" + ficheroNombre1;
                    ruta.URL_FOTO2 = urlArchivos + "/" + ficheroNombre2;
                    ruta.URL_FOTO3 = urlArchivos + "/" + ficheroNombre3;
                    ruta.URL_FOTO4 = urlArchivos + "/" + ficheroNombre4;
                    ruta.COMENTARIO = comentario;
                    ruta.ESTADO_BUS = "PROGRAMABLE";
                    lista.Add(ruta);

                }
            }
            catch (Exception)
            {
                List<CC_CONDUCTORES> lista1 = _BusesLN.Listar_Placa();
                ViewBag.lista = new SelectList(lista1, "PLACA", "PLACA");

                DateTime fecha_Actual1 = DateTime.Now;
                string fecha_nombre1 = fecha_Actual1.ToString("dd/MM/yyyy");
                string hora_actual1 = fecha_Actual1.ToString("HH:mm:ss");

                ViewBag.fecha_actual = fecha_nombre1;
                ViewBag.Hora_actual = hora_actual1;
                ViewBag.Message = "Completar";
                return View("Fuera_Campo");
            }


            BUSES_DESPACHO ruta1 = new BUSES_DESPACHO();

            //INSERTAR DATA

            _BusesLN.Insertar_Buses_Despacho(lista);

            //LOGICA DE ESTADO NO PROGRAMABLE


             
            foreach (var item in lista)
            {
                if (mensaje == "NO PROGRAMABLE")
                {
                    _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO);

                }

                //Fuga de fluidos
                if (item.CD_ID == 7)
                {
                    if (item.BD_ESTADO == "1")
                    {
                        _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO);
                    }
                }
                //Parabrizas delantero
                if (item.CD_ID == 8)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Luces delanteras
                if (item.CD_ID == 10)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Luces direccionales (maranjas)

                if (item.CD_ID == 11)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Indicador de destino (1)
                if (item.CD_ID == 13)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Llanta delantera LD
                if (item.CD_ID == 16)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Ventanas de pasajeros LD
                if (item.CD_ID == 18)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Llantas posteriores LD
                if (item.CD_ID == 21)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Puerta posterior
                if (item.CD_ID == 24)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Luces de freno (rojas)
                if (item.CD_ID == 25)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Luces direccionales (naranjas)
                if (item.CD_ID == 26)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                        if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Luna
                if (item.CD_ID == 27)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }

                //Luces de retroceso (blancas)
                if (item.CD_ID == 28)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Alarma de retroceso
                if (item.CD_ID == 29)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Llantas posteriores LI
                if (item.CD_ID == 30)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Ventanas de pasajeros LI
                if (item.CD_ID == 32)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Puertas Intermedias LI
                if (item.CD_ID == 33)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Llanta delantera LI
                if (item.CD_ID == 35)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Indicadores de destino laterales (uno para 9m) (dos para 12 y 18 m)
                if (item.CD_ID == 38)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }

                //Claxon(de ciudad)
                if (item.CD_ID == 47)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }

                //Asientos
                if (item.CD_ID == 50)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Pasamanos de techo
                if (item.CD_ID == 52)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Pasamanos de puertas
                if (item.CD_ID == 53)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Claraboyas
                if (item.CD_ID == 55)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Timbre de puerta
                if (item.CD_ID == 57)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Extintor (6kg)
                if (item.CD_ID == 58)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Cono de seguridad
                if (item.CD_ID == 59)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Botiquin
                if (item.CD_ID == 60)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
                //Limpieza interior
                if (item.CD_ID == 61)
                {
                    if (item.BD_ESTADO == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                    if (item.BD_CALIDAD == "0") { _BusesLN.Cambiar_Estado_Bus(item.BS_PLACA, item.BD_COD_DESPACHO); }
                }
            }

            List<CC_CONDUCTORES> lista_PLACAS = _BusesLN.Listar_Placa();
            ViewBag.lista = new SelectList(lista_PLACAS, "PLACA", "PLACA");
            ConductoresModel modelo_c = new ConductoresModel();

            string d = fecha_Actual.ToString("dd/MM/yyyy");
            string hora_actual = fecha_Actual.ToString("HH:mm:ss");

            ViewBag.fecha_actual = d;
            ViewBag.Hora_actual = hora_actual;
            return View("Reportes_Despacho");

        }

        public ActionResult Reportes_Despacho()
        {
            List<CC_CONDUCTORES> lista = _BusesLN.Listar_Placa();
            ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");
            ViewBag.Message = "Vacio";
            return View();

        }

        [HttpPost]
        public ActionResult Descargar_Formato_Despacho(string placa, int codigo_despacho)
        {

            string usuario = "";
            if (Session["user_login"] == null)
            {
                return View("../Home/Sistemas");
            }
            else
            {
                usuario = Session["user_login"].ToString();
            }

            List<CC_CONDUCTORES> lista = _BusesLN.Listar_Placa();
            ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");

            ReportesBuses reportes = new ReportesBuses();

            BusesModel buse_model = new BusesModel();

            var reportes_despacho = _BusesLN.Listar_Informes_Despacho();
            buse_model.Lista_Buses_despacho = reportes_despacho;

            try
            {
                string ruta_base = Server.MapPath("~");
                if (ruta_base.Substring(ruta_base.Length - 1) != "\\")
                {
                    ruta_base = ruta_base + "\\";
                }
                var direccion = reportes.Insertar_Buses_Despacho_Formato(placa, ruta_base, codigo_despacho, usuario);
                buse_model.archivo = "Download/" + direccion;
                ViewBag.Message = "Abrir Archivo";


            }
            catch (Exception e)
            {
                string mensaje = e.Message;
                ViewBag.Message = "Error Archivo";
                ViewBag.error = mensaje;

                return View("Reportes_Despacho", buse_model);

            }



            return View("Reportes_Despacho", buse_model);
        }

        [HttpPost]
        public ActionResult FIltrar_Docuemntos(string PLACA, DateTime? fecha, string estado)
        {
            DateTimeOffset fecha2 = Convert.ToDateTime(fecha);



            string fecha_actual = fecha2.ToString("dd.MM.yyyy");

            List<CC_CONDUCTORES> lista = _BusesLN.Listar_Placa();
            ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");

            ReportesBuses reportes = new ReportesBuses();
            BusesModel buse_model = new BusesModel();

            var reportes_despacho = _BusesLN.FIltrar_Archivo_Despacho(PLACA, fecha_actual, estado);


            if (reportes_despacho.Count <= 0)
            {
                ViewBag.Message = "No Existen Registros";

            }

            buse_model.Lista_Buses_despacho = reportes_despacho;

            ViewBag.reportes = "Lista Reportes";



            return View("Reportes_Despacho", buse_model);
        }


        [HttpPost]
        public ActionResult Ubicacion_Despachador(string longitud, string latitud)
        {
            ViewBag.latitud = latitud;
            ViewBag.longitud = longitud;
            return View();
        }
        

    }
}