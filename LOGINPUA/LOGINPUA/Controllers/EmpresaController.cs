﻿using AutoMapper;
using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class EmpresaController : Controller
    {

        private readonly PerfilLN _PerfilLN;
        Util.Util utilidades = new Util.Util();

        private readonly EmpresaLN _EmpresaLN;
        public EmpresaController(EmpresaLN EmpresaLN)
        {
            _EmpresaLN = EmpresaLN;
        }
        [HttpGet]
        //public ActionResult Index()
        //{
        //    var listaVistas = Session["menu_modulo"] as DataTable;
        //    var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
        //    //Session["menu_modulo"] = dt4;
        //    var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
        //    bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
        //    var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent);

        //    ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
        //    ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
        //    if (auxValida)
        //    {
        //        ViewBag.Accionesview = Lista_acciones;
        //        return View();
        //    }
        //    else
        //    {
        //        return RedirectToAction("Sistemas_", "Home");
        //    }
        //}
         public ActionResult Crear()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Empresa/IndexValidador");
        }
        [HttpGet]
        public ActionResult Modificar()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Empresa/IndexValidador");
        }
        [HttpGet]
        public ActionResult Empresa()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Empresa/IndexValidador");
        }
        [HttpPost]
        public JsonResult ListaEmpresas() => Json(_EmpresaLN.Datos(1));
        [HttpPost]
        public JsonResult IdPorEmpresa(EmpresaModel model)
        {
            TB_EMPRESA TB_EMPRESA = new TB_EMPRESA();          
            TB_EMPRESA = _EmpresaLN.ObtenerPorCodigo(model.EMPCOD);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<TB_EMPRESA, EmpresaModel>());
            model = config.CreateMapper().Map<EmpresaModel>(TB_EMPRESA);
            return Json(model);
        }
        [HttpPost]
        public JsonResult MantenimientoEmpresa(EmpresaModel model)
        {
            string Mensaje = "Valido";
            int valor = 0;
            TB_EMPRESA TB_EMPRESA = new TB_EMPRESA();
            var config = new MapperConfiguration(cfg => cfg.CreateMap<EmpresaModel, TB_EMPRESA>());
            switch (model.Nivel)
            {
                case 1:
                    TB_EMPRESA.USUREG = Convert.ToString(Session["user_login"]);
                    TB_EMPRESA.FECREG = DateTime.Today;
                    TB_EMPRESA.ESTREG = 1;
                    TB_EMPRESA = config.CreateMapper().Map<TB_EMPRESA>(model);
                    valor = _EmpresaLN.Insertar(TB_EMPRESA);
                    break;
                case 2:
                    TB_EMPRESA.USUMOD = Convert.ToString(Session["user_login"]);
                    TB_EMPRESA.FECMOD = DateTime.Today;
                    TB_EMPRESA.ESTREG = 1;
                    TB_EMPRESA = config.CreateMapper().Map<TB_EMPRESA>(model);
                    valor = _EmpresaLN.Modificar(TB_EMPRESA);
                    break;
                case 3:
                    TB_EMPRESA.USUMOD = Convert.ToString(Session["user_login"]);
                    TB_EMPRESA.FECMOD = DateTime.Today;
                    TB_EMPRESA = config.CreateMapper().Map<TB_EMPRESA>(model);
                    valor = _EmpresaLN.Eliminar(TB_EMPRESA);
                    break;
            }
            if (valor == 0)
            {
                Mensaje = "Fallido";
            }
            return Json(Mensaje);
        }
        [HttpGet]
        public ActionResult IndexValidador()
        {
            string url = "/Home/Sistemas";
            var data = ValidadorString();
            if (data != null && data == "JSOTOMAYOR")
            {
                url = "Index";
            }
            return Redirect(url);
        }
        [HttpPost]
        public string ValidadorString()
        {
            try
            {
                return HttpContext.Session["nivel_login"].ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}