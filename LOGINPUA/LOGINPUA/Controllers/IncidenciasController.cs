﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using LOGINPUA.Repositorio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class IncidenciasController : Controller
    {

        private readonly PerfilLN _PerfilLN;
        Util.Util utilidades = new Util.Util();

        private readonly LoginLN _LoginLN;
        private readonly RutaLN _rutaLN;
        private readonly CorredoresLN _CorredoresLN;
        private readonly IncidenciasLN _IncidenciasLN;
        private readonly AccesoLN _AccesoLN;
        private readonly AccEmpLN _AccEmpLN;
        public IncidenciasController(LoginLN LoginLN, AccesoLN AccesoLN, AccEmpLN AccEmpLN, IncidenciasLN IncidenciasLN, RutaLN RutaLN,CorredoresLN CorredoresLN)
        {
            _LoginLN = LoginLN;
            _rutaLN = RutaLN;
            _AccesoLN = AccesoLN;
            _AccEmpLN = AccEmpLN;
            _IncidenciasLN = IncidenciasLN;
            _CorredoresLN = CorredoresLN;
        }
        ReadExcel readExcel = new ReadExcel();

        public ActionResult Incidencias()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (auxValida)
            {
                var datosCorredores = _CorredoresLN.obtenerListaCorredores();
                ViewBag.rutas = _rutaLN.getRutaCorredor();
                ViewBag.corredores = JsonConvert.SerializeObject(datosCorredores);

                ViewBag.Accionesview = Lista_acciones;
                return View(_CorredoresLN.obtenerListaCorredores());
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }

        public string Listar_Incidencias()
        {
            var lista = _IncidenciasLN.Listar_Incidencias();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string Registrar_Incidencia(CC_INCIDENCIA Model_Incidencia)
        {
            RPTA_GENERAL r = new RPTA_GENERAL();
            var result = "";

            string usuario = Session["user_login"].ToString();
            Model_Incidencia.ID_ESTADO = 1;
            Model_Incidencia.USU_REG = usuario;
            Model_Incidencia.ID_RUTA = 1;

            var data = _IncidenciasLN.Registrar_Incidencia(Model_Incidencia);
            result = JsonConvert.SerializeObject(data);


            return result;
        }

        public string AnularIncidencia(int idIncidencia)
        {
            var anularBus = JsonConvert.SerializeObject(_IncidenciasLN.AnularIncidencia(idIncidencia, Session["user_login"].ToString()));
            return anularBus;
        }




        public ActionResult Infracciones()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();

            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }


        }

        public string Listar_Infracciones()
        {
            var lista = _IncidenciasLN.Listar_Infracciones();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string Listar_Persona_Incidencia()
        {
            var lista = _IncidenciasLN.Listar_Persona_Incidencia();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        public string Registrar_Infraccion(CC_INFRACCION Model_Infraccion)
        {
            RPTA_GENERAL r = new RPTA_GENERAL();
            var result = "";

            string usuario = Session["user_login"].ToString();
            Model_Infraccion.ID_ESTADO = 1;
            Model_Infraccion.USU_REG = usuario;

            Model_Infraccion.MULTA_UIT = Model_Infraccion.MULTA_UIT + "%";
            Model_Infraccion.MONTO_MULTA = "S/. "+ Model_Infraccion.MONTO_MULTA;

            var data = _IncidenciasLN.Registrar_Infraccion(Model_Infraccion);
            result = JsonConvert.SerializeObject(data);

            
            return result;
        }

        public string Editar_Infraccion(CC_INFRACCION Model_Infraccion)
        {
            string usuario = Session["user_login"].ToString();

            Model_Infraccion.USU_MODIF = usuario;


            var data = _IncidenciasLN.Editar_Infraccion(Model_Infraccion);
            var result = JsonConvert.SerializeObject(data);
            return result;
        }


    }

}