﻿using AutoMapper;
using ENTIDADES;
using ENTIDADES.InnerJoin;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class AccesosController : Controller
    {
        private readonly LoginLN _LoginLN;
        private readonly AccesoLN _AccesoLN;
        private readonly AccEmpLN _AccEmpLN;
        public AccesosController(LoginLN LoginLN, AccesoLN AccesoLN, AccEmpLN AccEmpLN)
        {
            _LoginLN = LoginLN;
            _AccesoLN = AccesoLN;
            _AccEmpLN = AccEmpLN;
        }

        // GET: Bitacora
        public JsonResult ListaAccesos()
        {
            var mensaje = "Fallido";
            List<InnerJoinUsuario> ListInnerJoinUsuario = new List<InnerJoinUsuario>();
            if (Session["user_login"] != null && Session["password_login"] != null)
            {
                ListInnerJoinUsuario = _LoginLN.Accesos(Session["user_login"].ToString(), Session["password_login"].ToString(), ref mensaje);
                return Json(new { Accesos = ListInnerJoinUsuario });
            }
            return Json(new { mensaje = mensaje });
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Acceso/IndexValidador");
        }
        [HttpGet]
        public ActionResult Crear()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Acceso/IndexValidador");
        }

        [HttpGet]
        public ActionResult Modificar()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Acceso/IndexValidador");
        }

        [HttpGet]
        public ActionResult Accesos()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR")
            {
                return View();
            }
            return Redirect("/Acceso/IndexValidador");
        }

        [HttpPost]
        public JsonResult ListaAcceso() => Json(_AccesoLN.Datos(1));
        [HttpPost]
        public JsonResult IdPorAcceso(AccesoModel model)
        {
            TM_ACCESO TM_ACCESO = new TM_ACCESO();
            TV_ACC_EMP TV_ACC_EMP = new TV_ACC_EMP();
            TM_ACCESO = _AccesoLN.ObtenerPorCodigo(model.ACCCOD);
            TV_ACC_EMP = _AccEmpLN.ObtenerPorCodigo(model.ACCCOD);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<TM_ACCESO, AccesoModel>());
            model = config.CreateMapper().Map<AccesoModel>(TM_ACCESO);

            model.ACCCOD = TV_ACC_EMP.ACCCOD;
            model.EMPCOD = TV_ACC_EMP.EMPCOD;
            model.ACCEMPCOD = TV_ACC_EMP.ACCEMPCOD;

            return Json(model);
        }
        [HttpPost]
        public JsonResult MantenimientoAccesos(AccesoModel model)
        {
            string Mensaje = "Error en el Controllador";
            int Tipo = 0;
            string _FileName = "";
            string _path = "";
            TM_ACCESO TM_ACCESO = new TM_ACCESO();
            TV_ACC_EMP TV_ACC_EMP = new TV_ACC_EMP();
            if (model.Nivel != 3)
            {
                if (model.ACCICOFILE != null)
                {
                    if (model.ACCICOFILE.ContentLength > 0)
                    {
                        _FileName = Path.GetFileName(model.ACCICOFILE.FileName);
                        _path = Path.Combine(Server.MapPath("~/Util/Image"), _FileName);
                    }
                    else
                    {
                        Mensaje = "Error al subir el Archivo";
                        model.Nivel = 4;
                    }
                }
            }
            var config = new MapperConfiguration(cfg => cfg.CreateMap<AccesoModel, TM_ACCESO>());
            TM_ACCESO = config.CreateMapper().Map<TM_ACCESO>(model);
            config = new MapperConfiguration(cfg => cfg.CreateMap<AccesoModel, TV_ACC_EMP>());
            TV_ACC_EMP = config.CreateMapper().Map<TV_ACC_EMP>(model);
            switch (model.Nivel)
            {
                case 1:
                    TM_ACCESO.ACCICO = "Util/Image/" + model.ACCICO;
                    TM_ACCESO.USUREG = Convert.ToString(Session["user_login"]);
                    TM_ACCESO.FECREG = DateTime.Today;
                    TM_ACCESO.ESTREG = 1;
                    TV_ACC_EMP.USUREG = Convert.ToString(Session["user_login"]);
                    TV_ACC_EMP.FECREG = DateTime.Today;
                    TV_ACC_EMP.ESTREG = 1;
                    Mensaje = _AccesoLN.InsertarAcceso(TM_ACCESO, TV_ACC_EMP);
                    if (Mensaje == "Valido")
                    {
                        model.ACCICOFILE.SaveAs(_path);
                    }
                    break;
                case 2:
                    TM_ACCESO.ACCICO = "Util/Image/" + model.ACCICO;
                    TM_ACCESO.USUMOD = Convert.ToString(Session["user_login"]);
                    TM_ACCESO.FECMOD = DateTime.Today;
                    TV_ACC_EMP.USUMOD = Convert.ToString(Session["user_login"]);
                    TV_ACC_EMP.FECMOD = DateTime.Today;
                    Mensaje = _AccesoLN.ModificarAcceso(TM_ACCESO, TV_ACC_EMP);
                    if (Mensaje == "Valido")
                    {
                        model.ACCICOFILE.SaveAs(_path);
                    }
                    break;
                case 3:
                    TM_ACCESO.USUMOD = Convert.ToString(Session["user_login"]);
                    TM_ACCESO.FECMOD = DateTime.Today;
                    TM_ACCESO.ESTREG = 0;
                    TV_ACC_EMP.USUMOD = Convert.ToString(Session["user_login"]);
                    TV_ACC_EMP.FECMOD = DateTime.Today;
                    TV_ACC_EMP.ESTREG = 0;
                    Mensaje = _AccesoLN.EliminarAcceso(TM_ACCESO, TV_ACC_EMP);
                    break;
            }
            return Json(Mensaje);
        }
        [HttpGet]
        public ActionResult IndexValidador()
        {
            string url = "/Home/Sistemas";
            var data = ValidadorString();
            if (data != null && data == "JSOTOMAYOR")
            {
                url = "Index";
            }
            return Redirect(url);
        }
        [HttpPost]
        public string ValidadorString()
        {
            try
            {
                return HttpContext.Session["nivel_login"].ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}