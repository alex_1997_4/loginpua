﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpreadsheetLight;
using Newtonsoft.Json;
using System.Globalization;
using System.Transactions;
using System.IO.Compression;
using LN.Reportes;

namespace LOGINPUA.Controllers
{
    public class RutaController : Controller
    {
        private readonly RutasLN _rutasLN;
        private readonly CorredoresLN _corredorLN;
        private readonly ModalidadTransporteLN _modalidadTransporteLN;
        Util.Util utilidades = new Util.Util();
        // GET: Ruta
        public RutaController(RutasLN RutasLN, ModalidadTransporteLN modalidadTransporteLN, CorredoresLN CorredorLN)
        {
            _rutasLN = RutasLN;
            _corredorLN = CorredorLN;
            _modalidadTransporteLN = modalidadTransporteLN;
        }

        public ActionResult Inicio()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();

            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                var datosCorredores = _corredorLN.obtenerListaCorredores();
                ViewBag.modalidadUsuario = 2; //[1 : CORREDOR ] -- [2: COSAC] esto viene del usuario
                ViewBag.modalidadTransporte = _modalidadTransporteLN.getModalidadTransporte();
                ViewBag.corredores = JsonConvert.SerializeObject(datosCorredores);
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }

        public string listarRutasByIdCorredor(int idCorredor)
        {
            var dataRecorrido = JsonConvert.SerializeObject(_rutasLN.listarRutasByIdCorredor(idCorredor));
            return dataRecorrido;
        }

        public string registrarRuta(int idCorredor, string nombre, string nroRuta, string distritos)
        {
            var registraRuta = JsonConvert.SerializeObject(_rutasLN.registrarRuta(idCorredor, nombre, nroRuta, distritos, Session["user_login"].ToString()));
            return registraRuta;
        }

        public string registrarRecorrido(int idRuta, string sentido, string lado)
        {
            var registrarRecorrido = JsonConvert.SerializeObject(_rutasLN.registrarRecorrido(idRuta, sentido, lado, Session["user_login"].ToString()));
            return registrarRecorrido;
        }

        public string anularRuta(int idRuta)
        {
            var anulaRecorrido = JsonConvert.SerializeObject(_rutasLN.anularRuta(idRuta, Session["user_login"].ToString()));
            return anulaRecorrido;

        }

        public string editarRuta(int idRuta, string nroRuta, string nombre, string distritos)
        {
            var editarRuta = JsonConvert.SerializeObject(_rutasLN.editarRuta(idRuta, nroRuta, nombre, distritos, Session["user_login"].ToString()));
            return editarRuta;
        }

    }
}
