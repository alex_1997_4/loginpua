﻿using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class AdministracionController : Controller
    {
        private readonly UsuarioLN _UsuarioLN;

        public AdministracionController(UsuarioLN UsuarioLN)
        {
            _UsuarioLN = UsuarioLN;
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR" || ValidadorString() == "ILUNA" || ValidadorString() == "WCUBAS")
            {
                return View();
            }
            return Redirect("/Administracion/IndexValidador");
        }
        [HttpGet]
        public ActionResult IndexValidador()
        {
            string url = "/Home/Sistemas";
            var data = ValidadorString();
            if (data != null && data == "JSOTOMAYOR" || data == "ILUNA" || data == "WCUBAS")
            {
                url = "Index";
            }
            return Redirect(url);
        }
        [HttpPost]
        public string ValidadorString()
        {
            try
            {
                return HttpContext.Session["nivel_login"].ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        
        public ActionResult Listar_Usuarios_CC()
        {
          
            return View();

        }
        [HttpPost]
        public JsonResult ListaDeEmpresa(AdministradorModel model)
        {

            var lista = _UsuarioLN.Listar_Usuarios_CC(Convert.ToInt32(model.Empresa));

            foreach (var item in lista)
            {
                if (item.CODEMP == "1")
                {
                    ViewBag.CODEMP = "HACOM";
                }
                else
                {
                    ViewBag.CODEMP = "ABEXA";
                }
            }


            //return View("Listar_Usuarios_CC", lista);
            return Json(lista);
        }

        public ActionResult Export_Excel_Users()
        {
            ReadExcel read = new ReadExcel();

            var lista = _UsuarioLN.Datos(1);


            DataTable table = read.ConvertToDataTable(lista);
            string nombre_reporte = "Reporte Usuarios";
            read.Export_Excel(table, nombre_reporte);

            return View();
        }


        [HttpPost]

        public JsonResult ListaUsuarios_Hacom()
        {
            var lista = _UsuarioLN.Listar_Usuarios_Hacom();
            return Json(lista);
        }


    }
}