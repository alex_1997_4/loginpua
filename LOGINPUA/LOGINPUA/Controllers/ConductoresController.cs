﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class ConductoresController : Controller
    {
        private readonly LoginLN _LoginLN;

        private readonly AccesoLN _AccesoLN;
        private readonly AccEmpLN _AccEmpLN;
        private readonly ConductoresLN _ConductoresLN;
        //CC_CONDUCTORES model = new CC_CONDUCTORES();

        public ConductoresController(ConductoresLN ConductoresLN, LoginLN LoginLN, AccesoLN AccesoLN, AccEmpLN AccEmpLN, DistanciaLN DistanciaLN)
        {
            _LoginLN = LoginLN;
            _AccesoLN = AccesoLN;
            _AccEmpLN = AccEmpLN;
            _ConductoresLN = ConductoresLN;
        }


        public ActionResult Conductores()
        {
            return View();
        }

        public string ListarConductores()
        {
            var lista = _ConductoresLN.Listar_Conductores();
            var result = JsonConvert.SerializeObject(lista); //para la lista principal
            return result;
        }

        //[HttpPost]
        //public ActionResult Buscar_Conductores(CC_CONDUCTORES model)
        //{
        //    List<CC_CONDUCTORES> lista = _ConductoresLN.Listar_Placa();

        //    var datos = _ConductoresLN.Buscar_Conductores(model);
        //    if (datos.Count()!=0)
        //    {
        //        ViewBag.Message = "PLACA";

        //    }

        //    ConductoresModel modelo_c = new ConductoresModel();
        //    modelo_c.Lista_conductores = datos;
        //    ViewBag.lista = new SelectList(lista, "PLACA", "PLACA");


        //    return View(modelo_c);
    }


    }
 