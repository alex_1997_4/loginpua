﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpreadsheetLight;
using Newtonsoft.Json;
using System.Globalization;
using System.Transactions;
using System.IO.Compression;
using LN.Reportes;

namespace LOGINPUA.Controllers
{
    public class RutaTipoServicioController : Controller
    {
        // GET: RutaTipoServicio
        private readonly CorredoresLN _CorredoresLN;
        private readonly RutaLN _rutaLN;
        private readonly RegistroPicoPlacaLN _registroPicoPlacaLN;
        private readonly RecorridoLN _recorridoLN;
        private readonly ParaderoLN _paraderoLN;
        private readonly ViaLN _viaLN;
        private readonly TipoServicioLN _tipoServicioLN;
        private readonly ModalidadTransporteLN _modalidadTransporteLN;
        Util.Util utilidades = new Util.Util();


        public RutaTipoServicioController(CorredoresLN CorredorLN, RutaLN RutaLN, RegistroPicoPlacaLN registroVelocidadPPlaca, RecorridoLN recorridoLN, ParaderoLN paraderoLN, ViaLN viaLN, TipoServicioLN tipoServicio, ModalidadTransporteLN modalidadTransporteLN)
        {
            _CorredoresLN = CorredorLN;
            _rutaLN = RutaLN;
            _registroPicoPlacaLN = registroVelocidadPPlaca;
            _recorridoLN = recorridoLN;
            _paraderoLN = paraderoLN;
            _viaLN = viaLN;
            _tipoServicioLN = tipoServicio;
            _modalidadTransporteLN = modalidadTransporteLN;
        }

        public ActionResult Inicio()
        {

            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();

            if (auxValida)
            {
                var datosCorredores = _CorredoresLN.obtenerListaCorredores();
                ViewBag.rutas = _rutaLN.getRutaCorredor();
                ViewBag.modalidadTransporte = _modalidadTransporteLN.getModalidadTransporte();
                ViewBag.corredores = JsonConvert.SerializeObject(datosCorredores);
                ViewBag.Accionesview = Lista_acciones;


                ViewBag.modalidadUsuario = 2; //[1 : CORREDOR ] -- [2: COSAC] esto viene del usuario
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }

        public string getTiposServicioByIdRuta(int idRuta)
        {
            var datos = _tipoServicioLN.getRutaTipoDeServicioOper(idRuta);
            return JsonConvert.SerializeObject(datos);
        }

        public string getTiposServicio()
        {
            var datos = _tipoServicioLN.getTiposServicio();
            return JsonConvert.SerializeObject(datos);
        }

        public string getTiposServicioOperacional()
        {
            var datos = _tipoServicioLN.getTiposServicioOperacional();
            return JsonConvert.SerializeObject(datos);
        }

        public string registrarRutaTipoServicio(int idRuta, int idTipoServicio, int idTipoServicioOper, string nombre)
        {
            var datosRegistro = _tipoServicioLN.registrarRutaTipoServicio(idRuta, idTipoServicio, idTipoServicioOper, nombre, Session["user_login"].ToString());
            return JsonConvert.SerializeObject(datosRegistro);
        }

        public string editarRutaTservic(int idRutaTipoServ, int idTipoServicio, int idTipoServicioOper, string nombre)
        {
            var editarRuta = JsonConvert.SerializeObject(_tipoServicioLN.editarRutaTservic(idRutaTipoServ, idTipoServicio, idTipoServicioOper, nombre, Session["user_login"].ToString()));
            return editarRuta;
        }


        public string anularRutaTipoServicio(int idRutaServicioOperacional)
        {
            var datosAnula = _tipoServicioLN.anularRutaTipoServicio(idRutaServicioOperacional, Session["user_login"].ToString());
            return JsonConvert.SerializeObject(datosAnula);
        }
        public string registraRecorridoServicioOperacional(int idRutaTipoServicio, string lado, string sentido)
        {
            var datosRegistra = _tipoServicioLN.registraRecorridoServicioOperacional(idRutaTipoServicio, lado, sentido, Session["user_login"].ToString());
            return JsonConvert.SerializeObject(datosRegistra);
        }

        public string getParaderoByIdRecorridTServ(int idRecorridoTipoServicio)
        {
            var datosParaderos = _tipoServicioLN.getParaderoByIdRecorridTServ(idRecorridoTipoServicio);
            return JsonConvert.SerializeObject(datosParaderos);
        }

        public string actualizarParaderoTservic(int idParaderoTipoServicio, int idEstado)
        {
            var actualizaParadero = _tipoServicioLN.actualizarParaderoTservic(idParaderoTipoServicio, idEstado, Session["user_login"].ToString());
            return JsonConvert.SerializeObject(actualizaParadero);
        }
    }
}