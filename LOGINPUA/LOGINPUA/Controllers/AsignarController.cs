﻿using AutoMapper;
using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class AsignarController : Controller
    {

        private readonly PerfilLN _PerfilLN;
        Util.Util utilidades = new Util.Util();


        private readonly UsuariosEmpresaLN _UsuariosEmpresaLN;
        private readonly LoginLN _LoginLN;
        public AsignarController(UsuariosEmpresaLN UsuariosEmpresaLN, LoginLN LoginLN)
        {
            _LoginLN = LoginLN;
            _UsuariosEmpresaLN = UsuariosEmpresaLN;
        }
        [HttpPost]
        public JsonResult ListaDeUsuariosPorAsignacion(AsignarModel model) => Json(_UsuariosEmpresaLN.ListaUsuariosPorAsignacion(1, model.LOGCOD));
        [HttpPost]
        public JsonResult ListaAsignar() => Json(_UsuariosEmpresaLN.Datos(1));
        [HttpPost]
        public JsonResult IdPorAsignar(AsignarModel model)
        {
            TM_USUARIOS_EMPRESA TM_USUARIOS_EMPRESA = new TM_USUARIOS_EMPRESA();
            TM_USUARIOS_EMPRESA = _UsuariosEmpresaLN.ObtenerPorCodigo(model.LOGCOD);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<TM_USUARIOS_EMPRESA, AsignarModel>());
            model = config.CreateMapper().Map<AsignarModel>(TM_USUARIOS_EMPRESA);
            return Json(model);
        }
        [HttpPost]
        public JsonResult MantenimientoAsignacion(List<AsignarModel> model) {
            string mensajeAviso = "";
            List<TM_USUARIOS_EMPRESA> TM_USUARIOS_EMPRESA = new List<TM_USUARIOS_EMPRESA>();
            model[0].USUMOD = Convert.ToString(Session["user_login"]);
            model[0].USUREG = Convert.ToString(Session["user_login"]);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<List<TM_USUARIOS_EMPRESA>, List<AsignarModel>>());
            TM_USUARIOS_EMPRESA = config.CreateMapper().Map<List<TM_USUARIOS_EMPRESA>>(model);
            if (_UsuariosEmpresaLN.Validacion(TM_USUARIOS_EMPRESA,ref mensajeAviso))
            {
                return Json(new { tipo = 1 ,msj = mensajeAviso });
            }

            _UsuariosEmpresaLN.AsignarUsuario(TM_USUARIOS_EMPRESA);
            return Json(model);
        }
         
        [HttpPost]
        public string ValidadorString()
        {
            try
            {
                return HttpContext.Session["nivel_login"].ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}