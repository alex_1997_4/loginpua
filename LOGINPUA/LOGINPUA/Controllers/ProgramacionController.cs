﻿using ENTIDADES;
using LN.EntidadesLN;
using LOGINPUA.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SpreadsheetLight;
using Newtonsoft.Json;
using System.Globalization;
using System.Transactions;
using System.IO.Compression;
using Entidades;
using LOGINPUA.Util;

namespace CORREDORES_COMPLEMENTARIOS.Controllers
{
    public class ProgramacionController : Controller
    {

        ImagenLN ImagenLN = new ImagenLN();
        ReadExcel readExcel = new ReadExcel();

        private readonly ProgramacionLN _ProgramacionLN = new ProgramacionLN();
        private readonly LoginLN _LoginLN;
        private readonly BUSESLN _BusesLN;
        private readonly ConductoresLN _ConductoresLN;

        private readonly DistanciaLN _DistanciaLN;
        private readonly AccesoLN _AccesoLN;
        private readonly AccEmpLN _AccEmpLN;
        private readonly CorredoresLN _CorredoresLN;
        private readonly RutaLN _rutaLN;
        private readonly TipoDiaLN _tipoDiaLN;
        private readonly SalidaProgramadaLN _salidaProgramadaLN;
        Util utilidades = new Util();

        public ProgramacionController(ConductoresLN ConductoresLN,BUSESLN BusesLN, LoginLN LoginLN, AccesoLN AccesoLN, AccEmpLN AccEmpLN, DistanciaLN DistanciaLN, ProgramacionLN ProgramacionLN,
                                        CorredoresLN CorredorLN, RutaLN RutaLN, TipoDiaLN tipoDiaLN, SalidaProgramadaLN salidaProgramadaLN)
        {
            _LoginLN = LoginLN;
            _AccesoLN = AccesoLN;
            _AccEmpLN = AccEmpLN;
            _DistanciaLN = DistanciaLN;
            _ProgramacionLN = ProgramacionLN;
            _CorredoresLN = CorredorLN;
            _rutaLN = RutaLN;
            _tipoDiaLN = tipoDiaLN;
            _salidaProgramadaLN = salidaProgramadaLN;
            _BusesLN = BusesLN;
            _ConductoresLN = ConductoresLN;


        }
        //RUTAS
        public ActionResult Subir_Rutas()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }


        //PROGRAMACION MOBIL 
        public ActionResult ProgracionDetallado()
        {
            var List_rutas = _rutaLN.getRuta_X_modalidad(1);
            ViewBag.Rutas = List_rutas;
            return View();
        }

        public string getRutaSerOpe_X_modalidad(int id_ruta)
        {
            var List_rutas = _rutaLN.getRutaSerOpe_X_modalidad(id_ruta);
            var resultado = JsonConvert.SerializeObject(List_rutas);
            return resultado;
        }

        public string getViajes_ProgrUnidades(int id_ruta, string sentido)
        {
            var fecha = "28/01/20";
            var List_rutas = _ProgramacionLN.getViajes_ProgrUnidades(fecha, id_ruta, sentido);
            var resultado = JsonConvert.SerializeObject(List_rutas);
            return resultado;
        }

        public string getviajesProgrServ(int id_ruta, string sentido, int idservicio)
        {
            var fecha = "28/01/20";
            var List_rutas = _ProgramacionLN.getviajesProgrServ(fecha, id_ruta, sentido, idservicio);
            var resultado = JsonConvert.SerializeObject(List_rutas);
            return resultado;
        }

        public string Actulizar_ViajeTiempoReal(int idsalida, string unidad, int cac_temporal, string hora_salida)
        {
            string usureg_real = Session["user_login"].ToString(); 
            var List_rutas = _ProgramacionLN.Actulizar_ViajeTiempoReal(idsalida, unidad, cac_temporal, hora_salida, usureg_real);
            var resultado = JsonConvert.SerializeObject(List_rutas); 
            return resultado;
        }

        public string getLado_X_Ruta(int id_ruta)
        {
            var List_rutas = _rutaLN.getLado_X_Ruta(id_ruta);
            var resultado = JsonConvert.SerializeObject(List_rutas);
            return resultado;
        }

        public string getPlaca_Buses()
        {
            var ListPlacas = _BusesLN.Listar_Buses();
            var resultado = JsonConvert.SerializeObject(ListPlacas);
            return resultado;
        }

        public string getConductores()
        {
            var ListConductores= _ConductoresLN.Listar_Conductores();
            var resultado = JsonConvert.SerializeObject(ListConductores);
            return resultado;
        }
        // FINALIZAR MOBIL


        public ActionResult SubirProgramacion()
        {

            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();
            if (auxValida)
            {
                var datosCorredores = _CorredoresLN.obtenerListaCorredores();
                ViewBag.corredores = JsonConvert.SerializeObject(datosCorredores);

                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
        }

        public JsonResult cargarArchivo(string fecha, int TipoDia, int reemplazar, int idServicio, int ruta_ajax)
        {
            var ob = Request.Files;
            var archivoSubido = Request.Files[0];
            var nombreArchivo = Path.GetFileName(archivoSubido.FileName);
            var extensionArchivo = Path.GetExtension(archivoSubido.FileName);
            var arrFileName = nombreArchivo.Split('.');
            var nuevoNombreArchivoExcel = arrFileName[0] + "_POD_" + DateTime.Now.ToString("dd_MM_yyyy_h_mm_ss") + extensionArchivo;
            var pathArchivo = Server.MapPath("~/Adjuntos/Programacion/" + nuevoNombreArchivoExcel);
            string pathFinal = Path.Combine(pathArchivo);
            archivoSubido.SaveAs(pathFinal);
            //return verificarFormatoExcel(Server.MapPath("~/Download/programacion_demo/" + "POD TGA 301 HABIL 50 BUSES.xlsx"), Request);
            return verificarFormatoExcel(pathFinal, Request, fecha, TipoDia, reemplazar, idServicio, ruta_ajax);
        }
        public ActionResult exportarPDF(string fechasA, string fechasB, string promedioVelocidadesA_M, string promedioVelocidadesB_M, string promedioVelocidadesA_T, string promedioVelocidadesB_T, string sumaTiemposViajeA_M, string sumaTiemposViajeB_M, string sumaTiemposViajeA_T, string sumaTiemposViajeB_T, string CORREDOR)
        {

            var direccion = "";
            CC_REPORTE_PICO_PLACA model = new CC_REPORTE_PICO_PLACA();

            //model.VEL_PROMEDIO_A_MAÑANA_IDA = VEL_PROMEDIO_A_MANANA_IDA;


            model.VEL_PROMEDIO_A_MAÑANA_IDA = promedioVelocidadesA_M;
            model.TIEMPO_PROM_A_1 = sumaTiemposViajeA_M;

            model.VEL_PROMEDIO_B_MAÑANA_IDA = promedioVelocidadesB_M;
            model.TIEMPO_PROM_B_2 = sumaTiemposViajeB_M;

            model.VEL_PROMEDIO_A_TARDE_IDA = promedioVelocidadesA_T;
            model.TIEMPO_PROM_A_3 = sumaTiemposViajeA_T;

            model.VEL_PROMEDIO_B_TARDE_IDA = promedioVelocidadesB_T;
            model.TIEMPO_PROM_B_4 = sumaTiemposViajeB_T;


            model.FECHA_INICIO = fechasA;
            model.FECHA_FIN = fechasB;
            model.CORREDOR = CORREDOR;


            string ruta_base = Server.MapPath("~");
            if (ruta_base.Substring(ruta_base.Length - 1) != "\\")
            {
                ruta_base = ruta_base + "\\";
            }

            Reportes_Comparativo model_rp = new Reportes_Comparativo();

            try
            {
                direccion = model_rp.Insertar_rp_comparativo_Salida(model, ruta_base);

            }
            catch (Exception e)
            {
                string mensaje = e.Message;
                var rutas_ = _rutaLN.getRutaCorredor();
                ViewBag.Error = "Abrir Archivo";
                return PartialView("_ValidacionPdf");
            }



            ViewBag.Message = "Abrir Archivo";
            ViewBag.archivo = "~/Download/" + direccion;

            var rutas = _rutaLN.getRutaCorredor();
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(ViewBag.archivo));
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "Reporte_PDF_COMPARATIVO_.pdf");

        }
        JsonResult verificarFormatoExcel(string rutaArchivo, HttpRequestBase rq, string fecha, int tipodia, int reemplazar, int idServicio, int ruta_ajax)
        {
            int ID_MAESTRO_SALIDA_PROG = 0;
            var count = 0;
            //
            RPTA_GENERAL e = new RPTA_GENERAL();
            using (FileStream fs = new FileStream(rutaArchivo, FileMode.Open))
            {
                SLDocument xlDoc = new SLDocument(fs);
                var hojasExcel = xlDoc.GetSheetNames();
                var encontroCabecera = false;
                var nombreHoja = "";


                var columnaInicioDetalle = 0;
                foreach (var hoja in hojasExcel)
                {
                    SLDocument sheet = new SLDocument(fs, hoja);
                    SLWorksheetStatistics stadisticasHoja = xlDoc.GetWorksheetStatistics();

                    for (int row = 0; row < stadisticasHoja.EndRowIndex; row++) //verificando cabecera
                    {
                        var textoColumnaOperador = sheet.GetCellValueAsString(row, 4); //fila, columna
                        var textoColumnaTipodia = sheet.GetCellValueAsString(row, 2); //fila, columna

                        if (textoColumnaTipodia == "TIPO DIA" || textoColumnaOperador == "OPERADOR")
                        {
                            encontroCabecera = true;
                            nombreHoja = hoja.ToString();
                            columnaInicioDetalle = row + 1;
                        }
                    }
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    try
                    {

                        if (nombreHoja != "")
                        {
                            SLDocument hojaProgramacion_ = new SLDocument(fs, nombreHoja);
                            SLWorksheetStatistics hojaProgramacion__ = xlDoc.GetWorksheetStatistics();

                            for (int row_ = columnaInicioDetalle; row_ <= hojaProgramacion__.EndRowIndex; row_++)
                            {
                                int ruta = hojaProgramacion_.GetCellValueAsInt32(row_, 3);
                                //VALIDAR LA RUTA
                                if (ruta != ruta_ajax)
                                {
                                    e.COD_ESTADO = 0;
                                    e.DES_ESTADO = "Ingresar Correctamente la ruta";
                                    return Json(e);
                                }
                            }
                        }



                        int igual = 0;
                        //VERIFICA_DATA_MAESTRO_PROG
                        if (reemplazar == 0)
                        { //aceptó reemplazar la data
                            List<RPTA_GENERAL> Listafecha = new List<RPTA_GENERAL>();

                            string[] array_fecha = new string[10];
                            string fechaexistenteBD = "";

                            var fecha_actual = fecha.Replace(" ", "");
                            array_fecha = fecha_actual.Split('-');
                            Session["id_maestrosalida"] = 0;
                            foreach (var item in array_fecha)
                            {
                                var rpta = _ProgramacionLN.ValidarFecha_Programados(item, idServicio);
                                //IDMAESTRO
                                Session["id_maestrosalida"] = rpta.AUX;
                                if (rpta.DES_ESTADO != "NO EXISTE")
                                {
                                    fechaexistenteBD = rpta.DES_ESTADO;
                                    if (fechaexistenteBD == fecha)
                                    {
                                        igual = 1;
                                    }
                                    break;
                                }
                            }

                            if (fechaexistenteBD != "" && igual == 0)
                            {
                                string[] array_fechaBD = new string[10];
                                var fecha_BD = fechaexistenteBD.Replace(" ", "");
                                array_fechaBD = fecha_BD.Split('-');


                                foreach (var item_array_fecha in array_fecha)
                                {
                                    foreach (var item_array_fechaBD in array_fechaBD)
                                    {
                                        RPTA_GENERAL RPT = new RPTA_GENERAL();

                                        if (item_array_fechaBD != item_array_fecha)
                                        {
                                            if (item_array_fechaBD != null)
                                            {
                                                RPT.DES_ESTADO = item_array_fechaBD;
                                            }
                                        }
                                        Listafecha.Add(RPT);
                                    }
                                }
                            }

                            string fechas = "";
                            int cont = 0;

                            var List_eliminado_duplicados = (from item in Listafecha
                                                             group item.DES_ESTADO by item.DES_ESTADO into g
                                                             where g.Count() == 1
                                                             select g.Key).ToArray();

                            List<string> lista = List_eliminado_duplicados.ToList();

                            foreach (var item_Fechas in List_eliminado_duplicados)
                            {
                                if (item_Fechas != null)
                                {
                                    fechas += item_Fechas + " - ";
                                }
                            }

                            if ((int)Session["id_maestrosalida"] != 0)
                            {

                                fechas = fechas == "" ? fechaexistenteBD : fechas;

                                RPTA_GENERAL e_rpt = new RPTA_GENERAL();
                                e_rpt.DES_ESTADO = fechas;
                                e_rpt.COD_ESTADO = 3;
                                return Json(e_rpt);
                            }
                        }

                        //VALIDAR LA RUTA


                        if (reemplazar == 1)
                        {
                            //e = _salidaProgramadaLN.AnularMaestroSalida(e.AUX);

                            int id_maestro = (int)Session["id_maestrosalida"];
                            if (id_maestro != 0)
                            {
                                e.COD_ESTADO = 1;
                            }

                            _salidaProgramadaLN.anular_maestro_prog(id_maestro);
                            if (e.COD_ESTADO == 0)
                            { //valida si ocurrio un error al momento de anular los registros
                                return Json(e);
                            }
                        }


                        var rptaInseraCabecera = _salidaProgramadaLN.registrarMaestroSalidaProgramada(tipodia, idServicio, fecha, Session["user_login"].ToString());
                        var id_maestro_salidaProgramada = 0;
                        if (rptaInseraCabecera.COD_ESTADO == 1)
                        {
                            id_maestro_salidaProgramada = rptaInseraCabecera.AUX;
                        }


                        //
                        if (nombreHoja != "")
                        { //encontro el excel con el formato
                            SLDocument hojaProgramacion = new SLDocument(fs, nombreHoja);
                            SLWorksheetStatistics hojaProgramacionStadistics = xlDoc.GetWorksheetStatistics();

                            var fechaTemporal = DateTime.Now.ToString("dd/MM/yyyy");
                            var fechaHoraTemporal = DateTime.Now.ToString("dd/MM/yyyy 12:00:00 tt");

                            for (int row = columnaInicioDetalle; row <= hojaProgramacionStadistics.EndRowIndex; row++)
                            {
                                var tipoDia = hojaProgramacion.GetCellValueAsString(row, 2).ToString();


                                var nroServicio = hojaProgramacion.GetCellValueAsString(row, 6).ToString();
                                var pog = hojaProgramacion.GetCellValueAsString(row, 7).ToString();
                                var pot = hojaProgramacion.GetCellValueAsString(row, 8).ToString() == "" ? "" : hojaProgramacion.GetCellValueAsDateTime(row, 8).ToString();
                                String[] arr_hora_pot;
                                if (pot != "") //para la hora de salida programada
                                {
                                    arr_hora_pot = pot.Split(' ');
                                    pot = arr_hora_pot[1];
                                }

                                var fnode = hojaProgramacion.GetCellValueAsString(row, 9).ToString();
                                var hsalida = hojaProgramacion.GetCellValueAsString(row, 10).ToString() == "" ? "" : hojaProgramacion.GetCellValueAsDateTime(row, 10).ToString();
                                var hllegada = hojaProgramacion.GetCellValueAsDateTime(row, 11).ToString();
                                String[] arr_hora_llegada_prog;

                                if (hsalida != "") //para la hora de salida programada
                                {
                                    hsalida = DateTime.Parse(hsalida).ToString("HH:mm:ss");
                                }

                                if (hllegada != "")//para la hora de llegada programada
                                {
                                    hllegada = DateTime.Parse(hllegada).ToString("HH:mm:ss");
                                }

                                var tnode = hojaProgramacion.GetCellValueAsString(row, 12).ToString();
                                var PIG = hojaProgramacion.GetCellValueAsString(row, 13).ToString();
                                /**************************************************************************************************/
                                var layover = hojaProgramacion.GetCellValueAsString(row, 15).ToString() == "" ? "" : hojaProgramacion.GetCellValueAsDateTime(row, 15).ToString();
                                var layoverFormatoFecha = "";
                                String[] arr_layover;
                                double minutosDiferenciaLayover = 0.0;
                                if (layover != "")//para la hora de llegada programada
                                {
                                    arr_layover = layover.Split(' ');
                                    layover = arr_layover[1] + " " + arr_layover[2];//obteniendo la hora y AM/PM
                                    layoverFormatoFecha = fechaTemporal + " " + layover;//concatenando la fecha temporal + layover hora
                                    minutosDiferenciaLayover = DateTime.Parse(fechaTemporal).Subtract(DateTime.Parse(layoverFormatoFecha)).TotalMinutes;//obtiene diferencia en minutos
                                    minutosDiferenciaLayover = Math.Abs(minutosDiferenciaLayover);
                                }

                                /*************************************************************************************************/
                                var acumulado = "0";
                                var sentido = hojaProgramacion.GetCellValueAsString(row, 19).ToString();
                                var turno = hojaProgramacion.GetCellValueAsString(row, 21).ToString();
                                var tipoServicio = hojaProgramacion.GetCellValueAsString(row, 22).ToString();
                                var placa = hojaProgramacion.GetCellValueAsString(row, 23).ToString();
                                var cacConductor = hojaProgramacion.GetCellValueAsString(row, 24).ToString();

                                var registraDetalle = _salidaProgramadaLN.guardarMSalidaProgramadaDet(id_maestro_salidaProgramada, tipoDia, nroServicio,
                                                                                                        pog, pot, fnode, hsalida, hllegada, tnode, PIG, minutosDiferenciaLayover, acumulado,
                                                                                                        sentido, turno, tipoServicio, placa, cacConductor, Session["user_login"].ToString());
                                count++;
                            }
                            scope.Complete();
                        }
                        else
                        { //no encontró el excel con el formato correcto

                        }
                    }
                    catch (TransactionAbortedException ex)
                    {
                        return Json(new { codResultado = 1, desResultado = "No se registraron los datos, ERROR->" + ex.Message });
                    }
                }
            }
            return Json(new { codResultado = 1, desResultado = "La programación se cargó correctamente y la Cantidad de Viajes es : " + count });
        }
        public string getRutaxCorredor(int idCorredor)
        {
            var result = JsonConvert.SerializeObject(_rutaLN.obtenerRutasPorCorredor(idCorredor));
            return result;
        }
        public string getTipoDias()
        {
            var result = JsonConvert.SerializeObject(_tipoDiaLN.obtenertipoDias());
            return result;
        }

        public string getResumenProgramado(int idServicio)
        {
            var rpta = JsonConvert.SerializeObject(_salidaProgramadaLN.getResumenProgramado(idServicio));
            return rpta;
        }

        public string getDataViajesProgramacion(int idMaestroSalidaProg)
        {
            var rpta = JsonConvert.SerializeObject(_salidaProgramadaLN.getDataViajesProgramacion(idMaestroSalidaProg));
            return rpta;
        }

        public RPTA_GENERAL verificarRegistroMaestro(string fechaConsulta, int idServicio)
        {
            var data = new RPTA_GENERAL();
            RPTA_GENERAL r = new RPTA_GENERAL();
            string fechausada = "";

            string fechaFinal = fechaConsulta.Replace(" ", "");

            string[] array = new string[4];

            array = fechaFinal.Split('-');

            foreach (var item in array)
            {
                data = _salidaProgramadaLN.verifica_maestro_prog(idServicio, item);
                if (data.COD_ESTADO == 1)
                {
                    fechausada = item;
                    break;
                }
            }

            if (data.AUX > 0)
            {
                r.COD_ESTADO = 1;
                r.DES_ESTADO = "Ya existe información para la fecha " + fechausada + ", ¿Desea reemplazarlo?";
                r.AUX = data.AUX;

            }
            else
            {
                r.COD_ESTADO = 0;
                r.DES_ESTADO = "No hay info en esta fecha";
            }
            return r;
        }

        public string getTipoServicioByCorredor(int idCorredor)
        {
            var rpta = JsonConvert.SerializeObject(_salidaProgramadaLN.getTipoServicioByCorredor(idCorredor));
            return rpta;
        }

    }
}