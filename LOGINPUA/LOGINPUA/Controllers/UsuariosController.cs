﻿using LN.EntidadesLN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ENTIDADES;
using LOGINPUA.Models;
using AutoMapper;
using LOGINPUA.Util.Seguridad;
using ENTIDADES.InnerJoin;

namespace LOGINPUA.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly LoginLN _LoginLN;
        private readonly UsuarioLN _UsuarioLN;
        private readonly EmpresaLN _EmpresaLN;
        private readonly UsuariosEmpresaLN _UsuariosEmpresaLN;
        private readonly LogAccLN _LogAccLN;
        private readonly AccEmpLN _AccEmpLN;
        private readonly AccesoLN _AccesoLN;
        private readonly LogLN _LogLN;
        public UsuariosController(LoginLN LoginLN, UsuarioLN UsuarioLN, 
            EmpresaLN EmpresaLN, AccesoLN AccesoLN, 
            LogLN LogLN, UsuariosEmpresaLN UsuariosEmpresaLN,
            LogAccLN LogAccLN, AccEmpLN AccEmpLN)
        {
            _LoginLN = LoginLN;
            _UsuarioLN = UsuarioLN;
            _EmpresaLN = EmpresaLN;
            _UsuariosEmpresaLN = UsuariosEmpresaLN;
            _AccesoLN = AccesoLN;
            _LogLN = LogLN;
            _LogAccLN = LogAccLN;
            _AccEmpLN = AccEmpLN;
        }
        [HttpGet]
        public ActionResult Index()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR" || ValidadorString() == "ILUNA" || ValidadorString() == "WCUBAS")
            {
                return View();
            }
            return Redirect("/Usuario/IndexValidador");
        }

        public ActionResult Login(string m, string u)
        {
            ViewBag.mensaje = m;
            ViewBag.usuario = u;
            return View();
        }
        [HttpGet]
        public ActionResult Crear()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR" || ValidadorString() == "ILUNA")
            {
                return View();
            }
            return Redirect("/Usuario/IndexValidador");
        }
        [HttpGet]
        public ActionResult Modificar()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR" || ValidadorString() == "ILUNA")
            {
                return View();
            }
            return Redirect("/Usuario/IndexValidador");
        }
        [HttpGet]
        public ActionResult Usuarios()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR" || ValidadorString() == "ILUNA")
            {
                return View();
            }
            return Redirect("/Usuario/IndexValidador");
        }
        [HttpGet]
        public ActionResult Asignar()
        {
            if (ValidadorString() != null && ValidadorString() == "JSOTOMAYOR" || ValidadorString() == "ILUNA")
            {
                return View();
            }
            return Redirect("/Usuario/IndexValidador");
        }
        [HttpPost]
        public JsonResult ListaUsuarios() => Json(_UsuarioLN.Datos(1));
        [HttpPost]
        public JsonResult ListaEmpresas() => Json(_EmpresaLN.Datos(1));
        [HttpPost]
        public JsonResult ListaDeAccesos(EmpresaModel model) => Json(_AccesoLN.DatosDe(model.EMPCOD));
        [HttpPost]
        public JsonResult IdPorUsuario(UsuarioModel model)
        {
            TB_USUARIO TB_USUARIO = new TB_USUARIO();
            TB_LOGIN TB_LOGIN = new TB_LOGIN();
            List<InnerJoinAccesos> InnerJoinAccesos = new List<InnerJoinAccesos>();
            
            TB_USUARIO = _UsuarioLN.ObtenerPorCodigo(model.USUCOD);
            TB_LOGIN = _LoginLN.ObtenerPorCodigo(model.USUCOD);
            InnerJoinAccesos = _LogAccLN.ObtenerListadoDeAccesosPorLogCod(model.USUCOD);
            List<UsuarioModel> modelLista = new List<UsuarioModel>();
            for (int i = 0; i < InnerJoinAccesos.Count; i++)
            {
                modelLista.Add(new UsuarioModel {
                    USUCOD = TB_USUARIO.USUCOD,
                    USUNOM = TB_USUARIO.USUNOM,
                    USUAPEPAT = TB_USUARIO.USUAPEPAT,
                    USUAPEMAT = TB_USUARIO.USUAPEMAT,
                    LOGCOD = TB_LOGIN.LOGCOD,
                    LOGUSU = TB_LOGIN.LOGUSU,
                    LOGCON = Encriptador.Desencriptar(TB_LOGIN.LOGCON),
                    NDOCUMENTO = TB_USUARIO.NDOCUMENTO,
                    PERFIL = TB_USUARIO.PERFIL,
                    EMPCOD = InnerJoinAccesos[i].EMPCOD,
                    ACCCOD = InnerJoinAccesos[i].ACCCOD,
                    LOGACCCOD = InnerJoinAccesos[i].LOGACCCOD,
                    ESTREG = InnerJoinAccesos[i].ESTREG,
                    USUREG = TB_USUARIO.USUREG,
                    FECREG = TB_USUARIO.FECREG
                });
            }
            return Json(modelLista);
        }
        [HttpPost]
        public JsonResult MantenimientoUsuarios(List<UsuarioModel> model)
        {
            string Mensaje = "Error en el Controllador";
            TB_USUARIO TB_USUARIO = new TB_USUARIO();
            TB_LOGIN TB_LOGIN = new TB_LOGIN();
            List<TV_LOG_ACC> TV_LOG_ACC = new List<TV_LOG_ACC>();
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UsuarioModel, TB_USUARIO>());
            TB_USUARIO = config.CreateMapper().Map<TB_USUARIO>(model[0]);
            config = new MapperConfiguration(cfg => cfg.CreateMap<UsuarioModel, TB_LOGIN>());
            TB_LOGIN = config.CreateMapper().Map<TB_LOGIN>(model[0]);
            TB_LOGIN.LOGCON = Encriptador.Encriptar(TB_LOGIN.LOGCON);
            config = new MapperConfiguration(cfg => cfg.CreateMap<List<TV_LOG_ACC>, List<UsuarioModel>>());
            TV_LOG_ACC = config.CreateMapper().Map<List<TV_LOG_ACC>>(model);


            switch (model[0].Nivel)
            {
                case 1:
                    TB_USUARIO.USUREG = Convert.ToString(Session["user_login"]);
                    TB_USUARIO.FECREG = DateTime.Today;
                    TB_USUARIO.ESTREG = 1;
                    Mensaje = _UsuarioLN.CrearUsuario(TB_USUARIO, TB_LOGIN, TV_LOG_ACC);
                    break;
                case 2:
                    TB_USUARIO.USUMOD = Convert.ToString(Session["user_login"]);
                    TB_USUARIO.FECMOD = DateTime.Today;
                    TB_USUARIO.ESTREG = 1;
                    TB_LOGIN.USUMOD = Convert.ToString(Session["user_login"]);
                    TB_LOGIN.FECMOD = DateTime.Today;
                    TB_LOGIN.ESTREG = 1;
                    Mensaje = _UsuarioLN.ModificarUsuario(TB_USUARIO, TB_LOGIN, TV_LOG_ACC);
                    break;
                case 3:
                    TB_USUARIO.USUMOD = Convert.ToString(Session["user_login"]);
                    TB_USUARIO.FECMOD = DateTime.Today;
                    TB_LOGIN.USUMOD = Convert.ToString(Session["user_login"]);
                    TB_LOGIN.FECMOD = DateTime.Today;
                    Mensaje = _UsuarioLN.EliminarUsuario(TB_USUARIO, TB_LOGIN, TV_LOG_ACC);
                    break;
            }
            return Json(Mensaje);
        }
        [HttpGet]
        public ActionResult IndexValidador()
        {
            string url = "/Home/Sistemas";
            var data = ValidadorString();
            if (data != null && data == "JSOTOMAYOR" || data == "ILUNA")
            {
                url = "Index";
            }
            return Redirect(url);
        }
        [HttpPost]
        public string ValidadorString() {
            try
            {
               return HttpContext.Session["nivel_login"].ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}