﻿using ENTIDADES;
using LN.EntidadesLN;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LOGINPUA.Controllers
{
    public class PerfilController : Controller
    {
        private readonly PerfilLN _PerfilLN;
        Util.Util utilidades = new Util.Util();


        public PerfilController(PerfilLN PerfilLN)
        {

            _PerfilLN = PerfilLN;
        }
        // GET: Perfil
        public ActionResult Perfil_Inicio()
        {
            var listaVistas = Session["menu_modulo"] as DataTable;
            var listacciones = Session["menu_acciones"] as List<CC_MENUPERFIL_ACCION>;
            //Session["menu_modulo"] = dt4;
            var nombreActionCurrent = this.ControllerContext.RouteData.Values["action"].ToString();
            bool auxValida = utilidades.validaVistaxPerfil(nombreActionCurrent, listaVistas);

            ViewBag.currentAction = this.ControllerContext.RouteData.Values["action"].ToString();
            ViewBag.currentRoute = this.ControllerContext.RouteData.Values["controller"].ToString();

 
            var Lista_acciones = utilidades.validadAccionMenu(listacciones, nombreActionCurrent, this.ControllerContext.RouteData.Values["controller"].ToString());

            if (auxValida)
            {
                ViewBag.Accionesview = Lista_acciones;
                return View();
            }
            else
            {
                return RedirectToAction("Sistemas_", "Home");
            }
         }

        public string listarPerfil(int idmodalidad)
        {
            var listar = _PerfilLN.ListarPerfil(idmodalidad);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }
        public string listarModalidad()
        {
            var listar = _PerfilLN.ListarModalidad();
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }

        public string ModificarPerfil(int idmodalidad, int id_modalidad_perfil, string nombre_perfil)
        {
            string session_usuario = Session["user_login"].ToString();
            var listar = _PerfilLN.ModificarPerfil(idmodalidad, id_modalidad_perfil, nombre_perfil, session_usuario);
            var result = JsonConvert.SerializeObject(listar); //para la lista principal
            return result;
        }
        public string AnularPerfil(int idperfil,string nombre_perfil)
        {
            var rpta = new RPTA_GENERAL();
            string session_usuario = Session["user_login"].ToString();

            var datos = _PerfilLN.Validar_Perfil(idperfil);

            if (datos.Count ==0)
            {
                rpta = _PerfilLN.AnularPerfil(idperfil, session_usuario);
            }
            else
            {
                rpta.COD_ESTADO = 0;
                rpta.DES_ESTADO = "Existen usuario con el perfil";
            }


            var result = JsonConvert.SerializeObject(rpta); //paraa anular
            return result;
        }
        public string AgregarPerfil(string nombre, int idmodalidad)
        {
            string session_usuario = "ADMINISTRADOR";

            //Se registra el Perfil
            var rpt = _PerfilLN.AgregarPerfil(nombre, session_usuario);
            var id_perfil = rpt.AUX;

            if (id_perfil != 0)
            {
                var rpt_final = _PerfilLN.AgregarPerfil_modalidad(id_perfil, idmodalidad, session_usuario);

            }

            var result = JsonConvert.SerializeObject(rpt); //para agregar
            return result;
        }

    }
}