﻿using Entidades;
using ENTIDADES;
using LN;
using LN.EntidadesLN;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace LOGINPUA
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            container.RegisterType<InterfaceLN<TB_EMPRESA>, EmpresaLN>();
            container.RegisterType<InterfaceLN<TB_LOGIN>, LoginLN>();
            container.RegisterType<InterfaceLN<TB_USUARIO>, UsuarioLN>();
            container.RegisterType<InterfaceLN<TL_LOG>, LogLN>();
            container.RegisterType<InterfaceLN<TM_ACCESO>, AccesoLN>();
            container.RegisterType<InterfaceLN<TM_USUARIOS_EMPRESA>, UsuariosEmpresaLN>();
            container.RegisterType<InterfaceLN<TV_ACC_EMP>, AccEmpLN>();
            container.RegisterType<InterfaceLN<TV_LOG_ACC>, LogAccLN>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}