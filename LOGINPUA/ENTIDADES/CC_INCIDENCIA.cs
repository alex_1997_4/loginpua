﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class CC_INCIDENCIA
    {
        public int ID_INCIDENCIA { get; set; }
        public int ID_RUTA { get; set; }
        public int ID_INFRACCION { get; set; }        
        public string NUM_DOCUMENTO { get; set; }
        public string EMPRESA { get; set; }
        public string PLACA_PADRON { get; set; }
        public string APELLIDOS { get; set; }
        public string NOMBRES { get; set; }
        public string COD_INFRACCION { get; set; }
        public string DESCRIPCION_INFRACCION { get; set; }
        public string NUM_SERVICIO { get; set; }
        public string LADO { get; set; }
        public string KM { get; set; }
        public string LUGAR_HECHO { get; set; }
        public string INTERVENCION_POLICIAL { get; set; }
        public string INFORMANTE { get; set; }
        public string PARADERO { get; set; }
        public string FECHA_INCIDENCIA { get; set; }       
        public string USU_REG { get; set; }
        public string FECHA_REG { get; set; }
        public string USU_MODIF { get; set; }
        public string FECHA_MODIF { get; set; }
        public int ID_ESTADO { get; set; }
        public string PROYECTO_CARTA { get; set; }
        public string DESCRIPCION_INCIDENCIA { get; set; }
        public string DESCARGO { get; set; }
        public string SANCION { get; set; }






    }
}
