﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class VECINOSPLACA
    {
        public int ID_VECINOSEMPA { get; set; }
        public string NUMERO_PLACA { get; set; }
        public int ID_TIPO_DOCUMENTO { get; set; }
        public int NUM_DOC { get; set; }

        public string NOMBRES { get; set; }

        public string APELLIDOS { get; set; }
}
        
    }

