﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class CC_ACCESO_CORREDORES
    {
        public int IDPROVUSU_CORREDOR { get; set; }
        public int ID_PROV_USUARIO { get; set; }
        public int ID_USUARIO { get; set; }
        public int ID_CORREDOR { get; set; }
        public string URL_SERV { get; set; }
        public string NOMBRE_ACCESO { get; set; }
        public string USUARIO { get; set; }
        public string CONTRASENA { get; set; }
        public string CORREDOR_NOMBRE { get; set; }
        public string PROVABREV_CORR { get; set; }
        public string COLOR_REPRESENTATIVO { get; set; }

        


    }
}
