﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class CONADISPLACA
    {
        public string PLACA { get; set; }
        public int ID_PLACA { get; set; }
        public int ID_TIPO_DOCUMENTO { get; set; }
        public int NUM_DOC { get; set; }

        public int NOMBRES { get; set; }

        public int APE_PATERNO { get; set; }

        public int APE_MATERNO { get; set; }
        
    }
}
