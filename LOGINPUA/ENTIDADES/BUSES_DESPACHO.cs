﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class BUSES_DESPACHO
    {
        public int BD_ID { get; set; }
        public int CD_ID { get; set; }
        public string CD_CONCEPTOS { get; set; }
        public string BS_PLACA { get; set; }

        public string BD_ESTADO { get; set; }
        public string BD_DIRECCION { get; set; }
        public string BD_CONCESIONARIO { get; set; }

        public int?  BD_KM { get; set; }


        public DateTime HORA { get; set; }


        public string CVS_FEC_FIN { get; set; }
        public string VEHICULOS_FIN { get; set; }
        public string RTV_FIN { get; set; }
        public string SOAT_FEC_FIN { get; set; }
        public string RC_FIN { get; set; }
        public string COMENTARIO { get; set; }


        public string BD_CALIDAD { get; set; }
        public string BD_OBSERVACION { get; set; }
        public string BD_FECHA { get; set; }
        public string USUREG { get; set; }
        public int ESTREG { get; set; }
        public string BD_LATITUD { get; set; }
        public string BD_LONGUITUD { get; set; }

        public string ESTADO_BUS { get; set; }

        public int BD_COD_DESPACHO { get; set; }
        public string URL_FOTO1 { get; set; }
        public string URL_FOTO2 { get; set; }
        public string URL_FOTO3 { get; set; }
        public string URL_FOTO4 { get; set; }










    }
}
