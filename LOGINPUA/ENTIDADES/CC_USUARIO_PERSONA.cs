﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class CC_USUARIO_PERSONA
    {
        public int ID_USUARIO { get; set; }
        public int ID_PERSONA { get; set; }
        public int ID_PERFIL { get; set; }
        public int ID_MODALIDAD_TRANS { get; set; }

        
        public string PERFIL { get; set; }
        public string ID_ESTADO { get; set; }

         	

        public string MODALIDAD { get; set; }
        public string NOMBRE { get; set; }
        public string APEPAT { get; set; }
        public string APEMAT { get; set; }

        public string USUARIO { get; set; }
        public string CLAVE { get; set; }
        public string FECHA_REG { get; set; }
        public string USU_REG { get; set; }


 

    }
}
