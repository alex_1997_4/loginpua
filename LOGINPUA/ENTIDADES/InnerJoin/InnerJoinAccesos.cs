﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.InnerJoin
{
    public class InnerJoinAccesos
    {
        public int ACCCOD { get; set; }
        public int EMPCOD { get; set; }
        public int LOGACCCOD { get; set; }
        public int ESTREG { get; set; }
    }
}
