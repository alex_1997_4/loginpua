﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.InnerJoin
{
    public class InnerJoinUsuario
    {
        public int LOGCOD { get; set; }
        public int ACCCOD { get; set; }
        public string ACCNOM { get; set; }
        public string ACCICO { get; set; }
        public string ACCDIR { get; set; }
        public string ACCGRU { get; set; }

    }
}
