﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class CC_PROVEEDORSERV
    {
        public int ID_PROV_SERV { get; set; }
        public string NOMBRE { get; set; }
        public string URL_SERV { get; set; }
        public string ID_ESTADO { get; set; }
        public string FECHA_REG { get; set; }
        public string USU_REG { get; set; }


        
    }
}
