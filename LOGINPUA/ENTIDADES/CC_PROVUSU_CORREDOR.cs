﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES
{
    public class CC_PROVUSU_CORREDOR
    {
        public int IDPROVUSU_CORREDOR { get; set; }
        public int ID_PROV_USUARIO { get; set; }
        public int ID_CORREDOR { get; set; }
        public int ID_ESTADO { get; set; }
        public string USU_REG { get; set; }
        public string FECHA_REG { get; set; }

    }
}
